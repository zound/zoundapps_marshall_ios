//
//  SetupTutorialViewModel.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 01/09/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import MinuetSDK

class SetupTutorialViewModel {
    let speaker: Speaker
    
    init(speaker: Speaker) {
        self.speaker = speaker
    }
}
