//
//  SetupPresetsViewModel.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 01/09/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import RxSwift
import MinuetSDK

protocol SetupPresetsViewModelDelegate: class {
    func setupViewModelDidLoginSpotify(_ setupViewModel:SetupPresetsViewModel)
    func setupViewModelDidNotLoginSpotify(_ setupViewModel:SetupPresetsViewModel, error: Error)
}

struct PresetTypesSelection: OptionSet {
    let rawValue: Int
    
    static let Spotify = PresetTypesSelection(rawValue: 1 << 0)
    static let IR = PresetTypesSelection(rawValue: 1 << 1)
    
    var hasSpotify: Bool { return self.contains(.Spotify) }
    var hasIR: Bool { return self.contains(.IR) }
}

enum PresetUploadState {
    case error
    case idle
    case downloadingPresets
    case uploadingPresets
    case done
}

enum SpotifyProductError: Error {
    case notPremium
    case notAvailable
}

class SetupPresetsViewModel {
    weak var delegate: SetupPresetsViewModelDelegate?
    
    let speaker: Speaker
    let speakerProvider: SpeakerProvider
    let presets: Variable<[Preset]> = Variable([])
    let locationProvider = GeoIPProvider()
    let spotifyProvider: SpotifyProvider
    let initialPresetsProvider = InitialPresetsProvider(baseURL: GlobalConfig.BitBucket.repo, path: GlobalConfig.BitBucket.path)
    
    let spotifyUsername: Variable<String?> = Variable(nil)
    let presetUploadStateVariable: Variable<PresetUploadState>  = Variable(PresetUploadState.idle)
    
    var presetUploadState: PresetUploadState {
        get { return presetUploadStateVariable.value }
        set { presetUploadStateVariable.value = newValue }
    }
    
    let spotifyLoggingInVariable = Variable(false)
    var spotifyLoggingIn: Bool {
        get { return spotifyLoggingInVariable.value }
        set { spotifyLoggingInVariable.value = newValue }
    }
    
    var disposeBag = DisposeBag()
    
    init(speaker: Speaker, speakerProvider: SpeakerProvider) {
        self.speaker = speaker
        self.speakerProvider = speakerProvider
        self.spotifyProvider = SpotifyProvider(isUrbanearsApp: false)
    }
    
    deinit {
    }
    
    // 1) User selects what kind of presets he would like. We store his wish in presetSelection
    let presetSelectionVariable: Variable<PresetTypesSelection> = Variable(PresetTypesSelection(rawValue: 0))
    var presetSelection: PresetTypesSelection {
        get { return presetSelectionVariable.value }
        set { presetSelectionVariable.value = newValue }
    }
    
    // 2) We provide support for the UI to know if the Spotify login explainer screens are necessary
    var isSpotifyLoginNeeded: Bool {
        return SpotifyTokenStore.getCurrentToken() == nil
    }
    
    // 3) When the user presses connecto to Spotify the UI calls this method which logs the user in and stores the token in SpotifyProvider
    func loginSpotify(forceWebLogin: Bool = false, clearExistingLogin: Bool = true) {
        if clearExistingLogin {
            spotifyProvider.tokenManager.clearToken()
            SpotifyArtworkCache.sharedInstace.clearData()
        }
        
        spotifyLoggingIn = true
        authorizedSpotifyUser(forceWebLogin)
            .subscribe(
                onNext: { [weak self] user in
                    guard let `self` = self else {return}
                    
                    self.spotifyLoggingIn = false
                    if let productType = user.productType {
                        if productType == .Premium {
                            self.spotifyUsername.value = user.id
                            self.delegate?.setupViewModelDidLoginSpotify(self)
                        } else {
                            self.spotifyLoggingIn = false
                            self.spotifyProvider.tokenManager.spotifyToken = nil
                            SpotifyTokenStore.clearToken()
                            self.delegate?.setupViewModelDidNotLoginSpotify(self, error: SpotifyProductError.notPremium)
                        }
                    } else {
                        self.spotifyLoggingIn = false
                        self.spotifyProvider.tokenManager.spotifyToken = nil
                        SpotifyTokenStore.clearToken()
                        self.delegate?.setupViewModelDidNotLoginSpotify(self, error: SpotifyProductError.notAvailable)
                    }
                },
                onError: { [weak self] error in
                    if let error = error as? SpotifyLoginError {
                        self?.spotifyLoggingIn = false
                        self?.spotifyProvider.tokenManager.spotifyToken = nil
                        SpotifyTokenStore.clearToken()
                        self?.delegate?.setupViewModelDidNotLoginSpotify(self!, error: error)
                    } else {
                        self?.spotifyLoggingIn = false
                        self?.spotifyProvider.tokenManager.spotifyToken = nil
                        SpotifyTokenStore.clearToken()
                        self?.delegate?.setupViewModelDidNotLoginSpotify(self!, error: SpotifyLoginError.other(error: error))
                    }
            }).disposed(by: disposeBag)
    }
    
    func authorizedSpotifyUser(_ forceWebLogin: Bool = false) -> Observable<SpotifyUser> {
        return spotifyProvider.userInfo(forceWebLogin: forceWebLogin)
    }
    
    // 4) fetch the Spotify presets for the current user
    fileprivate typealias PresetsToUpload = (spotifyPresets:[Preset], irPresets: [Preset])
    func loadPresets() {
        var fetchPresets: [Observable<[Preset]>] = []
        if presetSelection.hasSpotify {
            fetchPresets.append(spotifyPresets())
        } else {
            fetchPresets.append(Observable.just([Preset]()))
        }
        
        if presetSelection.hasIR {
            fetchPresets.append(locationBasedIRPresets())
        } else {
            fetchPresets.append(Observable.just([Preset]()))
        }
        
        presetUploadState = .downloadingPresets
        
        Observable.combineLatest(fetchPresets) { arrayOfPresetArrays -> PresetsToUpload in
            let spotifyPresets = arrayOfPresetArrays[0]
            let irPresets = arrayOfPresetArrays[1]
            return PresetsToUpload(spotifyPresets: spotifyPresets, irPresets: irPresets)
            }
            .subscribe(onNext: { [weak self] presets in
                guard let `self` = self else { return }
                var presetsToUpload: [Preset]?
                
                if self.presetSelection.hasSpotify && self.presetSelection.hasIR {
                    let maxSpotifyPresets = min(presets.spotifyPresets.count, 4)
                    let maxIRPresets = min(presets.irPresets.count,3)
                    var count = maxSpotifyPresets
                    let irPresets:[Preset] = presets.irPresets[0..<maxIRPresets].map { initialPreset in
                        let newPreset = Preset(name: initialPreset.name,
                                               number: count+1,
                                               imageURL: initialPreset.imageURL,
                                               typeID: initialPreset.typeID,
                                               blob: initialPreset.blob,
                                               playlistUrl: initialPreset.playlistUrl)
                        count = count + 1
                        return newPreset
                    }
                    
                    presetsToUpload = Array(presets.spotifyPresets[0..<maxSpotifyPresets]) + Array(irPresets)
                } else if self.presetSelection.hasSpotify {
                    let maxSpotifyPresets = min(presets.spotifyPresets.count, 7)
                    presetsToUpload = Array(presets.spotifyPresets[0..<maxSpotifyPresets])
                } else if self.presetSelection.hasIR {
                    let maxIRPresets = min(presets.irPresets.count, 7)
                    presetsToUpload = Array(presets.irPresets[0..<maxIRPresets])
                } else {
                    //do nothing :)
                }
                
                if let presetsToUpload = presetsToUpload {
                    self.uploadPresets(presetsToUpload, uploadSpotifyToken: self.presetSelection.hasSpotify)
                } else {
                    self.finish()
                }
                
                },
                       onError: { [weak self] error in
                        self?.onSpotifyError(error)
            })
            .disposed(by: disposeBag)
    }
    
    func uploadPresets(_ presets: [Preset], uploadSpotifyToken: Bool) {
        self.presets.value = presets
        presetUploadState = .uploadingPresets
        
        var uploadAccessToken = Observable.just(true)
        if let token = spotifyProvider.tokenManager.spotifyToken {
            if let uploadToken = token.accessToken {
                uploadAccessToken = self.speakerProvider
                    .setNode(ScalarNode.SpotifyLoginWithToken, value: uploadToken, forSpeaker: self.speaker)
                    .do(onError:{ error in
                        print("\(error)")
                    })
                    .timeout(5, scheduler: MainScheduler.instance)
                    .catchErrorJustReturn(false)
            }
        }
        
        if presets.count > 0 {
            let uploadPresets = SpeakerPresets.uploadPresets(presets, toSpeaker: speaker, provider: speakerProvider)
                .timeout(20.0, scheduler: MainScheduler.instance)
            
            uploadAccessToken
                .flatMap { ok -> Observable<Bool> in
                    guard ok else { return Observable.just(false) }
                    return uploadPresets.catchErrorJustReturn(false) }
                .subscribe(onNext:{ [weak self] done in
                    guard let `self` = self else { return }
                    if done {
                        self.finish()
                        print("done uploading")
                    } else {
                        self.errorUploadingPresets()
                        print("failed uploading")
                    }
                }).disposed(by: disposeBag)
        }
    }
    
    func errorUploadingPresets() {
        presetUploadState = .error
    }
    
    func finish() {
        presetUploadState = .done
    }
    
    func onSpotifyError(_ error: Error) {
        presetUploadState = .error
        print("Spotify error: \(error)")
    }
    
    
    func spotifyPresets() -> Observable<[Preset]> {
        return spotifyProvider.suggestedPresets()
            .map { return speakerPresetsFromSpotifyPlaylists($0) }
    }
    
    func locationBasedIRPresets() -> Observable<[Preset]> {
        return locationProvider.currentIPAddressLocation()
            .timeout(10.0, scheduler: MainScheduler.instance)
            .map { [weak self] location -> Observable<[SpeakerInitialPreset]> in
                let initialPresets = ExternalConfig.sharedInstance.presets!
                let fallback = Observable.just(InitialPresetsProvider.fallbackPresetsForCountry(location.countryCode, inInitialPresets: initialPresets))
                guard let `self` = self else { return fallback }
                
                return self.initialPresetsProvider
                    .initialPresets()
                    .map{ InitialPresetsProvider.presetsForCountry(location.countryCode, inInitialPresets: $0) }
                    .timeout(5.0, scheduler: MainScheduler.instance)
                    .catchErrorJustReturn(InitialPresetsProvider.fallbackPresetsForCountry(location.countryCode, inInitialPresets: initialPresets))
            }
            .switchLatest()
            .map { return speakerPresetsFromRadioPresets($0) }
    }
}




func speakerPresetsFromSpotifyPlaylists(_ presets: [SpotifyPreset]) -> [Preset] {
    let presets = presets.enumerated().map { (index,preset) in
        return Preset(name: preset.name ?? "",
                      number: index + 1,
                      imageURL: URL(string:preset.imageURL!),
                      typeID: "Spotify",
                      blob: preset.blob,
                      playlistUrl: "")
    }
    
    return presets
}

func speakerPresetsFromRadioPresets(_ radioPresets: [SpeakerInitialPreset]) -> [Preset] {
    return radioPresets.enumerated().map { index,initialPreset in
        let imageURL = URL(string: initialPreset.artworkUrl ?? "")
        let blobContent = "\(initialPreset.stationId!)\0"
        let blob = blobContent.base64EncodedString()
        
        return Preset(name: initialPreset.stationName ?? "",
                      number: index + 1,
                      imageURL: imageURL,
                      typeID: "IR",
                      blob: blob,
                      playlistUrl: blob)
    }
}
