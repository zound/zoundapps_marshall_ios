//
//  SetupNetworkViewModel.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 01/09/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import RxSwift
import Crashlytics
import MinuetSDK

protocol SetupNetworkViewModelDelegate: class {
    func setupNetworkViewModelDidChangeConfigurationState(_ setupViewModel: SetupNetworkViewModel, state: SetupConfigurationState)
}


enum SetupConfigurationState {
    case idle
    case loading
    case foundSpeakers
    case noSpeakersFound
}

class SetupNetworkViewModel: NSObject {
    
    weak var delegate: SetupNetworkViewModelDelegate?
    
    fileprivate let unconfiguredSpeakers : Variable<[SetupSpeaker]>
    
    let visibleUnconfiguredSpeakers : Variable<[SetupSpeaker]>
    let wacService: WACServiceType
    let audioSystem: AudioSystem
    let disposeBag = DisposeBag()
    let speakerProvider: SpeakerProvider
    
    let setupNetworkStateVariable: Variable<SetupConfigurationState>  = Variable(.idle)
    
    var setupNetworkState: SetupConfigurationState {
        get { return setupNetworkStateVariable.value }
        set { setupNetworkStateVariable.value = newValue }
    }
    
    var noSpeakersTimeoutDisposable: Disposable? = nil
    var noDiscoveryTimeoutDisposable: Disposable? = nil
    
    init(wacService: WACServiceType, audioSystem: AudioSystem, speakerProvider: SpeakerProvider) {
        self.wacService = wacService
        self.audioSystem = audioSystem
        self.speakerProvider = speakerProvider
        
        unconfiguredSpeakers = Variable([])
        visibleUnconfiguredSpeakers = Variable([])
        
        super.init()
        
        self.audioSystem.addDelegate(self)
        self.wacService.delegate = self
        
        self.visibleUnconfiguredSpeakers.asObservable()
            .map {$0.count }
            .subscribe(weak: self,
                       onNext: SetupNetworkViewModel.updateForUnconfiguredSpeakerCount)
            .disposed(by: disposeBag)
        
        self.unconfiguredSpeakers.asObservable()
            .subscribe(onNext: { [weak self] _ in
                self?.updateVisibleUnconfiguredSpeakers() })
            .disposed(by: disposeBag)
        
        self.setupNetworkStateVariable.asObservable()
            .subscribe(onNext:{ [weak self] newState in
                guard let `self` = self else  { return }
                print("did change network state to \(newState)")
                self.delegate?.setupNetworkViewModelDidChangeConfigurationState(self, state: newState) })
            .disposed(by: disposeBag)
    }
    
    deinit {
        audioSystem.removeDelegate(self)
        wacService.active = false
    }
    
    func updateVisibleUnconfiguredSpeakers() {
        self.visibleUnconfiguredSpeakers.value = self.unconfiguredSpeakers.value.filter({
            $0.didDoSetup == SpeakerDidDoSetupState.setupNotDone || $0.didDoSetup == SpeakerDidDoSetupState.unconfigured
        })
        
        for speaker in self.visibleUnconfiguredSpeakers.value {
            print("\(speaker.macEnd) - \(speaker.unconfiguredSpeaker?.macSegmentFromSSID ?? "-") - \(speaker.configuredSpeaker?.mac ?? "-")")
        }
        print("----------")
    }
    
    func addSpeaker(unconfiguredSpeaker: UnconfiguredSpeaker, didSetupState: SpeakerDidDoSetupState) {
        guard let macSegment = unconfiguredSpeaker.macSegmentFromSSID else { return }
            
            print("unconfigured - macsegment: \(macSegment)")
            if let setupSpeaker = self.unconfiguredSpeakers.value.first(where: { speaker in speaker.macEnd == macSegment }) {
                setupSpeaker.unconfiguredSpeaker = unconfiguredSpeaker
                setupSpeaker.didDoSetup = didSetupState
                self.updateVisibleUnconfiguredSpeakers()
            } else {
                let setupSpeaker = SetupSpeaker(unconfiguredSpeaker: unconfiguredSpeaker)
                setupSpeaker.didDoSetup = didSetupState
                self.unconfiguredSpeakers.value.append(setupSpeaker)
            }
        
    }
    
    func addSpeaker(configuredSpeaker: Speaker) {
        speakerProvider.getDidSetupFromUserDataStoreInSpeaker(configuredSpeaker)
            .timeout(5, scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] didSetup in
                let macEnd = SetupNetworkViewModel.macEnd(mac: configuredSpeaker.mac)
                print("configured - macsegment: \(macEnd)")
                
                if let setupSpeaker = self?.unconfiguredSpeakers.value.first(where: { speaker in speaker.macEnd == macEnd }) {
                    setupSpeaker.configuredSpeaker = configuredSpeaker
                    setupSpeaker.didDoSetup = didSetup ? .setupDone : .setupNotDone
                    self?.updateVisibleUnconfiguredSpeakers()
                } else {
                    let speaker = SetupSpeaker(configuredSpeaker: configuredSpeaker)
                    speaker.didDoSetup =  didSetup ? .setupDone : .setupNotDone
                    self?.unconfiguredSpeakers.value.append(speaker)
                }
                }, onError: { [weak self] error in
                    let macEnd = SetupNetworkViewModel.macEnd(mac: configuredSpeaker.mac)
                    if let setupSpeaker = self?.unconfiguredSpeakers.value.first(where: { speaker in speaker.macEnd == macEnd }) {
                        setupSpeaker.didDoSetup = .unconfigured
                        self?.updateVisibleUnconfiguredSpeakers()
                    }
                    print("could not fetch didDoSetup for speaker") })
            .disposed(by: disposeBag)
    }
    
    func removeSpeaker(unconfiguredSpeaker: UnconfiguredSpeaker) {
        NSLog("unconf sp: \(unconfiguredSpeaker)")
        guard let setupSpeaker = self.unconfiguredSpeakers.value.first(where: { speaker in speaker.macEnd == unconfiguredSpeaker.macSegmentFromSSID! }) else { return }
        
            setupSpeaker.unconfiguredSpeaker = nil
            if setupSpeaker.configuredSpeaker == nil {
                self.unconfiguredSpeakers.value.removeObject(setupSpeaker)
            } else {
                self.updateVisibleUnconfiguredSpeakers()
            }
    }
    
    func removeSpeaker(configuredSpeaker: Speaker) {
        let macEnd = SetupNetworkViewModel.macEnd(mac: configuredSpeaker.mac)
        guard let setupSpeaker = self.unconfiguredSpeakers.value.first(where: { speaker in speaker.macEnd == macEnd }) else { return }
        
            setupSpeaker.configuredSpeaker = nil
            if setupSpeaker.unconfiguredSpeaker == nil {
                self.unconfiguredSpeakers.value.removeObject(setupSpeaker)
            } else {
                self.updateVisibleUnconfiguredSpeakers()
            }
    }
    
    func searchForSpeakers() {
        setupNetworkState = .loading
        unconfiguredSpeakers.value.removeAll()
        
        let sortedUnconfiguredSpeakers = wacService.unconfiguredSpeakers.sorted(by: { sp1, sp2 in sp1.completeName < sp2.completeName })
        sortedUnconfiguredSpeakers.forEach { speaker in
            let isConfigured = self.audioSystem.speakers.value.first(where: { configuredSpeaker in
                let macEnd = SetupNetworkViewModel.macEnd(mac: configuredSpeaker.mac)
                return macEnd == speaker.macSegmentFromSSID!
            }) != nil
            addSpeaker(unconfiguredSpeaker: speaker, didSetupState: isConfigured ? .checking : .unconfigured)
        }
        
        audioSystem.speakers.value.forEach {
            if let connectable = ExternalConfig.sharedInstance.speakerImageForColor($0.color)?.connectable, connectable {
                addSpeaker(configuredSpeaker: $0)
            }
        }
        
        startNoSpeakersFoundTimeout()
        if wacService.active {
            wacService.active = false
            runAfterDelay(1.0, block: { [weak self] in
                self?.wacService.active = true
            })
        } else {
            self.wacService.active = true
        }
    }
    
    func startNoSpeakersFoundTimeout() {
        noSpeakersTimeoutDisposable?.dispose()
        noSpeakersTimeoutDisposable = Observable<Int>
            .interval(5.0, scheduler: MainScheduler.instance)
            .subscribe(weak: self, onNext: SetupNetworkViewModel.searchDidTimeout)

        noSpeakersTimeoutDisposable?.disposed(by: disposeBag)
    }
    
    func stopNoSpeakersFoundTimeout() {
        noSpeakersTimeoutDisposable?.dispose()
        noSpeakersTimeoutDisposable = nil
    }
    
    func searchDidTimeout(_ one: Int) {
        stopNoSpeakersFoundTimeout()
        if case .loading = setupNetworkState {
            setupNetworkState = .noSpeakersFound
        }
    }
    
    func updateForUnconfiguredSpeakerCount(_ count: Int) {
        switch setupNetworkState {
        case .loading, .noSpeakersFound:
            if count > 0 {
                setupNetworkState = .foundSpeakers
                stopNoSpeakersFoundTimeout()
            }
            
        case .foundSpeakers:
            if count == 0 {
                setupNetworkState = .loading
                startNoSpeakersFoundTimeout()
            }
            
        default: break
        }
    }
}


extension SetupNetworkViewModel: WACServiceDelegate {
    func didCancelConfigurationForAccessory(_ speaker: UnconfiguredSpeaker) {}
    func didFailConfigurationForAccessory(_ speaker: UnconfiguredSpeaker) {}
    func didFinishConfigurationForAccessory(_ speakerBeingConfigured: UnconfiguredSpeaker) {}
    
    func didAdd(unconfiguredSpeaker: UnconfiguredSpeaker) {
        let isConfigured = self.audioSystem.speakers.value.first(where: { configuredSpeaker in
            let macEnd = SetupNetworkViewModel.macEnd(mac: configuredSpeaker.mac)
            return macEnd == unconfiguredSpeaker.macSegmentFromSSID!
        }) != nil
        addSpeaker(unconfiguredSpeaker: unconfiguredSpeaker, didSetupState: isConfigured ? .checking : .unconfigured)
    }
    
    func didRemove(unconfiguredSpeaker: UnconfiguredSpeaker) {
        removeSpeaker(unconfiguredSpeaker: unconfiguredSpeaker)
    }
}

extension SetupNetworkViewModel: AudioSystemDelegate {
    func audioSystemDidAddGroup(_ group: SpeakerGroup, fromMove: Bool) {
        if (group.isMulti && group.speakers.count == 1) || !group.isMulti {
            if let speaker = group.speakers.first,
                let connectable = ExternalConfig.sharedInstance.speakerImageForColor(speaker.color)?.connectable, connectable {
                addSpeaker(configuredSpeaker: speaker)
            }
        }
    }
    
    func audioSystemDidRemoveGroup(_ group: SpeakerGroup, fromMove: Bool) {
        for speaker in group.speakers {
            if let connectable = ExternalConfig.sharedInstance.speakerImageForColor(speaker.color)?.connectable, connectable {
                addSpeaker(configuredSpeaker: speaker)
            }
        }
    }
    
    func audioSystemDidAddSpeaker(_ speaker: Speaker, toGroup group: SpeakerGroup, fromMove: Bool) {
        if let connectable = ExternalConfig.sharedInstance.speakerImageForColor(speaker.color)?.connectable, connectable {
            removeSpeaker(configuredSpeaker: speaker)
        }
    }
    
    func audioSystemDidRemoveSpeaker(_ speaker: Speaker, fromGroup group: SpeakerGroup, fromMove: Bool) {
        if let connectable = ExternalConfig.sharedInstance.speakerImageForColor(speaker.color)?.connectable, connectable {
            removeSpeaker(configuredSpeaker: speaker)
        }
    }
    
    var delegateIdentifier: String {
        return "setup_view_model"
    }
}

func alphabeticalIndexForUnconfiguredSpeaker(_ speaker: UnconfiguredSpeaker, toInsertInto speakers: [UnconfiguredSpeaker]) -> Int {
    var index = 0
    
    for currentSpeaker in speakers {
        if currentSpeaker.completeName.lowercased() < speaker.completeName.lowercased() {
            index = index + 1
        } else {
            break
        }
    }
    
    return index
}

extension SetupNetworkViewModel {
    static func macEnd(mac: String) -> String {
        let index = mac.index(mac.endIndex, offsetBy: -4)
        return String(mac[index...])
    }
}
