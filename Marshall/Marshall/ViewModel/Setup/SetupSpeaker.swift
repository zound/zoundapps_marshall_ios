//
//  SetupSpeaker.swift
//  Marshall
//
//  Created by Claudiu Alin Luminosu on 28/08/2017.
//  Copyright © 2017 Zound Industries. All rights reserved.
//

import Foundation
import MinuetSDK
import RxSwift

enum SpeakerDidDoSetupState {
    case unknown
    case unconfigured
    case checking
    case setupNotDone
    case setupDone
}

class SetupSpeaker: Equatable {
    var unconfiguredSpeaker: UnconfiguredSpeaker? {
        didSet {
            updateStatus()
        }
    }
    
    var configuredSpeaker: Speaker? {
        didSet {
            updateStatus()
        }
    }
    
    var didDoSetup: SpeakerDidDoSetupState = .unknown
    
    var mac: String {
        if let speaker = configuredSpeaker {
            return speaker.mac
        }

        if let unconfiguredSpeaker = unconfiguredSpeaker {
            return unconfiguredSpeaker.mac
        }

        return ""
    }
    
    var macEnd: String {
        if let speaker = configuredSpeaker {
            let index = speaker.mac.index(speaker.mac.endIndex, offsetBy: -4)
            return String(speaker.mac[index...])
        }
        
        if let unconfiguredSpeaker = unconfiguredSpeaker {
            return unconfiguredSpeaker.macSegmentFromSSID!
        }
        
        return ""
    }
    
    var speakerImage: SpeakerImage? {
        get {
            var speakerImage: SpeakerImage?
            if isConnected, let configuredSpeaker = configuredSpeaker {
                speakerImage = ExternalConfig.sharedInstance.speakerImageForColor(configuredSpeaker.color)
            } else if !isConnected, let unonfiguredSpeaker = unconfiguredSpeaker {
                speakerImage = ExternalConfig.sharedInstance.speakerImages?.speakerImages?.filter({ speakerImage in unonfiguredSpeaker.ssid.hasPrefix(speakerImage.ssid!) }).first
                if speakerImage == nil {
                    speakerImage = ExternalConfig.sharedInstance.speakerImages?.speakerImages?.filter({ speakerImage in unonfiguredSpeaker.model.hasPrefix(speakerImage.name!) }).first
                }
            }
            return speakerImage
        }
    }
    
    var name: String {
        get {
            if let _ = configuredSpeaker, let displayModel = speakerImage?.displayModel {
                return displayModel
            } else if let unconfiguredSpeaker = unconfiguredSpeaker {
                if let model = speakerImage?.displayModel {
                    var modelText = model
                    let nameIndex = unconfiguredSpeaker.nameIndex
                    if nameIndex > 0 {
                        modelText = "\(model) (\(nameIndex + 1))"
                    }
                    return modelText.uppercased()
                } else {
                    return unconfiguredSpeaker.ssid
                }
            }
            return "Unknown"
        }
    }
    
    var model: String? {
        get {
            return speakerImage != nil ? "Multi-Room" : nil
        }
    }
    
    var imageName: String {
        get {
            guard let speakerImageName = speakerImage?.listItemImageName else { return "list_item_placeholder" }
            return speakerImageName
        }
    }
    
    let isConnectedVariable: Variable<Bool> = Variable(false)
    var isConnected: Bool {
        get { return self.isConnectedVariable.value }
        set { self.isConnectedVariable.value = newValue }
    }
    
    func updateStatus() {
        isConnected = (self.configuredSpeaker != nil)
    }
    
    init(unconfiguredSpeaker: UnconfiguredSpeaker) {
        self.unconfiguredSpeaker = unconfiguredSpeaker
        updateStatus()
    }
    
    init(configuredSpeaker: Speaker) {
        self.configuredSpeaker = configuredSpeaker
        updateStatus()
    }
    
}

func == (a: SetupSpeaker, b: SetupSpeaker) -> Bool {
    return a.macEnd == b.macEnd
}
