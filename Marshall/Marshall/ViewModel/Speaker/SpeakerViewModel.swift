//
//  SpeakerViewModel.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 12/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import MinuetSDK

class SpeakerViewModel {
    let disposeBag = DisposeBag()
    var connectionDisposable: CompositeDisposable?
    
    var spotifyProvider: SpotifyProvider?
    
    var clientSpeakerState: Variable<GroupClientState?> { return connectionManager.clientState }
    var masterSpeakerState: Variable<GroupMasterState?> { return connectionManager.masterState }
    var connectionStatus: Variable<SpeakerConnectionStatus> { return connectionManager.connectionStatus }
    
    var clientSpeaker: Speaker { return connectionManager.clientSpeaker}
    var masterSpeaker: Speaker { return connectionManager.masterSpeaker}
    var speakerNotifier: SpeakerNotifier { return connectionManager.clientNotifier }
    var provider: SpeakerProvider { return connectionManager.provider }
    
    let presetsViewModel: PresetsViewModel
    let playerViewModel: PlayerViewModel
    
    let connectionManager: SpeakerConnectionManager
    
    init(connectionManager: SpeakerConnectionManager, spotifyProvider: SpotifyProvider) {
        self.connectionManager = connectionManager
        self.presetsViewModel = PresetsViewModel(connectionManager: connectionManager, spotifyProvider: spotifyProvider)
        self.playerViewModel = PlayerViewModel(connectionManager: connectionManager, isGuest: false)
        self.presetsViewModel.delegate = self
        
        startPositionUpdater()
    }
    
    deinit {
        print("deinit viewModel")
    }
    
    func startPositionUpdater() {
        connectionDisposable?.dispose()
        connectionDisposable = CompositeDisposable()
        disposeBag.insert(connectionDisposable!)
        
        let pollPosition: Observable<Int> = Observable<Int>.interval(3.0, scheduler: MainScheduler.instance)
            .startWith(0)
            .flatMapLatest { [weak self] _ -> Observable<Int> in
                guard let vm = self else { return Observable.just(0) }
                return vm.provider.getNode(ScalarNode.PlayPosition, forSpeaker: vm.connectionManager.masterSpeaker)
                    .map { Int($0!) ?? 0 }
        }
        
        let positionInterpolator = pollPosition
            .flatMapLatest { position -> Observable<Int> in
                let interval = 0.5
                return Observable<Int>.interval(interval, scheduler: MainScheduler.instance)
                    .map { position + Int(Double($0 + 1) * interval * 1000) }
                    .startWith(position)
        }
        
        let hasProgress = playerViewModel.hasProgress.asObservable().distinctUntilChanged()
        let isPlaying = playerViewModel.isPlaying.asObservable()
        let needsProgressUpdates = playerViewModel.needsUpdateProgress.asObservable().distinctUntilChanged()
        
        let position: Observable<Int> = Observable.combineLatest(hasProgress, isPlaying, needsProgressUpdates) { $0 && $1 && $2 }
            .flatMapLatest { shouldUpdateProgress -> Observable<Int> in
                guard shouldUpdateProgress else { return Observable.empty() }
                return positionInterpolator
        }
        
        let positionPollingDisposable = position
            .subscribe(onNext: { [weak self] (nodeValue: Int) in
                self?.updateSpeakerStateWithNodes([ScalarNode.PlayPosition:String(nodeValue)]) })
        
        _ = connectionDisposable?.insert(positionPollingDisposable)
    }
    
    func stopPositionUpdater() {
        connectionDisposable?.dispose()
        connectionDisposable = nil
    }
    
    func updateSpeakerStateWithNodes(_ nodes:[ScalarNode:String]) {
        for (node,value) in nodes {
            masterSpeakerState.value?.updateWithNode(node, value: value)
        }
    }
    
    func playPause() {
        self.playerViewModel.playPause()
    }
}

extension SpeakerViewModel: PresetsViewModelDelegate {
    func presetsViewModelDidRequestPause(_ viewModel: PresetsViewModel) {
        var commandToSend = PlayControl.pause.rawValue
        
        if self.masterSpeakerState.value?.currentMode?.isIR == true {
            commandToSend = PlayControl.stop.rawValue
        }
        
        provider.setNode(ScalarNode.PlayControl, value: String(commandToSend), forSpeaker: connectionManager.masterSpeaker)
            .subscribe()
            .disposed(by: disposeBag)
    }
    
    func presetsViewModel(_ viewModel: PresetsViewModel, didRequestUndoPreset preset: Preset, toPreset: Preset) {
        let clientSpeaker = self.connectionManager.clientSpeaker
        if !toPreset.isEmpty {
            SpeakerPresets.uploadPreset(toPreset, toSpeaker: clientSpeaker, provider: provider)
                .subscribe(onNext: { done in
                    print("done uploading preset")
                }).disposed(by: disposeBag)
        }
    }
    
    func presetsViewModel(_ viewModel: PresetsViewModel, didRequestDeletePreset preset: Preset) {
        //speakerState.value?.presetIndex = nil
        let clientSpeaker = self.connectionManager.clientSpeaker
        let provider = self.provider
        
        SpeakerNavBrowser.setStateOn(true, onSpeaker: clientSpeaker, provider: provider)
            .flatMapLatest { done in
                return provider.setNode(ScalarNode.NavPresetDelete, value: String(preset.number - 1), forSpeaker: clientSpeaker)
            }.subscribe()
            .disposed(by: disposeBag)
        
    }
    
    func presetsViewModel(_ viewModel: PresetsViewModel, didRequestSavePreset preset: Preset) {
        let clientSpeaker = self.connectionManager.clientSpeaker
        let provider = self.provider
        
        let savePreset = SpeakerNavBrowser.setStateOn(true, onSpeaker: clientSpeaker, provider: provider)
            .flatMapLatest { done in
                return provider.setNode(ScalarNode.PlayAddPreset, value: String(preset.number - 1), forSpeaker: clientSpeaker)
        }
        
        savePreset
            .distinctUntilChanged()
            .map { !$0 }
            .subscribe(onNext: { [unowned self] saved in
                guard saved else { return }
                self.clientSpeakerState.value?.presetIndex = preset.number - 1
            }).disposed(by: self.disposeBag)
    }
    
    func presetsViewModel(_ viewModel: PresetsViewModel, didRequestSelectPreset preset: Preset) {
        let selectPreset = self.provider.setNode(ScalarNode.NavActionSelectPreset,
                                                 value: String(preset.number - 1),
                                                 forSpeaker: self.connectionManager.clientSpeaker)
        
        SpeakerNavBrowser.setStateOn(true, onSpeaker: connectionManager.clientSpeaker, provider: provider)
            .flatMap { [weak self] (didSetNode: Bool) -> Observable<Bool> in
                guard let `self` = self else { return Observable.just(false) }
                
                if let currentPreset = self.clientSpeakerState.value?.currentPreset {
                    if currentPreset == preset {
                        return self.provider.setNode(ScalarNode.PlayControl, value: String(PlayControl.play.rawValue), forSpeaker: self.connectionManager.clientSpeaker)
                    } else {
                        return selectPreset
                    }
                } else {
                    return selectPreset
                }
            }
            .subscribe()
            .disposed(by: disposeBag)
    }
    
    func presetsViewModel(_ viewModel: PresetsViewModel, didRequestMode mode: Mode) {
        clientSpeakerState.value?.modeIndex = mode.key
        clientSpeakerState.value?.presetIndex = nil
        provider.setNode(ScalarNode.Mode, value: String(mode.key), forSpeaker: connectionManager.clientSpeaker)
            .subscribe()
            .disposed(by: disposeBag)
    }
}
