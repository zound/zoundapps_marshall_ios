//
//  PlayerViewModel.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 24/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import RxSwift
import MinuetSDK

class PlayerViewModel {
    let line1Text:Variable<String?> = Variable(nil)
    let line2Text:Variable<String?> = Variable(nil)
    
    let miniPlayerLine1Text:Variable<String?> = Variable(nil)
    let miniPlayerLine2Text:Variable<String?> = Variable(nil)
    
    let line1TextVisible:Variable<Bool> = Variable(true)
    let line2TextVisible:Variable<Bool> = Variable(true)
    
    let currentTimeText: Variable<String?> = Variable(nil)
    let totalTimeText: Variable<String?> = Variable(nil)
    let progress: Variable<Float> = Variable(0)
    let hasProgress: Variable<Bool> = Variable(false)
    let playCaps: Variable<PlayCaps> = Variable(PlayCaps(rawValue: 0))
    let navCaps: Variable<NavCaps> = Variable(NavCaps(rawValue: 0))
    let artworkURL: Variable<URL?> = Variable(nil)
    let modeArtworkPlaceholder: Variable<UIImage?> = Variable(nil)
    let isPlayingOrBuffering: Variable<Bool> = Variable(false)
    let isPlaying: Variable<Bool> = Variable(false)
    let canStop: Variable<Bool> = Variable(false)
    let shuffleOn: Variable<Bool> = Variable(false)
    let repeatOn: Variable<Bool> = Variable(false)
    let isBuffering: Variable<Bool> = Variable(false)
    let isInStandby: Variable<Bool> = Variable(false)
    let needsUpdateProgress: Variable<Bool>  = Variable(false)
    let spotifyPlaying: Variable<Bool> = Variable(false)
    let chromecastPlaying: Variable<Bool> = Variable(false)
    var multi: Variable<Bool> = Variable(false)
    
    let currentSelectionText: Variable<String?> = Variable(nil)
    let currentDuration: Variable<Int> = Variable(0)
    
    let connectionManager: SpeakerConnectionManager
    
    let disposeBag = DisposeBag()
    
    var clientState: Variable<GroupClientState?> { return connectionManager.clientState }
    var masterState: Variable<GroupMasterState?> { return connectionManager.masterState }
    var connectionStatus: Variable<SpeakerConnectionStatus> { return connectionManager.connectionStatus }
    
    var clientSpeaker: Speaker { return connectionManager.clientSpeaker}
    var masterSpeaker: Speaker { return connectionManager.masterSpeaker}
    var speakerNotifier: SpeakerNotifier { return connectionManager.clientNotifier }
    var provider: SpeakerProvider { return connectionManager.provider }
    var isGuestSpeaker : Bool
    
    var progressCompoundDisposable: CompositeDisposable?
    
    init(connectionManager: SpeakerConnectionManager, isGuest: Bool) {
        self.connectionManager = connectionManager
        self.isGuestSpeaker = isGuest
        self.clientState.asObservable()
            .map { ($0 != nil &&  $0?.groupState != GroupState.solo) }
            .distinctUntilChanged()
            .bind(to: multi)
            .disposed(by: disposeBag)
        
        let optionalStringComparer = { (rhs: String?, lhs: String?) in rhs == lhs }
        let playName = self.masterState.asObservable()
            .map { $0?.playName ?? "" }
            .distinctUntilChanged(optionalStringComparer)
        
        let playArtist = self.masterState.asObservable()
            .map { $0?.playArtist ?? "" }
            .distinctUntilChanged(optionalStringComparer)
        
        let playText = self.masterState.asObservable()
            .map { $0?.playText ?? "" }
            .distinctUntilChanged(optionalStringComparer)
        
        let connectedBluetoothDeviceName = self.clientState.asObservable()
            .map { $0?.bluetoothDevices.filter({$0.state == BlueoothDeviceState.connected}).first?.name ?? "" }
            .distinctUntilChanged(optionalStringComparer)
        //let playAlbum = self.speakerState.asObservable().map{ $0.playAlbum ?? ""}.distinctUntilChanged(optionalStringComparer)
        
        let optionalModeComparer = { (rhs: Mode?, lhs: Mode?) in rhs == lhs }
        let mode = self.masterState.asObservable()
            .map { $0?.currentMode }
            .distinctUntilChanged(optionalModeComparer)
        
        let friendlyName = self.clientState.asObservable()
            .map { $0?.friendlyName }
            .distinctUntilChanged(optionalStringComparer)
        
        let playArtistAndName: Observable<String?> = Observable.combineLatest(playName, playArtist) { name, artist in
            if let name = name, let artist = artist {
                if name != "" && artist != "" {
                    return "\(artist) - \(name)"
                }
                if name != "" && artist == "" {
                    return name
                }
                if name == "" && artist != "" {
                    return artist
                }
            }
            return nil
        }
        
        let line1Visible: Observable<Bool> = mode.flatMapLatest{ mode -> Observable<Bool> in
            guard let `mode` = mode else { return Observable.just(false) }
            switch mode.selectableType {
                
            case .aux, .rca: return Observable.just(false)
            default: return Observable.just(true)
            }
        }
        
        line1Visible
            .bind(to: line1TextVisible)
            .disposed(by: disposeBag)
        
        line1Visible
            .bind(to: line2TextVisible)
            .disposed(by: disposeBag)
        
        let line1: Observable<String?> = mode
            .flatMapLatest { mode -> Observable<String?> in
                guard let `mode` = mode else { return Observable.just(nil) }
                
                switch mode.selectableType {
                case .aux: return Observable.just(Localizations.Player.Aux.ModeName)
                case .rca: return Observable.just(Localizations.Player.Rca.ModeName)
                case .bluetooth:
                    return Observable.combineLatest(playName, connectedBluetoothDeviceName){ playName, bluetoothDeviceName in
                        if playName != nil && playName! != "" {
                            return playName
                        } else if bluetoothDeviceName != nil && bluetoothDeviceName != "" {
                            return bluetoothDeviceName!
                        } else {
                            return mode.label.uppercased()
                        }
                    }
                case .cloud, .preset(_): return playName
                case .internetRadio:
                    return playName
                }
        }
        
        line1
            .bind(to: line1Text)
            .disposed(by: disposeBag)
        
        let line2: Observable<String?> = mode
            .flatMapLatest { mode -> Observable<String?> in
                guard let `mode` = mode else { return Observable.just(nil) }
                
                switch mode.selectableType {
                case .aux: return Observable.just(Localizations.Player.Aux.AudioActive)
                case .rca: return Observable.just(Localizations.Player.Rca.AudioActive)
                case .bluetooth:
                    return Observable.combineLatest(playArtist, connectedBluetoothDeviceName){ playArtist, bluetoothDeviceName in
                        if playArtist != nil && playArtist! != "" {
                            return playArtist
                        } else if bluetoothDeviceName != nil && bluetoothDeviceName != ""{
                            return Localizations.Player.Bluetooth.Connected
                        } else {
                            return Localizations.Player.Bluetooth.Idle
                        }
                    }
                case .cloud, .preset(_): return playArtist
                case .internetRadio: return playText
                }
        }
        
        line2
            .bind(to: line2Text)
            .disposed(by: disposeBag)
        
        let speakerName: Observable<String?> = Observable.combineLatest(multi.asObservable(), friendlyName) { $0 ? Localizations.Volume.Role.Multi.uppercased() : $1 }
        
        speakerName
            .bind(to: miniPlayerLine1Text)
            .disposed(by: disposeBag)
        
        let miniPlayInfo: Observable<String?> = mode
            .flatMapLatest { mode -> Observable<String?> in
                guard let `mode` = mode else { return Observable.just(nil) }
                
                switch mode.selectableType {
                case .aux: return Observable.just("\(Localizations.Player.Aux.ModeName) - \(Localizations.Player.Aux.AudioActive)")
                case .rca: return Observable.just("\(Localizations.Player.Rca.ModeName) - \(Localizations.Player.Rca.AudioActive)")
                case .bluetooth:
                    return Observable.combineLatest(playArtistAndName, connectedBluetoothDeviceName){ playArtistAndName, bluetoothDeviceName in
                        if playArtistAndName != nil && playArtistAndName! != "" {
                            return playArtistAndName
                        } else if bluetoothDeviceName != nil && bluetoothDeviceName != ""{
                            return Localizations.Player.Bluetooth.Connected
                        } else {
                            return Localizations.Player.Bluetooth.Idle
                        }
                    }
                case .cloud, .preset(_): return playArtistAndName
                case .internetRadio: return playName
                }
        }
        
        miniPlayInfo
            .bind(to: miniPlayerLine2Text)
            .disposed(by: disposeBag)
        
        let isInStandbyObservable : Observable<Bool>  = mode
            .map { mode -> Bool in
                return mode != nil && mode!.id.lowercased() == "standby"
        }
        
        isInStandbyObservable
            .bind(to: isInStandby)
            .disposed(by: disposeBag)
        
        let playGraphicURI = self.masterState.asObservable()
            .map { $0?.playGraphicURI }
            .distinctUntilChanged(optionalStringComparer)
        
        let spotifyPhotoUri = self.masterState.asObservable()
            .map { $0?.playText }
            .distinctUntilChanged(optionalStringComparer)
            .map { spotifyPhotoId -> String? in
                if let spotifyId = spotifyPhotoId {
                    if spotifyId.hasPrefix("spotify:image:"){
                        let id = spotifyId.replacingOccurrences(of: "spotify:image:", with: "")
                        return "https://i.scdn.co/image/\(id)"
                    } else if spotifyId.hasPrefix("http://images.spotify.com") {
                        return spotifyId
                    }
                }
                return nil
        }
        
        Observable.combineLatest(playGraphicURI, spotifyPhotoUri) { playGraphicURI, spotifyPhotoURI in
            if let spotfyURL = spotifyPhotoURI {
                return spotfyURL
            }
            if let `playGraphicURI` = playGraphicURI {
                if playGraphicURI != "" {
                    return playGraphicURI
                }
            }
            return nil
            }
            .map { $0 != nil ? URL(string:$0!) : nil }
            .bind(to: artworkURL)
            .disposed(by: disposeBag)
        
        
        let modePlaceholderImage = mode
            .map { mode -> UIImage? in
                guard let `mode` = mode else { return nil }
                
                switch mode.selectableType {
                case .aux: return UIImage(named: "carousel_artwork_aux")
                case .rca: return UIImage(named: "carousel_artwork_rca")
                case .bluetooth: return UIImage(named: "carousel_artwork_bluetooth")
                default: return nil
                }
        }
        
        modePlaceholderImage
            .bind(to: modeArtworkPlaceholder)
            .disposed(by: disposeBag)
        
        let duration = self.masterState.asObservable()
            .map { $0?.duration ?? 0 }
        
        duration
            .bind(to: currentDuration)
            .disposed(by: disposeBag)
        
        duration
            .map { $0 > 0 }
            .bind(to: hasProgress)
            .disposed(by: disposeBag)
        
        duration
            .map(milisecondsToHoursMinutesSecondsText)
            .bind(to: totalTimeText)
            .disposed(by: disposeBag)
        
        let position = self.masterState.asObservable()
            .map { $0?.position ?? 0 }
        
        let progress = Observable.combineLatest(duration, position) { (duration, position) ->  Float in return Float(position) / Float(duration) }
        position
            .map(milisecondsToHoursMinutesSecondsText)
            .bind(to: currentTimeText)
            .disposed(by: disposeBag)
        
        progress
            .bind(to: self.progress)
            .disposed(by: disposeBag)
        
        self.masterState.asObservable()
            .map { $0?.isPlayingOrBuffering ?? false }
            .distinctUntilChanged()
            .bind(to: isPlayingOrBuffering)
            .disposed(by: disposeBag)
        
        self.masterState.asObservable()
            .map { $0?.isPlaying ?? false }
            .distinctUntilChanged()
            .bind(to: isPlaying)
            .disposed(by: disposeBag)
        
        self.masterState.asObservable()
            .map { $0?.playCaps?.canStop ?? false }
            .distinctUntilChanged()
            .bind(to: canStop)
            .disposed(by: disposeBag)
        
        self.masterState.asObservable()
            .map { state -> Bool in
                guard let playStatus = state?.playStatus else { return false }
                return [.buffering, .rebuffering].contains(playStatus) }
            .distinctUntilChanged()
            .bind(to: isBuffering)
            .disposed(by: disposeBag)
        
        self.masterState.asObservable()
            .map { $0?.shuffle == true }
            .distinctUntilChanged()
            .bind(to: shuffleOn)
            .disposed(by: disposeBag)
        
        self.masterState.asObservable()
            .map { $0?.repeats == true }
            .distinctUntilChanged()
            .bind(to: repeatOn)
            .disposed(by: disposeBag)
        
        self.masterState.asObservable()
            .map { $0?.playCaps ?? PlayCaps(rawValue: 0) }
            .distinctUntilChanged()
            .bind(to: playCaps)
            .disposed(by: disposeBag)
        
        self.masterState.asObservable()
            .map { $0?.navCaps ?? NavCaps(rawValue: 0) }
            .distinctUntilChanged()
            .bind(to: navCaps)
            .disposed(by: disposeBag)
        
        let presetComparer = { (rhs: Preset?, lhs: Preset?) in rhs == lhs }
        let currentPreset = self.clientState.asObservable()
            .map { $0?.currentPreset }
            .distinctUntilChanged(presetComparer)
        
        let currentMode = self.masterState.asObservable()
            .map { $0?.currentMode }
            .distinctUntilChanged(optionalModeComparer)
        
        let castApp = self.masterState.asObservable()
            .map { $0?.castAppName }
            .distinctUntilChanged(optionalStringComparer)
        
        let currentPlayingTitleTextObservable = Observable.combineLatest(currentPreset, currentMode, castApp) { (preset, source, castApp) -> String? in
            if let p = preset {
                return p.name
            }
            
            if let s = source {
                if s.isCast && castApp != nil && castApp != "" {
                    return castApp
                } else {
                    return s.playableTitle
                }
            }
            
            return nil
        }
        
        currentPlayingTitleTextObservable
            .bind(to: currentSelectionText)
            .disposed(by: disposeBag)
        
        Observable.combineLatest(currentMode, castApp, resultSelector: { (currentMode, castApp) -> Bool in
            let returnValue = (currentMode != nil) && (currentMode!.isSpotify || castApp == "Spotify")
            return returnValue })
            .bind(to: spotifyPlaying)
            .disposed(by: disposeBag)
        
        currentMode
            .unwrapOptional()
            .map { $0.isCast }
            .bind(to: chromecastPlaying)
            .disposed(by: disposeBag)
    }
    
    var savedNotification: Observable<Bool> {
        return speakerNotifier.notificationsForNode(ScalarNode.PlayAddPresetStatus)
            .map { $0 == "0" }
    }
    
    func stopUpdatingProgress() {
        needsUpdateProgress.value = false
    }
    
    func startUpdatingProgress() {
        needsUpdateProgress.value = true
    }
    
    func runAfterDelay(_ delay: TimeInterval, block: @escaping ()->()) {
        let time = DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: time, execute: block)
    }
    
    func commitProgress(_ progress: Float) {
        let currentTime = Int(progress * Float(currentDuration.value))
        
        provider.setNode(ScalarNode.PlayPosition, value: String(currentTime), forSpeaker: masterSpeaker)
            .subscribe(onCompleted: { [weak self] in
                self?.runAfterDelay(1.0, block: {
                    self?.startUpdatingProgress()
                })
            }).disposed(by: disposeBag)
    }
    
    func seekToProgress(_ progress: Float) {
        stopUpdatingProgress()
        let currentTime = Int(progress * Float(currentDuration.value))
        masterState.value?.position = currentTime
    }
    
    func playPause() {
        guard case SpeakerConnectionStatus.connected = connectionStatus.value else { return }
        
        var command: PlayControl
        if self.isPlayingOrBuffering.value {
            command = self.playCaps.value.contains(PlayCaps.Stop) ? PlayControl.stop : PlayControl.pause
        } else {
            command = self.playCaps.value.contains(PlayCaps.Stop) ? PlayControl.stop : PlayControl.play
        }
        
        provider.sendCommand(command, toSpeaker: masterSpeaker)
            .subscribe()
            .disposed(by: disposeBag)
    }
    
    func prev() {
        guard case SpeakerConnectionStatus.connected = connectionStatus.value else { return }
        
        provider.sendCommand(.previous, toSpeaker: masterSpeaker)
            .subscribe()
            .disposed(by: disposeBag)
    }
    
    func next() {
        guard case SpeakerConnectionStatus.connected = connectionStatus.value else { return }
        
        provider.sendCommand(.next, toSpeaker: masterSpeaker)
            .subscribe()
            .disposed(by: disposeBag)
    }
    
    func shuffleToggle() {
        guard case SpeakerConnectionStatus.connected = connectionStatus.value else { return }
        
        self.shuffleOn.value = !self.shuffleOn.value // toggle value
        provider.setNode(ScalarNode.PlayShuffle, value: self.shuffleOn.value ? "1" : "0", forSpeaker: masterSpeaker)
            .subscribe()
            .disposed(by: disposeBag)
        
    }
    
    func repeatToggle() {
        guard case SpeakerConnectionStatus.connected = connectionStatus.value else { return }
        
        self.repeatOn.value = !self.repeatOn.value // toggle value
        provider.setNode(ScalarNode.PlayRepeat, value: self.repeatOn.value ? "1" : "0", forSpeaker: masterSpeaker)
            .subscribe()
            .disposed(by: disposeBag)
    }
}

func milisecondsToHoursMinutesSecondsText (_ miliseconds : Int) ->  String{
    let seconds = miliseconds / 1000
    let t: (hours:Int, minutes:Int, seconds:Int) = (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    
    if t.hours > 0 {
        return String(format: "%01d:%02d:%02d", t.hours, t.minutes, t.seconds)
    }
    
    return String(format: "%01d:%02d", t.minutes, t.seconds)
}
