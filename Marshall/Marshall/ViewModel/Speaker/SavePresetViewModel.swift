//
//  SavePresetViewModel.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 28/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import MinuetSDK

class SavePresetViewModel {
    let disposeBag =  DisposeBag()
    let speaker: Speaker
    let clientState: Variable<GroupClientState?>
    let provider: SpeakerProvider
    let speakerNotifier: SpeakerNotifier
    let presetsViewModelsVariable: Variable<[PresetCellViewModel]> = Variable([])
    
    var presetsViewModels: [PresetCellViewModel] {
        get { return presetsViewModelsVariable.value }
        set { presetsViewModelsVariable.value = newValue }
    }
    
    init(speaker: Speaker, clientState: Variable<GroupClientState?>, provider: SpeakerProvider, notifier: SpeakerNotifier, spotifyProvider: SpotifyProvider) {
        self.speaker = speaker
        self.clientState = clientState
        self.provider = provider
        self.speakerNotifier = notifier
        
        for preset in clientState.value?.presets ?? [] {
            let presetViewModel = PresetCellViewModel(preset: preset, speaker: speaker, provider: provider, notifier: notifier, spotifyProvider: spotifyProvider)
            self.presetsViewModels.append(presetViewModel)
        }
        
        let presetComparer = {(rhs: [Preset], lhs: [Preset]) in rhs == lhs }
        clientState.asObservable()
            .map { $0?.presets ?? [] }
            .distinctUntilChanged(presetComparer)
            .skip(1)
            .subscribe(weak: self, onNext: SavePresetViewModel.updatePresetsInViewModels)
            .disposed(by: disposeBag)
    }
    
    func updatePresetsInViewModels(presets: [Preset]) {
        for preset in presets {
            if let presetViewModel = presetsViewModels.find({ $0.preset.number == preset.number }) {
                if presetViewModel.preset != preset {
                    presetViewModel.preset = preset
                }
            }
        }
    }
}
