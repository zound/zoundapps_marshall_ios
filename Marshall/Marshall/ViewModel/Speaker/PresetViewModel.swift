//
//  PresetViewModel.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 05/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import RxSwift
import MinuetSDK

protocol PresetViewModelDelegate: class {
    func presetViewModelDidRequestUndoToPreset(_ presetViewModel: PresetViewModel, preset: Preset)
    func presetViewModelDidRequestSavePreset(_ presetViewModel: PresetViewModel)
    func presetViewModelDidRequestDeletePreset(_ presetViewModel: PresetViewModel)
}

class PresetViewModel: SelectableViewModel {
    weak var delegate: SelectableViewModelDelegate?
    weak var presetDelegate: SelectableViewModelDelegate?
    var undoTimerDisposable: Disposable?
    
    let disposeBag = DisposeBag()
    let preset: Preset
    
    var previousPreset: Preset? = nil {
        didSet {
            canUndoSave.value = (previousPreset != nil && !previousPreset!.isEmpty && !preset.isEmpty)
            canUndoDelete.value = (previousPreset != nil && !previousPreset!.isEmpty && preset.isEmpty)
            if previousPreset != nil {
                startUndoTimer()
            }
        }
    }
    
    var willBeReplacedByUndonePreset = false
    var presetIsUndone = false
    var isSelected: Variable<Bool> = Variable(false)
    var selectableText: String? { return String(self.preset.number) }
    var playableItem: PlayableItem? { return preset }
    
    let speakerNotifier: SpeakerNotifier
    let clientState: Variable<GroupClientState?>
    let masterState: Variable<GroupMasterState?>
    let spotifyProvider: SpotifyProvider
    let playlistArtworkURL: Variable<URL?> = Variable(nil)
    let spotifyLoginNeeded: Variable<Bool> = Variable(false)
    let canAddPreset: Variable<Bool> = Variable(false)
    let canUndoSave: Variable<Bool> = Variable(false)
    let canUndoDelete: Variable<Bool> = Variable(false)
    let isEmptyPreset: Variable<Bool> = Variable(false)
    
    init(preset: Preset, clientState: Variable<GroupClientState?>, masterState: Variable<GroupMasterState?>, speakerNotifier: SpeakerNotifier, spotifyProvider: SpotifyProvider) {
        self.preset = preset
        self.speakerNotifier = speakerNotifier
        self.clientState = clientState
        self.masterState = masterState
        self.spotifyProvider = spotifyProvider
        
        self.clientState.asObservable()
            .unwrapOptional()
            .map { $0.playCaps?.canAddPreset ?? false }
            .distinctUntilChanged()
            .bind(to: canAddPreset)
            .disposed(by: disposeBag)
        
        if let spotifyURI = self.preset.spotifyURI {
            let hasToken = spotifyProvider.tokenManager.spotifyTokenVariable.asObservable().map { $0 != nil }
            let artworkIsCached = playlistArtworkURL.asObservable()
            
            Observable.combineLatest(hasToken, artworkIsCached) { hasToken, cachedArtwork in
                return !hasToken && (cachedArtwork == nil) }
                .bind(to: spotifyLoginNeeded)
                .disposed(by: disposeBag)
            
            if spotifyURI.contains("playlist") || spotifyURI.contains("album") || spotifyURI.contains("artist") || spotifyURI.contains("track") ||  spotifyURI.contains("user") {
                let imageURL = self.preset.imageURL
                spotifyProvider.artworkURLForPlaylistURI(spotifyURI)
                    .retryWhen({ (errorObservable) -> Observable<Bool> in
                        return errorObservable
                            .flatMapLatest { error in
                                hasToken.filter({ hasToken in
                                    hasToken == true
                                })
                        }
                    })
                    .map { $0 ?? imageURL }
                    .bind(to: self.playlistArtworkURL)
                    .disposed(by: disposeBag)
            } else {
                self.playlistArtworkURL.value = self.preset.imageURL
            }
        } else {
            self.playlistArtworkURL.value = self.preset.imageURL
        }
    }
    
    func startUndoTimer() {
        undoTimerDisposable = Observable<Int>
            .timer(5.0, scheduler: MainScheduler.instance)
            .subscribe(weak: self, onNext: PresetViewModel.forgetPreviousPreset)
        
        undoTimerDisposable!.disposed(by: disposeBag)
    }
    
    func stopUndoTimer() {
        undoTimerDisposable?.dispose()
        undoTimerDisposable = nil
    }
    
    func forgetPreviousPreset(_: Int) {
        self.previousPreset = nil
    }
    
    var savedNotification: Observable<Bool> {
        return speakerNotifier.notificationsForNode(ScalarNode.PlayAddPresetStatus)
            .map { $0 == "0" }
    }
    
    func undoPreset() {
        self.willBeReplacedByUndonePreset = true
        guard let previousPreset = self.previousPreset else { return }
        self.delegate?.selectableViewModelDidRequestUndo(self, fromPreset: preset, toPreset: previousPreset)
    }
    
    func savePreset() {
        self.delegate?.selectableViewModelDidRequestSavePreset(self, preset: preset)
    }
    
    func deletePreset() {
        self.delegate?.selectableViewModelDidRequestDeletePreset(self, preset: preset)
    }
    
    var isPlayingOrBuffering: Observable<Bool> {
        return self.masterState.asObservable()
            .map { $0?.isPlayingOrBuffering ?? false }
            .distinctUntilChanged()
    }
    
    var canStop: Observable<Bool> {
        return self.masterState.asObservable()
            .map { $0?.playCaps?.canStop ?? false }
            .distinctUntilChanged()
    }
}
