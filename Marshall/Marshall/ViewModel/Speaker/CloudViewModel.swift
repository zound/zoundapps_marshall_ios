//
//  CloudViewModel.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 05/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import RxSwift
import MinuetSDK

class CloudViewModel: SelectableViewModel {
    weak var delegate: SelectableViewModelDelegate?
    
    var isSelected: Variable<Bool> = Variable(false)
    var isPlayingInternetRadio: Variable<Bool> = Variable(false)
    var selectableImageName: String? { return "source_cloud_icon" }
    var selectableType: SelectableType { return SelectableType.cloud }
    var playableItem: PlayableItem? { return nil }
    var radioMode: Mode?
    
    let disposeBag = DisposeBag()
    
    init(radioMode: Mode?, clientState: Variable<GroupClientState?>) {
        self.radioMode = radioMode
        
        clientState.asObservable()
            .map { $0?.currentMode?.key == radioMode?.key }
            .bind(to: isPlayingInternetRadio)
            .disposed(by: disposeBag)
    }
}
