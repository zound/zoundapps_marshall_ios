//
//  RCAViewModel.swift
//  Marshall
//
//  Created by Raul Andrisan on 09/05/17.
//  Copyright © 2017 Zound Industries. All rights reserved.
//

import Foundation
import RxSwift
import MinuetSDK

class RCAViewModel: SelectableViewModel {
    weak var delegate: SelectableViewModelDelegate?
    
    var mode: Mode
    var isSelected: Variable<Bool> = Variable(false)
    var selectableImageName: String? { return "source_rca_icon" }
    var playableItem: PlayableItem? { return mode }
    
    init (mode: Mode) {
        self.mode = mode
    }
}
