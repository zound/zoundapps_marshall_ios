//
//  PresetCellViewModel.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 18/10/2016.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import RxSwift
import MinuetSDK

class PresetCellViewModel {
    let disposeBag = DisposeBag()
    let presetVariable: Variable<Preset>
    let speaker: Speaker
    let provider: SpeakerProvider
    let notifier: SpeakerNotifier
    let spotifyProvider: SpotifyProvider
    let savedSuccessfullyNotification: PublishSubject<Bool> = PublishSubject()
    let playlistArtworkURL: Variable<URL?> = Variable(nil)
    
    var preset: Preset {
        get { return presetVariable.value }
        set { presetVariable.value = newValue }
    }
    
    var placeholderImageURL: URL? {
        var name = "presets_unavailable_placeholder"
        let scale = Int(UIScreen.main.scale)
        if scale >= 2 {
            name = "presets_unavailable_placeholder@\(scale)x"
        }
        
        let fallbackURL = Bundle.main.url(forResource: name, withExtension: "png")
        return fallbackURL
    }
    
    init(preset: Preset, speaker: Speaker, provider: SpeakerProvider, notifier: SpeakerNotifier, spotifyProvider: SpotifyProvider) {
        self.speaker = speaker
        self.provider = provider
        self.notifier = notifier
        self.spotifyProvider = spotifyProvider
        presetVariable = Variable(preset)
        
        self.presetVariable.asObservable()
            .distinctUntilChanged()
            .subscribe(weak: self, onNext: PresetCellViewModel.updateForPreset)
            .disposed(by: disposeBag)
    }
    
    func updateForPreset(_ preset: Preset) {
        
        if let spotifyURI = preset.spotifyURI {
            if spotifyURI.contains("playlist") || spotifyURI.contains("album") || spotifyURI.contains("artist") || spotifyURI.contains("track") {
                let imageURL = preset.imageURL
                spotifyProvider.artworkURLForPlaylistURI(spotifyURI)
                    .map { $0 ?? imageURL }
                    .take(1)
                    .catchErrorJustReturn(placeholderImageURL)
                    .bind(to: self.playlistArtworkURL)
                    .disposed(by: disposeBag)
            } else {
                self.playlistArtworkURL.value = self.preset.imageURL
            }
        } else {
            self.playlistArtworkURL.value = self.preset.imageURL
        }
    }
    
    func savePreset() {
        let presetIndex: Int = self.preset.number - 1
        
        SpeakerNavBrowser.setStateOn(true, onSpeaker: speaker, provider: provider)
            .flatMapLatest { [weak self] done -> Observable<Bool> in
                guard let `self` = self else { return Observable.just(false) }
                return self.provider.setNode(ScalarNode.PlayAddPreset, value: String(presetIndex), forSpeaker: self.speaker) }
            .flatMapLatest { [weak self] done -> Observable<Bool> in
                guard let `self` = self else { return Observable.just(false) }
                return self.notifier.notificationsForNode(ScalarNode.PlayAddPresetStatus)
                    .map { $0 == "0"}
        }
        .take(1)
        .timeout(2.0, scheduler: MainScheduler.instance)
        .catchErrorJustReturn(false)
        .subscribe(weak: self, onNext: PresetCellViewModel.savedPresetSuccessfully)
        .disposed(by: disposeBag)
    }
    
    func savedPresetSuccessfully(success: Bool) {
        self.savedSuccessfullyNotification.onNext(success)
    }
}
