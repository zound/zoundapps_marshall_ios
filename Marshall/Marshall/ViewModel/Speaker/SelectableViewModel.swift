//
//  SelectableViewModel.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 05/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//
import Foundation
import RxSwift
import MinuetSDK

protocol SelectableViewModelDelegate: class {
    func selectableViewModelDidRequestSavePreset(_ viewModel: SelectableViewModel, preset: Preset)
    func selectableViewModelDidRequestDeletePreset(_ viewModel: SelectableViewModel, preset: Preset)
    func selectableViewModelDidRequestSelection(_ viewModel: SelectableViewModel, playableItem: PlayableItem)
    func selectableViewModelDidRequestPause(_ viewModel: SelectableViewModel)
    func selectableViewModelDidRequestUndo(_ viewModel: SelectableViewModel, fromPreset: Preset, toPreset: Preset)
}

protocol SelectableViewModel {
    var delegate: SelectableViewModelDelegate? { get set}
    var isSelected: Variable<Bool> { get set }
    var selectableText: String? { get }
    var selectableImageName: String? { get }
    var selectableType: SelectableType { get }
    var playableItem: PlayableItem? { get }
    func select(_ playableItem: PlayableItem)
}

extension SelectableViewModel {
    var selectableText: String? { return nil }
    var selectableImageName: String? { return nil }
    var selectableType: SelectableType { return playableItem != nil ? playableItem!.selectableType : SelectableType.cloud }
    
    func select(_ playableItem: PlayableItem) {
        if self.isSelected.value {
            self.delegate?.selectableViewModelDidRequestPause(self)
        } else {
            self.delegate?.selectableViewModelDidRequestSelection(self, playableItem: playableItem)
        }
    }
}
