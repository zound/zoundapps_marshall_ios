//
//  BrowseViewModel.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 05/05/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import RxSwift
import MinuetSDK

protocol BrowseViewModelDelegate: class {
    func didLoadMoreInCurrentPageInBrowseViewModel(_ browseViewModel: BrowseViewModel)
    func didNavigateToPage(_ page: SpeakerNavPage, inBrowseViewModel: BrowseViewModel)
    func didNavigateToParentPage(_ page: SpeakerNavPage, inBrowseViewModel: BrowseViewModel)
    func didNavigateToSearch(_ page: SpeakerNavPage, inBrowseViewModel: BrowseViewModel)
}

class BrowseViewModel {
    weak var delegate: BrowseViewModelDelegate?
    
    let speaker: Speaker
    let speakerNotifier: SpeakerNotifier
    let provider: SpeakerProvider
    let speakerBrowser: SpeakerNavBrowser
    var loadingMore: Bool = false
    var isSearching = Variable(false)
    
    var loadingNextPage = Variable(false)
    var loadingPreviousPage = Variable(false)
    var loadingSearchPage = Variable(false)
    
    var currentPagePath: [UInt32] = []
    let currentPage: Variable<SpeakerNavPage?> = Variable(nil)
    let currentSearchPage: Variable<SpeakerNavPage?> = Variable(nil)
    
    init(speaker: Speaker, speakerNotifier: SpeakerNotifier, provider: SpeakerProvider) {
        self.speaker = speaker
        self.speakerNotifier = speakerNotifier
        self.provider = provider
        self.speakerBrowser = SpeakerNavBrowser(speaker:speaker, speakerNotifier:  speakerNotifier, provider: provider)
        self.speakerBrowser.delegate = self
        
        currentPagePath = IRNavPathStore.getNavPath()
        self.loadingNextPage.value = true
        self.speakerBrowser.initNavigationIfNeeded(false, steps: currentPagePath)
    }
    
    func startSearch() {
        self.currentSearchPage.value = nil
        isSearching.value = true
        self.loadingSearchPage.value = false
        self.loadingNextPage.value = false
        self.loadingPreviousPage.value = false
        self.speakerBrowser.cancelOperations()
    }
    
    func search(_ term: String) {
        guard term != "" else {
            self.cancelSearch()
            return
        }
        
        self.currentPage.value = nil
        self.currentSearchPage.value = nil
        self.cancelLoading()
        self.speakerBrowser.search(term)
        
        isSearching.value = true
        loadingSearchPage.value = true
    }
    
    func cancelSearch() {
        
        self.currentSearchPage.value = nil
        isSearching.value = false
        self.loadingSearchPage.value = false
        self.loadingNextPage.value = true
        self.loadingPreviousPage.value = false
        self.speakerBrowser.cancelOperations()
        
        if currentPage.value == nil {
            speakerBrowser.navigateWithSteps(currentPagePath)
        }
    }
    
    func cancelLoading() {
        speakerBrowser.cancelOperations()
        loadingNextPage.value = false
        loadingSearchPage.value = false
        loadingMore = false
        loadingPreviousPage.value = false
    }
    
    func selectItem(_ item: NavListItem) {
        if(item.type != NavListType.searchDirectory) {
            cancelLoading()
            speakerBrowser.navigateToNavigationItem(item)
            
        } else {
            cancelLoading()
            startSearch()
        }
        
        if(item.type == NavListType.directory) {
            currentPagePath.append(item.key)
            IRNavPathStore.storeNavPath(currentPagePath)
            loadingNextPage.value = true
        }
    }
    
    func back() {
        if isSearching.value {
            cancelSearch()
        } else {
            if currentPage.value != nil { //must have loaded the initial page
                cancelLoading()
                
                speakerBrowser.navigateToParent()
                if currentPagePath.count > 0 {
                    currentPagePath.removeLast()
                }
                IRNavPathStore.storeNavPath(currentPagePath)
                loadingPreviousPage.value = true
            }
        }
    }
    
    func loadMore() {
        if !loadingMore && !loadingNextPage.value && !loadingPreviousPage.value {
            if let searchPage = currentSearchPage.value {
                if searchPage.canLoadMore {
                    loadingMore = true
                    speakerBrowser.loadMoreForPage(searchPage)
                }
            }
            else if let page = currentPage.value {
                if page.canLoadMore {
                    loadingMore = true
                    speakerBrowser.loadMoreForPage(page)
                }
            }
        }
    }
}

extension BrowseViewModel: SpeakerNavBrowserDelegate {
    func didResetNavigationInNavBrowser(_ navBrowser: SpeakerNavBrowser) {}
    
    func navBrowser(_ navBrowser: SpeakerNavBrowser, didReceiveFatalError error: Error) {
        loadingMore = false
        loadingNextPage.value = false
        loadingPreviousPage.value = false
        loadingSearchPage.value = false
    }
    
    func didLoadMoreForPage(_ page: SpeakerNavPage, navBrowser: SpeakerNavBrowser) {
        loadingMore = false
        
        if(isSearching.value) {
            self.currentSearchPage.value = page
        } else {
            self.currentPage.value = page
        }
        
        self.delegate?.didLoadMoreInCurrentPageInBrowseViewModel(self)
    }
    
    func didNavigateToPage(_ page: SpeakerNavPage, navBrowser: SpeakerNavBrowser) {
        if(isSearching.value) {
            self.currentSearchPage.value = page
            self.delegate?.didNavigateToSearch(page, inBrowseViewModel: self)
            loadingSearchPage.value = false
        } else {
            self.currentPage.value = page
            self.delegate?.didNavigateToPage(page, inBrowseViewModel: self)
            loadingNextPage.value = false
        }
    }
    
    func didNavigateToParentPage(_ page: SpeakerNavPage, navBrowser: SpeakerNavBrowser) {
        self.currentPage.value = page
        self.delegate?.didNavigateToParentPage(page, inBrowseViewModel: self)
        loadingPreviousPage.value = false
    }
}
