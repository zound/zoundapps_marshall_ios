//
//  PresetsViewModel.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 04/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift
import MinuetSDK

protocol PresetsViewModelDelegate: class {
    func presetsViewModel(_ viewModel: PresetsViewModel, didRequestSelectPreset preset: Preset)
    func presetsViewModelDidRequestPause(_ viewModel: PresetsViewModel)
    func presetsViewModel(_ viewModel: PresetsViewModel,didRequestMode mode: Mode)
    func presetsViewModel(_ viewModel: PresetsViewModel,didRequestDeletePreset preset: Preset)
    func presetsViewModel(_ viewModel: PresetsViewModel,didRequestSavePreset preset: Preset)
    func presetsViewModel(_ viewModel: PresetsViewModel,didRequestUndoPreset preset: Preset, toPreset: Preset)
}

class PresetsViewModel {
    weak var delegate: PresetsViewModelDelegate?
    
    //var samplePresets: [Preset]
    
    let selectableItems: Variable<[SelectableViewModel]> = Variable([])
    let currentSelectedViewModelIndex: Variable<Int>
    
    let clientState: Variable<GroupClientState?>
    let masterState: Variable<GroupMasterState?>
    
    let disposeBag = DisposeBag()
    let speakerProvider: SpeakerProvider
    let speakerNotifier: SpeakerNotifier
    let spotifyProvider: SpotifyProvider
    let connectionManager: SpeakerConnectionManager
    
    init(connectionManager: SpeakerConnectionManager, spotifyProvider: SpotifyProvider) {
        self.connectionManager = connectionManager
        self.clientState = connectionManager.clientState
        self.masterState = connectionManager.masterState
        self.speakerProvider = connectionManager.provider
        self.speakerNotifier = connectionManager.clientNotifier
        self.spotifyProvider = spotifyProvider
        
        self.currentSelectedViewModelIndex = Variable(0)
        
        let modesComparer = {(rhs: [Mode], lhs: [Mode]) in rhs == lhs }
        let modesObservable = clientState.asObservable()
            .map { $0?.modes ?? [] }
            .distinctUntilChanged(modesComparer)
        
        let presetComparer = {(rhs: [Preset], lhs: [Preset]) in rhs == lhs }
        let presetsObservable = clientState.asObservable()
            .map { $0?.presets ?? [] }
            .distinctUntilChanged(presetComparer)
        
        let clientNotifier = connectionManager.clientNotifier
        Observable.combineLatest(modesObservable,
                                 presetsObservable,
                                 Observable.just(clientState),
                                 Observable.just(masterState),
                                 Observable.just(connectionManager.clientSpeaker),
                                 Observable.just(speakerProvider),
                                 Observable.just(spotifyProvider))
        {[weak self] modes, presets, clientState, masterState, speaker, speakerProvider, spotifyProvider in
            let viewModels = selectableViewModelsFromModes(modes, presets: presets,
                                                           speaker: speaker,
                                                           speakerProvider: speakerProvider,
                                                           clientState: clientState,
                                                           masterState: masterState,
                                                           speakerNotifier: clientNotifier,
                                                           spotifyProvider: spotifyProvider)
            
            for var viewModel in viewModels {
                viewModel.delegate = self
            }
            return viewModels
            }
            .bind(to: selectableItems)
            .disposed(by: disposeBag)
        
        let optionalIntComparer = {(rhs: Int?, lhs: Int?) in rhs == lhs }
        let mode = self.clientState.asObservable()
            .map { $0?.modeIndex }
            .distinctUntilChanged(optionalIntComparer)
        
        let preset = self.clientState.asObservable()
            .map { $0?.presetIndex }
            .distinctUntilChanged(optionalIntComparer)
        
        Observable.combineLatest(mode,preset, self.selectableItems.asObservable()) { modeIndex, presetIndex, selectableItems in return (modeIndex, presetIndex, selectableItems) }
            .subscribe(onNext:{ [weak self] (modeIndex, presetIndex, selectableItems) in
                self?.updateCurrentPlayableItem(modeIndex, presetIndex: presetIndex)
            }).disposed(by: disposeBag)
    }
    
    
    
    func updateCurrentPlayableItem(_ modeIndex: Int?, presetIndex: Int?) {
        if let item = self.clientState.value?.currentPlayableItem {
            if let index = selectableItems.value.index(where: {
                $0.selectableType == item.selectableType ||
                    (item.selectableType == .internetRadio && $0.selectableType == .cloud)
            }) {
                currentSelectedViewModelIndex.value =  index
                for (itemIndex,item) in selectableItems.value.enumerated() {
                    if itemIndex == index {
                        item.isSelected.value = true
                    } else {
                        item.isSelected.value = false
                    }
                }
            }
        }
    }
}

func urlForArtworkNamed(_ name: String) -> URL? {
    let url = Bundle.main.url(forResource: name, withExtension: "jpeg")
    return url
}

func selectableViewModelsFromModes(_ modes: [Mode], presets:[Preset], speaker: Speaker, speakerProvider: SpeakerProvider, clientState: Variable<GroupClientState?>, masterState: Variable<GroupMasterState?>, speakerNotifier: SpeakerNotifier, spotifyProvider: SpotifyProvider) -> [SelectableViewModel] {
    
    let presetsAsSelectableItems = presets.map { preset in
        return PresetViewModel(preset: preset,
                               clientState: clientState,
                               masterState: masterState,
                               speakerNotifier: speakerNotifier,
                               spotifyProvider:spotifyProvider)
    }
    
    var otherSelectableItems: [SelectableViewModel] = []
    if let auxMode = modes.first(where: {$0.selectableType == .aux }) {
        let auxViewModel = AuxViewModel(mode: auxMode)
        otherSelectableItems.append(auxViewModel)
    }
    
    if let rcaMode = modes.first(where: {$0.selectableType == .rca }) {
        let rcaViewModel = RCAViewModel(mode: rcaMode)
        otherSelectableItems.append(rcaViewModel)
    }
    
    if let bluetoothMode = modes.first(where: { $0.selectableType == .bluetooth }) {
        let bluetoothViewModel = BluetoothViewModel(mode: bluetoothMode,
                                                    clientState: clientState,
                                                    speaker: speaker,
                                                    speakerProvider: speakerProvider)
        otherSelectableItems.append(bluetoothViewModel)
    }
    
    let radioMode = modes.first(where: { $0.selectableType == .internetRadio })
    otherSelectableItems.append(CloudViewModel(radioMode: radioMode,
                                               clientState: clientState))
    
    return presetsAsSelectableItems + otherSelectableItems
}

extension PresetsViewModel: SelectableViewModelDelegate {
    func selectableViewModelDidRequestSelection(_ viewModel: SelectableViewModel, playableItem: PlayableItem) {
        if let vm = viewModel as? PresetViewModel {
            self.delegate?.presetsViewModel(self, didRequestSelectPreset: vm.preset)
        }
        
        if viewModel is AuxViewModel {
            if let mode = clientState.value?.modes.first(where: { $0.selectableType == .aux }) {
                self.delegate?.presetsViewModel(self, didRequestMode: mode)
            }
        }
        
        if viewModel is RCAViewModel {
            if let mode = clientState.value?.modes.first(where: { $0.selectableType == .rca }) {
                self.delegate?.presetsViewModel(self, didRequestMode: mode)
            }
        }
        
        if viewModel is BluetoothViewModel {
            if let mode = clientState.value?.modes.first(where: { $0.selectableType == .bluetooth }) {
                self.delegate?.presetsViewModel(self, didRequestMode: mode)
            }
        }
        
        if viewModel is CloudViewModel {
            if let mode = clientState.value?.modes.first(where: { $0.selectableType == .internetRadio }) {
                self.delegate?.presetsViewModel(self, didRequestMode: mode)
            }
        }
    }
    
    func selectableViewModelDidRequestUndo(_ viewModel: SelectableViewModel, fromPreset: Preset, toPreset: Preset) {
        self.delegate?.presetsViewModel(self, didRequestUndoPreset: fromPreset, toPreset: toPreset)
    }
    
    func selectableViewModelDidRequestSavePreset(_ viewModel: SelectableViewModel, preset: Preset) {
        self.delegate?.presetsViewModel(self, didRequestSavePreset: preset)
    }
    
    func selectableViewModelDidRequestDeletePreset(_ viewModel: SelectableViewModel, preset: Preset) {
        self.delegate?.presetsViewModel(self, didRequestDeletePreset: preset)
    }
    
    func selectableViewModelDidRequestPause(_ viewModel: SelectableViewModel) {
        self.delegate?.presetsViewModelDidRequestPause(self)
    }
}
