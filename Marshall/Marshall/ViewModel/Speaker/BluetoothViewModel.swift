//
//  BluetoothViewModel.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 05/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import RxSwift
import MinuetSDK

class BluetoothViewModel: SelectableViewModel {
    weak var delegate: SelectableViewModelDelegate?
    
    var isSelected: Variable<Bool> = Variable(false)
    var isPairing: Variable<Bool> = Variable(false)
    var bluetoothDevices: Variable<[BluetoothDevice]> = Variable([])
    var selectableImageName: String? { return "source_bluetooth_icon" }
    var playableItem: PlayableItem? { return mode }
    var mode: Mode
    
    let clientState: Variable<GroupClientState?>
    let disposeBag = DisposeBag()
    let speakerProvider: SpeakerProvider
    let speaker: Speaker
    
    init(mode: Mode, clientState: Variable<GroupClientState?>, speaker: Speaker, speakerProvider: SpeakerProvider) {
        self.mode = mode
        self.clientState = clientState
        self.speaker = speaker
        self.speakerProvider = speakerProvider
        
        self.clientState.asObservable()
            .map { $0?.bluetoothDiscoverable }
            .map { $0 != nil ? $0! : false }
            .bind(to: isPairing)
            .disposed(by: disposeBag)
        
        self.clientState.asObservable().map{ $0?.bluetoothDevices }
            .map { $0 != nil ? $0! : [] }
            .map { $0.filter({$0.state == BlueoothDeviceState.connected }) }
            .bind(to: bluetoothDevices)
            .disposed(by: disposeBag)
    }
    
    func startPairing() {
        speakerProvider.setNode(ScalarNode.BluetoothDiscoverableState, value: "1", forSpeaker: speaker)
            .subscribe()
            .disposed(by: disposeBag)
    }
    
    func endPairing() {
        speakerProvider.setNode(ScalarNode.BluetoothDiscoverableState, value: "0", forSpeaker: speaker)
            .subscribe()
            .disposed(by: disposeBag)
    }
}
