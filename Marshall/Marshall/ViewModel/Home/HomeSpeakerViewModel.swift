//
//  HomeSpeakerViewModel.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 14/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import RxSwift
import MinuetSDK

enum HomeSpeakerMultiState {
    case notSupported
    case unknown
    case loading
    case solo
    case multi
}

class HomeSpeakerViewModel {
    let speaker: Speaker
    let nowPlayingDescription: Variable<String?>
    let isPlaying: Variable<Bool>
    let isUnlocked: Variable<Bool>
    let isPlayingCast: Variable<Bool>
    let isEnabled: Variable<Bool>
    let loading: Variable<Bool> = Variable(false)
    let multi: Variable<HomeSpeakerMultiState>
    let provider: SpeakerProvider
    let disposeBag = DisposeBag()
    
    init(speaker: Speaker, multi: HomeSpeakerMultiState, provider: SpeakerProvider) {
        self.speaker = speaker
        self.multi = Variable(multi)
        self.provider = provider
        self.nowPlayingDescription = Variable(nil)
        self.isPlaying = Variable(false)
        self.isPlayingCast = Variable(false)
        self.isEnabled = Variable(true)
        self.isUnlocked = Variable(false)
    }
}
