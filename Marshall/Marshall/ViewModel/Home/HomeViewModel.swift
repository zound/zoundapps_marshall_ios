//
//  MainViewModel.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 10/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import RxSwift
import UIKit
import Alamofire
import MinuetSDK

protocol HomeViewModelDelegate: class {
    func homeViewModelWillMakeGroupChanges()
    func homeViewModelDidMakeGroupChanges()
    func homeViewModelDidAddMultiGroup(atIndex: Int, fromMove: Bool)
    func homeViewModelDidAddSoloGroup(atIndex: Int, fromMove: Bool)
    func homeViewModelDidRemoveMultiGroup(fromIndex: Int, fromMove: Bool)
    func homeViewModelDidRemoveSoloGroup(fromIndex: Int, fromMove: Bool)
    
}

class HomeViewModel {
    weak var delegate: HomeViewModelDelegate?
    
    let audioSystem: AudioSystem
    let multiViewModels: Variable<[HomeGroupViewModel]> = Variable([])
    let soloViewModels: Variable<[HomeGroupViewModel]> = Variable([])
    let rx_disposeBag = DisposeBag()
    let provider: SpeakerProvider
    let disposeBag = DisposeBag()
    
    var currentGroupIds: [String] = []
    
    init(audioSystem: AudioSystem, speakerProvider: SpeakerProvider) {
        self.provider = speakerProvider
        self.audioSystem = audioSystem
        
        audioSystem.addDelegate(self)
        audioSystem.groups.value.forEach { audioSystemDidAddGroup($0, fromMove: false) }
    }
    
    deinit {
        audioSystem.removeDelegate(self)
    }
}

func alphabeticalIndexForGroup(_ group: SpeakerGroup, toInsertInto groups: [SpeakerGroup]) -> Int {
    var index = 0
    
    for currentGroup in groups {
        if currentGroup.groupName!.lowercased() < group.groupName!.lowercased() {
            index = index + 1
        } else {
            break
        }
    }
    
    return index
}

extension HomeViewModel: AudioSystemDelegate {
    var delegateIdentifier: String {
        return "home_view_model"
    }
    
    func audioSystemWillMakeChanges() {
        self.delegate?.homeViewModelWillMakeGroupChanges()
    }
    
    func audioSystemDidMakeChanges() {
        self.delegate?.homeViewModelDidMakeGroupChanges()
    }
    
    func audioSystemDidAddGroup(_ group: SpeakerGroup, fromMove: Bool) {
        
        //this makes sure we never have the same group on the screen multiple times
        if !(currentGroupIds.contains(group.groupId)) {
            currentGroupIds.append(group.groupId)
            let groupViewModel = HomeGroupViewModel(group: group, nowPlayingState: audioSystem.nowPlayingInfo, unlockedInfo: self.audioSystem.unlockedInfo, provider: self.provider)
            
            if group.isMulti {
                let insertionIndex = alphabeticalIndexForGroup(group, toInsertInto: self.multiViewModels.value.map { $0.group })
                self.multiViewModels.value.insert(groupViewModel, at: insertionIndex)
                self.delegate?.homeViewModelDidAddMultiGroup(atIndex: insertionIndex, fromMove: fromMove)
                
                if fromMove {
                    for speaker in group.speakers {
                        if group.masterSpeaker != speaker {
                            audioSystem.nowPlayingInfo.value.removeValue(forKey: speaker)
                        } else {
                            audioSystem.discoveryService.confirmDeviceAtIP(speaker.ipAddress)
                        }
                    }
                }
            } else {
                let insertionIndex = alphabeticalIndexForGroup(group, toInsertInto: self.soloViewModels.value.map { $0.group })
                self.soloViewModels.value.insert(groupViewModel, at: insertionIndex)
                self.delegate?.homeViewModelDidAddSoloGroup(atIndex: insertionIndex, fromMove: fromMove)
                
                if fromMove {
                    if let speaker = group.speakers.first {
                        audioSystem.nowPlayingInfo.value.removeValue(forKey: speaker)
                        audioSystem.discoveryService.confirmDeviceAtIP(speaker.ipAddress)
                    }
                }
            }
        }
    }
    
    func audioSystemDidRemoveGroup(_ group: SpeakerGroup, fromMove: Bool) {
        currentGroupIds.removeObject(group.groupId)
        
        if group.isMulti {
            if let indexToRemove = self.multiViewModels.value.index(where: { $0.group.groupId == group.groupId }) {
                self.multiViewModels.value.remove(at: indexToRemove)
                self.delegate?.homeViewModelDidRemoveMultiGroup(fromIndex: indexToRemove, fromMove: fromMove)
            }
        } else {
            if let indexToRemove = self.soloViewModels.value.index(where: { $0.group.groupId == group.groupId }) {
                self.soloViewModels.value.remove(at: indexToRemove)
                self.delegate?.homeViewModelDidRemoveSoloGroup(fromIndex: indexToRemove, fromMove: fromMove)
            }
        }
    }
    
    func audioSystemDidUpdateMasterTo(newMaster: Speaker?, inGroup group: SpeakerGroup) {
        if group.isMulti {
            if let maintainedGroupViewModel = self.multiViewModels.value.find({ $0.group.groupId == group.groupId }) {
                maintainedGroupViewModel.changeMasterTo(newMaster: newMaster)
            }
        }
    }
    
    func audioSystemDidAddSpeaker(_ speaker: Speaker, toGroup group: SpeakerGroup, fromMove: Bool) {
        if group.isMulti {
            if let maintainedGroupViewModel = self.multiViewModels.value.find({ $0.group.groupId == group.groupId }) {
                let index = alphabeticalIndexForSpeaker(speaker, toInsertInto: maintainedGroupViewModel.speakersViewModels.value.map{ $0.speaker })
                maintainedGroupViewModel.addSpeaker(speaker, atIndex: index, fromMove: fromMove)
            }
        } else {
            if let maintainedGroupViewModel = self.soloViewModels.value.find({ $0.group.groupId == group.groupId }) {
                maintainedGroupViewModel.addSpeaker(speaker, atIndex: 0, fromMove: fromMove)
            }
        }
    }
    
    func audioSystemDidRemoveSpeaker(_ speaker: Speaker, fromGroup group: SpeakerGroup, fromMove: Bool) {
        if group.isMulti {
            if let maintainedGroupViewModel = self.multiViewModels.value.find({ $0.group.groupId == group.groupId }) {
                maintainedGroupViewModel.removeSpeaker(speaker, fromMove: fromMove)
            }
        } else {
            if let maintainedGroupViewModel = self.soloViewModels.value.find({ $0.group.groupId == group.groupId }) {
                maintainedGroupViewModel.removeSpeaker(speaker, fromMove: fromMove)
            }
        }
    }
}
