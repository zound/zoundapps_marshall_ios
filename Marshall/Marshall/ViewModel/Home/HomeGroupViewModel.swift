//
//  HomeSpeakerViewModel.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 14/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import RxSwift
import MinuetSDK

protocol HomeGroupViewModelDelegate: class {
    func homeGroupViewModelDidAddSpeaker(atIndex: Int, fromMove: Bool, homeGroupViewModel: HomeGroupViewModel)
    func homeGroupViewModelDidRemoveSpeaker(fromIndex: Int, fromMove: Bool, homeGroupViewModel: HomeGroupViewModel)
    func homeGroupViewModelDidMoveSpeaker(fromIndex: Int, toIndex: Int, homeGroupViewModel: HomeGroupViewModel)
}

class HomeGroupViewModel {
    let provider: SpeakerProvider
    let disposeBag = DisposeBag()
    let speakersViewModels: Variable<[HomeSpeakerViewModel]> = Variable([])
    let masterSpeakerState:Variable<NowPlayingState?>! = Variable(nil)
    let groupVariable: Variable<SpeakerGroup>
    
    var nowPlayingState: Variable<[Speaker: NowPlayingState]>
    var unlockedInfo: Variable<[Speaker: Bool]>
    
    var group: SpeakerGroup {
        get { return groupVariable.value }
        set { groupVariable.value = newValue }
    }
    
    weak var delegate: HomeGroupViewModelDelegate?
    
    init(group: SpeakerGroup, nowPlayingState: Variable<[Speaker: NowPlayingState]>, unlockedInfo: Variable<[Speaker: Bool]>, provider: SpeakerProvider) {
        self.groupVariable = Variable(group)
        self.provider = provider
        self.nowPlayingState = nowPlayingState
        self.unlockedInfo = unlockedInfo
        
        var speakerViewModels = [HomeSpeakerViewModel]()
        
        for speaker in group.speakers {
            var multiState = HomeSpeakerMultiState.notSupported
            multiState = group.isMulti ? .multi : .solo
            
            let index = insertionIndexInSpeakerViewModelsForSpeaker(speaker, inSpeakers: speakerViewModels.map { $0.speaker })
            let viewModel = HomeSpeakerViewModel(speaker: speaker, multi: multiState, provider: provider)
            speakerViewModels.insert(viewModel, at: index)
        }
        
        self.speakersViewModels.value = speakerViewModels
        
        let speakerComparer = { (rhs: Speaker?, lhs: Speaker?) in rhs == lhs }
        self.groupVariable.asObservable()
            .map { $0.masterSpeaker }
            .distinctUntilChanged(speakerComparer)
            .flatMapLatest { masterSpeaker -> Observable<NowPlayingState?> in
                guard let masterSpeaker = masterSpeaker else { return Observable.just(nil) }
                return nowPlayingState.asObservable()
                    .map { $0[masterSpeaker] } }
            .bind(to: masterSpeakerState)
            .disposed(by: disposeBag)
        
        unlockedInfo.asObservable()
            .subscribe(weak: self, onNext: HomeGroupViewModel.updateSpeakersUnlockedInfo)
            .disposed(by: disposeBag)
        
        masterSpeakerState.asObservable()
            .subscribe(weak: self, onNext: HomeGroupViewModel.updateDescriptionsWithMasterState)
            .disposed(by: disposeBag)
        
        groupVariable.asObservable().map{ $0.masterSpeaker != nil }
            .subscribe(weak: self, onNext: HomeGroupViewModel.updateForGroupEnabled)
            .disposed(by: disposeBag)
    }
    
    func addSpeaker(_ speaker: Speaker, atIndex index: Int, fromMove: Bool) {
        var multiState = HomeSpeakerMultiState.notSupported
        multiState = group.isMulti ? .multi : .solo
        
        let homeSpeakerViewModel = HomeSpeakerViewModel(speaker: speaker, multi: multiState, provider: provider)
        print("+adding speaker \(speaker.friendlyName) to \(group.groupName ?? "")")
        let vmIndex = insertionIndexInSpeakerViewModelsForSpeaker(speaker, inSpeakers: self.group.speakers)
        
        self.group.speakers.insert(speaker, at: index)
        self.speakersViewModels.value.insert(homeSpeakerViewModel, at: vmIndex)
        self.delegate?.homeGroupViewModelDidAddSpeaker(atIndex: vmIndex, fromMove: fromMove, homeGroupViewModel: self)
        
        updateForGroupEnabled(self.group.masterSpeaker != nil)
        updateDescriptionsWithMasterState(masterSpeakerState.value)
    }
    
    func changeMasterTo(newMaster speaker: Speaker?) {
        let oldMaster = self.group.masterSpeaker
        let newMaster = speaker
        self.group.masterSpeaker = speaker
        
        var speakers = self.speakersViewModels.value.map { $0.speaker }
        if let oldMaster = oldMaster {
            if let oldMasterIndex = speakers.index(of: oldMaster) {
                let oldMasterVM = self.speakersViewModels.value[oldMasterIndex]
                var localSpeakerViewModels = self.speakersViewModels.value
                localSpeakerViewModels.remove(at: oldMasterIndex)
                let vmIndex = insertionIndexInSpeakerViewModelsForSpeaker(oldMaster, inSpeakers: localSpeakerViewModels.map { $0.speaker })
                
                if vmIndex != oldMasterIndex  {
                    self.speakersViewModels.value.remove(at: oldMasterIndex)
                    self.speakersViewModels.value.insert(oldMasterVM, at: vmIndex)
                    self.delegate?.homeGroupViewModelDidMoveSpeaker(fromIndex: oldMasterIndex, toIndex: vmIndex, homeGroupViewModel: self)
                }
            }
        }
        
        speakers = self.speakersViewModels.value.map{ $0.speaker }
        if let newMaster = newMaster {
            if let newMasterIndex = speakers.index(of: newMaster) {
                let newMasterVM = self.speakersViewModels.value[newMasterIndex]
                var localSpeakerViewModels = self.speakersViewModels.value
                localSpeakerViewModels.remove(at: newMasterIndex)
                let vmIndex = insertionIndexInSpeakerViewModelsForSpeaker(newMaster, inSpeakers: localSpeakerViewModels.map { $0.speaker })
                
                if vmIndex != newMasterIndex {
                    self.speakersViewModels.value.remove(at: newMasterIndex)
                    self.speakersViewModels.value.insert(newMasterVM, at: vmIndex)
                    self.delegate?.homeGroupViewModelDidMoveSpeaker(fromIndex: newMasterIndex, toIndex: vmIndex, homeGroupViewModel: self)
                }
            }
        }
        updateForGroupEnabled( newMaster != nil)
        updateDescriptionsWithMasterState(masterSpeakerState.value)
    }
    
    func removeSpeaker(_ speaker: Speaker, fromMove: Bool) {
        if let vmIndex = self.speakersViewModels.value.index(where: { $0.speaker == speaker }) {
            print("-removing speaker \(speaker.friendlyName) from \(group.groupName ?? "")")
            self.group.speakers.removeObject(speaker)
            
            self.speakersViewModels.value.remove(at: vmIndex)
            self.delegate?.homeGroupViewModelDidRemoveSpeaker(fromIndex: vmIndex, fromMove: fromMove, homeGroupViewModel: self)
            
            updateForGroupEnabled(self.group.masterSpeaker != nil)
            updateDescriptionsWithMasterState(masterSpeakerState.value)
        }
    }
    
    func insertionIndexInSpeakerViewModelsForSpeaker(_ speaker: Speaker, inSpeakers speakers: [Speaker]) -> Int {
        var speakers = speakers
        var index = 0
        
        for currentSpeaker in speakers {
            if currentSpeaker.sortName.lowercased() < speaker.sortName.lowercased() {
                index = index + 1
            } else {
                break
            }
        }
        
        if let masterSpeaker = self.group.masterSpeaker {
            if speaker == masterSpeaker {
                return 0
            } else {
                speakers.insert(speaker, at: index)
                if speakers.contains(masterSpeaker) {
                    speakers.removeObject(masterSpeaker)
                    speakers.insert(masterSpeaker, at: 0)
                }
                
                return speakers.index(of: speaker)!
            }
        } else {
            speakers.insert(speaker, at: index)
            return speakers.index(of: speaker)!
        }
    }
    
    func updateForGroupEnabled(_ enabled: Bool) {
        for speakerViewModel in speakersViewModels.value {
            speakerViewModel.isEnabled.value = enabled
        }
    }
    
    func clearDescriptionFor(speakerViewModel: HomeSpeakerViewModel) {
        speakerViewModel.nowPlayingDescription.value = nil
        speakerViewModel.isPlaying.value = false
        speakerViewModel.isPlayingCast.value = false
    }
    
    func updateSpeakersUnlockedInfo(_ info: [Speaker: Bool]) {
        for speakerViewModel in speakersViewModels.value {
            if let isUnlocked = info[speakerViewModel.speaker] {
                speakerViewModel.isUnlocked.value = isUnlocked
            } else {
                speakerViewModel.isUnlocked.value = false
            }
        }
    }
    
    func updateDescriptionsWithMasterState(_ state: NowPlayingState?) {
        for speakerViewModel in speakersViewModels.value {
            if state?.isIdle == false {
                if let masterSpeaker = group.masterSpeaker {
                    if speakerViewModel.speaker != masterSpeaker{
                        speakerViewModel.nowPlayingDescription.value = nil
                        speakerViewModel.isPlaying.value = false
                        speakerViewModel.isPlayingCast.value = false
                    } else {
                        if let `state` = state {
                            speakerViewModel.nowPlayingDescription.value = Localizations.Home.Speaker.PlayingSource(state.currentNowPlaying ?? "")
                            speakerViewModel.isPlaying.value = (state.isIdle == false)
                            speakerViewModel.isPlayingCast.value = (state.currentMode?.isCast == true)
                        } else {
                            clearDescriptionFor(speakerViewModel: speakerViewModel)
                        }
                    }
                } else {
                    clearDescriptionFor(speakerViewModel: speakerViewModel)
                }
            } else {
                clearDescriptionFor(speakerViewModel: speakerViewModel)
            }
        }
    }
}
