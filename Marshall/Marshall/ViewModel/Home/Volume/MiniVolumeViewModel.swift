//
//  MiniVolumeViewModel.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 12/10/2016.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import RxSwift
import MinuetSDK

class MiniVolumeViewModel {
    let speakerProvider: SpeakerProvider
    let speaker: Speaker
    let masterState: Variable<GroupMasterState?>
    let clientState: Variable<GroupClientState?>
    let disposeBag = DisposeBag()
    var updatingVolume: Bool  = false
    
    init(speaker: Speaker, clientState: Variable<GroupClientState?>, masterState: Variable<GroupMasterState?>, speakerProvider: SpeakerProvider) {
        self.speaker = speaker
        self.speakerProvider = speakerProvider
        self.masterState = masterState
        self.clientState = clientState
    }
    
    func updateVolumeOnSpeaker() {
        if !updatingVolume {
            updatingVolume = true
            if let volume = clientState.value?.volume {
                speakerProvider.setNode(ScalarNode.Volume, value: String(volume), forSpeaker: speaker)
                    .subscribe(onNext: { [weak self] done in
                        self?.updatingVolume = false
                        if self?.clientState.value?.volume != volume {
                            self?.updateVolumeOnSpeaker()
                        } })
                    .disposed(by: disposeBag)
            }
        }
    }
    
    func updateMasterVolume() {
        if !updatingVolume {
            updatingVolume = true
            self.masterState.value?.userInteractionInProgress = true
            if let volume = masterState.value?.masterVolume {
                speakerProvider.setNode(ScalarNode.MultiroomMasterVolume, value: String(volume), forSpeaker: speaker)
                    .subscribe(onNext: { [weak self] done in
                        self?.updatingVolume = false
                        self?.masterState.value?.userInteractionInProgress = false
                        if self?.masterState.value?.masterVolume != volume {
                            self?.updateMasterVolume()
                        } })
                    .disposed(by: disposeBag)
            }
        }
    }
    
    func setVolume(_ volume: Int) {
        if clientState.value?.groupState == GroupState.solo {
            if clientState.value?.volume! != volume {
                clientState.value?.volume = volume
                updateVolumeOnSpeaker()
            }
        }
        
        if clientState.value?.groupState == GroupState.server || clientState.value?.groupState == GroupState.client  {
            if masterState.value?.masterVolume! != volume {
                masterState.value?.masterVolume = volume
                updateMasterVolume()
            }
        }
    }
}
