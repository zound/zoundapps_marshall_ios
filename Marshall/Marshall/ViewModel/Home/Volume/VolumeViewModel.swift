//
//  SpeakersViewModel.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 15/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import RxSwift
import MinuetSDK

protocol VolumeViewModelDelegate: class {
    func volumeViewModelDidSetVolumeForGroup(_ group: SpeakerGroup, volume: GroupVolume)
}

class VolumeViewModel {
    weak var delegate: VolumeViewModelDelegate?
    
    let speakerProvider: SpeakerProvider
    var groupVolumes: [SpeakerGroup:GroupVolume]
    let globalMute: Variable<Bool>
    let disposeBag = DisposeBag()
    var volumeUpdateDisposable: Disposable?
    var cachedClientIndexes: [UInt:Speaker]? = nil
    let audioSystem: AudioSystem
    
    let volumeUpdatePublishSubject: BehaviorSubject<Bool> = BehaviorSubject(value: true)
    let masterVolumeUpdatePublishSubject: BehaviorSubject<Bool> = BehaviorSubject(value: true)
    
    var volumeUpdateObservable:Observable<[SpeakerGroup:GroupVolume]> = Observable.just([:])
    
    let soloGroupsViewModels: Variable<[GroupVolumeViewModel]?>
    let multiGroupsViewModels: Variable<[GroupVolumeViewModel]?>
    
    init(groupVolumes: [SpeakerGroup:GroupVolume], audioSystem: AudioSystem, speakerProvider: SpeakerProvider) {
        self.speakerProvider = speakerProvider
        self.groupVolumes = groupVolumes
        self.soloGroupsViewModels = Variable(nil)
        self.multiGroupsViewModels = Variable(nil)
        self.globalMute = Variable(false)
        self.audioSystem = audioSystem
        self.audioSystem.addDelegate(self)
        
        initGroupVolumeObservables()
        
        updateViewModelsWithGroups(groups: audioSystem.groups.value, groupVolumes: groupVolumes)
        self.updateGlobalMuteWithVolumes(groupVolumes)
        
        audioSystem.nowPlayingInfo.asObservable()
            .subscribe(weak: self, onNext: VolumeViewModel.updateWithNowPlayingInfo)
            .disposed(by: disposeBag)
        
        startVolumeUpdates()
    }
    
    deinit {
        self.audioSystem.removeDelegate(self)
    }
    
    func initGroupVolumeObservables() {
        let groupVolumesObservable = speakerProvider.groupVolumesForGroups(self.audioSystem.groups.value)
        let volumeUpdateTrigger = self.volumeUpdatePublishSubject
            .throttle(1.0, scheduler: MainScheduler.instance)
            .startWith(true)
        
        volumeUpdateObservable = groupVolumesObservable.repeatWhen(notificationHandler: {
            return volumeUpdateTrigger
        })
            .do(onNext: { [weak self] _ in
                DispatchQueue.main.async {
                    self?.volumeUpdatePublishSubject.onNext(true)
                }
            })
    }
    
    func updateWithNowPlayingInfo(_ nowPlayingInfo: [Speaker: NowPlayingState]) {
        guard let soloVMs = soloGroupsViewModels.value, let multiVMs = multiGroupsViewModels.value else { return }
        
        for groupViewModel in (soloVMs + multiVMs) {
            for soloVM in groupViewModel.speakerViewModels.value {
                if let nowPlayingState = nowPlayingInfo[soloVM.speaker] {
                    let isCast = (nowPlayingState.currentMode?.isCast == true)
                    soloVM.isEnabled = !isCast
                }
            }
        }
    }
    
    func updateViewModelsWithGroups(groups: [SpeakerGroup], groupVolumes: [SpeakerGroup: GroupVolume]) {
        let groupsViewModels = groups.map{ group -> GroupVolumeViewModel in
            if let groupVolume = groupVolumes.find({ (groupWithVolume, groupVolume) in return groupWithVolume == group}).map({ $0.1 }) {
                let speakerViewModel = GroupVolumeViewModel(group: group, groupVolume: groupVolume, speakerProvider: speakerProvider)
                speakerViewModel.delegate = self
                return speakerViewModel
            }
            
            let emptyGroupVolume = GroupVolume(masterVolume: nil, clientsVolumes: nil)
            return GroupVolumeViewModel(group: group,
                                        groupVolume: emptyGroupVolume,
                                        speakerProvider: speakerProvider)
        }
        
        self.soloGroupsViewModels.value = groupsViewModels.filter({ $0.group.isMulti == false }).sorted(by: { group1, group2 in
            if let name1 = group1.group.groupName,
                let name2 = group2.group.groupName {
                return name1.lowercased() < name2.lowercased()
            }
            return true
        })
        
        self.multiGroupsViewModels.value = groupsViewModels.filter({ $0.group.isMulti == true })
        
        stopVolumeUpdates()
        initGroupVolumeObservables()
        startVolumeUpdates()
        
    }
    
    func startVolumeUpdates() {
        stopVolumeUpdates()
        
        volumeUpdateDisposable = volumeUpdateObservable
            .subscribe(onNext:{ [weak self] speakerVolumes in
                self?.updateVolumesWithSpeakerVolumes(speakerVolumes)
                self?.updateGlobalMuteWithVolumes(speakerVolumes)
            })
        
        _ = disposeBag.insert(volumeUpdateDisposable!)
    }
    
    func stopVolumeUpdates() {
        volumeUpdateDisposable?.dispose()
        volumeUpdateDisposable = nil
    }
    
    func muteAll() {
        let muteValueToSet = !globalMute.value
        let soloViewModels = soloGroupsViewModels.value ?? []
        let multiViewModels = multiGroupsViewModels.value ?? []
        
        let viewModels = (soloViewModels + multiViewModels)
        globalMute.value = muteValueToSet
        stopVolumeUpdates()
        
        var setMuteOperations: [Observable<Bool>] = []
        
        for vm in viewModels {
            var groupVolume = vm.volume.value
            var newClientsVolume = [Speaker:SpeakerVolume]()
            if let clientsVolumes = groupVolume.clientsVolumes {
                for (speaker, var volume) in clientsVolumes {
                    volume.mute = muteValueToSet
                    newClientsVolume[speaker] = volume
                    let operation = speakerProvider.setNode(ScalarNode.Mute, value: muteValueToSet ? "1" : "0", forSpeaker: speaker)
                    setMuteOperations.append(operation)
                }
                groupVolume.clientsVolumes = newClientsVolume
                vm.volume.value = groupVolume
            }
        }
        
        Observable.from(setMuteOperations)
            .merge()
            .subscribe(weak: self, onCompleted: VolumeViewModel.startVolumeUpdates)
            .disposed(by: disposeBag)
    }
    
    fileprivate func updateVolumesWithSpeakerVolumes(_ volumes: [SpeakerGroup:GroupVolume]) {
        self.groupVolumes = volumes
        print("update volumes")
        
        if let viewModels = soloGroupsViewModels.value {
            for vm in  viewModels {
                if let speakerVolume = volumes[vm.group] {
                    vm.volume.value = speakerVolume
                }
            }
        }
        
        if let viewModels = multiGroupsViewModels.value {
            for vm in  viewModels {
                if let speakerVolume = volumes[vm.group] {
                    vm.volume.value = speakerVolume
                }
            }
        }
    }
    
    fileprivate func updateGlobalMuteWithVolumes(_ volumes: [SpeakerGroup:GroupVolume]) {
        let soloViewModels = soloGroupsViewModels.value ?? []
        let multiViewModels = multiGroupsViewModels.value ?? []
        let allGroupsMute = volumes.filter({ (group, groupVolume) in groupVolume.isMute == true }).count
        globalMute.value = (allGroupsMute == (soloViewModels.count + multiViewModels.count))
    }
}

extension VolumeViewModel {
    func setInitialMultiClientVolumes() {
        var setInitialClientVolumeObservables: [Observable<Bool>] = []
        for groupVM in multiGroupsViewModels.value! {
            for speakerVM in groupVM.speakerViewModels.value {
                let operation = speakerProvider.setNode(ScalarNode.Volume, value: String(speakerVM.volume.value.volume!), forSpeaker: speakerVM.speaker)
                setInitialClientVolumeObservables.append(operation)
            }
        }
        
        Observable.from(setInitialClientVolumeObservables)
            .take(setInitialClientVolumeObservables.count)
            .subscribe(onCompleted: { [weak self] in
                guard let`self` = self else { return }
                self.startVolumeUpdates() })
            .disposed(by: disposeBag)
    }
    
    func clientIndexesForGroup(_ group: SpeakerGroup) -> Observable<[UInt: Speaker]> {
        return Observable.just(true)
            .flatMapLatest { [weak self] _ -> Observable<[UInt: Speaker]> in
                guard let `self` = self else { return Observable.just([:]) }
                
                if self.cachedClientIndexes != nil {
                    print("using cached values")
                    return Observable.just(self.cachedClientIndexes!)
                } else {
                    print("using fresh values")
                    return self.speakerProvider.getClientIndexesForGroup(group).do(onNext: { indexes in
                        self.cachedClientIndexes = indexes
                    })
                }
        }
    }
    
    func startMultiVolumeUpdates() {
        print("start multi volume updates")
        self.cachedClientIndexes = nil
        var volumeUpdateSignal:Observable<[Speaker:SpeakerVolume]> = Observable.empty()
        
        if let multiGroup = self.multiGroupsViewModels.value?.first,
            let masterSpeaker = multiGroup.group.masterSpeaker {
            let getSpeakers = self.clientIndexesForGroup(multiGroup.group)
                .map { [weak self] clientIndexes -> Observable<[Speaker:SpeakerVolume]> in
                    guard let `self` = self else { return Observable.empty() }
                    return self.speakerProvider.getGroupVolumesIncludingClientIndexes(clientIndexes, fromMasterSpeaker: masterSpeaker) }
                .switchLatest()
            
            volumeUpdateSignal = getSpeakers
        }
        
        volumeUpdateSignal = volumeUpdateSignal.repeatWhen(notificationHandler: { [weak self] () -> Observable<Bool> in
            guard let `self` = self else { return Observable.just(false) }
            return self.masterVolumeUpdatePublishSubject.startWith(true)
        })
            .do(onNext: { [weak self] _ in
                self?.masterVolumeUpdatePublishSubject.onNext(true)
            })
        
        volumeUpdateDisposable = volumeUpdateSignal.subscribe(onNext:{ [weak self] speakerVolumes in
            self?.updateMultiSpeakerVolumes(speakerVolumes: speakerVolumes)
        })
        
        _ = disposeBag.insert(volumeUpdateDisposable!)
    }
    
    func stopMultiVolumeUpdates() {
        print("stop multi volume updates")
        volumeUpdateDisposable?.dispose()
        volumeUpdateDisposable = nil
        self.cachedClientIndexes = nil
    }
    
    func updateMultiSpeakerVolumes(speakerVolumes: [Speaker:SpeakerVolume]) {
        guard let viewModels = multiGroupsViewModels.value else { return }
        
        for vm in  viewModels {
            var prevGroupVolume = vm.volume.value
            prevGroupVolume.clientsVolumes = speakerVolumes
            vm.volume.value = prevGroupVolume
        }
    }
}

extension VolumeViewModel: AudioSystemDelegate {
    var delegateIdentifier: String {
        return "volume_view_model"
    }
    
    func audioSystemDidAddGroup(_ group: SpeakerGroup, fromMove: Bool) {
        updateViewModelsWithGroups(groups: audioSystem.groups.value, groupVolumes: self.groupVolumes)
    }
    
    func audioSystemDidRemoveGroup(_ group: SpeakerGroup, fromMove: Bool) {
        updateViewModelsWithGroups(groups: audioSystem.groups.value, groupVolumes: self.groupVolumes)
    }
    
    func audioSystemDidUpdateMasterTo(newMaster: Speaker?, inGroup group: SpeakerGroup) {
        updateViewModelsWithGroups(groups: audioSystem.groups.value, groupVolumes: self.groupVolumes)
    }
    
    func audioSystemDidAddSpeaker(_ speaker: Speaker, toGroup group: SpeakerGroup, fromMove: Bool) {
        updateViewModelsWithGroups(groups: audioSystem.groups.value, groupVolumes: self.groupVolumes)
    }
    
    func audioSystemDidRemoveSpeaker(_ speaker: Speaker, fromGroup group: SpeakerGroup, fromMove: Bool) {
        updateViewModelsWithGroups(groups: audioSystem.groups.value, groupVolumes: self.groupVolumes)
    }
}

extension VolumeViewModel: GroupVolumeViewModelDelegate {
    func groupSpeakerViewModelDidStartSettingVolume(_ viewModel: GroupVolumeViewModel) {
        stopVolumeUpdates()
        startMultiVolumeUpdates()
    }
    
    //this gets called when the user lifts his finger from the volume slider
    func groupSpeakerViewModelDidStopSettingVolume(_ viewModel: GroupVolumeViewModel) {
    }
    
    //this gets called when the request has finished on the speaker (thus the volume on the speaker is the same as our state)
    func groupSpeakerViewModelDidFinishSettingVolume(_ viewModel: GroupVolumeViewModel) {
        self.delegate?.volumeViewModelDidSetVolumeForGroup(viewModel.group, volume: viewModel.volume.value)
        stopMultiVolumeUpdates()
        startVolumeUpdates()
    }
}
