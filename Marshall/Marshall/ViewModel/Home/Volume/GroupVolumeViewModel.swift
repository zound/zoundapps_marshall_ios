//
//  VolumeSpeakerViewModel.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 15/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import RxSwift
import MinuetSDK

protocol GroupVolumeViewModelDelegate: class {
    func groupSpeakerViewModelDidStartSettingVolume(_ viewModel: GroupVolumeViewModel)
    func groupSpeakerViewModelDidStopSettingVolume(_ viewModel: GroupVolumeViewModel)
    func groupSpeakerViewModelDidFinishSettingVolume(_ viewModel: GroupVolumeViewModel)
}

class GroupVolumeViewModel {
    weak var delegate: GroupVolumeViewModelDelegate?
    
    var group: SpeakerGroup
    let volume: Variable<GroupVolume>
    let hasMaster: Variable<Bool>
    
    var initialVolume: GroupVolume
    let speakerViewModels: Variable<[SpeakerVolumeViewModel]>
    let disposeBag = DisposeBag()
    let speakerProvider: SpeakerProvider
    var updatingMasterVolume = false
    var updatingClientVolume = false
    var isSettingMasterVolume = false
    
    var isEnabled: Bool {
        get {
            return self.speakerViewModels.value.filter({ $0.isEnabled }).count == self.speakerViewModels.value.count
        }
        set {
            self.speakerViewModels.value.forEach{ $0.isEnabled = newValue }
        }
    }
    
    init(group:SpeakerGroup, groupVolume: GroupVolume, speakerProvider: SpeakerProvider) {
        self.group = group
        self.volume = Variable(groupVolume)
        self.initialVolume = groupVolume
        self.speakerViewModels = Variable([])
        self.speakerProvider = speakerProvider
        self.hasMaster = Variable(true)
        
        self.hasMaster.value = group.masterSpeaker != nil
        var speakerViewModels:[SpeakerVolumeViewModel] = []
        for speaker in group.speakers {
            var speakerVolumeViewModel: SpeakerVolumeViewModel! = nil
            if let speakerVolume = groupVolume.clientsVolumes?[speaker] {
                speakerVolumeViewModel = SpeakerVolumeViewModel(speaker: speaker, speakerVolume: speakerVolume, speakerProvider: speakerProvider, isMulti: group.isMulti)
            } else {
                let speakerVolume = SpeakerVolume(volume: nil, mute: nil, bass: nil, treble: nil)
                speakerVolumeViewModel = SpeakerVolumeViewModel(speaker: speaker,
                                                                speakerVolume:speakerVolume ,
                                                                speakerProvider: speakerProvider,
                                                                isMulti: group.isMulti)
            }
            speakerVolumeViewModel.delegate = self
            speakerViewModels.append(speakerVolumeViewModel)
        }
        
        
        if let masterIndex = speakerViewModels.index(where: { $0.speaker == group.masterSpeaker}) {
            if masterIndex != 0 {
                let masterViewModel = speakerViewModels[masterIndex]
                speakerViewModels.remove(at: masterIndex)
                speakerViewModels.insert(masterViewModel, at: 0)
            }
        }
        self.speakerViewModels.value = speakerViewModels
        self.volume.asObservable()
            .subscribe(weak: self, onNext: GroupVolumeViewModel.updateViewModelsFromGroupVolume)
            .disposed(by: disposeBag)
    }
    
    func updateViewModelsFromGroupVolume(_ groupVolume: GroupVolume) {
        guard !updatingClientVolume else { return }
        
        for viewModel in speakerViewModels.value {
            if let newSpeakerVolume = groupVolume.clientsVolumes?[viewModel.speaker] {
                if newSpeakerVolume.volume != nil {
                    viewModel.volume.value = newSpeakerVolume
                }
            }
        }
    }
    
    func startSettingMasterVolume() {
        self.delegate?.groupSpeakerViewModelDidStartSettingVolume(self)
        isSettingMasterVolume = true
    }
    
    func stopSettingMasterVolume() {
        self.delegate?.groupSpeakerViewModelDidStopSettingVolume(self)
        
        if !updatingMasterVolume {
            self.delegate?.groupSpeakerViewModelDidFinishSettingVolume(self)
        }
        
        isSettingMasterVolume = false
    }
    
    func setMasterVolume(_ normlizedVolume: Float) {
        guard let clientsVolumes = volume.value.clientsVolumes,
            let masterSpeaker = group.masterSpeaker else { return }
        
        if let masterSpeakerVolume = clientsVolumes[masterSpeaker],
            let volumeSteps = masterSpeakerVolume.volumeSteps {
            
            let volume = Double(normlizedVolume) * Double(volumeSteps - 1)
            let roundedVolume = Int(round(volume))
            
            if let groupVolumeValue = self.volume.value.masterVolume {
                if roundedVolume != groupVolumeValue {
                    self.volume.value.masterVolume = roundedVolume
                    updateMasterVolume(roundedVolume)
                }
            } else {
                self.volume.value.masterVolume = roundedVolume
                updateMasterVolume(roundedVolume)
            }
        }
    }
    
    func updateMasterVolume(_ volume: Int) {
        guard let masterSpeaker = self.group.masterSpeaker else { return }
        guard updatingMasterVolume == false else { return }
        
        updatingMasterVolume = true
        speakerProvider.setNode(ScalarNode.MultiroomMasterVolume, value: String(volume), forSpeaker: masterSpeaker)
            .subscribe(onNext: { [weak self] _ in
            guard let `self` = self else { return }
            self.updatingMasterVolume = false
            
            if let currentVolume = self.volume.value.masterVolume {
                if currentVolume != volume {
                    self.updateMasterVolume(currentVolume)
                } else {
                    if self.isSettingMasterVolume == false {
                        self.delegate?.groupSpeakerViewModelDidFinishSettingVolume(self)
                    }
                }
            } })
            .disposed(by: disposeBag)
    }
}

extension GroupVolumeViewModel: SpeakerVolumeViewModelDelegate {
    func volumeSpeakerViewModel(_ viewModel: SpeakerVolumeViewModel, didSetVolume volume: Int) {
        var volumeSum = 0
        for speakerVM in self.speakerViewModels.value {
            if let vol = speakerVM.volume.value.volume {
                volumeSum = volumeSum + vol
            }
        }
        
        let averageVolume = Int(round( Double(volumeSum) / Double(speakerViewModels.value.count)))
        self.volume.value.masterVolume = averageVolume
    }
    
    func didFinishSettingVolumeOnVolumeSpeakerViewModel(_ viewModel: SpeakerVolumeViewModel) {
        volume.value.clientsVolumes?[viewModel.speaker]? = viewModel.volume.value
        self.delegate?.groupSpeakerViewModelDidFinishSettingVolume(self)
        updatingClientVolume = false
    }
    
    func didStartSettingVolumeOnVolumeSpeakerViewModel(_ viewModel: SpeakerVolumeViewModel) {
        self.delegate?.groupSpeakerViewModelDidStartSettingVolume(self)
        updatingClientVolume = true
    }
    
    func didStopSettingVolumeOnVolumeSpeakerViewModel(_ viewModel: SpeakerVolumeViewModel) {
        self.delegate?.groupSpeakerViewModelDidStopSettingVolume(self)
    }
}
