//
//  VolumeSpeakerViewModel.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 15/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import RxSwift
import MinuetSDK

protocol SpeakerVolumeViewModelDelegate: class {
    func volumeSpeakerViewModel(_ viewModel: SpeakerVolumeViewModel, didSetVolume volume: Int)
    func didStartSettingVolumeOnVolumeSpeakerViewModel(_ viewModel: SpeakerVolumeViewModel)
    func didStopSettingVolumeOnVolumeSpeakerViewModel(_ viewModel: SpeakerVolumeViewModel)
    func didFinishSettingVolumeOnVolumeSpeakerViewModel(_ viewModel: SpeakerVolumeViewModel)
}

class SpeakerVolumeViewModel {
    weak var delegate: SpeakerVolumeViewModelDelegate?
    
    var speaker: Speaker
    let volume: Variable<SpeakerVolume>
    let isMulti: Variable<Bool>
    let isEnabledVariable: Variable<Bool>
    let speakerProvider: SpeakerProvider
    let disposeBag = DisposeBag()
    var updatingVolume = false
    var isSettingVolume = false
    var volumeOnStartSetting: Int?
    let showEqVariable: Variable<Bool> = Variable(false)
    
    var showEq: Bool {
        get { return showEqVariable.value }
        set { showEqVariable.value = newValue }
    }
    
    var isEnabled: Bool {
        get { return isEnabledVariable.value }
        set {
            isEnabledVariable.value = newValue
            if newValue == false {
                showEq = false
            }
        }
    }
    
    init(speaker:Speaker, speakerVolume: SpeakerVolume, speakerProvider: SpeakerProvider, isMulti: Bool) {
        self.speaker = speaker
        self.volume = Variable(speakerVolume)
        self.speakerProvider = speakerProvider
        self.isMulti = Variable(isMulti)
        self.isEnabledVariable = Variable(true)
    }
    
    func startSettingVolume() {
        isSettingVolume = true
        self.delegate?.didStartSettingVolumeOnVolumeSpeakerViewModel(self)
    }
    
    func stopSettingVolume() {
        self.delegate?.didStopSettingVolumeOnVolumeSpeakerViewModel(self)
        
        if !updatingVolume {
            self.delegate?.didFinishSettingVolumeOnVolumeSpeakerViewModel(self)
        }
        
        isSettingVolume = false
    }
    
    func setVolume(_ normlizedVolume: Float) {
        guard let currentVolume = self.volume.value.volume else { return }
        
        let volume = normlizedVolume * 32.0
        let roundedVolume = Int(round(volume))
        
        if roundedVolume != currentVolume {
            self.volume.value.volume = roundedVolume
            self.volume.value.mute = false
            updateVolume(roundedVolume)
            
            self.delegate?.volumeSpeakerViewModel(self, didSetVolume: roundedVolume)
        }
    }
    
    func setBass(_ normlizedBass: Float) {
        let bass = normlizedBass * 10.0
        let roundedBass = Int(round(bass))
        
        guard let currentBass = self.volume.value.bass, roundedBass != currentBass else { return }
        
        self.volume.value.bass = roundedBass
        updateBass(roundedBass)
    }
    
    func setTreble(_ normlizedTreble: Float) {
        let treble = normlizedTreble * 10.0
        let roundedTreble = Int(round(treble))
        
        guard let currentTreble = self.volume.value.treble, roundedTreble != currentTreble else { return }
        self.volume.value.treble = roundedTreble
        updateTreble(roundedTreble)
        
    }
    
    func updateBass(_ bass: Int) {
        guard updatingVolume == false else { return }
        
        updatingVolume = true
        speakerProvider.setNode(ScalarNode.EqCustomParam0, value: String(bass), forSpeaker: self.speaker)
            .subscribe(onNext: { [weak self] _ in
                guard let `self` = self else { return }
                self.updatingVolume = false
                
                if let currentBass = self.volume.value.bass  {
                    if currentBass != bass {
                        self.updateBass(currentBass)
                    } else {
                        if self.isSettingVolume == false {
                            self.delegate?.didFinishSettingVolumeOnVolumeSpeakerViewModel(self)
                        }
                    }
                }
            })
            .disposed(by: disposeBag)
    }
    
    func updateTreble(_ treble: Int) {
        guard updatingVolume == false else { return }
        
        updatingVolume = true
        speakerProvider.setNode(ScalarNode.EqCustomParam1, value: String(treble), forSpeaker: self.speaker)
            .subscribe(onNext: { [weak self] _ in
                guard let `self` = self else { return }
                self.updatingVolume = false
                
                if let currentTreble = self.volume.value.treble  {
                    if treble != currentTreble{
                        self.updateTreble(currentTreble)
                    } else {
                        if self.isSettingVolume == false {
                            self.delegate?.didFinishSettingVolumeOnVolumeSpeakerViewModel(self)
                        }
                    }
                }
            })
            .disposed(by: disposeBag)
    }
    
    func updateVolume(_ volume: Int) {
        guard updatingVolume == false else { return }
        
        updatingVolume = true
        speakerProvider.setNode(ScalarNode.Volume, value: String(volume), forSpeaker: self.speaker)
            .subscribe(onNext: { [weak self] _ in
                guard let `self` = self else { return }
                self.updatingVolume = false
                if let currentVolume = self.volume.value.volume  {
                    if currentVolume != volume{
                        self.updateVolume(currentVolume)
                    } else {
                        if self.isSettingVolume == false {
                            self.delegate?.didFinishSettingVolumeOnVolumeSpeakerViewModel(self)
                        }
                    }
                }
            })
            .disposed(by: disposeBag)
    }
}
