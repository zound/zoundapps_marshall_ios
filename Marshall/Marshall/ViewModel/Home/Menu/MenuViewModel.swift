//
//  MenuViewModel.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 10/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import RxSwift
import MinuetSDK

enum MenuItemType: String {
    case addSpeaker, shop, help, about, settings
    
    var title: String {
        switch self {
        case .addSpeaker: return Localizations.Menu.AddSpeaker
        case .shop: return Localizations.Menu.Shop
        case .help: return Localizations.Menu.Help
        case .about: return Localizations.Menu.About
        case .settings: return Localizations.Menu.SpeakerSettings
        }
    }
    
    var icon: String {
        switch self {
        case .addSpeaker: return "menu_add_speaker"
        case .shop: return "menu_shop"
        case .help: return "menu_help"
        case .about: return "menu_about"
        case .settings: return "menu_speakers"
        }
    }
}

struct MenuItem {
    var title: String { return type.title }
    var icon: String { return type.icon }
    let type: MenuItemType
    
    init(type: MenuItemType) {
        self.type = type
    }
}

class MenuViewModel {
    fileprivate var _defaultItems: [MenuItem] = []
    
    var items : Variable<[MenuItem]>
    
    let audioSystem: AudioSystem
    let disposeBag: DisposeBag = DisposeBag()
    
    init(audioSystem: AudioSystem) {
        self.audioSystem = audioSystem
        items = Variable([])
        
        self.audioSystem.groups.asObservable()
            .subscribe(weak: self, onNext: MenuViewModel.updateForCurrentGroups)
            .disposed(by: disposeBag)
    }
    
    func updateForCurrentGroups(_ groups: [SpeakerGroup]) {
        if groups.count == 0 {
            _defaultItems = [ MenuItem(type: .addSpeaker),
                              MenuItem(type: .help),
                              MenuItem(type: .shop),
                              MenuItem(type: .about) ]
        } else {
            _defaultItems = [ MenuItem(type: .addSpeaker),
                              MenuItem(type: .settings),
                              MenuItem(type: .help),
                              MenuItem(type: .shop),
                              MenuItem(type: .about) ]
        }
        
        items.value = _defaultItems
    }
}
