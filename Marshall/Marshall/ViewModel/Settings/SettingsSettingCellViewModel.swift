//
//  SettingsSettingCellViewModel.swift
//  UrbanEars
//
//  Created by Robert Sandru on 11/11/2016.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import RxSwift

enum SpeakerSetting {
    case about
    case ledIntensity
    case rename
    
    var localizedDisplayName: String {
        switch self {
        case .about: return Localizations.Settings.Speaker.About
        case .rename: return Localizations.Settings.Speaker.Rename
        case .ledIntensity: return Localizations.Settings.Speaker.LedIntensity
        }
    }
}

class SettingsSettingCellViewModel {
    var loading: Variable<Bool> = Variable(false)
    let setting: SpeakerSetting
    
    init(setting: SpeakerSetting) {
        self.setting = setting
    }
}
