//
//  SettingsEqualizerViewModel.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 17/05/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import RxSwift
import MinuetSDK

class SettingsEqualizerViewModel {
    let speaker: Speaker
    let provider: SpeakerProvider
    let eqStateVariable: Variable<EqState>
    
    let bass: Variable<Float>
    let treble: Variable<Float>
    let disposebag = DisposeBag()
    
    var eqState: EqState {
        get { return eqStateVariable.value }
        set { eqStateVariable.value = newValue }
    }
    
    init(eqState: EqState, speaker: Speaker, provider: SpeakerProvider) {
        self.speaker = speaker
        self.provider = provider
        self.eqStateVariable = Variable(eqState)
        
        let bassNormalized = normalizedValueFromForBand(eqState.bassBand, state: eqState)
        let trebleNormalized = normalizedValueFromForBand(eqState.trebleBand, state: eqState)
        
        self.bass = Variable(bassNormalized)
        self.treble = Variable(trebleNormalized)
        
        if let bassBand = eqState.bassBand {
            self.bass.asObservable()
                .sample(Observable<Int>.timer(0.0, period: 0.1, scheduler: MainScheduler.instance))
                .map { valueFromNormalizedValue($0, band: bassBand) }
                .skip(1)
                .distinctUntilChanged()
                .flatMapLatest { [weak self] bassValue -> Observable<Bool> in
                    guard let bassBand = eqState.bassBand else { return Observable.just(false) }
                    guard let `self` = self else { return Observable.just(false) }
                    
                    return self.setBand(bassBand, toValueInt: bassValue) }
                .subscribe()
                .disposed(by: disposebag)
        }
        
        if let trebleBand = eqState.trebleBand {
            self.treble.asObservable()
                .sample(Observable<Int>.timer(0.0, period: 0.1, scheduler: MainScheduler.instance))
                .map { valueFromNormalizedValue($0, band: trebleBand) }
                .skip(1)
                .distinctUntilChanged()
                .flatMapLatest { [weak self] trebleValue -> Observable<Bool> in
                    guard let trebleBand = eqState.trebleBand else { return Observable.just(false) }
                    guard let `self` = self else { return Observable.just(false) }
                    
                    return self.setBand(trebleBand, toValueInt: trebleValue) }
                .subscribe()
                .disposed(by: disposebag)
        }
    }
    
    func setBand(_ band: EqBand, toValueInt valueInt: Int) -> Observable<Bool> {
        guard let node = nodeForEqParamKey(band.key) else { return Observable.just(true) }
        
        return setCustomEqPresetIfNeededForSpeaker(speaker, provider: provider, eqState: eqStateVariable)
            .flatMapLatest { [weak self] done -> Observable<Bool> in
                guard let `self` = self else { return Observable.just(true) }
                return self.provider.setNode(node, value: String(valueInt), forSpeaker: self.speaker)
        }
        
    }
    
    func nodeForEqParamKey(_ key: Int) -> ScalarNode? {
        let nodeForBandKey = [0: ScalarNode.EqCustomParam0,
                              1: ScalarNode.EqCustomParam1]
        return nodeForBandKey[key]
    }
    
    func setTrebleToValue(_ value: Float) {
        guard let trebleBand = self.eqState.trebleBand else { return }
        guard let customPreset = self.eqState.customPreset else { return }
        
        let intValue = valueFromNormalizedValue(value, band: trebleBand)
        self.eqState.bandValues[trebleBand] = intValue
        self.eqState.currentEqPresetIndex = UInt32(customPreset.key)
        
        let floatValue = normalizedValueFromForBand(trebleBand, state: eqState)
        self.treble.value = floatValue
    }
    
    func setBassToValue(_ value: Float) {
        guard let bassBand = self.eqState.bassBand else { return }
        guard let customPreset = self.eqState.customPreset else { return }
        
        let intValue = valueFromNormalizedValue(value, band: bassBand)
        self.eqState.bandValues[bassBand] = intValue
        self.eqState.currentEqPresetIndex = UInt32(customPreset.key)
        
        let floatValue = normalizedValueFromForBand(bassBand, state: eqState)
        self.bass.value = floatValue
    }
    
    func resetEq() {
        setBassToValue(0.5)
        setTrebleToValue(0.5)
    }
}

func setCustomEqPresetIfNeededForSpeaker(_ speaker: Speaker,provider: SpeakerProvider, eqState: Variable<EqState>) -> Observable<Bool> {
    guard let customPreset = eqState.value.customPreset,
        customPreset.key != Int(eqState.value.currentEqPresetIndex) else { return Observable.just(true) }
    
    eqState.value.currentEqPresetIndex = UInt32(customPreset.key)
    return provider.setNode(ScalarNode.EqPreset, value: String(customPreset.key), forSpeaker: speaker)
}

func normalizedValueFromForBand(_ band: EqBand?, state: EqState) -> Float {
    guard let band = band, let bandValue = state.bandValues[band] else { return 0 }
    
    let interval = band.max - band.min
    let zeroBasedValue = bandValue - band.min
    let percent =  Float(zeroBasedValue) / Float(interval)
    return percent
}

func valueFromNormalizedValue(_ value: Float,band: EqBand) -> Int {
    let interval = band.max - band.min
    let zeroBasedValue = Int(value * Float(interval))
    let value = zeroBasedValue + band.min
    return value
}
