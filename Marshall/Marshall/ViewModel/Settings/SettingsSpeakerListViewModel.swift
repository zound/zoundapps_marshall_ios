//
//  SettingsSpeakerListViewModel.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 16/05/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import MinuetSDK

class SettingsSpeakerListViewModel {
    let audioSystem: AudioSystem
    var cells: Variable<[SettingsSpeakerListCell]> = Variable([])
    var disposeBag = DisposeBag()
    
    init(audioSystem: AudioSystem) {
        self.audioSystem = audioSystem
        
        self.audioSystem.speakers.asObservable()
            .subscribe(onNext: { [weak self] speakers in
                self?.buildCells() })
            .disposed(by: disposeBag)
    }
    
    
    func buildCells() {
        cells.value.removeAll()
        
        var builtCells: [SettingsSpeakerListCell] = []
        
        audioSystem.speakers.value
            .sorted() { $0.sortName.lowercased() < $1.sortName.lowercased() }
            .filter() { ExternalConfig.sharedInstance.speakerImageForColor($0.color)?.connectable ?? false }
            .forEach() { speaker in
                let speakerCell = SettingsSpeakerCell()
                speakerCell.viewModel = speaker
                builtCells.append(SettingsSpeakerListCell.speakerCell(speakerCell))
            }
        
        cells.value = builtCells
    }
}
