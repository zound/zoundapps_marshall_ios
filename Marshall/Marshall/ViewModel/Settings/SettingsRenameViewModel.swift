//
//  SettingsRenameViewModel.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 17/05/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import RxSwift
import MinuetSDK

protocol SettingsRenameViewModelDelegate: class {
    func settingsRenameViewModelDidFinishRenamingSpeaker(_ speaker: Speaker)
    func settingsRenameViewModelDidFailRenamingSpeaker(_ speaker: Speaker)
}

class SettingsRenameViewModel {
    weak var delegate: SettingsRenameViewModelDelegate?
    
    let speakerVariable:Variable<Speaker>
    let loading = Variable(false)
    let provider: SpeakerProvider
    
    var speaker: Speaker {
        get { return speakerVariable.value }
        set { speakerVariable.value = newValue }
    }
    
    let disposeBag = DisposeBag()
    
    init(speaker: Speaker, provider: SpeakerProvider) {
        self.speakerVariable = Variable(speaker)
        self.provider = provider
    }
    
    func renameSpeakerWithName(_ name: String) {
        let sanitezedFriendlyName = name.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        loading.value = true
        provider.setNode(ScalarNode.FriendlyName, value: sanitezedFriendlyName, forSpeaker: speaker)
            .subscribe(onNext: { [weak self] done in
                self?.loading.value = false
                if var newSpeaker = self?.speaker {
                    newSpeaker.friendlyName = sanitezedFriendlyName
                    self?.delegate?.settingsRenameViewModelDidFinishRenamingSpeaker(newSpeaker)
                }
                }, onError: { [weak self] error in
                    
                    self?.loading.value = false
                    self?.delegate?.settingsRenameViewModelDidFailRenamingSpeaker(self!.speaker)
            })
            .disposed(by: disposeBag)
    }
}
