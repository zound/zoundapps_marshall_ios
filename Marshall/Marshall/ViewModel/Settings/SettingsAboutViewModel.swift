
//
//  SettingsSpeakerAbout.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 17/05/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import MinuetSDK

class SettingsAboutViewModel {
    let speaker: Speaker
    let provider: SpeakerProvider
    let aboutState: AboutState
    
    let wifiSignal: BehaviorRelay<Int>
    let updateAvailable: BehaviorRelay<Bool>
    
    typealias AboutDictType = [String:String]
    let infoDict: BehaviorRelay<AboutDictType>
    
    let ip_key = "ip_key"
    let name_key = "name_key"
    let modelName_key = "modelName_key"
    let modelColor_key = "modelColor_key"
    let mac_key = "mac_key"
    let model_key = "model_key"
    let fwVersion_key = "fwVersion_key"
    let buildVersion_key = "buildVersion_key"
    let wifiName_key = "wifiName_key"
    let wifiSignal_key = "wifiSignal_key"
    let castVersion_key = "castVersion_key"
    
    let disposeBag = DisposeBag()
    
    init(aboutState: AboutState, speaker: Speaker, provider: SpeakerProvider) {
        self.aboutState = aboutState
        self.speaker = speaker
        self.provider = provider
        
        let modelName = aboutState.modelName ?? ""
        let modelColor = aboutState.colorName ?? ""
        
        let infoDictionary = [
            name_key: speaker.friendlyName,
            model_key: "Marshall \(modelName) Multi-Room \(modelColor)",
            wifiName_key: aboutState.wifiName ?? "",
            ip_key: speaker.ipAddress,
            mac_key: speaker.mac,
            fwVersion_key: aboutState.softwareVersion ?? "",
            buildVersion_key: aboutState.buildVersion ?? "",
            castVersion_key: aboutState.castVersion ?? ""
        ]
        
        self.wifiSignal = BehaviorRelay<Int>(value: aboutState.wifiSignal ?? 0)
        self.updateAvailable = BehaviorRelay<Bool>(value: false)
        self.infoDict = BehaviorRelay<AboutDictType>(value: infoDictionary)
        
        setupBindings()
    }
}

extension SettingsAboutViewModel {
    func setupBindings() {
        provider.pollNode(ScalarNode.WlanSignalStrength, atInterval: 1.5, onSpeaker: speaker)
            .map { $0 != nil ? (Int($0!) ?? 0) : 0 }
            .bind(to: wifiSignal)
            .disposed(by: disposeBag)
        
        provider.getUpdateStateForSpeaker(speaker)
            .map { $0 == .updateAvailable }
            .bind(to: updateAvailable)
            .disposed(by: disposeBag)
    }
}
