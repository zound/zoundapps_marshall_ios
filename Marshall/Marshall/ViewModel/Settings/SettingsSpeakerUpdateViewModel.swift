//
//  SettingsSpeakerUpdateViewModel.swift
//  Marshall
//
//  Created by Grzegorz Kiel on 09/01/2018.
//  Copyright © 2018 Zound Industries. All rights reserved.
//

import Foundation
import RxSwift
import MinuetSDK

import Foundation
import RxSwift
import MinuetSDK

class SettingsSpeakerUpdateViewModel {
    let speaker: Speaker
    let provider: SpeakerProvider
    let disposeBag = DisposeBag()
    
    init(speaker: Speaker, provider: SpeakerProvider) {
        self.speaker = speaker
        self.provider = provider
    }
    
    func startUpdate() {
        self.provider.setNode(ScalarNode.UpdateControl, value: "1", forSpeaker: speaker)
            .subscribe()
            .disposed(by: disposeBag)
    }
}
