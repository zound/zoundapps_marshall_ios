//
//  SettingsStreamingQualityViewModel.swift
//  UrbanEars
//
//  Created by Robert Sandru on 24/11/2016.
//  Copyright © 2016 Zound Industries. All rights reserved.
//


import Foundation
import RxSwift
import MinuetSDK

struct StreamingQualityConstants {
    static let key = "networkOptimizationSetting"
}

enum StreamingQuality {
    case high
    case normal
    
    init(optimization: Bool) {
        self = optimization ? .normal : .high
    }
    
    var optimizationEnabled: Bool {
        switch self {
        case .high: return false
        case .normal: return true
        }
    }
}

protocol SettingsStreamViewModelInputs {
    func change(quality: StreamingQuality)
}

protocol SettingsStreamViewModelOutputs {
    var streamingQuality: Variable<StreamingQuality> { get }
}

class SettingsStreamingQualityViewModel: SettingsStreamViewModelInputs, SettingsStreamViewModelOutputs {
    // MARK: Properties
    var audioSystem: AudioSystem?
    var streamingQuality: Variable<StreamingQuality>
    
    var disposeBag: DisposeBag = DisposeBag()
    
    // MARK: Init methods
    init(audioSystem: AudioSystem?) {
        self.audioSystem = audioSystem
        self.streamingQuality = Variable<StreamingQuality>(SettingsStreamingQualityViewModel.getStoredSettings())
    }
    
    func change(quality: StreamingQuality) {
        // if key is not found it will return "false" (according to Apple's documentation)
        // false for us means no optimization which is "High (Recommended)"
        streamingQuality.value = quality
        SettingsStreamingQualityViewModel.setStoredSettings(quality: quality)
        
        if let audioSystem = self.audioSystem {
            NSLog("Audio system network optimization: \(quality.optimizationEnabled)")
            audioSystem.networkOptimization(enabled: quality.optimizationEnabled)
        }
    }
}

extension SettingsStreamingQualityViewModel {
    static func getStoredSettings() -> StreamingQuality {
        guard SettingsStreamingQualityViewModel.containsConfiguration() else {
            return StreamingQuality.normal
        }
        
        let optimization = StreamingQuality(optimization: UserDefaults.standard.bool(forKey: StreamingQualityConstants.key))
        NSLog("Get network optimization setting from UserDefaults: \(optimization)")
        return optimization
    }
    
    static func setStoredSettings(quality: StreamingQuality) {
        NSLog("Store network optimization setting in UserDefaults: \(quality)")
        UserDefaults.standard.set(quality.optimizationEnabled, forKey: StreamingQualityConstants.key)
    }
    
    static func containsConfiguration() -> Bool {
        return UserDefaults.standard.object(forKey: StreamingQualityConstants.key) != nil
    }
}
