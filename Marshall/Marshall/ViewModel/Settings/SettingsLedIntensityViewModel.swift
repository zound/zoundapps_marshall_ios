//
//  SettingsLedIntensityViewModel.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 12/06/17.
//  Copyright © 2017 Zound Industries. All rights reserved.
//

import Foundation
import RxSwift
import MinuetSDK

class SettingsLedIntensityViewModel {
    let speaker: Speaker
    let provider: SpeakerProvider
    let intensityNormalizedVariable: Variable<Float>
    let disposebag = DisposeBag()
    let numberOfSteps = 255
    
    var intensity: Int {
        get {
            return Int(intensityNormalizedVariable.value * Float(numberOfSteps))
        }
        set {
            intensityNormalizedVariable.value = Float(newValue) / Float(numberOfSteps)
        }
    }
    
    init(intensity: Int, speaker: Speaker, provider: SpeakerProvider) {
        self.speaker = speaker
        self.provider = provider
        self.intensityNormalizedVariable = Variable(0)
        self.intensity = intensity
        
        let numberOfStepsLocal = numberOfSteps
        self.intensityNormalizedVariable.asObservable()
            .sample(Observable<Int>.timer(0.0, period: 0.1, scheduler: MainScheduler.instance))
            .map { Int($0 * Float(numberOfStepsLocal)) }
            .skip(1)
            .distinctUntilChanged()
            .flatMapLatest {[weak self] intensityValue -> Observable<Bool> in
                guard let `self` = self else { return Observable.just(false) }
                return self.setIntensity(toValueInt: intensityValue) }
            .subscribe()
            .disposed(by: disposebag)
    }
    
    func setIntensity(toValueInt valueInt: Int) -> Observable<Bool> {
        return self.provider.setNode(ScalarNode.LedIntensity, value: String(valueInt), forSpeaker: self.speaker)
    }
}
