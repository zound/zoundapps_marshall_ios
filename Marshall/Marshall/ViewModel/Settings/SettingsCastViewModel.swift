//
//  SettingsCastViewModel.swift
//  UrbanEars
//
//  Created by Robert Sandru on 04/10/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import RxSwift
import MinuetSDK

class SettingsCastViewModel {
    // MARK: Properties
    var shareUsageData: Variable<Bool> = Variable(true)
    var provider: SpeakerProvider
    var speaker: Speaker
    var disposeBag = DisposeBag()
    
    // MARK: Init methods
    init(shareUsageDataInitialState shareUsageData: Bool, provider: SpeakerProvider, speaker: Speaker) {
        self.shareUsageData.value = shareUsageData
        self.provider = provider
        self.speaker = speaker
    }
    
    func updateShareUsageDataNode(toState state: Bool) {
        let nodeValue = String(state.hashValue)
        
        self.shareUsageData.value = state
        provider.setNode(ScalarNode.CastUsageReport, value: nodeValue, forSpeaker: speaker)
            .subscribe(onError: { [weak self] error in
                self?.shareUsageData.value = !state
            })
            .disposed(by: disposeBag)
    }
}
