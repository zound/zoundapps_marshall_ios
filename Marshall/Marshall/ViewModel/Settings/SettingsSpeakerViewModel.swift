//
//  SettingsSpeakerViewModel.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 16/05/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import RxSwift
import MinuetSDK

class SettingsSpeakerViewModel {
    let speakerProvider: SpeakerProvider
    let speakerVariable: Variable<Speaker>
    
    var speaker: Speaker {
        get { return speakerVariable.value }
        set { speakerVariable.value = newValue }
    }
    
    init(speaker: Speaker, provider: SpeakerProvider) {
        self.speakerVariable = Variable(speaker)
        self.speakerProvider = provider
    }
}
