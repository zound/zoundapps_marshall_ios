//
//  SettingsLedIntensityViewModel.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 12/06/17.
//  Copyright © 2017 Zound Industries. All rights reserved.
//

import Foundation
import RxSwift
import MinuetSDK

class SettingsSoundsViewModel {
    let speaker: Speaker
    let provider: SpeakerProvider
    let disposebag = DisposeBag()
    
    init(speaker: Speaker, provider: SpeakerProvider) {
        self.speaker = speaker
        self.provider = provider
    }
}
