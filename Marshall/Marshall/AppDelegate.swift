//
//  AppDelegate.swift
//  Marshall
//
//  Created by Raul Andrisan on 04/05/2017.
//  Copyright © 2017 Zound Industries. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics
import MinuetSDK

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    var appCoordinator: AppCoordinator!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        Crashlytics.sharedInstance().delegate = self
        Fabric.with([Crashlytics.self])
        
        var performAdditionalHandling = true
        
        if let window = self.window {
            //window.layer.speed = 0.2
            let rootViewController:RootViewController = window.rootViewController as! RootViewController
            
            appCoordinator = AppCoordinator(rootViewControllerController: rootViewController)
            window.makeKeyAndVisible()
            appCoordinator.start()
            
            if let shortcutItem = launchOptions?[UIApplication.LaunchOptionsKey.shortcutItem] as? UIApplicationShortcutItem {
                appCoordinator.showSpeakerFromHomeScreenQuickActionItem(shortcutItem)
                performAdditionalHandling = false
            }
        }
        
        return performAdditionalHandling
    }
    
    func application(_ application: UIApplication, performActionFor shortcutItem: UIApplicationShortcutItem, completionHandler: @escaping (Bool) -> Void) {
        if let appCoordinator = self.appCoordinator {
            appCoordinator.showSpeakerFromHomeScreenQuickActionItem(shortcutItem)
        }
        
        completionHandler(true)
    }
    
    func application(_ application: UIApplication, handleOpen url: URL) -> Bool {
        SpotifyLogin.handleOpenURL(url)
        appCoordinator.handleNavigationFromURL(url: url)
        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
        if let sourceApplication = options[.sourceApplication] as? String {
            SpotifyLogin.handleOpenURL(url, from: sourceApplication)
        }
        
        appCoordinator.handleNavigationFromURL(url: url)
        return true
    }
    
    func applicationDidReceiveMemoryWarning(_ application: UIApplication) {
        CLSLogv("applicationDidReceiveMemoryWarning", getVaList([]))
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}

extension AppDelegate: CrashlyticsDelegate {
    func crashlyticsDidDetectReport(forLastExecution report: CLSReport, completionHandler: @escaping (Bool) -> Void) {
        guard report.isCrash else { return }
        
        //if the crashed was caused intentionally in setup then we don't send it to crashlyitics as it will f%*k up our stats :)
        let didCrashIntentionally = UserDefaults.standard.bool(forKey: "DidCrashIntentionally")
        UserDefaults.standard.removeObject(forKey: "DidCrashIntentionally")
        if didCrashIntentionally {
            //we do log it as an event
            Answers.logCustomEvent(withName: "ClosedAppAfterWACFailure", customAttributes: nil)
        }
        completionHandler(!didCrashIntentionally)
    }
}
