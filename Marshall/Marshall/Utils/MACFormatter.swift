//
//  MACFormatter.swift
//  UrbanEars
//
//  Created by Dolewski Bartosz A (Ext) on 30.04.2018.
//  Copyright © 2018 Zound Industries. All rights reserved.
//

import Foundation

extension String {
    func formatToMAC() -> String {
        let groupSize = 2
        let separator = ":"
        
        if self.count <= groupSize {
            return self
        }
        
        let splitIndex = index(startIndex, offsetBy: groupSize)
        return String(self[..<splitIndex]) + separator + String(self[splitIndex...]).formatToMAC()
    }
}

