//
//  UIStoryboard+Controllers.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 24/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//
import UIKit

extension UIStoryboard {
    static var main: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: nil)
    }
}

extension UIStoryboard {
    var homeViewController: HomeViewController {
        guard let vc = UIStoryboard.main.instantiateViewController(withIdentifier: "main") as? HomeViewController else {
            fatalError("HomeViewController couldn't be found in Storyboard file")
        }
        return vc
    }
    
    var menuViewController: MenuViewController {
        guard let vc = UIStoryboard.main.instantiateViewController(withIdentifier: "menu") as? MenuViewController else {
            fatalError("MenuViewController couldn't be found in Storyboard file")
        }
        return vc
    }
    
    var aboutViewController: AboutViewController {
        guard let vc = UIStoryboard.main.instantiateViewController(withIdentifier: "about") as? AboutViewController else {
            fatalError("AboutViewController couldn't be found in Storyboard file")
        }
        return vc
    }
    
    var volumeViewController: VolumeViewController {
        guard let vc = UIStoryboard.main.instantiateViewController(withIdentifier: "volume") as? VolumeViewController else {
            fatalError("VolumeViewController couldn't be found in Storyboard file")
        }
        return vc
    }
    
    var welcomeViewController: WelcomeViewController {
        guard let vc = UIStoryboard.main.instantiateViewController(withIdentifier: "welcome") as? WelcomeViewController else {
            fatalError("WelcomeViewController couldn't be found in Storyboard file")
        }
        return vc
    }
    
    var unlockedAlertViewController : UnlockedAlertViewController {
        guard let vc = UIStoryboard.main.instantiateViewController(withIdentifier: "unlockedSpeakerInfoAlert") as? UnlockedAlertViewController else {
            fatalError("WelcomeViewController couldn't be found in Storyboard file")
        }
        return vc
    }

    var updateAvailableAlertViewController : UpdateAvailableAlertViewController {
        guard let vc = UIStoryboard.main.instantiateViewController(withIdentifier: "updateAvailableInfoAlert") as? UpdateAvailableAlertViewController else {
            fatalError("UpdateAvailableAlertViewController couldn't be found in Storyboard file")
        }
        return vc
    }

    var loadingViewController: LoadingViewController {
        guard let vc = UIStoryboard.main.instantiateViewController(withIdentifier: "loading") as? LoadingViewController else {
            fatalError("LoadingViewController couldn't be found in Storyboard file")
        }
        return vc
    }
    
    var reconnectingViewController: ReconnectingViewController {
        guard let vc = UIStoryboard.main.instantiateViewController(withIdentifier: "reconnecting") as? ReconnectingViewController else {
            fatalError("ReconnectingViewController couldn't be found in Storyboard file")
        }
        return vc
    }
    
    var noWiFiViewController: NoWiFiViewController {
        guard let vc = UIStoryboard.main.instantiateViewController(withIdentifier: "noWifi") as? NoWiFiViewController else {
            fatalError("LoadingViewController couldn't be found in Storyboard file")
        }
        return vc
    }
    
    var noSpeakersViewController: NoSpeakersViewController {
        guard let vc = UIStoryboard.main.instantiateViewController(withIdentifier: "noSpeakers") as? NoSpeakersViewController else {
            fatalError("NoSpeakersViewController couldn't be found in Storyboard file")
        }
        return vc
    }
    
    var castSwitchAlertViewController: CastSwitchAlertViewController {
        guard let vc = UIStoryboard.main.instantiateViewController(withIdentifier: "castSwitchAlert") as? CastSwitchAlertViewController else {
            fatalError("CastSwitchAlertViewController couldn't be found in Storyboard file")
        }
        return vc
    }
    
    var groupFullInfoAlertViewController: GroupFullInfoAlertViewController {
        guard let vc = UIStoryboard.main.instantiateViewController(withIdentifier: "groupFullInfoAlert") as? GroupFullInfoAlertViewController else {
            fatalError("GroupFullInfoAlertViewController couldn't be found in Storyboard file")
        }
        return vc
    }
    
    var miniVolumeViewController: MiniVolumeViewController {
        guard let vc = UIStoryboard.main.instantiateViewController(withIdentifier: "miniVolume") as? MiniVolumeViewController else {
            fatalError("MiniVolumeViewController couldn't be found in Storyboard file")
        }
        return vc
    }
    
    
    var eulaViewController: EULAViewController {
        guard let vc = UIStoryboard.main.instantiateViewController(withIdentifier: "eula") as? EULAViewController else {
            fatalError("EULAViewController couldn't be found in Storyboard file")
        }
        return vc
    }
    
    var fossViewController: FOSSViewController {
        guard let vc = UIStoryboard.main.instantiateViewController(withIdentifier: "foss") as? FOSSViewController else {
            fatalError("FOSSViewController couldn't be found in Storyboard file")
        }
        return vc
    }
    
    var contactViewController: ContactViewController {
        guard let vc = UIStoryboard.main.instantiateViewController(withIdentifier: "contact") as? ContactViewController else {
            fatalError("ContactViewController couldn't be found in Storyboard file")
        }
        return vc
    }
    
    var onlineManualViewController: OnlineManualViewController {
        guard let vc = UIStoryboard.main.instantiateViewController(withIdentifier: "onlineManual") as? OnlineManualViewController else {
            fatalError("OnlineManualViewController couldn't be found in Storyboard file")
        }
        return vc
    }
}
