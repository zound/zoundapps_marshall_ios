//
//  GroupFullInfoAlertViewController.swift
//  UrbanEars
//
//  Created by Robert Sandru on 07/09/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import ZoundCommon

protocol GroupFullInfoAlertViewControllerDelegate: class {
    func groupFullInfoAlertDidRequestDismiss(_ alertViewController: GroupFullInfoAlertViewController)
}

class GroupFullInfoAlertViewController: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var dismissButton: UIButton!
    
    weak var delegate: GroupFullInfoAlertViewControllerDelegate?
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.text = Localizations.Appwide.Note
        
        titleLabel.font = Fonts.UrbanEars.Bold(23)
        
        contentLabel.text = Localizations.GroupFull.Content
        contentLabel.font = Fonts.MainContentFont
        
        UIView.setAnimationsEnabled(false)
        
        dismissButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.Appwide.Back), for: .normal)
        
        dismissButton.layoutIfNeeded()
        UIView.setAnimationsEnabled(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onDismiss(_ sender: AnyObject) {
        delegate?.groupFullInfoAlertDidRequestDismiss(self)
    }
}















