//
//  LoadingViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 26/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit

class LoadingViewController: UIViewController {
    @IBOutlet weak var loadingIndicatorImage: UIImageView!
    @IBOutlet weak var loadingLabel: UILabel!
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadingLabel.text = Localizations.Loading.LoadingSpeakers
        loadingLabel.font = Fonts.UrbanEars.Regular(18)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadingIndicatorImage.rotate()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        loadingIndicatorImage.stopRotation()
    }
}
