//
//  MainViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 09/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit
import Cartography
import RxSwift

protocol HomeViewControllerDelegate: class {
    func homeViewControllerDidRequestMenuToggle(_ homeViewController:HomeViewController)
    func homeViewControllerDidRequestVolumeControls(_ homeViewController:HomeViewController)
    func homeViewControllerDidRequestRefresh(_ homeViewController:HomeViewController)
    func homeViewController(_ homeViewController: HomeViewController, didSelectSpeakerViewModel:HomeSpeakerViewModel, fromGroupViewModel: HomeGroupViewModel)
    func homeViewControllerDidRequestSetup(_ homeViewController: HomeViewController, didSelectSpeakerViewModel:HomeSpeakerViewModel, fromGroupViewModel: HomeGroupViewModel)
    func homeViewControllerDidRequestFactoryReset(_ homeViewController: HomeViewController, didSelectSpeakerViewModel:HomeSpeakerViewModel, fromGroupViewModel: HomeGroupViewModel)
    func homeViewController(_ homeViewController: HomeViewController, didRequestChangeMultiTo: Bool, inSpeakerViewModel:HomeSpeakerViewModel, fromGroupViewModel: HomeGroupViewModel)
    func homeViewControllerDidRequestSettings(_ homeViewController: HomeViewController,didSelectSpeakerViewModel:HomeSpeakerViewModel, fromGroupViewModel: HomeGroupViewModel)
}

class HomeViewController: UIViewController {
    @IBOutlet weak var rootContainerView: UIView!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var hamburgerCloseButton: UIButton!
    @IBOutlet weak var titleImageView: UIImageView!
    @IBOutlet weak var separatorHeight: NSLayoutConstraint!
    
    weak var delegate: HomeViewControllerDelegate?
    
    var multiSectionHeaderView: HomeSectionHeaderView!
    var soloSectionHeaderView: HomeSectionHeaderView!
    var viewModel:HomeViewModel!
    var refresher : HomePullToRefresh!
    var footerHeight:CGFloat = 60
    let footerLineHeight:CGFloat = 1.5
    var footerLineMargin:CGFloat = 26
    var footerContainerView: UIView!
    var footerLineView: UIView!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder:aDecoder)
    }
    
    deinit {
        self.tableView.removeAllPullToRefresh()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {  return .lightContent }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        refresher = HomePullToRefresh()
        self.tableView.addPullToRefresh(refresher, action: { [weak self] in
            
            self?.delegate?.homeViewControllerDidRequestRefresh(self!)
        })
        
        multiSectionHeaderView = HomeSectionHeaderView(frame: CGRect(x: 0,y: 0,width: 320,height: 44))
        soloSectionHeaderView = HomeSectionHeaderView(frame: CGRect(x: 0,y: 0,width: 320,height: 44))
        
        if UIScreen.main.bounds.size.width < 375 {
            rootContainerView.layoutMargins = UIEdgeInsets(top: 20.0, left: 18.0, bottom: 0, right: 18.0)
            footerLineMargin = 18.0
        } else {
            rootContainerView.layoutMargins = UIEdgeInsets(top: 20.0, left: 30.0, bottom: 0, right: 30.0)
            footerLineMargin = 26.0
        }
        
        separatorHeight.constant = 0.5
        tableView.delegate = self
        tableView.dataSource = self
        tableView.contentInset = UIEdgeInsets(top:40, left:0, bottom:0, right:0)
        
        updateSeparatorViewVisibility()
        
        footerHeight = HomeSpeakerCell.verticalMargin * 2 + 30
        
        let separator = UIView()
        
        separator.backgroundColor = UIColor(white: 0.0, alpha: 1.0)
        footerLineView = separator
        let containerView = UIView(frame: CGRect(x: 0,y: 0,width: tableView.bounds.width,height: footerHeight))
        containerView.addSubview(separator)
        separator.translatesAutoresizingMaskIntoConstraints = false
        constrain(separator) { view in
            view.width == view.superview!.width - footerLineMargin * 2 ~ UILayoutPriority(999)
            view.height == footerLineHeight
            view.width <= 600
            view.center == view.superview!.center
        }
        
        containerView.backgroundColor = UIColor.clear
        footerContainerView = containerView
        
        if let vm = viewModel {
            let hasMulti =  vm.multiViewModels.asObservable().map{ $0.count > 0 }
            let hasSolo = vm.soloViewModels.asObservable().map{ $0.count > 0 }
            
            self.updateFooterForHasMultiAndSolo(vm.multiViewModels.value.count > 0 && vm.soloViewModels.value.count > 0, animated: false)
            
            Observable.combineLatest(hasMulti, hasSolo) { $0 && $1 }
                .skip(1)
                .subscribe(weak: self, onNext: HomeViewController.updateFooterForHasMultiAndSolo)
                .disposed(by: rx_disposeBag)
            
            vm.delegate = self
        }
    }
    
    func updateSeparatorViewVisibility() {
        separatorView.alpha = max(0,min(1.0,tableView.contentOffset.y/22.0))
    }
    
    func updateFooterForHasMultiAndSolo(_ hasMultiAndSolo: Bool) {
        updateFooterForHasMultiAndSolo(hasMultiAndSolo, animated: true)
    }
    
    func updateFooterForHasMultiAndSolo(_ hasMultiAndSolo: Bool, animated: Bool) {
        UIView.setAlphaOfView(self.footerLineView, visible: hasMultiAndSolo, animated: animated)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.updateSeparatorViewVisibility()
    }
    
    @IBAction func onHamburgerMenuPressed(_ sender: AnyObject) {
        delegate?.homeViewControllerDidRequestMenuToggle(self)
    }
    
    @IBAction func onVolumePressed(_ sender: AnyObject) {
        delegate?.homeViewControllerDidRequestVolumeControls(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension HomeViewController: HomeViewModelDelegate {
    func homeViewModelWillMakeGroupChanges() {}
    
    func homeViewModelDidMakeGroupChanges() {
        self.tableView.beginUpdates()
        self.tableView.endUpdates()
    }
    
    func totalSpeakers() -> Int {
        var sum = self.viewModel.soloViewModels.value.count
        
        for mvm in self.viewModel.multiViewModels.value {
            sum = sum + mvm.speakersViewModels.value.count
        }
        
        return sum
    }
    
    func homeViewModelDidAddSoloGroup(atIndex: Int, fromMove: Bool) {
        let indexPath = IndexPath(row: atIndex, section: 1)
        self.tableView.insertRows(at: [indexPath], with: fromMove ? .left : .fade)
    }
    
    func homeViewModelDidRemoveSoloGroup(fromIndex: Int, fromMove: Bool) {
        let indexPath = IndexPath(row: fromIndex, section: 1)
        self.tableView.deleteRows(at: [indexPath], with: fromMove ? .left : .fade)
    }
    
    func homeViewModelDidAddMultiGroup(atIndex: Int, fromMove: Bool) {
        let indexPath = IndexPath(row: atIndex, section: 0)
        self.tableView.insertRows(at: [indexPath], with: fromMove ? .left : .fade)
    }
    
    func homeViewModelDidRemoveMultiGroup(fromIndex: Int, fromMove: Bool) {
        let indexPath = IndexPath(row: fromIndex, section: 0)
        self.tableView.deleteRows(at: [indexPath], with: fromMove ? .left : .fade)
    }
}

extension HomeViewController: HomeGroupCellDelegate {
    func homeGroupCellDidRequestSettingsForSpeakerViewModel(_ speakerViewModel: HomeSpeakerViewModel, fromGroupViewModel: HomeGroupViewModel) {
        self.delegate?.homeViewControllerDidRequestSettings(self, didSelectSpeakerViewModel: speakerViewModel, fromGroupViewModel:fromGroupViewModel)
    }
    
    func homeGroupCellDidRequestSetupForSpeakerViewModel(_ speakerViewModel: HomeSpeakerViewModel, fromGroupViewModel groupViewModel: HomeGroupViewModel) {
        self.delegate?.homeViewControllerDidRequestSetup(self, didSelectSpeakerViewModel: speakerViewModel, fromGroupViewModel: groupViewModel)
    }
    
    func homeGroupCellDidSelectSpeakerViewModel(_ speakerViewModel: HomeSpeakerViewModel, fromGroupViewModel groupViewModel: HomeGroupViewModel) {
        self.delegate?.homeViewController(self, didSelectSpeakerViewModel: speakerViewModel, fromGroupViewModel: groupViewModel)
    }
    
    func homeGroupCellDidRequestChangeMultiTo(_ multi: Bool, speakerViewModel: HomeSpeakerViewModel, fromGroupViewModel: HomeGroupViewModel) {
        self.delegate?.homeViewController(self, didRequestChangeMultiTo: multi, inSpeakerViewModel: speakerViewModel, fromGroupViewModel: fromGroupViewModel)
    }
    
    func homeGroupCellDidRequestFactoryResetForSpeakerViewModel(_ speakerViewModel: HomeSpeakerViewModel, fromGroupViewModel groupViewModel: HomeGroupViewModel) {
        self.delegate?.homeViewControllerDidRequestFactoryReset(self, didSelectSpeakerViewModel: speakerViewModel, fromGroupViewModel: groupViewModel)
    }
}

extension HomeViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
}

extension HomeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let vm = viewModel {
            if (indexPath as NSIndexPath).section == 0 {
                if let speakerCount = vm.multiViewModels.value.first?.group.speakers.count {
                    return CGFloat(speakerCount)*HomeSpeakerCell.cellHeight
                } else {
                    return 0
                }
            }
            if (indexPath as NSIndexPath).section == 1 {
                return HomeSpeakerCell.cellHeight
            }
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let vm = viewModel {
            switch section {
            case 0: return vm.multiViewModels.value.count
            case 1: return vm.soloViewModels.value.count
            default: break
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "homeGroupItem") as? HomeGroupCell else { return UITableViewCell() }
        
        if let vm = viewModel {
            switch (indexPath as NSIndexPath).section {
            case 0: cell.viewModel = vm.multiViewModels.value[indexPath.row]
            case 1: cell.viewModel = vm.soloViewModels.value[indexPath.row]
            default: break
            }
            //cell.viewModel.value?.delegate = cell
        }
        
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == 0 {
            return footerContainerView
        }
        
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section ==  0 {
            if let vm = viewModel {
                if vm.multiViewModels.value.count > 0 && vm.soloViewModels.value.count > 0 {
                    return footerHeight
                }
            }
        }
        
        return CGFloat.leastNormalMagnitude
    }
}
