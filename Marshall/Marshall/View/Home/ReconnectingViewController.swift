//
//  ReconnectingViewController.swift
//  Marshall
//
//  Created by Raul Andrisan on 26/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit
import MinuetSDK

class ReconnectingViewController: UIViewController {
    @IBOutlet weak var loadingIndicatorImage: UIImageView!
    @IBOutlet weak var loadingLabel: UILabel!
    
    var speaker: Speaker?
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let speaker = speaker {
            loadingLabel.text = Localizations.Reconnecting.ReconnectingTo(speaker.friendlyName)
        }

        loadingLabel.font = Fonts.UrbanEars.Regular(18)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadingIndicatorImage.rotate()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        loadingIndicatorImage.stopRotation()
    }
}
