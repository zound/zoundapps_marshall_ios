//
//  UpdateAvailableAlertViewController.swift
//  Marshall
//
//  Created by Grzegorz Kiel on 09/01/2018.
//  Copyright © 2018 Zound Industries. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import ZoundCommon

protocol UpdateAvailableAlertViewControllerDelegate : class {
    func didAcknowledgeUpdateAvailabilityWarning(_ alertViewController : UpdateAvailableAlertViewController)
}

class UpdateAvailableAlertViewController: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var continueButton: UIButton!
    
    weak var delegate: UpdateAvailableAlertViewControllerDelegate?
    
    var dontShow = false
    var speakerViewModel: HomeSpeakerViewModel?
    var groupViewModel: HomeGroupViewModel?
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.text = Localizations.Appwide.Note
        titleLabel.font = Fonts.UrbanEars.Medium(23)
        
        contentLabel.text = Localizations.UpdateavailableModule.Content((speakerViewModel?.speaker.friendlyName)!)
        contentLabel.font = Fonts.MainContentFont
        
        UIView.setAnimationsEnabled(false)
        
        continueButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.Appwide._Continue), for: .normal)
        
        continueButton.layoutIfNeeded()
        UIView.setAnimationsEnabled(true)
    }
    
    @IBAction func onContinue(_ sender: AnyObject) {
        self.delegate?.didAcknowledgeUpdateAvailabilityWarning(self)
    }
}
