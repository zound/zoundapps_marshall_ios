//
//  NoSpeakersViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 26/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit

protocol NoSpeakersViewControllerDelegate: class {
    func noSpeakersViewControllerDidRequestSetup(_ noSpeakersViewController: NoSpeakersViewController)
    func noSpeakersViewControllerDidRequestRefresh(_ noSpeakersViewController: NoSpeakersViewController)
    func noSpeakersViewControllerDidRequestMenu(_ noSpeakersViewController: NoSpeakersViewController)
}

class NoSpeakersViewController: UIViewController {
    @IBOutlet weak var messageTitleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var gotoSetupButton: UIButton!
    @IBOutlet weak var refreshButton: UIButton!
    
    weak var delegate: NoSpeakersViewControllerDelegate?
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        messageTitleLabel.attributedText = Fonts.UrbanEars.Bold(23).AttributedTextWithString(Localizations.NoSpeakers.Title, color: UIColor.white, letterSpacing: 1.12)
        
        messageLabel.text = Localizations.NoSpeakers.Description
        messageLabel.font = Fonts.MainContentFont
        
        UIView.setAnimationsEnabled(false)
        
        gotoSetupButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.NoSpeakers.Buttons.SetUpSpeakers), for: .normal)
        refreshButton.setAttributedTitle(Fonts.SecondaryButtonFont.AttributedSecondaryButtonWithString(Localizations.NoSpeakers.Buttons.Refresh), for: .normal)
        
        gotoSetupButton.layoutIfNeeded()
        refreshButton.layoutIfNeeded()
        UIView.setAnimationsEnabled(true)
    }
    
    @IBAction func onMenu(_ sender: AnyObject) {
        self.delegate?.noSpeakersViewControllerDidRequestMenu(self)
    }
    
    @IBAction func onGotoSetup(_ sender: AnyObject) {
        self.delegate?.noSpeakersViewControllerDidRequestSetup(self)
    }
    
    @IBAction func onRefresh(_ sender: AnyObject) {
        self.delegate?.noSpeakersViewControllerDidRequestRefresh(self)
    }
}
