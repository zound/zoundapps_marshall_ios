//
//  RefreshAnimator.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 03/03/2017.
//  Copyright © 2017 Zound Industries. All rights reserved.
//

import Foundation
import CoreGraphics
import UIKit

class RefreshAnimator:RefreshViewAnimator {
    
    private let refreshView: RefreshView
    
    init(refreshView: RefreshView) {
        self.refreshView = refreshView
    }
    
    func animate(_ state: State) {
        
        switch state {
        case .initial:
            applyTransformFoProgress(progress: 0)
        case .releasing(let progress):
            applyTransformFoProgress(progress: progress)
        case .loading:
            //refreshView.spinnerView.transform = CGAffineTransform.identity
            refreshView.spinnerView.rotate(1.0)
        case .finished:
            refreshView.spinnerView.stopRotation()
            UIView.animate(withDuration: 0.25, delay: 0.0, options: [.beginFromCurrentState], animations: { [weak self] in
                self?.refreshView.spinnerView.alpha = 0.0
                }, completion: nil)
        }
    }
    
    func applyTransformFoProgress(progress: CGFloat) {
        
        self.refreshView.spinnerView.alpha = 1.0
        var transform  = CGAffineTransform.identity
        //let translateY = (self.refreshView.bounds.size.height/2)*(1.0-min(1.0,progress))
        //transform = transform.translatedBy(x: 0, y: translateY)
        transform = transform.rotated(by: (progress * 2 * CGFloat.pi))
        transform = transform.scaledBy(x: min(1.0,progress), y: min(1.0,progress))
        
        refreshView.spinnerView.transform = transform
    }
}
