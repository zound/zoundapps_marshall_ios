//
//  RefreshView.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 03/03/2017.
//  Copyright © 2017 Zound Industries. All rights reserved.
//

import Foundation
import UIKit

class RefreshView: UIView {
    
    @IBOutlet weak var spinnerView: UIImageView!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.backgroundColor = UIColor.clear
    }
}
