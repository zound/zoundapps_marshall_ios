//
//  UnlockedAlertViewController.swift
//  Marshall
//
//  Created by Claudiu Alin Luminosu on 16/11/2017.
//  Copyright © 2017 Zound Industries. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import ZoundCommon

protocol UnlockedViewControllerDelegate : class {
    func didAcknowledgeUnlockedModuleWarning(_ alertViewController : UnlockedAlertViewController)
}

class UnlockedAlertViewController: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var continueButton: UIButton!
    
    weak var delegate: UnlockedViewControllerDelegate?
    
    var dontShow = false
    var speakerViewModel: HomeSpeakerViewModel?
    var groupViewModel: HomeGroupViewModel?
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.text = Localizations.Appwide.Note
        titleLabel.font = Fonts.UrbanEars.Medium(23)
        
        contentLabel.text = Localizations.UnlockedModule.Content((speakerViewModel?.speaker.friendlyName)!)
        contentLabel.font = Fonts.MainContentFont
        
        UIView.setAnimationsEnabled(false)
        
        continueButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.Appwide._Continue), for: .normal)
        
        continueButton.layoutIfNeeded()
        UIView.setAnimationsEnabled(true)
    }
    
    @IBAction func onContinue(_ sender: AnyObject) {
        self.delegate?.didAcknowledgeUnlockedModuleWarning(self)
    }
}

