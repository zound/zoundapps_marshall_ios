//
//  MenuViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 10/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit
import RxSwift

protocol MenuViewControllerDelegate: class {
    func didSelectMenuItem(_ menuItem:MenuItem,menuViewController:MenuViewController)
    func didCloseMenu(menuViewController: MenuViewController)
}

class MenuViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var viewModel: MenuViewModel!
    weak var delegate: MenuViewControllerDelegate?
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.separatorStyle = .none
        tableView.rx
            .setDelegate(self)
            .disposed(by: rx_disposeBag)
        
        if let vm = viewModel {
            vm.items.asObservable()
                .bind(to: tableView.rx.items(cellIdentifier:"menuItem", cellType: MenuTableViewCell.self)) { (row, element, cell) in
                    cell.menuItemLabel?.attributedText = Fonts.ListItemFont.AttributedTextWithString(element.title.capitalized,
                                                                                                     color: UIColor(red: 216.0/255.0, green: 216.0/255.0, blue: 216.0/255.0, alpha: 1.0),
                                                                                                     letterSpacing: 0,
                                                                                                     lineSpacing: 0)
                    cell.menuItemImageView?.image = UIImage(named: element.icon)
                    cell.menuItemImageView.alpha = 1.0 }
                .disposed(by: rx_disposeBag)
        }
    }
    
    @IBAction func onClose(_ sender: AnyObject) {
        delegate?.didCloseMenu(menuViewController: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension MenuViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let menuItem = viewModel.items.value[indexPath.row]
        delegate?.didSelectMenuItem(menuItem, menuViewController: self)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    private func tableView(_ tableView: UITableView, editingStyleForRowAt editingStyleForRowAtIndexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return UITableViewCell.EditingStyle.none
    }
}
