//
//  MultiVolumeHeaderReusableView.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 08/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import ZoundCommon

class MultiVolumeHeaderReusableView: UICollectionReusableView {
    @IBOutlet weak var masterVolumeSlider: AnimatedSlider!
    @IBOutlet weak var sliderLabel: UILabel!
    
    let sliderThumbImage = UIImage(named: "master_slider_thumb")
    let trackMinImage = UIImage(named:"slider_track_min")
    let trackMaxImage = UIImage(named:"slider_track_max")
    
    let sliderThumbImageDisabled = UIImage(named: "master_slider_thumb_disabled")
    let trackMinImageDisabled = UIImage(named:"slider_track_min_disabled")
    let trackMaxImageDisabled = UIImage(named:"slider_track_max_disabled")
    
    let groupViewModelObservable: Variable<GroupVolumeViewModel?> = Variable(nil)
    var groupViewModel: GroupVolumeViewModel? {
        get { return groupViewModelObservable.value }
        set { groupViewModelObservable.value = newValue }
    }
    
    override func awakeFromNib() {
        sliderLabel.text = Localizations.Volume.Headers.MasterVolumeLabel
        sliderLabel.font = Fonts.UrbanEars.Regular(17)
        
        masterVolumeSlider.setThumbImage(sliderThumbImage, for: .normal)
        masterVolumeSlider.setMinimumTrackImage(trackMinImage, for: .normal)
        masterVolumeSlider.setMaximumTrackImage(trackMaxImage, for: .normal)
        
        masterVolumeSlider.addTarget(self, action: #selector(onVolumeTouchDown), for: .touchDown)
        masterVolumeSlider.addTarget(self, action: #selector(onVolumeTouchUp), for: .touchUpInside)
        masterVolumeSlider.addTarget(self, action: #selector(onVolumeTouchUp), for: .touchUpOutside)
        
        let masterVolume = groupViewModelObservable.asObservable()
            .flatMapLatest { viewModel -> Observable<Float> in
                guard let `viewModel` = viewModel else { return Observable.just(0) }
                return viewModel.volume.asObservable()
                    .filter({ $0.masterVolume != nil })
                    .map { $0.masterVolume}
                    .unwrapOptional()
                    .map { masterVolume in Float(Float(masterVolume) / 32.0)
                }
        }
        
        groupViewModelObservable.asObservable()
            .unwrapOptional()
            .flatMapLatest { $0.volume.asObservable() }
            .filter({ $0.masterVolume != nil })
            .map { $0.isMute }
            .subscribe(onNext: {[weak self] mute in
                self?.masterVolumeSlider.setThumbImage(!mute ? self?.sliderThumbImage : self?.sliderThumbImageDisabled , for: .normal)
                self?.masterVolumeSlider.setMinimumTrackImage(!mute ? self?.trackMinImage : self?.trackMinImageDisabled, for: .normal)
                self?.masterVolumeSlider.setMaximumTrackImage(!mute ? self?.trackMaxImage : self?.trackMaxImageDisabled, for: .normal) })
            .disposed(by: rx_disposeBag)
        
        masterVolumeSlider.willSetValueFromUserInteraction = true
        masterVolume
            .subscribe(onNext:{ [weak self] masterVolumeNormalized in
                if self?.masterVolumeSlider.willSetValueFromUserInteraction == false {
                    self?.masterVolumeSlider.setValue(masterVolumeNormalized, animated: true)
                }
            })
            .disposed(by: rx_disposeBag)
        
        masterVolumeSlider.willSetValueFromUserInteraction = false
        
        groupViewModelObservable.asObservable()
            .flatMapLatest { viewModel -> Observable<Bool> in
                guard let `viewModel` = viewModel else { return Observable.just(false) }
                return viewModel.hasMaster.asObservable() }
            .subscribe(weak: self, onNext: MultiVolumeHeaderReusableView.updateForMultiSliderEnabled)
            .disposed(by: rx_disposeBag)
    }
    
    func updateForMultiSliderEnabled(_ isEnabled:Bool) {
        UIView.animate(withDuration: 0.25, delay: 0, options: [.beginFromCurrentState], animations: {[weak self] in
            self?.masterVolumeSlider.alpha = isEnabled ? 1.0 : 0.5
            }, completion: nil)
        
        self.masterVolumeSlider.isUserInteractionEnabled = isEnabled
    }
    
    @IBAction func onVolumeTouchUp(_ sender: AnyObject) {
        self.masterVolumeSlider.willSetValueFromUserInteraction = false
        guard let vm = groupViewModel else { return }
        
        vm.stopSettingMasterVolume()
    }
    
    @IBAction func onVolumeTouchDown(_ sender: AnyObject) {
        self.masterVolumeSlider.willSetValueFromUserInteraction = true
    
        guard let vm = groupViewModel else { return }
        
        vm.startSettingMasterVolume()
    }
    
    @IBAction func onSliderChanged(_ sender: AnyObject) {
        guard let vm = groupViewModel else { return }
        
        vm.setMasterVolume(self.masterVolumeSlider.value)
    }
}
