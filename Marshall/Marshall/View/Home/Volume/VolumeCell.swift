//
//  VolumeCell.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 08/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import MinuetSDK
import ZoundCommon

protocol VolumeCellDelegate:class {
    func volumeCellDidRequestShowEq(_ showEq: Bool, volumeCell:VolumeCell)
}

class VolumeCell: UICollectionViewCell {
    @IBOutlet weak var speakerNameLabel: UILabel!
    @IBOutlet weak var speakerRoleLabel: UILabel!
    @IBOutlet weak var volumeSlider: AnimatedSlider!
    @IBOutlet weak var speakerImageView: UIImageView!
    @IBOutlet weak var volumeLabel: UILabel!
    
    @IBOutlet weak var bassLabel: UILabel!
    @IBOutlet weak var bassSlider: AnimatedSlider!
    @IBOutlet weak var trebleLabel: UILabel!
    @IBOutlet weak var trebleSlider: AnimatedSlider!
    @IBOutlet weak var expandingContainer: UIView!
    @IBOutlet weak var expandingContainerTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var eqButton: UIButton!
    
    weak var delegate: VolumeCellDelegate?
    
    let sliderThumbImage = UIImage(named: "small_volume_slider_thumb")
    let trackMinImage = UIImage(named: "slider_track_min")
    let trackMaxImage = UIImage(named: "slider_track_max")
    
    let sliderThumbImageDisabled = UIImage(named: "small_volume_slider_thumb_disabled")
    let trackMinImageDisabled = UIImage(named: "slider_track_min_disabled")
    let trackMaxImageDisabled = UIImage(named: "slider_track_max_disabled")
    
    fileprivate let viewModelVariable: Variable<SpeakerVolumeViewModel?> = Variable(nil)
    var viewModel: SpeakerVolumeViewModel? {
        get { return viewModelVariable.value }
        set { viewModelVariable.value = newValue }
    }
    
    override func awakeFromNib() {
        speakerNameLabel.font = Fonts.UrbanEars.Bold(17)
        speakerRoleLabel.font = Fonts.UrbanEars.Regular(17)
        volumeLabel.font = Fonts.UrbanEars.Regular(15)
        trebleLabel.font = Fonts.UrbanEars.Regular(15)
        bassLabel.font = Fonts.UrbanEars.Regular(15)
        
        volumeLabel.text = Localizations.Volume.VolumeSliderLabel
        bassLabel.text = Localizations.Volume.BassSliderLabel
        trebleLabel.text = Localizations.Volume.TrebleSliderLabel
        
        eqButton.setAttributedTitle(Fonts.UrbanEars.Bold(12).AttributedPageTitleWithString(Localizations.Volume.EqButtonShowText), for: .normal)
        
        volumeSlider.setThumbImage(UIImage(named: "small_volume_slider_thumb"), for: .normal)
        volumeSlider.setMinimumTrackImage(UIImage(named: "slider_track_min"), for: .normal)
        volumeSlider.setMaximumTrackImage(UIImage(named: "slider_track_max"), for: .normal)
        volumeSlider.addTarget(self, action: #selector(onVolumeTouchDown), for: .touchDown)
        volumeSlider.addTarget(self, action: #selector(onVolumeTouchUp), for: .touchUpInside)
        volumeSlider.addTarget(self, action: #selector(onVolumeTouchUp), for: .touchUpOutside)
        volumeSlider.addTarget(self, action: #selector(onVolumeTouchUp), for: .touchCancel)
        
        bassSlider.setThumbImage(UIImage(named: "small_volume_slider_thumb"), for: .normal)
        bassSlider.setMinimumTrackImage(UIImage(named: "slider_track_min"), for: .normal)
        bassSlider.setMaximumTrackImage(UIImage(named: "slider_track_max"), for: .normal)
        bassSlider.addTarget(self, action: #selector(onVolumeTouchDown), for: .touchDown)
        bassSlider.addTarget(self, action: #selector(onVolumeTouchUp), for: .touchUpInside)
        bassSlider.addTarget(self, action: #selector(onVolumeTouchUp), for: .touchUpOutside)
        bassSlider.addTarget(self, action: #selector(onVolumeTouchUp), for: .touchCancel)
        
        trebleSlider.setThumbImage(UIImage(named: "small_volume_slider_thumb"), for: .normal)
        trebleSlider.setMinimumTrackImage(UIImage(named: "slider_track_min"), for: .normal)
        trebleSlider.setMaximumTrackImage(UIImage(named: "slider_track_max"), for: .normal)
        trebleSlider.addTarget(self, action: #selector(onVolumeTouchDown), for: .touchDown)
        trebleSlider.addTarget(self, action: #selector(onVolumeTouchUp), for: .touchUpInside)
        trebleSlider.addTarget(self, action: #selector(onVolumeTouchUp), for: .touchUpOutside)
        trebleSlider.addTarget(self, action: #selector(onVolumeTouchUp), for: .touchCancel)
        
        viewModelVariable.asObservable()
            .map { $0 == nil ? "" : $0!.speaker.friendlyNameWithIndex.uppercased() }
            .bind(to: speakerNameLabel.rx.text)
            .disposed(by: rx_disposeBag)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let audioSystem = appDelegate.appCoordinator.audioSystem
        let isUnlocked = viewModelVariable.asObservable()
            .map { $0 != nil ? $0!.speaker : nil }
            .map { speaker -> Bool in
                guard let currentSpeaker = speaker else { return false }
                return audioSystem.isSpeakerUnlocked(currentSpeaker)
        }
        
        let speakerImageObservable = viewModelVariable.asObservable()
            .map { ExternalConfig.sharedInstance.speakerImageForColor($0?.speaker.color)?.listItemImageName }
            .map { ($0 != nil ? UIImage(named: $0!) : UIImage(named: "list_item_placeholder")) }
        
        Observable.combineLatest(speakerImageObservable, isUnlocked) { speakerImageObservable, isUnlocked -> (speakerImageObservable: UIImage, isUnlocked: Bool) in
            return (speakerImageObservable: speakerImageObservable!, isUnlocked: isUnlocked) }
            .subscribe(onNext: { [weak self] (speakerImageObservable: UIImage, isUnlocked: Bool) in
                if isUnlocked == true {
                    self?.updateSpeakerImage(speakerImg: UIImage(named: "list_item_unlocked")!)
                } else {
                    self?.updateSpeakerImage(speakerImg: speakerImageObservable)
                }
            }).disposed(by: rx_disposeBag)
        
        
        viewModelVariable.asObservable()
            .unwrapOptional()
            .flatMapLatest { $0.volume.asObservable() }
            .map { $0.volume }
            .unwrapOptional()
            .map { (volume: Int) -> Float in return Float(volume) / Float(32) }
            .subscribe(onNext: {[weak self] normalizedVolume in
                guard self?.volumeSlider.willSetValueFromUserInteraction == false else { return }
                self?.setVolumeSliderToValue(normalizedVolume)
            })
            .disposed(by: rx_disposeBag)
        
        viewModelVariable.asObservable()
            .unwrapOptional()
            .flatMapLatest { $0.volume.asObservable() }
            .map { $0.bass }
            .unwrapOptional()
            .map { (volume: Int) -> Float in return Float(volume) / Float(10) }
            .subscribe(onNext:{[weak self] normalizedVolume -> () in
                guard self?.bassSlider.willSetValueFromUserInteraction == false else { return }
                self?.setBassSliderToValue(normalizedVolume)
            })
            .disposed(by: rx_disposeBag)
        
        viewModelVariable.asObservable()
            .unwrapOptional()
            .flatMapLatest { $0.volume.asObservable() }
            .map { $0.treble }
            .unwrapOptional()
            .map { (volume: Int) -> Float in return Float(volume) / Float(10) }
            .subscribe(onNext: {[weak self] normalizedVolume in
                guard self?.trebleSlider.willSetValueFromUserInteraction == false else { return }
                self?.setTrebleSliderToValue(normalizedVolume)
            })
            .disposed(by: rx_disposeBag)
        
        let isMute = viewModelVariable.asObservable()
            .unwrapOptional()
            .flatMapLatest { $0.volume.asObservable() }
            .map { $0.mute }
            .unwrapOptional()
        
        let isEnabled =  viewModelVariable.asObservable()
            .unwrapOptional()
            .flatMapLatest { $0.isEnabledVariable.asObservable() }
        
        let volumeSliderEnabled = Observable.combineLatest(isMute, isEnabled) { !$0 && $1}
        volumeSliderEnabled
            .subscribe(onNext: {[weak self] isEnabled in
                self?.volumeSlider.setThumbImage(isEnabled ? self?.sliderThumbImage : self?.sliderThumbImageDisabled , for: .normal)
                self?.volumeSlider.setMinimumTrackImage(isEnabled ? self?.trackMinImage : self?.trackMinImageDisabled, for: .normal)
                self?.volumeSlider.setMaximumTrackImage(isEnabled ? self?.trackMaxImage : self?.trackMaxImageDisabled, for: .normal) })
            .disposed(by: rx_disposeBag)
        
        isEnabled
            .subscribe(onNext: { [weak self] isEnabled in
                self?.volumeSlider.isUserInteractionEnabled = isEnabled
                self?.eqButton.isEnabled = isEnabled
                self?.bassSlider.isUserInteractionEnabled = isEnabled
                self?.trebleSlider.isUserInteractionEnabled = isEnabled })
            .disposed(by: rx_disposeBag)
        
        viewModelVariable.asObservable()
            .unwrapOptional()
            .flatMapLatest { $0.isMulti.asObservable() }
            .subscribe(onNext: { [weak self] isMulti in
                self?.speakerRoleLabel.text = isMulti ? Localizations.Volume.Role.Multi.capitalized : Localizations.Volume.Role.Solo.capitalized })
            .disposed(by: rx_disposeBag)
        
        viewModelVariable.asObservable()
            .unwrapOptional()
            .flatMapLatest { $0.showEqVariable.asObservable() }
            .skip(1)
            .subscribe(weak: self, onNext: VolumeCell.updateForShowEq)
            .disposed(by: rx_disposeBag)
        
        updateForShowEq(showEq: false , animated: false)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(HomeSpeakerCell.handleNotification(notification:)),
                                               name: Notification.Name("UnlockedFound"), object: nil)
    }
    
    func handleNotification(notification: UIKit.Notification) {
        guard let speakerUnlockDict = notification.userInfo else { return }
        
        let speakerMac = speakerUnlockDict["speakerMac"] as! String
        if viewModelVariable.value?.speaker.mac == speakerMac {
            self.updateSpeakerImage(speakerImg: UIImage(named: "list_item_unlocked")!)
        }
    }
    
    func updateSpeakerImage(speakerImg : UIImage) {
        self.speakerImageView.image = speakerImg
        self.speakerImageView.contentMode = .scaleAspectFit
    }
    
    override func prepareForReuse() {
        NotificationCenter.default.removeObserver(self, name: Notification.Name("UnlockedFound"), object: nil)
    }
    
    func updateForShowEq(showEq: Bool) {
        updateForShowEq(showEq: showEq, animated: true)
    }
    
    func updateForShowEq(showEq: Bool, animated: Bool) {
        if animated {
            expandingContainerTopConstraint.constant = showEq ? 0 : -expandingContainer.bounds.size.height
            UIView.animate(withDuration: 0.3, delay: 0, options: [.beginFromCurrentState], animations: { [weak self] in
                self?.expandingContainer.superview?.superview?.layoutIfNeeded()
                self?.expandingContainer.alpha = showEq ? 1.0 : 0.0
                }, completion: nil)
        } else {
            expandingContainerTopConstraint.constant = showEq ? 0 : -expandingContainer.bounds.size.height
            self.expandingContainer.alpha = showEq ? 1.0 : 0.0
        }
        
        UIView.setAnimationsEnabled(false)
        
        if !showEq {
            let attributedTitle = Fonts.UrbanEars.Bold(12).AttributedTextWithString(Localizations.Volume.EqButtonShowText,
                                                                                    color: UIColor.white,
                                                                                    letterSpacing: 0.5,
                                                                                    shadow: true)
            eqButton.setAttributedTitle(attributedTitle, for: .normal)
            eqButton.setBackgroundImage(UIImage(named:"small_edit_button"), for: .normal)
            eqButton.setBackgroundImage(UIImage(named:"small_edit_button_disabled"), for: .disabled)
            
        } else {
            let attributedTitle = Fonts.UrbanEars.Bold(12).AttributedTextWithString(Localizations.Volume.EqButtonHideText,
                                                                                    color: UIColor("#867047"),
                                                                                    shadow: true)
            eqButton.setAttributedTitle(attributedTitle, for: .normal)
            eqButton.setBackgroundImage(UIImage(named:"small_empty_edit_button"), for: .normal)
        }
        eqButton.layoutIfNeeded()
        UIView.setAnimationsEnabled(true)
    }
    
    func setVolumeSliderToValue(_ value: Float) {
        self.volumeSlider.setValue(value, animated: false)
    }
    
    func setBassSliderToValue(_ value: Float) {
        self.bassSlider.setValue(value, animated: false)
    }
    
    func setTrebleSliderToValue(_ value: Float) {
        self.trebleSlider.setValue(value, animated: false)
    }
    
    @IBAction func onVolumeTouchUp(_ sender: AnyObject) {
        self.volumeSlider.willSetValueFromUserInteraction = false
        viewModel?.stopSettingVolume()
    }
    
    @IBAction func onVolumeTouchDown(_ sender: AnyObject) {
        self.volumeSlider.willSetValueFromUserInteraction = true
        viewModel?.startSettingVolume()
    }
    
    @IBAction func onChangedVolume(_ sender: AnyObject) {
        viewModel?.setVolume(volumeSlider.value)
    }
    
    @IBAction func onChangedBass(_ sender: AnyObject) {
        viewModel?.setBass(bassSlider.value)
    }
    
    @IBAction func onChangedTreble(_ sender: AnyObject) {
        viewModel?.setTreble(trebleSlider.value)
    }
    
    @IBAction func onEqToggle(_ sender: Any) {
        guard let vm = viewModel else { return }
        
        self.delegate?.volumeCellDidRequestShowEq(!vm.showEq, volumeCell: self)
    }
}
