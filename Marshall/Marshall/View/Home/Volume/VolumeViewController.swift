//
//  VolumeViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 08/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import VisualEffectView
import RxDataSources
import ZoundCommon

protocol VolumeViewControllerDelegate: class {
    func volumeViewControllerDidRequestClose(_ volumeViewController: VolumeViewController)
}

enum Group {
    case multi
    case solo
}

class VolumeViewController: UIViewController {
    @IBOutlet weak var muteAllButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var separatorViewHeight: NSLayoutConstraint!
    
    var delegate: VolumeViewControllerDelegate?
    var viewModel: VolumeViewModel?
    let dataSource = RxCollectionViewSectionedReloadDataSource<SectionModel<Group, SpeakerVolumeViewModel>>(configureCell: { _, _, _, _ in return fatalError("This should not be called") })
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.attributedText = Fonts.PageTitleFont.AttributedPageTitleWithString(Localizations.Volume.Title)
        muteAllButton.setAttributedTitle(Fonts.UrbanEars.Regular(10).AttributedTextWithString(Localizations.Volume.Buttons.MuteAll, color: UIColor(white: 1.0, alpha: 0.8), letterSpacing: 1.5), for: .normal)
        
        muteAllButton.titleLabel?.numberOfLines = 2
        muteAllButton.titleLabel?.adjustsFontSizeToFitWidth = true
        muteAllButton.titleLabel?.minimumScaleFactor = 0.7
        
        updateSeparatorViewVisibility()
        separatorViewHeight.constant = 0.9
        
        if let vm = viewModel {
            let dataSource = self.dataSource
            let soloViewModels = vm.soloGroupsViewModels.asObservable().unwrapOptional()
            let multiViewModels = vm.multiGroupsViewModels.asObservable().unwrapOptional()
            let items = Observable.combineLatest(soloViewModels, multiViewModels) { solos, multis -> [SectionModel<Group, SpeakerVolumeViewModel>] in
                var sectionModels = [SectionModel<Group, SpeakerVolumeViewModel>]()
                let multiVolumeViewModels = multis.flatMap{ viewModel in viewModel.speakerViewModels.value }
                
                if multiVolumeViewModels.count > 0 {
                    sectionModels.append(SectionModel(model: Group.multi, items: multiVolumeViewModels))
                }
                
                let soloVolumeViewModels = solos.flatMap{ viewModel in viewModel.speakerViewModels.value }
                
                if soloVolumeViewModels.count > 0 {
                    sectionModels.append(SectionModel(model: Group.solo, items: soloVolumeViewModels))
                }
                
                return sectionModels
            }
            
            collectionView.delegate = self
            collectionView.alwaysBounceVertical = true
            
            collectionView.register(UINib(nibName: "SoloVolumeHeaderReusableView", bundle: nil),
                                    forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "soloHeader")
            collectionView.register(UINib(nibName: "MultiVolumeHeaderReusableView", bundle: nil),
                                    forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "multiHeader")
            collectionView.register(UICollectionReusableView.self,
                                    forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "empty")
            collectionView.register(UICollectionViewCell.self,
                                    forCellWithReuseIdentifier: "empty")
            
            dataSource.configureCell = { [weak self] (_, collectionView, indexPath, item) in
                if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "volumeCell", for: indexPath as IndexPath) as? VolumeCell {
                    cell.viewModel = item
                    cell.delegate = self
                    return cell
                }
                
                return collectionView.dequeueReusableCell(withReuseIdentifier: "empty", for: indexPath as IndexPath)
            }
            
            dataSource.configureSupplementaryView = { [weak self] (dataSource, collectionView, kind, indexPath) in
                let sectionModel = dataSource[(indexPath as NSIndexPath).section]
                if sectionModel.model == Group.multi {
                    if let section = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "multiHeader", for: indexPath as IndexPath) as? MultiVolumeHeaderReusableView {
                        
                        if let multiGroupViewModels = self?.viewModel?.multiGroupsViewModels.value {
                            if multiGroupViewModels.count > indexPath.section {
                                let groupViewModel = multiGroupViewModels[indexPath.section]
                                section.groupViewModel = groupViewModel
                            }
                        }
                        
                        section.backgroundColor = UIColor.clear
                        return section
                    }
                } else if sectionModel.model == Group.solo {
                    if let section = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "soloHeader", for: indexPath as IndexPath) as? SoloVolumeHeaderReusableView {
                        section.backgroundColor = UIColor.clear
                        
                        return section
                    }
                }
                
                return collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "empty", for: indexPath as IndexPath)
            }
            
            items
                .bind(to: collectionView.rx.items(dataSource:dataSource))
                .disposed(by: rx_disposeBag)
            
            vm.globalMute.asObservable()
                .subscribe(onNext: { [weak self] mute in
                self?.upateForGlobalMute(mute) })
                .disposed(by: rx_disposeBag)
        }
    }
    
    deinit {
        print("deinit volume view controller")
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    func updateSeparatorViewVisibility() {
        separatorView.alpha = max(0, min(1.0, collectionView.contentOffset.y / 22.0))
    }
    
    func upateForGlobalMute(_ globalMute: Bool) {
            UIView.setAnimationsEnabled(false)
        
            if globalMute {
                let unmuteAttrTitle = Fonts.UrbanEars.Bold(12).AttributedTextWithString(Localizations.Volume.Buttons.UnmuteAll,
                                                                                        color: UIColor.white,
                                                                                        letterSpacing: 0.5,
                                                                                        shadow: true)
                muteAllButton.setAttributedTitle(unmuteAttrTitle, for: .normal)
                muteAllButton.setBackgroundImage(UIImage(named: "small_empty_edit_button"), for: .normal)
            } else {
                let muteAttrTitle = Fonts.UrbanEars.Bold(12).AttributedTextWithString(Localizations.Volume.Buttons.MuteAll,
                                                                                        color: UIColor.white,
                                                                                        letterSpacing: 0.5,
                                                                                        shadow: true)
                muteAllButton.setAttributedTitle(muteAttrTitle, for: .normal)
                muteAllButton.setBackgroundImage(UIImage(named: "small_edit_button"), for: .normal)
            }
        
            muteAllButton.layoutIfNeeded()
            UIView.setAnimationsEnabled(true)
    }
    
    override func viewDidLayoutSubviews() {
        //let flowLayout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        //flowLayout.itemSize = CGSize(width: self.collectionView.bounds.width-self.collectionView.contentInset.left-self.collectionView.contentInset.right, height: flowLayout.itemSize.height)
    }
    
    @IBAction func onMuteAll(_ sender: AnyObject) {
        viewModel?.muteAll()
    }
    
    @IBAction func onClose(_ sender: AnyObject) {
        self.delegate?.volumeViewControllerDidRequestClose(self)
    }
}

extension VolumeViewController: VolumeCellDelegate {
    func volumeCellDidRequestShowEq(_ showEq: Bool, volumeCell: VolumeCell) {
        guard let vm = self.viewModel else { return }
        
            for groupVolumeViewModel in (vm.soloGroupsViewModels.value! + vm.multiGroupsViewModels.value!) {
                for volumeViewModel in groupVolumeViewModel.speakerViewModels.value {
                    volumeViewModel.showEq = false
                }
            }

            volumeCell.viewModel?.showEq = showEq
            self.collectionView.performBatchUpdates(nil, completion: nil)
        }
}

extension VolumeViewController: UICollectionViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.updateSeparatorViewVisibility()
    }
}

extension VolumeViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = self.collectionView.frame.size.width - self.collectionView.contentInset.left - self.collectionView.contentInset.right
        let expandedHeight: CGFloat = 370
        let collapsedHeight: CGFloat = expandedHeight - 179.0
        var height: CGFloat =  collapsedHeight
        
        if let cell = collectionView.cellForItem(at: indexPath) as? VolumeCell {
            if cell.viewModel?.showEq == true {
                height = expandedHeight
            }
        }
        
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        let sectionModel = dataSource[section]
        
        if sectionModel.model == Group.multi {
            return CGSize(width: 320, height: 70)
        } else if sectionModel.model == Group.solo {
            return CGSize(width: 320, height: 30)
        }
        
        return CGSize.zero
    }
}


