//
//  SoloVolumeHeaderReusableView.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 08/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit

class SoloVolumeHeaderReusableView: UICollectionReusableView {
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var separatorViewHeight: NSLayoutConstraint!
    
    override func awakeFromNib() {
        separatorViewHeight.constant = 1.5
    }
}
