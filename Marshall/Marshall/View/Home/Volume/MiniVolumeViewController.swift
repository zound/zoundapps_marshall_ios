//
//  MiniVolumeViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 11/10/2016.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import Cartography
import MinuetSDK

protocol MiniVolumeViewControllerDelegate: class {
    func miniVolumeViewControllerDidStartInteractingWithVolume()
    func miniVolumeViewControllerDidStopInteractingWithVolume()
}

class MiniVolumeViewController: UIViewController {
    @IBOutlet weak var progressBar: UIView!
    @IBOutlet weak var progressBarWidthConstraint: NSLayoutConstraint!
    
    var viewModel: MiniVolumeViewModel?
    
    weak var delegate: MiniVolumeViewControllerDelegate?
    
    override func viewDidLoad() {
        guard let vm = viewModel else { return }
        
        let isMulti = vm.clientState.asObservable()
            .unwrapOptional()
            .map { $0.groupState }
            .unwrapOptional()
            .distinctUntilChanged()
            .map { groupState in return [GroupState.solo].contains(groupState) ? false : true }
        
        let volume = isMulti
            .map { isMulti -> Observable<Int> in
                if !isMulti {
                    return  vm.clientState.asObservable()
                        .unwrapOptional()
                        .map { $0.volume }
                        .unwrapOptional()
                } else {
                    return  vm.masterState.asObservable()
                        .unwrapOptional()
                        .map { $0.masterVolume }
                        .unwrapOptional() }
            }.switchLatest()
            .map { Float($0) }
        
        volume
            .take(1)
            .subscribe(onNext: { [weak self] volume in
                self?.updateForVolume(volume: volume, animated: false) })
            .disposed(by: rx_disposeBag)
        
        volume
            .skip(1)
            .subscribe(weak: self, onNext: MiniVolumeViewController.updateForVolume)
            .disposed(by: rx_disposeBag)
    }
    
    func updateForVolume(volume: Float) {
        
        self.updateForVolume(volume: volume, animated: true)
    }
    
    func updateForVolume(volume: Float, animated: Bool) {
        
        let volumeNormalized: CGFloat = CGFloat(volume / 32.0)
        self.view.removeConstraint(progressBarWidthConstraint)
        constrain(progressBar) { [weak self] view in
            self?.progressBarWidthConstraint = (view.width == view.superview!.width * volumeNormalized)
        }
        
        if animated {
            UIView.animate(withDuration: 0.20, animations: { [weak self] in
                self?.view.layoutIfNeeded()
            })
        }
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return [.all]
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return .fade
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func willRotate(to toInterfaceOrientation: UIInterfaceOrientation, duration: TimeInterval) {
        self.view.window?.isHidden = true
    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        self.view.window?.isHidden = false
    }
}
