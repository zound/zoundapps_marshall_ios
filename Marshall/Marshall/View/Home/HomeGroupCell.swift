//
//  HomeTableViewCell.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 22/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

protocol HomeGroupCellDelegate: class {
    func homeGroupCellDidSelectSpeakerViewModel(_ speakerViewModel:HomeSpeakerViewModel, fromGroupViewModel: HomeGroupViewModel)
    func homeGroupCellDidRequestSetupForSpeakerViewModel(_ speakerViewModel:HomeSpeakerViewModel, fromGroupViewModel: HomeGroupViewModel)
    func homeGroupCellDidRequestFactoryResetForSpeakerViewModel(_ speakerViewModel:HomeSpeakerViewModel, fromGroupViewModel: HomeGroupViewModel)
    func homeGroupCellDidRequestChangeMultiTo(_ multi: Bool, speakerViewModel:HomeSpeakerViewModel, fromGroupViewModel: HomeGroupViewModel)
    func homeGroupCellDidRequestSettingsForSpeakerViewModel(_ speakerViewModel:HomeSpeakerViewModel, fromGroupViewModel: HomeGroupViewModel)
}

class HomeGroupCell: UITableViewCell, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    
    weak var viewModel: HomeGroupViewModel? {
        didSet {
            viewModel?.delegate = self
            self.tableViewHeight.constant = 5 * HomeSpeakerCell.cellHeight
            self.tableView.reloadData()
        }
    }
    
    weak var delegate: HomeGroupCellDelegate?
    
    override func prepareForReuse() {
        guard let vm = viewModel else { return }
        
        if let delegate = vm.delegate as? HomeGroupCell, delegate == self {
            vm.delegate = nil
        }
        
        self.viewModel = nil
        self.tableView.reloadData()
    }
    
    override func awakeFromNib() {
        self.contentView.backgroundColor = UIColor.clear
        self.backgroundColor = UIColor.clear
        
        let customColorView = UIView()
        customColorView.backgroundColor = UIColor(white: 0.0, alpha: 0.24)
        self.selectedBackgroundView =  customColorView
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        longPressGesture.minimumPressDuration = 1.0 // 1 second press
        self.tableView.addGestureRecognizer(longPressGesture)
        
        let twoFingerLongPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(handle2FingerLongPress))
        twoFingerLongPressGesture.minimumPressDuration = 1.0 // 1 second press
        twoFingerLongPressGesture.numberOfTouchesRequired = 2
        self.tableView.addGestureRecognizer(twoFingerLongPressGesture)
    }
    
    @objc func handle2FingerLongPress(_ longPressGesture: UIGestureRecognizer) {
        let p = longPressGesture.location(in: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: p)
        
        if indexPath == nil {
            print("Long press on table view, not row.")
        } else if (longPressGesture.state == UIGestureRecognizer.State.began) {
            print("Long press on row, at \((indexPath! as NSIndexPath).row)")
            guard let vm = viewModel else { return }
            
            let speakerViewModel = vm.speakersViewModels.value[indexPath!.row]
            self.delegate?.homeGroupCellDidRequestFactoryResetForSpeakerViewModel(speakerViewModel, fromGroupViewModel: vm)
        }
    }
    
    @objc func handleLongPress(_ longPressGesture: UIGestureRecognizer) {
        let p = longPressGesture.location(in: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: p)
        
        if indexPath == nil {
            print("Long press on table view, not row.")
        } else if (longPressGesture.state == UIGestureRecognizer.State.began) {
            print("Long press on row, at \((indexPath! as NSIndexPath).row)")
            guard let vm = viewModel else { return }
            
            let speakerViewModel = vm.speakersViewModels.value[indexPath!.row]
            self.delegate?.homeGroupCellDidRequestSetupForSpeakerViewModel(speakerViewModel, fromGroupViewModel: vm)
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let vm = viewModel {
            let speakerViewModel = vm.speakersViewModels.value[indexPath.row]
            self.delegate?.homeGroupCellDidSelectSpeakerViewModel(speakerViewModel, fromGroupViewModel: vm)
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let vm = viewModel else { return 0 }
        
        return vm.speakersViewModels.value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "homeSpeakerItem") as? HomeSpeakerCell else { return UITableViewCell() }
        
        if let vm = viewModel {
            cell.viewModel.value = vm.speakersViewModels.value[indexPath.row]
            cell.delegate = self
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return HomeSpeakerCell.cellHeight
    }
}

extension HomeGroupCell: HomeGroupViewModelDelegate {
    func homeGroupViewModelDidAddSpeaker(atIndex: Int, fromMove: Bool, homeGroupViewModel: HomeGroupViewModel) {
        guard let group = self.viewModel?.group, group == homeGroupViewModel.group else { return }
        
        let indexPath = IndexPath(row: atIndex, section: 0)
        self.tableView.insertRows(at: [indexPath], with: fromMove ? .left : .fade)
    }
    
    func homeGroupViewModelDidRemoveSpeaker(fromIndex: Int, fromMove: Bool, homeGroupViewModel: HomeGroupViewModel) {
        guard let group = self.viewModel?.group, group == homeGroupViewModel.group else { return }
        
        let indexPath = IndexPath(row: fromIndex, section: 0)
        self.tableView.deleteRows(at: [indexPath], with: fromMove ? .left : .fade)
    }
    
    func homeGroupViewModelDidMoveSpeaker(fromIndex: Int, toIndex: Int, homeGroupViewModel: HomeGroupViewModel) {
        guard let group = self.viewModel?.group, group == homeGroupViewModel.group else { return }
        
        let fromIndexPath = IndexPath(row: fromIndex, section: 0)
        let toIndexPath = IndexPath(row: toIndex, section: 0)
        self.tableView.moveRow(at: fromIndexPath, to: toIndexPath)
    }
}

extension HomeGroupCell: HomeSpeakerCellDelegate {
    func homeSpeakerCell(_ cell: HomeSpeakerCell, didRequestSwitchMultiToMulti multi: Bool) {
        guard let vm = self.viewModel, let cellVM = cell.viewModel.value else { return }
        
        self.delegate?.homeGroupCellDidRequestChangeMultiTo(multi, speakerViewModel: cellVM , fromGroupViewModel: vm)
    }
    
    func homeSpeakerCellDidRequestSpeakerSettings(_ cell: HomeSpeakerCell) {
        guard let vm = cell.viewModel.value, let groupViewModel = self.viewModel else { return }
        
        self.delegate?.homeGroupCellDidRequestSettingsForSpeakerViewModel(vm, fromGroupViewModel: groupViewModel)
    }
}
