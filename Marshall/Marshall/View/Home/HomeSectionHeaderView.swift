//
//  HomeSectionHeaderView.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 12/05/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit
import Cartography

class HomeSectionHeaderView: UIView {
    let titleLabel: UILabel
    fileprivate var _text: String?
    fileprivate var _labelColor: UIColor = UIColor.white
    
    var text: String? {
        get { return _text }
        set {
            _text = newValue
            updateLabel()
        }
    }
    
    var labelColor: UIColor {
        get { return _labelColor }
        set {
            _labelColor = newValue
            updateLabel()
        }
    }
    
    init() {
        self.titleLabel = UILabel()
        self.titleLabel.font = UIFont.systemFont(ofSize: 10.0)
        super.init(frame: CGRect.zero)
        createSubviewConstraints()
    }
    
    override init(frame: CGRect) {
        self.titleLabel = UILabel()
        self.titleLabel.font = UIFont.systemFont(ofSize: 10.0)
        super.init(frame: frame)
        
        self.titleLabel.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(self.titleLabel)
        
        createSubviewConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.titleLabel = UILabel()
        self.titleLabel.font = UIFont.systemFont(ofSize: 10.0)
        super.init(coder: aDecoder)
        
        self.titleLabel.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(self.titleLabel)
        
        createSubviewConstraints()
    }
    
    func createSubviewConstraints() {
        constrain(self.titleLabel) { titleLabel in
            titleLabel.centerX == titleLabel.superview!.centerX
            titleLabel.centerY == titleLabel.superview!.centerY + 8
            titleLabel.leftMargin >= titleLabel.superview!.leftMargin
            titleLabel.rightMargin <= titleLabel.superview!.rightMargin
        }
    }
    
    fileprivate func updateLabel() {
        self.titleLabel.attributedText = Fonts.UrbanEars.Regular(10).AttributedTextWithString(_text ?? "", color: _labelColor, letterSpacing: 1.5, lineSpacing: 0.0)
    }
}
