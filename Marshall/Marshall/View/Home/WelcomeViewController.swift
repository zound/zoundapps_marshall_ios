//
//  WelcomeViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 26/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import AVKit
import AVFoundation

protocol WelcomeViewControllerDelegate:class {
    func welcomeViewControllerDidAccept(_ welcomeViewController: WelcomeViewController)
}

class WelcomeViewController:UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var placeholderImage: UIImageView!
    
    weak var delegate: WelcomeViewControllerDelegate?
    
    var playerLayer: AVPlayerLayer?
    var player: AVPlayer?
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.attributedText = Fonts.UrbanEars.Bold(45).AttributedTextWithString(Localizations.Welcome.Title, color: titleLabel.textColor, letterSpacing: 1)
        
        subtitleLabel.attributedText = Fonts.UrbanEars.Regular(17).AttributedTextWithString(Localizations.Welcome.Subtitle, color: titleLabel.textColor, letterSpacing: 0.25)
        
        UIView.setAnimationsEnabled(false)
        
        acceptButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.Welcome.Buttons.Accept), for: .normal)
        acceptButton.layoutIfNeeded()
        UIView.setAnimationsEnabled(true)
        
        titleLabel.adjustsFontSizeToFitWidth = true
        
        initVideoPlayer()
        
        let didEnterBackground = NotificationCenter.default.rx.notification(UIApplication.didEnterBackgroundNotification)
        let willEnterForeground = NotificationCenter.default.rx.notification(UIApplication.willEnterForegroundNotification)
        
        didEnterBackground
            .subscribe(onNext: { [weak self] _ in self?.didEnterBackground() })
            .disposed(by: rx_disposeBag)
        
        willEnterForeground
            .subscribe(onNext: { [weak self] _ in self?.willEnterForeground() })
            .disposed(by: rx_disposeBag)
        
        animateControls()
    }

    override func viewDidAppear(_ animated: Bool) {
        player?.play()
    }
    
    override func viewDidLayoutSubviews() {
        playerLayer?.frame = view.frame
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        player?.pause()
    }
    
    func willEnterForeground() -> Void {}
    
    func didEnterBackground() -> Void {}

    func animateControls() {
        titleLabel.alpha = 0.0
        subtitleLabel.alpha = 0.0
        acceptButton.alpha = 0.0
        
        UIView.animate(withDuration: 3.0, delay: 0.0, options: [.beginFromCurrentState, .allowUserInteraction], animations: { [weak self] in
            self?.titleLabel.alpha = 1.0
            self?.subtitleLabel.alpha = 1.0
            self?.acceptButton.alpha = 1.0
        }, completion: nil)
    }
    
    func initVideoPlayer() {
        if let videoURL = Bundle.main.url(forResource: "intro", withExtension: "mp4") {
            if #available(iOS 10.0, *) {
                _ = try? AVAudioSession.sharedInstance().setCategory(.playback, mode: .default, options: .mixWithOthers)
            } else {
                // Set category with options (iOS 9+) setCategory(_:options:)
                AVAudioSession.sharedInstance().perform(NSSelectorFromString("setCategory:withOptions:error:"),
                                                        with: AVAudioSession.Category.playback,
                                                        with: [AVAudioSession.CategoryOptions.mixWithOthers])
            }
            
            player = AVPlayer(url: videoURL)
            player?.actionAtItemEnd = .none
            player?.isMuted = false
            
            playerLayer = AVPlayerLayer(player: player)
            playerLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
            playerLayer?.zPosition = -1
            
            playerLayer?.frame = view.frame
            
            view.layer.addSublayer(playerLayer!)
            
            NotificationCenter.default.addObserver(self, selector: #selector(loopVideo), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
            
            if let player = player {
                player.rx
                    .observe(AVPlayer.Status.self, "status")
                    .filter { $0 != nil && $0! == .readyToPlay }
                    .subscribe( onNext: { [weak self] status in
                        guard let status = status else { return }
                        self?.didChangePlayerStatusTo(status: status) })
                    .disposed(by: rx_disposeBag)
            }
        }
    }
    
    func didChangePlayerStatusTo(status: AVPlayer.Status) {
        
        if case AVPlayer.Status.readyToPlay = status {
            self.placeholderImage.isHidden = true
        }
    }
    
    @objc func loopVideo() {
        let t1 = CMTimeMake(value: 5, timescale: 100)
        player?.seek(to: t1)
        player?.play()
        self.placeholderImage.isHidden = true
    }
    
    @IBAction func onAccept(_ sender: AnyObject) {
        self.delegate?.welcomeViewControllerDidAccept(self)
    }
}
