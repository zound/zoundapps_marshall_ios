//
//  VolumeViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 08/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import ZoundCommon

protocol CastInfoAlertViewControllerDelegate: class {
    func castInfoAlertDidRequestContinue(_ alertViewController: CastInfoAlertViewController)
    func castInfoAlertDidRequestLearnMore(_ alertViewController: CastInfoAlertViewController)
    func castInfoAlertDidRequestClose(_ alertViewController: CastInfoAlertViewController)
}


class CastInfoAlertViewController: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var getCastAppButton: UIButton!
    @IBOutlet weak var learnMoreButton: UIButton!
    
    var dontShow = false
    weak var delegate: CastInfoAlertViewControllerDelegate?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.text = Localizations.Appwide.Note
        titleLabel.font = Fonts.UrbanEars.Bold(23)               
        
        contentLabel.text = Localizations.CastInfo.Content
        contentLabel.font = Fonts.MainContentFont
        
        getCastAppButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.CastInfo.Buttons.GetGoogleCast), for: .normal)
        learnMoreButton.setAttributedTitle(Fonts.SecondaryButtonFont.AttributedSecondaryButtonWithString(Localizations.CastInfo.Buttons.LearnMore), for: .normal)
    }
    
    @IBAction func onContinue(_ sender: AnyObject) {
        self.delegate?.castInfoAlertDidRequestContinue(self)
    }
    
    @IBAction func onLearnMore(_ sender: AnyObject) {
        self.delegate?.castInfoAlertDidRequestLearnMore(self)
    }
    
    @IBAction func onClose(_ sender: AnyObject) {
        delegate?.castInfoAlertDidRequestClose(self)
    }
}


