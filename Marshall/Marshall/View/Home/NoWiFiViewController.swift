//
//  NoWiFiViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 26/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit

protocol NoWiFiViewControllerDelegate: class {
    func noWifiViewControllerDidRequestMenu(_ noWifiViewController: UIViewController)
}

class NoWiFiViewController: UIViewController {
    @IBOutlet weak var messageTitleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var goToSettingsButton: UIButton!
    
    weak var delegate: NoWiFiViewControllerDelegate?
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        messageTitleLabel.attributedText = Fonts.UrbanEars.Bold(23).AttributedTextWithString(Localizations.NoWifi.Title, color: UIColor.white, letterSpacing: 1.12)
        
        messageLabel.text = Localizations.NoWifi.Description
        messageLabel.font = Fonts.MainContentFont
        
        goToSettingsButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.NoWifi.Buttons.GoToSettings), for: .normal)
    }
    
    @IBAction func onMenu(_ sender: AnyObject) {
        self.delegate?.noWifiViewControllerDidRequestMenu(self)
    }
    
    @IBAction func onGotoSettings(_ sender: Any) {
        guard let settingsURL = URL(string: UIApplication.openSettingsURLString) else { return }
        guard UIApplication.shared.canOpenURL(settingsURL) else { return }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(settingsURL)
        } else {
            UIApplication.shared.openURL(settingsURL)
        }
    }
}
