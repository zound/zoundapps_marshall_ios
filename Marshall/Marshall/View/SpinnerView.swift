//
//  SpinnerView.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 15/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit
import Cartography

class SpinnerView: UIView {
    var spinnerImageView: UIImageView!
    
    override init(frame: CGRect) {
        let spinnerImage = UIImage(named: "loader")
        spinnerImageView = UIImageView(image: spinnerImage)
        isAnimating = false
        super.init(frame: frame)
        setupSpinnerImage()
    }
    
    required init?(coder aDecoder: NSCoder) {
        let spinnerImage = UIImage(named: "loader")
        spinnerImageView = UIImageView(image: spinnerImage)
        isAnimating = false
        super.init(coder: aDecoder)
        setupSpinnerImage()
        backgroundColor = UIColor.clear
    }
    
    @IBInspectable var isAnimating: Bool {
        didSet {
            if isAnimating {
                let animation = CABasicAnimation(keyPath: "transform.rotation.z")
                animation.duration = 1.0
                animation.fromValue = NSNumber(value: 0 as Double)
                animation.toValue = NSNumber(value: 2*Double.pi as Double)
                animation.repeatCount = Float.infinity
                animation.isRemovedOnCompletion = false
                spinnerImageView.layer.add(animation, forKey: "animateRotation")
            } else {
                spinnerImageView.layer.removeAllAnimations()
            }
        }
    }
    
    func setupSpinnerImage() {
        spinnerImageView?.translatesAutoresizingMaskIntoConstraints = false
        addSubview(spinnerImageView)
        
        constrain(spinnerImageView) { view in
            view.center == view.superview!.center
        }
    }
}
