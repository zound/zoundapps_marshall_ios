//
//  AppCoordinator.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 11/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import WebKit
import STKWebKitViewController
import SafariServices
import Crashlytics
import Fabric
import MinuetSDK
import ZoundCommon
import ObjectMapper

protocol Coordinator: class {
    func start()
}

enum AppStatus {
    case none
    case termsNotAccepted
    case loading
    case wifiNotAvailable
    case noSpeakersAvailable
    case speakersAvailable
}

enum WarningMessageType {
    case updateMessage
    case unlockedMessage
}

class AppCoordinator: RunAfterDelay {
    let factoryResetsSpeakersOnTwoFingerLongPress = false
    let setupSpeakerOnLongPress = false
    let mock = false
    
    fileprivate let unlockedMacPrefixForUserStore = "unlocked_"
    fileprivate let updateMacPrefixForUserStore = "updated_"
    
    let audioSystem: AudioSystem
    let rootViewController: UIViewController
    let provider: SpeakerProvider
    
    var childCoordinators: [Coordinator] = []
    var menuViewModel: MenuViewModel
    var navController: UINavigationController?
    var disposeBag = DisposeBag()
    
    let appStatus: Variable<AppStatus> = Variable(.none)
    let initialScan: Variable<Bool> = Variable(false)
    var initialScanDisposable: Disposable?
    let discoveryService: DiscoveryServiceType
    let termsAcceptedKey = "TermsAccepted"
    var lastRefreshTriggerDate: Date?
    var isShowingVolumeViewController: Bool = false
    var openSpeaker: HomeSpeakerViewModel?
    var loadingSpeakerDisposable: Disposable?
    var wacService: WACServiceType?
    
    let fadeTransitioningDelegate = FadeTransitioningDelegate()
    
    var homeViewModel: HomeViewModel?
    
    let lastConnectedSpeakerStore = SingleSpeakerStore(key: "last_connected_speaker")
    let recentSpeakerStore = RecentSpeakerStore(storageKey: "recent_speaker_store", capacity: 4)
    
    var lastConnectedSpeaker: Speaker? {
        get { return lastConnectedSpeakerStore.speaker }
        set { lastConnectedSpeakerStore.speaker = newValue }
    }
    
    init(rootViewControllerController:UIViewController) {
        provider = SpeakerProvider()
        
        self.rootViewController = rootViewControllerController
        self.discoveryService = (mock ? DiscoveryServiceMock(): DiscoveryService())
        
        self.audioSystem = AudioSystem(discoveryService: self.discoveryService, provider: self.provider)
        audioSystem.groupUpdatesActive = true
        
        self.menuViewModel = MenuViewModel(audioSystem: audioSystem)
        if let rootViewController = rootViewController as? RootViewController {
            let menuViewController = UIStoryboard.main.menuViewController
            menuViewController.viewModel = menuViewModel
            menuViewController.delegate = self
            rootViewController.menuViewController = menuViewController
        }
        
        let termsAcceptedKey = self.termsAcceptedKey
        let termsAccepted = NotificationCenter.default.rx.notification(UserDefaults.didChangeNotification)
            .map { _ in return UserDefaults.standard.bool(forKey: termsAcceptedKey) }
            .startWith(UserDefaults.standard.bool(forKey:termsAcceptedKey))
            .distinctUntilChanged()
        
        let wifiReachable = NetworkInfoService.sharedInstance.wifiReachable
        
        let scanActive = Observable.combineLatest(wifiReachable, termsAccepted) { wifiReachable, termsAccepted in
            (wifiReachable && termsAccepted) }
        
        self.discoveryService.addDelegate(self)
        scanActive.distinctUntilChanged()
            .subscribe(onNext: { [weak self] active in
                if active {
                    self?.discoveryService.search()
                } else {
                    self?.stopInitialScan()
                    self?.discoveryService.stop()
                }
            })
            .disposed(by: disposeBag)
        
        let speakerCount = self.audioSystem.groups.asObservable()
            .map { $0.count }
        
        Observable.combineLatest(termsAccepted,
                                 wifiReachable,
                                 initialScan.asObservable().distinctUntilChanged(),
                                 speakerCount,
                                 UIApplication.appIsInForeground) { (termsAccepted, wifiReachable, initialScan, speakerCount, foreground) -> (termAccepted: Bool, wifiReachable: Bool, initialScan: Bool, foreground: Bool, speakerCount: Int) in
                                    return (termAccepted: termsAccepted, wifiReachable: wifiReachable, initialScan: initialScan, foreground: foreground, speakerCount: speakerCount) }
            .subscribe(onNext:{ [weak self] (termAccepted: Bool, wifiReachable: Bool, initialScan: Bool, foreground: Bool, speakerCount: Int) in
                self?.updateAppStatusFor(termsAccepted:termAccepted, wifiReachable: wifiReachable, initialScan: initialScan, foreground: foreground, speakerCount: speakerCount)
            }).disposed(by: disposeBag)
        
        self.appStatus.asObservable()
            .distinctUntilChanged()
            .subscribe(onNext:{ [weak self] appStatus in
                self?.updateForCurrentAppStatus() })
            .disposed(by: disposeBag)
        
        audioSystem.addDelegate(self)
        discoveryService.addDelegate(self)
    }
    
    func startInitialScan() {
        initialScan.value = true
        stopInitialScan()
        initialScanDisposable = Observable<Int>
            .interval(5.0, scheduler: MainScheduler.instance)
            .take(1)
            .subscribe(weak: self, onNext: AppCoordinator.initialScanDone)
        
        initialScanDisposable!.disposed(by: disposeBag)
    }
    
    func stopInitialScan() {
        initialScanDisposable?.dispose()
        initialScanDisposable = nil
    }
    
    func initialScanDone(done: Int) {
        initialScan.value = false
    }
    
    func refreshDiscovery(clear: Bool) {
        Answers.logCustomEvent(withName: "ForceRefreshHomeScreen", customAttributes: nil)
        self.audioSystem.refresh(clear: clear)
        self.lastRefreshTriggerDate = Date()
    }
    
    func updateAppStatusFor(termsAccepted: Bool, wifiReachable: Bool, initialScan: Bool, foreground: Bool, speakerCount: Int) {
        if !termsAccepted {
            appStatus.value = AppStatus.termsNotAccepted
            return
        }
        
        if !wifiReachable {
            appStatus.value = AppStatus.wifiNotAvailable
            return
        }
        
        if initialScan {
            if speakerCount < 1 {
                appStatus.value = AppStatus.loading
            } else {
                appStatus.value = AppStatus.speakersAvailable
            }
        } else {
            if speakerCount > 0 {
                appStatus.value = AppStatus.speakersAvailable
                return
            } else {
                appStatus.value = AppStatus.noSpeakersAvailable
                return
            }
        }
    }
    
    func updateForCurrentAppStatus() {
        DispatchQueue.main.async { [weak self] in
            guard let status = self?.appStatus.value else { return }
            switch status {
            case .none: break
            case .termsNotAccepted: self?.showWelcome()
            case .loading: self?.showLoading()
            case .noSpeakersAvailable: self?.showNoSpeakersAvailable()
            case .wifiNotAvailable: self?.showNoWifiAvailable()
            case .speakersAvailable:
                if let lastConnectedSpeaker = self?.lastConnectedSpeaker,
                    let speakers = self?.audioSystem.speakers.value,
                    speakers.contains(lastConnectedSpeaker) {
                    
                    if let connectable = ExternalConfig.sharedInstance.speakerImageForColor(lastConnectedSpeaker.color)?.connectable, connectable == true {
                        self?.showReconnecting(forSpeaker: lastConnectedSpeaker)
                        self?.showSpeakerFromReconnect(lastConnectedSpeaker)
                    } else {
                        self?.showHome()
                    }
                } else {
                    self?.showHome()
                }
            }
        }
        
    }
    
    func updateForCurrentGroups(_ groups: [SpeakerGroup]) {
        if let playerCoordinator = childCoordinators.find({ $0 is PlayerCoordinator }) as? PlayerCoordinator {
            let currentSpeaker = playerCoordinator.connectionManager.clientSpeaker
            let currentGroupsContainCurrentSpeaker = groups.find({$0.speakers.contains(currentSpeaker)}) != nil
            if !currentGroupsContainCurrentSpeaker {
                playerCoordinator.stop()
            }
        }
        
        if let settingsCoordinator = childCoordinators.find({ $0 is SettingsCoordinator }) as? SettingsCoordinator {
            let currentSpeaker = settingsCoordinator.currentSpeaker
            if let currentSpeaker = currentSpeaker {
                let currentGroupsContainCurrentSpeaker = groups.find({$0.speakers.contains(currentSpeaker)}) != nil
                if !currentGroupsContainCurrentSpeaker {
                    _ = self.rootViewController.dismiss(animated: true, completion: {})
                }
            }
        }
    }
    
    func updateNowPlayingInfo(_ info: NowPlayingState, forSpeaker speaker: Speaker) {
        audioSystem.nowPlayingInfo.value[speaker] = info
    }
    
    func start() {
        updateForCurrentAppStatus()
    }
    
    func showHome() {
        guard let rootVC = self.rootViewController as? RootViewController else { return }
        
        if navController == nil {
            navController = UENavigationController()
            navController?.navigationBar.isHidden = true
            navController?.automaticallyAdjustsScrollViewInsets = false
        }
        
        if rootVC.displayedViewController != navController {
            rootVC.showChildViewController(navController, animated: false)
        }
        
        let topViewController = navController?.viewControllers.first
        if topViewController == nil || !(topViewController is HomeViewController) {
            let homeViewController = UIStoryboard.main.homeViewController
            homeViewController.delegate = self
            
            if homeViewModel == nil {
                homeViewModel = HomeViewModel(audioSystem: audioSystem, speakerProvider: provider)
            }
            
            homeViewController.viewModel = homeViewModel
            navController?.setViewControllers([homeViewController], animated: false)
        }
    }
    
    func showReconnecting(forSpeaker speaker: Speaker) {
        guard let rootVC = self.rootViewController as? RootViewController else { return }
        
        let reconnectingViewController = UIStoryboard.main.reconnectingViewController
        reconnectingViewController.speaker = speaker
        rootVC.showChildViewController(reconnectingViewController, animated: false)
    }
    
    func showWelcome() {
        guard let rootVC = self.rootViewController as? RootViewController else { return }
        
        let welcomeViewController = UIStoryboard.main.welcomeViewController
        welcomeViewController.delegate = self
        
        if navController == nil {
            navController = UENavigationController()
            navController?.navigationBar.isHidden = true
        }
        
        rootVC.showChildViewController(navController, animated: false)
        navController?.setViewControllers([welcomeViewController], animated: false)
    }
    
    func showLoading() {
        guard let rootVC = self.rootViewController as? RootViewController else { return }
        let loadingViewController = UIStoryboard.main.loadingViewController
        rootVC.showChildViewController(loadingViewController, animated: false)
    }
    
    func showNoSpeakersAvailable() {
        guard let rootVC:RootViewController = self.rootViewController as? RootViewController else { return }
        let noSpeakersViewController = UIStoryboard.main.noSpeakersViewController
        noSpeakersViewController.delegate = self
        rootVC.showChildViewController(noSpeakersViewController, animated: false)
    }
    
    func showNoWifiAvailable() {
        guard let rootVC = self.rootViewController as? RootViewController else { return }
        let noWifiViewController = UIStoryboard.main.noWiFiViewController
        noWifiViewController.delegate = self
        rootVC.showChildViewController(noWifiViewController, animated: false)
        stopSettingsCoordinator()
        closeAnyOpenScreens {}
    }
    
    func stopSettingsCoordinator() {
        self.childCoordinators.forEach { coordinator in
            guard let coordinator = coordinator as? SettingsCoordinator else { return }
            coordinator.delegate?.settingsCoordinatorDidFinishCoordinating(coordinator)
        }
    }
    
    func showGroupFullInfoAlert() {
        guard let rootVC = self.rootViewController as? RootViewController else { return }
        let groupFullAlert = UIStoryboard.main.groupFullInfoAlertViewController
        rootVC.definesPresentationContext = true
        
        groupFullAlert.providesPresentationContextTransitionStyle = true
        groupFullAlert.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        groupFullAlert.transitioningDelegate = fadeTransitioningDelegate
        groupFullAlert.delegate = self
        
        rootVC.present(groupFullAlert, animated: true, completion: nil)
    }
    
    func showCastSwitchAlert(forSpeakerViewModel speakerViewModel:HomeSpeakerViewModel) {
        guard let rootVC = self.rootViewController as? RootViewController else { return }
        let castAlert = UIStoryboard.main.castSwitchAlertViewController
        rootVC.definesPresentationContext = true
        
        castAlert.providesPresentationContextTransitionStyle = true
        castAlert.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        castAlert.transitioningDelegate = fadeTransitioningDelegate
        castAlert.delegate = self
        castAlert.speakerViewModel = speakerViewModel
        
        rootVC.present(castAlert, animated: true, completion: nil)
    }
    
    func logSuccessfulConnectionWithDuration(_ duration: Double) {
        let eventNamesForDuration = [0.0: "Connected00Second",
                                     0.5: "Connected05Second",
                                     1.0: "Connected1Second",
                                     2.0: "Connected2Second",
                                     3.0: "Connected3Second",
                                     4.0: "Connected4Second",
                                     5.0: "Connected5Second",
                                     6.0: "Connected6Second",
                                     7.0: "Connected7Second",
                                     8.0: "Connected8Second",
                                     9.0: "Connected9Second",
                                     10.0: "Connected10Second"]
        
        var minDuration:Double = 0
        let allKeys = Array(eventNamesForDuration.keys).sorted()
        
        for i in 0..<(allKeys.count-1) {
            let first = allKeys[i]
            let second = allKeys[i+1]
            if duration < second && duration > first {
                minDuration = first
            }
            if duration > second {
                minDuration = second
            }
        }
        
        if let eventName = eventNamesForDuration[minDuration] {
            Answers.logCustomEvent(withName: "ConnectedFromHomeScreen",
                                   customAttributes:
                ["Status":eventName])
        }
    }
    
    func showSpeakerFromHomeScreenQuickActionItem(_ item: UIApplicationShortcutItem) {
        guard let speakerJSON = item.userInfo, let speaker = Mapper<Speaker>().map(JSON: speakerJSON) else { return }
        
        self.lastConnectedSpeaker = speaker
        
        if self.navController?.topViewController is HomeViewController || self.navController?.topViewController is SpeakerViewController {
            showSpeakerFromReconnect(speaker)
        }
    }
    
    func handleNavigationFromURL(url: URL) {
        guard let infoPlist = Bundle.main.infoDictionary,
            let appSettings = infoPlist["AppSettings"] as? Dictionary<String, AnyObject>,
            let openSetupDeepLink = appSettings["OpenSetupDeepLink"] as? String else { return }
        
        if url.absoluteString.contains(openSetupDeepLink),
            let urlComponents = NSURLComponents(url: url, resolvingAgainstBaseURL: false),
            let ipQueryItem = urlComponents.queryItems?.first(where: { item in item.name == "ip" }),
            let ip = ipQueryItem.value {
            
            closeAnyOpenScreens { [weak self] in
                let speaker = Speaker(friendlyName: "",
                                      ipAddress: ip,
                                      wifiName: "",
                                      UDN: "und",
                                      color: "red")
                
                self?.provider.getDidSetupFromUserDataStoreInSpeaker(speaker)
                    .timeout(5, scheduler: MainScheduler.instance)
                    .catchErrorJustReturn(false)
                    .subscribe(onNext: { [weak self] didSetup in
                        if !didSetup {
                            self?.startSetup(speaker)
                        } else {
                            self?.showSpeakerFromReconnect(speaker, useOnlyIp: true)
                        }
                    }).disposed(by: (self?.disposeBag)!)
            }
        }
    }
    
    func closeAnyOpenScreens(with completion: () -> () ) {
        if let playerCoordinator = childCoordinators.find({ $0 is PlayerCoordinator }) as? PlayerCoordinator {
            playerCoordinator.stop()
            removeChildCoordinator(playerCoordinator)
        }
        else if let settingsCoordinator = childCoordinators.find({ $0 is SettingsCoordinator }) as? SettingsCoordinator {
            _ = self.rootViewController.dismiss(animated: true, completion: {})
            removeChildCoordinator(settingsCoordinator)
        }
        else if let setupCoordinator = childCoordinators.find({ $0 is SetupCoordinator }) as? SetupCoordinator {
            _ = self.rootViewController.dismiss(animated: true, completion: {})
            removeChildCoordinator(setupCoordinator)
        } else if let helpCoordinator = childCoordinators.find({ $0 is HelpCoordinator }) as? HelpCoordinator {
            _ = self.rootViewController.dismiss(animated: true, completion: {})
            removeChildCoordinator(helpCoordinator)
        }
        else if isShowingVolumeViewController {
            self.isShowingVolumeViewController = false
            if let _ = self.rootViewController.presentedViewController as? VolumeViewController {
                self.rootViewController.dismiss(animated: true, completion: {})
            }
        } else if let guestPlayerCoordinator = childCoordinators.find({ $0 is GuestPlayerCoordinator }) as? GuestPlayerCoordinator {
            guestPlayerCoordinator.stop()
            removeChildCoordinator(guestPlayerCoordinator)
        }
        completion()
    }
    
    func showSpeakerFromReconnect(_ speaker: Speaker, useOnlyIp : Bool = false) {
        var groupOfSpeaker: SpeakerGroup?
        var speakerToUse : Speaker?
        if useOnlyIp {
            for group in self.audioSystem.groups.value {
                for currentSpeaker in group.speakers {
                    if currentSpeaker.ipAddress == speaker.ipAddress {
                        groupOfSpeaker = group
                        speakerToUse = currentSpeaker
                        break
                    }
                }
            }
        } else {
            for group in self.audioSystem.groups.value {
                if group.speakers.contains(speaker) {
                    groupOfSpeaker = group
                    speakerToUse = speaker
                    break
                }
            }
        }
        
        guard let group = groupOfSpeaker,
            let masterSpeaker = group.masterSpeaker, let speakerClient = speakerToUse else {
                self.showHome()
                return
        }
        
        self.showReconnecting(forSpeaker: speakerClient)
        let getGroupState = Observable.combineLatest(provider.getClientStateForSpeaker(speakerClient),
                                                     provider.getMasterStateForSpeaker(masterSpeaker)) { (clientState: $0, masterState: $1) }
            .timeout(10.0, scheduler: MainScheduler.instance)
        
        getGroupState
            .subscribe(onNext: { [weak self] state in
                self?.showHome()
                self?.startPlayerForSpeaker(speakerClient,
                                            withClientState: state.clientState,
                                            masterSpeaker: masterSpeaker,
                                            masterState: state.masterState,
                                            animated: false) },
            onError: { [weak self] error in
                self?.lastConnectedSpeaker = nil
                self?.recentSpeakerStore.removeSpeaker(speakerClient)
                self?.updateHomeScreenQuickActionsWithRecentSpeakers()
                self?.showHome() })
            .disposed(by: disposeBag)
    }
    
    func showSpeakerForHomeSpeakerViewModel(_ speakerViewModel: HomeSpeakerViewModel, inGroupViewModel groupViewModel:HomeGroupViewModel) {
        if let loadingDisposable = loadingSpeakerDisposable {
            if openSpeaker != nil && openSpeaker!.speaker.mac != speakerViewModel.speaker.mac {
                
                loadingDisposable.dispose()
                loadingSpeakerDisposable = nil
                openSpeaker?.loading.value = false
                openSpeaker = nil
            }
        }
        
        if loadingSpeakerDisposable == nil {
            if let masterSpeaker = groupViewModel.group.masterSpeaker {
                speakerViewModel.loading.value = true
                openSpeaker = speakerViewModel
                
                provider.setTOSAcceptedForGroup(groupViewModel.group)
                    .subscribe()
                    .disposed(by: disposeBag)
                
                let selectedSpeaker = speakerViewModel.speaker
                let getGroupState = Observable.combineLatest(provider.getClientStateForSpeaker(selectedSpeaker),
                                                             provider.getMasterStateForSpeaker(masterSpeaker)) { (clientState: $0, masterState: $1) }
                    .timeout(10.0, scheduler: MainScheduler.instance)
                
                let start = CACurrentMediaTime()
                
                let getStateDisposable = getGroupState
                    .subscribe(onNext: { [weak self] state in
                        let end = CACurrentMediaTime()
                        let duration = end-start
                        
                        self?.logSuccessfulConnectionWithDuration(duration)
                        self?.startPlayerForSpeaker(selectedSpeaker, withClientState: state.clientState, masterSpeaker: masterSpeaker, masterState: state.masterState)
                        },
                               onError: { [weak self] error in
                                Answers.logCustomEvent(withName: "ConnectedFromHomeScreen",
                                                       customAttributes:
                                    ["Status":"Failed"])
                                print("get speaker state error \(error)")
                                speakerViewModel.loading.value = false
                                self?.openSpeaker = nil
                                self?.loadingSpeakerDisposable = nil
                        },
                               onCompleted: { [weak self] in
                                speakerViewModel.loading.value = false
                                self?.loadingSpeakerDisposable = nil
                    })
                
                loadingSpeakerDisposable = getStateDisposable
                loadingSpeakerDisposable?.disposed(by: disposeBag)
            }
        }
    }
    
    func updateHomeScreenQuickActionsWithRecentSpeakers() {
        var shortcutItems = [UIApplicationShortcutItem]()
        
        for speaker in recentSpeakerStore.speakers {
            let type = "com.zoundindustries.marshall.speaker"
            
            let color = ExternalConfig.sharedInstance.speakerImageForColor(speaker.color)?.displayColor ?? ""
            let model = ExternalConfig.sharedInstance.speakerImageForColor(speaker.color)?.displayModel ?? ""
            var speakerCompleteName: String? = "\(model) \(color)"
            if speakerCompleteName == speaker.friendlyName {
                speakerCompleteName = nil
            }
            
            if speakerCompleteName != nil && speakerCompleteName!.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
                speakerCompleteName = nil
            }
            
            let speakerJSON = speaker.toJSON()
            let item = UIApplicationShortcutItem(type: type, localizedTitle: speaker.friendlyName,
                                                 localizedSubtitle: speakerCompleteName,
                                                 icon: UIApplicationShortcutIcon(templateImageName: "mini_volume_speaker_icon"),
                                                 userInfo: speakerJSON as? [String : NSSecureCoding])
            
            shortcutItems.append(item)
        }
        
        UIApplication.shared.shortcutItems = shortcutItems
    }
    
    func startPlayerForSpeaker(_ speaker: Speaker, withClientState clientState: GroupClientState, masterSpeaker: Speaker,  masterState: GroupMasterState, animated: Bool = true) {
        guard let navController = self.navController else { return }
        
        let clientNotifier = SpeakerNotifier(speaker: speaker, provider: provider)
        
        self.lastConnectedSpeaker = speaker
        self.recentSpeakerStore.storeSpeaker(speaker)
        updateHomeScreenQuickActionsWithRecentSpeakers()
        
        let speakerConnectionManager = SpeakerConnectionManager(clientSpeaker: speaker,
                                                                clientNotifier: clientNotifier,
                                                                clientState: clientState,
                                                                masterSpeaker: masterSpeaker,
                                                                masterState: masterState,
                                                                audioSystem: audioSystem,
                                                                speakerProvider: provider)
        
        let playerCoordinator : PlayerCoordinator
        if let connectable = ExternalConfig.sharedInstance.speakerImageForColor(speaker.color)?.connectable, connectable == true {
            playerCoordinator = PlayerCoordinator(connectionManager: speakerConnectionManager, navigationController: navController)
            playerCoordinator.delegate = self
            
            print("old coordinator")
        } else {
            print("new coordinator")
            playerCoordinator = GuestPlayerCoordinator(connectionManager: speakerConnectionManager, navigationController: navController)
            playerCoordinator.delegate = self
        }
        
        self.childCoordinators.append(playerCoordinator)
        playerCoordinator.start(animated: animated)
        
        if #available(iOS 10.0, *) {
            let feedbackGenerator = UIImpactFeedbackGenerator(style: .medium)
            feedbackGenerator.impactOccurred()
        }
    }
    
    func showVolumesFromViewCotroller(_ viewController: UIViewController) {
        provider.groupVolumesForGroups(self.audioSystem.groups.value, knownVolumes: audioSystem.volumeInfo.value)
            .subscribe(onNext: { [weak self] groupVolumes in
                guard let `self` = self else { return }
                let volume = UIStoryboard.main.volumeViewController
                viewController.definesPresentationContext = true
                
                if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.phone {
                    volume.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                } else {
                    volume.modalPresentationStyle = UIModalPresentationStyle.formSheet
                }
                
                volume.delegate = self
                volume.viewModel = VolumeViewModel(groupVolumes:groupVolumes,  audioSystem: self.audioSystem, speakerProvider: self.provider)
                self.isShowingVolumeViewController = true
                viewController.present(volume, animated: true, completion: nil)
            }).disposed(by: disposeBag)
    }
    
    func startSetup(_ speaker: Speaker?) {
        if wacService == nil {
            wacService = WACService()
        }
        
        let setupCoordinator = SetupCoordinator(displayViewController: rootViewController, moyaProvider: provider, audioSystem: audioSystem, wacService:  wacService!)
        setupCoordinator.delegate = self
        self.childCoordinators.append(setupCoordinator)
        setupCoordinator.start(speaker)
    }
    
    func startSettings() {
        let settingsCoordinator = SettingsCoordinator(viewController: rootViewController, audioSystem: audioSystem, provider: provider, discoveryService: discoveryService)
        settingsCoordinator.delegate = self
        self.childCoordinators.append(settingsCoordinator)
        settingsCoordinator.start()
    }
    
    func startSettings(forSpeaker speaker:Speaker) {
        let settingsCoordinator = SettingsCoordinator(viewController: rootViewController, audioSystem: audioSystem, provider: provider, discoveryService: discoveryService)
        settingsCoordinator.delegate = self
        self.childCoordinators.append(settingsCoordinator)
        settingsCoordinator.startForSpeaker(speaker)
    }
    
    func showAbout() {
        let aboutCoordinator = AboutCoordinator(parentViewController: rootViewController)
        aboutCoordinator.delegate = self
        self.childCoordinators.append(aboutCoordinator)
        aboutCoordinator.start()
    }
    
    func showHelp() {
        let helpCoordinator = HelpCoordinator(parentViewController: rootViewController)
        helpCoordinator.delegate = self
        self.childCoordinators.append(helpCoordinator)
        helpCoordinator.start()
        
    }
    
    func showBrowserForURL(_ url: URL) {
        guard let rootVC = self.rootViewController as? RootViewController else { return }
        
        if #available(iOS 9, *) {
            let safari = SFSafariViewController(url: url)
            safari.modalPresentationStyle = .overFullScreen
            safari.modalPresentationCapturesStatusBarAppearance = true
            rootVC.present(safari, animated: true, completion: nil)
        } else {
            let webViewController = STKWebKitModalViewController(url: url)
            rootVC.present(webViewController!, animated: true, completion: nil)
        }
    }
    
    func toggleMenu() {
        guard let rootVC = self.rootViewController as? RootViewController else { return }
        
        if(rootVC.menuVisible) {
            rootVC.hideMenuViewController(with: {})
        } else {
            rootVC.showMenuViewController(with: {})
        }
    }
    
    func removeChildCoordinator(_ element: Coordinator) {
        childCoordinators = childCoordinators.filter() { $0 !== element }
    }
}

extension AppCoordinator: DiscoveryServiceDelegate {
    func discoveryServiceDidStartSearching() {
        startInitialScan()
    }
    
    func discoveryServiceDidFinishConfirmingKnownSpeakers() {
        guard let startTime = self.lastRefreshTriggerDate else {
            endRefreshing()
            return
        }
        
        let confirmDuration = abs(startTime.timeIntervalSince(Date()))
        
        if confirmDuration > 2.0 {
            endRefreshing()
        } else {
            runAfterDelay(2.0 - confirmDuration, block: { [weak self] in
                self?.endRefreshing()
            })
        }
    }
    
    func endRefreshing() {
        guard let homeViewController = navController?.viewControllers.first as? HomeViewController,
            let tableView = homeViewController.tableView else { return }
        tableView.endRefreshing(at: .top)
    }
    
    func showUnlockedModuleAlert(forSpeakerViewModel speakerViewModel:HomeSpeakerViewModel, inGroupViewModel groupViewModel: HomeGroupViewModel) {
        guard  let rootVC = self.rootViewController as? RootViewController else { return }
        let unlockedController = UIStoryboard.main.unlockedAlertViewController
        rootVC.definesPresentationContext = true
        
        unlockedController.providesPresentationContextTransitionStyle = true
        unlockedController.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        unlockedController.delegate = self
        unlockedController.speakerViewModel = speakerViewModel
        unlockedController.groupViewModel = groupViewModel
        
        rootVC.present(unlockedController, animated: true, completion: nil)
    }
    
    func showSpeakerUpadateAvailable(forSpeakerViewModel speakerViewModel:HomeSpeakerViewModel, inGroupViewModel groupViewModel: HomeGroupViewModel) {
        guard let rootVC = self.rootViewController as? RootViewController else { return }
        let updateAvailableController = UIStoryboard.main.updateAvailableAlertViewController
        rootVC.definesPresentationContext = true
        
        updateAvailableController.providesPresentationContextTransitionStyle = true
        updateAvailableController.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        updateAvailableController.delegate = self
        updateAvailableController.speakerViewModel = speakerViewModel
        updateAvailableController.groupViewModel = groupViewModel
        
        rootVC.present(updateAvailableController, animated: true, completion: nil)
    }
}

extension AppCoordinator: AudioSystemDelegate {
    var delegateIdentifier: String {
        return "app_coordinator"
    }
    
    func audioSystemDidMakeChanges() {
        updateForCurrentGroups(self.audioSystem.groups.value)
    }
}

extension AppCoordinator: NoSpeakersViewControllerDelegate {
    func noSpeakersViewControllerDidRequestRefresh(_ noSpeakersViewController: NoSpeakersViewController) {
        self.refreshDiscovery(clear: true)
    }
    
    func noSpeakersViewControllerDidRequestMenu(_ noSpeakersViewController: NoSpeakersViewController) {
        toggleMenu()
    }
    
    func noSpeakersViewControllerDidRequestSetup(_ noSpeakersViewController: NoSpeakersViewController) {
        startSetup(nil)
    }
}

extension AppCoordinator: NoWiFiViewControllerDelegate {
    func noWifiViewControllerDidRequestMenu(_ noWifiViewController: UIViewController) {
        toggleMenu()
    }
}

extension AppCoordinator: WelcomeViewControllerDelegate {
    func welcomeViewControllerDidAccept(_ welcomeViewController: WelcomeViewController) {
        UserDefaults.standard.set(true, forKey: termsAcceptedKey)
    }
}

extension AppCoordinator: HomeViewControllerDelegate {
    func homeViewControllerDidRequestSettings(_ homeViewController: HomeViewController, didSelectSpeakerViewModel: HomeSpeakerViewModel, fromGroupViewModel: HomeGroupViewModel) {
        startSettings(forSpeaker: didSelectSpeakerViewModel.speaker)
    }
    
    func homeViewController(_ homeViewController: HomeViewController, didRequestChangeMultiTo multi: Bool, inSpeakerViewModel speakerViewModel: HomeSpeakerViewModel, fromGroupViewModel groupViewModel: HomeGroupViewModel) {
        
        var speakersInMulti = 0
        if let multiGroup = audioSystem.groups.value.find({$0.isMulti}) {
            speakersInMulti = multiGroup.speakers.count
        }
        
        if speakersInMulti >= 5 && multi == true {
            showGroupFullInfoAlert()
            speakerViewModel.multi.value = HomeSpeakerMultiState.solo
        } else if speakerViewModel.isPlayingCast.value == true && multi == true {
            showCastSwitchAlert(forSpeakerViewModel: speakerViewModel)
        } else {
            audioSystem.setMultiroomOn(multi, forSpeaker: speakerViewModel.speaker)
            //speakerViewModel.multi.value = multi ? .multi : .solo
        }
    }
    
    func homeViewControllerDidRequestFactoryReset(_ homeViewController: HomeViewController, didSelectSpeakerViewModel speakerViewModel: HomeSpeakerViewModel, fromGroupViewModel: HomeGroupViewModel) {
        
        guard factoryResetsSpeakersOnTwoFingerLongPress else { return }
        
        provider.setNode(ScalarNode.FactoryReset, value: "1", forSpeaker: speakerViewModel.speaker)
            .subscribe()
            .disposed(by: disposeBag)
        
        runAfterDelay(3.0, block: { [weak self] in
            self?.discoveryService.confirmDeviceAtIP(speakerViewModel.speaker.ipAddress, removeImmediatelyIfFail: true)
        })
    }
    
    func homeViewControllerDidRequestSetup(_ homeViewController: HomeViewController, didSelectSpeakerViewModel speakerViewModel: HomeSpeakerViewModel, fromGroupViewModel: HomeGroupViewModel) {
        if setupSpeakerOnLongPress {
            self.startSetup(speakerViewModel.speaker)
        }
    }
    
    func homeViewController(_ homeViewController: HomeViewController, didSelectSpeakerViewModel speakerViewModel: HomeSpeakerViewModel, fromGroupViewModel groupViewModel: HomeGroupViewModel) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let audioSystem = appDelegate.appCoordinator.audioSystem
        let updateAvailable = audioSystem.isUpdateAvailable(for: speakerViewModel.speaker)
        
        if updateAvailable {
            checkAndShowWarningMessageAndShowSpeaker(forSpeakerViewModel: speakerViewModel, groupViewModel: groupViewModel, messageType: .updateMessage)
            return
        }
        
        if speakerViewModel.isUnlocked.value == true {
            
            checkAndShowWarningMessageAndShowSpeaker(forSpeakerViewModel: speakerViewModel, groupViewModel: groupViewModel, messageType: .unlockedMessage)
        } else {
            showSpeakerForHomeSpeakerViewModel(speakerViewModel, inGroupViewModel: groupViewModel)
        }
    }
    
    func checkAndShowWarningMessageAndShowSpeaker(forSpeakerViewModel speakerViewModel: HomeSpeakerViewModel, groupViewModel: HomeGroupViewModel, messageType : WarningMessageType) {
        let checkLastMessageShowDateFunction : (String) -> Date? = (messageType == WarningMessageType.unlockedMessage ? getLastCheckUnlockedMessageDate : getLastCheckUpdateMessageDate)
        let storeCheckMessageDateFunction : (String, Date) -> Void = (messageType == WarningMessageType.unlockedMessage ? storeUnlockMessageLastShownDate : storeUpdateMessageLastShownDate)
        let showAlertFuncion : (HomeSpeakerViewModel,HomeGroupViewModel) -> Void = (messageType == WarningMessageType.unlockedMessage ? showUnlockedModuleAlert : showSpeakerUpadateAvailable)
        let lastAlertDisplayDate = checkLastMessageShowDateFunction(speakerViewModel.speaker.mac)
        
        if let lastCheckDate = lastAlertDisplayDate {
            let today = Date()
            let daysSince = today.since(lastCheckDate, in: .day)
            
            if daysSince >= 7 {
                // update the saved date
                storeCheckMessageDateFunction(speakerViewModel.speaker.mac, Date())
                showAlertFuncion(speakerViewModel, groupViewModel)
            } else {
                showSpeakerForHomeSpeakerViewModel(speakerViewModel, inGroupViewModel: groupViewModel)
            }
        } else {
            storeCheckMessageDateFunction(speakerViewModel.speaker.mac, Date())
            showAlertFuncion(speakerViewModel, groupViewModel)
        }
    }
    
    func storeUnlockMessageLastShownDate(forSpeakerMac speakerMac:String, dateChecked: Date)  {
        let storedSpeakerKey = unlockedMacPrefixForUserStore + speakerMac
        UserDefaults.standard.set(dateChecked, forKey: storedSpeakerKey)
    }
    
    func getLastCheckUnlockedMessageDate(forSpreakerMac speakerMac:String) -> Date? {
        let storedSpeakerKey = unlockedMacPrefixForUserStore + speakerMac
        return UserDefaults.standard.object(forKey: storedSpeakerKey) as? Date
    }
    
    func storeUpdateMessageLastShownDate(forSpeakerMac speakerMac:String, dateChecked: Date)  {
        let storedSpeakerKey = updateMacPrefixForUserStore + speakerMac
        UserDefaults.standard.set(dateChecked, forKey: storedSpeakerKey)
    }
    
    func getLastCheckUpdateMessageDate(forSpreakerMac speakerMac:String) -> Date? {
        let storedSpeakerKey = updateMacPrefixForUserStore + speakerMac
        return UserDefaults.standard.object(forKey: storedSpeakerKey) as? Date
    }
    
    func homeViewControllerDidRequestVolumeControls(_ homeViewController: HomeViewController) {
        showVolumesFromViewCotroller(self.rootViewController)
    }
    
    func homeViewControllerDidRequestMenuToggle(_ homeViewController: HomeViewController) {
        toggleMenu()
    }
    
    func homeViewControllerDidRequestRefresh(_ homeViewController: HomeViewController) {
        self.refreshDiscovery(clear: false)
    }
}

extension AppCoordinator: VolumeViewControllerDelegate {
    func volumeViewControllerDidRequestClose(_ volumeViewController: VolumeViewController) {
        self.isShowingVolumeViewController = false
        if let vc = volumeViewController.presentingViewController {
            vc.dismiss(animated: true, completion: nil)
        }
    }
}

extension AppCoordinator: AboutCoordinatorDelegate {
    func aboutCoordinatorDidFinishCoordinating(_ coordinator: AboutCoordinator) {
        removeChildCoordinator(coordinator)
        rootViewController.dismiss(animated: true, completion: {})
    }
}

extension AppCoordinator: CastSwitchAlertViewControllerDelegate {
    func castSwitchAlertDidRequestContinue(_ alertViewController: CastSwitchAlertViewController) {
        guard let vc = alertViewController.presentingViewController else { return }
        
        let speakerViewModel = alertViewController.speakerViewModel
        vc.dismiss(animated: true, completion: { [weak self] in
            if let speaker = speakerViewModel?.speaker {
                DispatchQueue.main.async {
                    self?.audioSystem.setMultiroomOn(true, forSpeaker: speaker)
                }
            }
        })
    }
    
    func castSwitchAlertDidRequestCancel(_ alertViewController: CastSwitchAlertViewController) {
        guard let vc = alertViewController.presentingViewController else { return }
        let speakerViewModel = alertViewController.speakerViewModel
        vc.dismiss(animated: true, completion: {
            speakerViewModel?.multi.value = .solo
        })
    }
}

extension AppCoordinator: CastInfoAlertViewControllerDelegate {
    func castInfoAlertDidRequestContinue(_ alertViewController: CastInfoAlertViewController) {
        if let vc = alertViewController.presentingViewController {
            vc.dismiss(animated: true, completion: nil)
        }
    }
    
    func castInfoAlertDidRequestLearnMore(_ alertViewController: CastInfoAlertViewController) {
        if let vc = alertViewController.presentingViewController {
            vc.dismiss(animated: true, completion: nil)
        }
    }
    
    func castInfoAlertDidRequestClose(_ alertViewController: CastInfoAlertViewController) {
        if let vc = alertViewController.presentingViewController {
            vc.dismiss(animated: true, completion: nil)
        }
    }
}

extension AppCoordinator: GroupFullInfoAlertViewControllerDelegate {
    func groupFullInfoAlertDidRequestDismiss(_ alertViewController: GroupFullInfoAlertViewController) {
        if let vc = alertViewController.presentingViewController {
            vc.dismiss(animated: true, completion: nil)
        }
    }
}

extension AppCoordinator: MenuViewControllerDelegate {
    func didSelectMenuItem(_ menuItem: MenuItem, menuViewController: MenuViewController) {
        guard let rootVC = self.rootViewController as? RootViewController else { return }
        
        var completion: () -> Void
        
        switch menuItem.type {
        case .addSpeaker:
            completion = { [weak self] in self?.startSetup(nil) }
        case .shop:
            completion = { [weak self] in self?.showBrowserForURL(GlobalConfig.officialWebsite) }
        case .help:
            completion = { [weak self] in self?.showHelp() }
        case .about:
            completion = { [weak self] in self?.showAbout() }
        case .settings:
            completion = { [weak self] in self?.startSettings() }
        }
        
        rootVC.hideMenuViewController(with: completion)
    }
    
    func didCloseMenu(menuViewController: MenuViewController) {
        self.toggleMenu()
    }
}

extension AppCoordinator: VolumeViewModelDelegate {
    //here we forward the state of the speaker volumes that change in the volume view for which the speaker does not notify
    func volumeViewModelDidSetVolumeForGroup(_ group: SpeakerGroup, volume: GroupVolume) {
        guard let playerCoordinator = childCoordinators.find({ $0 is PlayerCoordinator }) as? PlayerCoordinator else { return }
        
        if let volumeChangedGroupMaster = group.masterSpeaker, volumeChangedGroupMaster == playerCoordinator.connectionManager.masterSpeaker {
            playerCoordinator.connectionManager.masterState.value?.masterVolume = volume.masterVolume
        }
        
        if let speakerVolume = volume.clientsVolumes?[playerCoordinator.connectionManager.clientSpeaker] {
            playerCoordinator.connectionManager.clientState.value?.volume = speakerVolume.volume
        }
    }
}

extension AppCoordinator: PlayerCoordinatorDelegate {
    func playerCoordinatorDidRequestVolumeFromViewController(_ viewController: UIViewController) {
        showVolumesFromViewCotroller(viewController)
    }
    
    func playerCoordinatorDidFinishCoordinating(_ coordinator: PlayerCoordinator) {
        openSpeaker = nil
        
        if let masterState = coordinator.connectionManager.masterState.value, let clientState = coordinator.connectionManager.clientState.value {
            let nowPlayingState = NowPlayingState(modeIndex: clientState.modeIndex,
                                                  presetIndex: clientState.presetIndex,
                                                  playStatus: masterState.playStatus,
                                                  modes: clientState.modes)
            
            updateNowPlayingInfo(nowPlayingState, forSpeaker: coordinator.connectionManager.clientSpeaker)
        }
        
        lastConnectedSpeaker = nil
        coordinator.stop()
        removeChildCoordinator(coordinator)
        
        if coordinator is GuestPlayerCoordinator {
            self.navController?.dismiss(animated: true, completion: nil)
            (self.rootViewController as? RootViewController)?.forcedHiddenStatusBar = false
        } else {
            _ = self.navController?.popToRootViewController(animated: true)
            (self.rootViewController as? RootViewController)?.forcedHiddenStatusBar = false
        }
    }
    
    func playerCoordinatorDidStartControllingVolume(_ coordinator: PlayerCoordinator) {
        (self.rootViewController as? RootViewController)?.forcedHiddenStatusBar = true
    }
    
    func playerCoordinatorDidFinishControllingVolume(_ coordinator: PlayerCoordinator) {
        (self.rootViewController as? RootViewController)?.forcedHiddenStatusBar = false
    }
}

extension AppCoordinator: SettingsCoordinatorDelegate {
    func settingsCoordinatorDidFinishCoordinating(_ coordinator: SettingsCoordinator) {
        removeChildCoordinator(coordinator)
        _ = self.rootViewController.dismiss(animated: true, completion: {})
    }
}

extension AppCoordinator: SetupCoordinatorDelegate {
    func setupCoordinatorDidFinishSetup(_ coordinator: SetupCoordinator) {
        removeChildCoordinator(coordinator)
        rootViewController.dismiss(animated: true, completion: {})
        //audioSystem.groupUpdatesActive = true
    }
    
    func setupCoordinatorDidCancelSetup(_ coordinator: SetupCoordinator) {
        removeChildCoordinator(coordinator)
        rootViewController.dismiss(animated: true, completion: {})
        //audioSystem.groupUpdatesActive = true
    }
}

extension AppCoordinator: HelpCoordinatorDelegate {
    func helpCoordinatorDidFinishCoordinating(_ coordinator: HelpCoordinator) {
        removeChildCoordinator(coordinator)
        rootViewController.dismiss(animated: true, completion: {})
    }
}

extension AppCoordinator : UnlockedViewControllerDelegate {
    func didAcknowledgeUnlockedModuleWarning(_ alertViewController: UnlockedAlertViewController) {
        guard let vc = alertViewController.presentingViewController else { return }
        
        let speakerViewModel = alertViewController.speakerViewModel
        let groupViewModel = alertViewController.groupViewModel
        vc.dismiss(animated: true, completion: { [weak self] in
            DispatchQueue.main.async {
                // continue connecting to speaker
                self?.showSpeakerForHomeSpeakerViewModel(speakerViewModel!, inGroupViewModel: groupViewModel!)
            }
        })
    }
}

extension AppCoordinator: UpdateAvailableAlertViewControllerDelegate {
    func didAcknowledgeUpdateAvailabilityWarning(_ alertViewController : UpdateAvailableAlertViewController) {
        guard let vc = alertViewController.presentingViewController else { return }
        
        let speakerViewModel = alertViewController.speakerViewModel
        let groupViewModel = alertViewController.groupViewModel
        vc.dismiss(animated: true, completion: { [weak self] in
            DispatchQueue.main.async {
                // continue connecting to speaker
                self?.showSpeakerForHomeSpeakerViewModel(speakerViewModel!, inGroupViewModel: groupViewModel!)
            }
        })
    }
}

