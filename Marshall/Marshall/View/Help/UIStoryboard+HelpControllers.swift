//
//  UIStoryboard+Controllers.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 24/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//
import UIKit

extension UIStoryboard {
    static var help: UIStoryboard {
        return UIStoryboard(name: "Help", bundle: nil)
    }
}

extension UIStoryboard {
    var helpViewController: HelpViewController {
        guard let vc = UIStoryboard.help.instantiateViewController(withIdentifier: "help") as? HelpViewController else {
            fatalError("HelpViewController couldn't be found in Storyboard file")
        }
        return vc
    }
    
    var quickGuideViewController: QuickGuideViewController {
        guard let vc = UIStoryboard.help.instantiateViewController(withIdentifier: "quickGuide") as? QuickGuideViewController else {
            fatalError("QuickGuideViewController couldn't be found in Storyboard file")
        }
        return vc
    }
    
    var localWebViewController: LocalWebViewController {
        guard let vc = UIStoryboard.help.instantiateViewController(withIdentifier: "localWeb") as? LocalWebViewController else {
            fatalError("LocalWebViewController couldn't be found in Storyboard file")
        }
        return vc
    }
}
