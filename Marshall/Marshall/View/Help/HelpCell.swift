//
//  SettingsSettingCell.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 16/05/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

class HelpItemCell: UITableViewCell {
    @IBOutlet weak var settingLabel: UILabel!
    
    let helpItemVariable: Variable<HelpItem?> = Variable(nil)
    var helpItem: HelpItem? {
        get { return helpItemVariable.value }
        set { helpItemVariable.value = newValue }
    }
    
    override func awakeFromNib() {
        self.contentView.backgroundColor = UIColor.clear
        self.backgroundColor = UIColor.clear
        
        settingLabel.font = Fonts.UrbanEars.Regular(17)
        
        let customColorView = UIView()
        customColorView.backgroundColor = UIColor(white: 0.0, alpha: 0.24)
        self.selectedBackgroundView =  customColorView
        
        helpItemVariable.asObservable()
            .map { $0 != nil ?  $0!.localizedDisplayName : "" }
            .bind(to: settingLabel.rx.text)
            .disposed(by: rx_disposeBag)
    }
}
