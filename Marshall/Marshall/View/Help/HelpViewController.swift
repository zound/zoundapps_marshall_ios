//
//  AboutViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 09/09/2016.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

protocol HelpViewControllerDelegate: class {
    func helpViewControllerDidRequestBack(_ helpViewController: HelpViewController)
    func helpViewControllerDidRequestHelpItem(_ helpItem: HelpItem, _ helpViewController: HelpViewController)
}

enum HelpItem {
    case quickGuide
    case onlineManual
    case contact
    
    var localizedDisplayName: String {
        switch  self {
        case .quickGuide: return Localizations.Help.MenuItem.QuickGuide
        case .onlineManual: return Localizations.Help.MenuItem.OnlineManual
        case .contact: return Localizations.Help.MenuItem.Contact
        }
    }
}

class HelpViewController: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableTopSeparatorHeight: NSLayoutConstraint!
    @IBOutlet weak var separatorView: UIView!

    weak var delegate: HelpViewControllerDelegate?
    
    let menuItems: [HelpItem]
    let disposeBag = DisposeBag()
    
    required init?(coder aDecoder: NSCoder) {
        self.menuItems = [.quickGuide, .onlineManual, .contact]
        super.init(coder: aDecoder)
    }
    
    deinit {
        print(#function, String(describing: self))
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    override func viewDidLoad() {
        titleLabel.attributedText = Fonts.PageTitleFont.AttributedPageTitleWithString(Localizations.Help.Title.uppercased())
        self.tableTopSeparatorHeight.constant = 0.5
        
        self.tableView.backgroundColor = UIColor.clear
        self.tableView.separatorStyle = .none
        
        updateSeparatorViewVisibility()
        setupTableView()
    }
    
    @IBAction func onBack(_ sender: AnyObject) {
        self.delegate?.helpViewControllerDidRequestBack(self)
    }
}

// MARK: - Rx bindings for UITableView
extension HelpViewController {
    func setupTableView() {
        Observable.just(menuItems)
            .bind(to: tableView.rx.items(cellIdentifier: "helpItem", cellType: HelpItemCell.self)) { (row, element, cell) in
                cell.helpItem = element }
            .disposed(by: disposeBag)
        
        tableView.rx.itemSelected
            .debounce(0.1, scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] indexPath in
                guard let `self` = self else { return }
                
                let menuItem = self.menuItems[(indexPath as NSIndexPath).row]
                self.delegate?.helpViewControllerDidRequestHelpItem(menuItem, self)
                self.tableView.deselectRow(at: indexPath, animated: true) })
            .disposed(by: disposeBag)
        
        tableView.rx.didScroll
            .subscribe(onNext: { [weak self] _ in
                self?.updateSeparatorViewVisibility() })
            .disposed(by: disposeBag)

    }
}

private extension HelpViewController {
    func updateSeparatorViewVisibility() {
        separatorView.alpha = max(0, min(1.0, tableView.contentOffset.y / 22.0))
    }
}
