//
//  OnlineManualViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 16/02/2017.
//  Copyright © 2017 Zound Industries. All rights reserved.
//

import Foundation
import UIKit

protocol OnlineManualViewControllerDelegate: class {
    func onlineManualiewControllerDidRequestBack(_ contactViewController: OnlineManualViewController)
    func onlineManualiewControllerDidRequestGotoWebsite(_ website: String, contactViewController: OnlineManualViewController)
}

class OnlineManualViewController: UIViewController {
    weak var delegate: OnlineManualViewControllerDelegate?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var gotoWebsiteButton: UIButton!
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    override func viewDidLoad() {
        titleLabel.attributedText = Fonts.PageTitleFont.AttributedPageTitleWithString(Localizations.Help.OnlineManual.Title.uppercased())
        
        contentLabel.text = Localizations.Help.OnlineManualContent
        contentLabel.font = Fonts.MainContentFont
        
        
        UIView.setAnimationsEnabled(false)
        gotoWebsiteButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.Appwide.GoToWebsite), for: .normal)
        
        gotoWebsiteButton.layoutIfNeeded()
        UIView.setAnimationsEnabled(true)
    }
    
    @IBAction func onGotoWebsite(_ sender: Any) {
        self.delegate?.onlineManualiewControllerDidRequestGotoWebsite(Localizations.Help.OnlineManual.Website, contactViewController: self)
    }
    
    @IBAction func onBack(_ sender: Any) {
        self.delegate?.onlineManualiewControllerDidRequestBack(self)
    }
}
