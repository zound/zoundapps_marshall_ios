//
//  HelpCoordinator.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 25/10/2016.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import STKWebKitViewController
import SafariServices
import MessageUI
import ZoundCommon

protocol HelpCoordinatorDelegate: class {
    func helpCoordinatorDidFinishCoordinating(_ coordinator: HelpCoordinator)
}

class HelpCoordinator: NSObject, Coordinator {
    weak var delegate: HelpCoordinatorDelegate?
    
    var navController: UINavigationController!
    let parentViewController: UIViewController
    
    init(parentViewController: UIViewController) {
        self.parentViewController = parentViewController
    }
    
    func start() {
        self.navController = UENavigationController()
        self.navController.navigationBar.isHidden = true
        self.navController.modalPresentationStyle = .formSheet
        
        showHelpRoot(animated: false)
        self.parentViewController.present(navController, animated: true, completion: nil)
    }
    
    func showHelpRoot(animated: Bool = true) {
        let helpController = UIStoryboard.main.helpViewController
        helpController.delegate = self
        helpController.modalPresentationStyle = .formSheet
        navController.pushViewController(helpController, animated: animated)
    }
    
    func showQuickGuide(animated: Bool = true) {
        let quickGuide = UIStoryboard.main.quickGuideViewController
        quickGuide.delegate = self
        navController.pushViewController(quickGuide, animated: animated)
    }
    
    func showOnlineManual(animated: Bool = true) {
        let onlineManual = UIStoryboard.main.onlineManualViewController
        onlineManual.delegate = self
        navController.pushViewController(onlineManual, animated: animated)
    }
    
    func showContact(animated: Bool = true) {
        let contact = UIStoryboard.main.contactViewController
        contact.delegate = self
        navController.pushViewController(contact, animated: animated)
    }
    
    func showHelpWebPage(withURL url: URL, animated: Bool = true) {
        let localWeb = UIStoryboard.main.localWebViewController
        localWeb.delegate = self
        localWeb.localPageURL = url
        
        self.navController.pushViewController(localWeb, animated: true)
    }
    
    func showBrowserForURL(_ url: URL) {
        if let rootVC = self.navController {
            if #available(iOS 9, *) {
                let safari = SFSafariViewController(url: url)
                safari.modalPresentationStyle = .overFullScreen
                safari.modalPresentationCapturesStatusBarAppearance = true
                rootVC.present(safari, animated: true, completion: nil)
            } else {
                let webViewController = STKWebKitModalViewController(url: url)
                rootVC.present(webViewController!, animated: true, completion: nil)
            }
        }
    }
}

extension HelpCoordinator: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}

extension HelpCoordinator: ContactViewControllerDelegate {
    func contactViewControllerDidRequestBack(_ contactViewController: ContactViewController) {
        _ = navController?.popViewController(animated: true)
    }
    
    func contactViewControllerDidRequestGotoWebsite(_ website: String, contactViewController: ContactViewController) {
        guard let url = URL(string: website) else { return }
        showBrowserForURL(url)
    }
    
    func contactViewControllerDidRequestGotoSupportWebsite(_ supportWebsite: String, contactViewController: ContactViewController) {
        guard let url = URL(string: supportWebsite) else { return }
        showBrowserForURL(url)
    }
}

extension HelpCoordinator:OnlineManualViewControllerDelegate {
    func onlineManualiewControllerDidRequestBack(_ contactViewController: OnlineManualViewController) {
        _ = navController?.popViewController(animated: true)
    }
    
    func onlineManualiewControllerDidRequestGotoWebsite(_ website: String, contactViewController: OnlineManualViewController) {
        guard let url = URL(string: website) else { return }
        showBrowserForURL(url)
    }
}

extension HelpCoordinator: LocalWebViewControllerDelegate {
    func localWebViewControllerDidRequestBack(_ localWebViewController: LocalWebViewController) {
        _ = navController?.popViewController(animated: true)
    }
    
    func localWebViewControllerDidRequestLoadPageNamed(_ pageName: String, localWebViewController: LocalWebViewController) {
        var subdirectory = "QuickGuide/en"
        if let language = NSLocale.current.languageCode {
            subdirectory = "QuickGuide/\(language)"
        }
        
        if let pageURL = Bundle.main.url(forResource: pageName, withExtension: "html", subdirectory: subdirectory) {
            self.showHelpWebPage(withURL: pageURL)
        } else {
            if let pageURL = Bundle.main.url(forResource: pageName, withExtension: "html", subdirectory: "QuickGuide/en") {
                self.showHelpWebPage(withURL: pageURL)
            }
        }
    }
}

extension HelpCoordinator: QuickGuideViewControllerDelegate {
    func quickGuideViewControllerDidRequestBack(_ quickGuideViewController: QuickGuideViewController) {
        _ = navController?.popViewController(animated: true)
    }
    
    func quickGuideViewControllerDidRequestQuickGuideItem(_ guickGuideItem: QuickGuideItem, _ quickGuideViewController: QuickGuideViewController) {
        guard let localWebPageURL = guickGuideItem.localWebPageURL else { return }
        showHelpWebPage(withURL: localWebPageURL)
    }
}


extension HelpCoordinator: HelpViewControllerDelegate {
    func helpViewControllerDidRequestBack(_ helpViewController: HelpViewController) {
        self.delegate?.helpCoordinatorDidFinishCoordinating(self)
    }
    
    func helpViewControllerDidRequestHelpItem(_ helpItem: HelpItem, _ helpViewController: HelpViewController) {
        switch helpItem {
        case .contact:
            showContact()
        case .onlineManual:
            showOnlineManual()
        case .quickGuide:
            showQuickGuide()
        }
    }
}
