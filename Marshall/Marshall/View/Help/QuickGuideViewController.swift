//
//  AboutViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 09/09/2016.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

protocol QuickGuideViewControllerDelegate: class {
    func quickGuideViewControllerDidRequestBack(_ quickGuideViewController: QuickGuideViewController)
    func quickGuideViewControllerDidRequestQuickGuideItem(_ guickGuideItem: QuickGuideItem, _ quickGuideViewController: QuickGuideViewController)
}

enum QuickGuideItem {
    case speakerKnobs
    case presets
    case soloMulti
    
    var localizedDisplayName: String {
        switch  self {
        case .speakerKnobs: return Localizations.Help.QuickGuide.MenuItem.SpeakerKnobs
        case .presets: return Localizations.Help.QuickGuide.MenuItem.Presets
        case .soloMulti: return Localizations.Help.QuickGuide.MenuItem.SoloMulti
        }
    }
    
    var localWebPageURL: URL? {
        var pageName: String? = nil
        
        switch self {
        case .speakerKnobs: pageName = "speaker_knobs"
        case .presets: pageName = "presets"
        case .soloMulti: pageName = "solo_multi"
        }
        
        var subdirectory = "QuickGuide/en"
        if let language = NSLocale.current.languageCode {
            subdirectory = "QuickGuide/\(language)"
        }
        
        if let url = Bundle.main.url(forResource: pageName, withExtension: "html", subdirectory: subdirectory) {
            return url
        } else {
            return Bundle.main.url(forResource: pageName, withExtension: "html", subdirectory: "QuickGuide/en")
        }
    }
}

class QuickGuideViewController: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableTopSeparatorHeight: NSLayoutConstraint!
    @IBOutlet weak var separatorView: UIView!
    
    weak var delegate: QuickGuideViewControllerDelegate?
    
    let menuItems: [QuickGuideItem]
    let disposeBag = DisposeBag()
    
    required init?(coder aDecoder: NSCoder) {
        self.menuItems = [.speakerKnobs, .presets, .soloMulti]
        super.init(coder: aDecoder)
    }
    
    deinit {
        print(#function, String(describing: self))
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    override func viewDidLoad() {
        titleLabel.attributedText = Fonts.PageTitleFont.AttributedPageTitleWithString(Localizations.Help.QuickGuide.Title.uppercased())
        self.tableTopSeparatorHeight.constant = 0.5
        
        self.tableView.backgroundColor = UIColor.clear
        self.tableView.separatorStyle = .none
        
        updateSeparatorViewVisibility()
        setupTableView()
    }
    
    @IBAction func onBack(_ sender: AnyObject) {
        self.delegate?.quickGuideViewControllerDidRequestBack(self)
    }
}

// MARK: - Rx bindings for UITableView
extension QuickGuideViewController {
    func setupTableView() {
        Observable.just(menuItems)
            .bind(to: tableView.rx.items(cellIdentifier: "quickGuideItem", cellType: QuickGuideCell.self)) { (row, element, cell) in
                cell.quickGuideItem = element }
            .disposed(by: disposeBag)
        
        tableView.rx.itemSelected
            .debounce(0.1, scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] indexPath in
                guard let `self` = self else { return }
                
                let menuItem = self.menuItems[(indexPath as NSIndexPath).row]
                self.delegate?.quickGuideViewControllerDidRequestQuickGuideItem(menuItem, self)
                self.tableView.deselectRow(at: indexPath, animated: true) })
            .disposed(by: disposeBag)
        
        tableView.rx.didScroll
            .subscribe(onNext: { [weak self] _ in
                self?.updateSeparatorViewVisibility() })
            .disposed(by: disposeBag)
    }
}

private extension QuickGuideViewController {
    func updateSeparatorViewVisibility() {
        separatorView.alpha = max(0, min(1.0, tableView.contentOffset.y / 22.0))
    }
}
