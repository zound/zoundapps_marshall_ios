//
//  LocalWebViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 25/10/2016.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import WebKit
import Cartography
import RxSwift

protocol LocalWebViewControllerDelegate: class {
    func localWebViewControllerDidRequestBack(_ localWebViewController: LocalWebViewController)
    func localWebViewControllerDidRequestLoadPageNamed(_ pageName: String, localWebViewController: LocalWebViewController)
}

class LocalWebViewController: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var loadingIndicator: UIImageView!
    @IBOutlet weak var webViewContainer: UIView!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var topSeparatorHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var backButton: UIButton!
    
    var webView: WKWebView!
    var didLoadView = false
    var isRoot: Bool = false
    
    var localPageURL: URL? {
        didSet {
            loadCurrentLocalPageURL()
        }
    }
    
    weak var delegate: LocalWebViewControllerDelegate?
    
    override func viewDidLoad() {
        self.titleLabel.text = nil
        topSeparatorHeightConstraint.constant = 0.5
        separatorView.alpha = 0.0
        
        let webkitConfiguration = WKWebViewConfiguration()
        webkitConfiguration.allowsAirPlayForMediaPlayback = false
        webkitConfiguration.allowsInlineMediaPlayback = true
        webkitConfiguration.allowsPictureInPictureMediaPlayback = false
        
        if #available(iOS 10.0, *) {
            webkitConfiguration.ignoresViewportScaleLimits = false
            webkitConfiguration.dataDetectorTypes = []
        }
        
        webkitConfiguration.selectionGranularity = .character
        let contentController = WKUserContentController()
        contentController.add(self, name: "navigateToPage")
        webkitConfiguration.userContentController = contentController
        
        self.webViewContainer.backgroundColor = UIColor.clear
        self.webView = WKWebView(frame: CGRect.zero, configuration: webkitConfiguration)
        self.webView.isOpaque = false
        self.webView.scrollView.isOpaque = false
        self.webView.backgroundColor = UIColor.clear
        self.webView.scrollView.backgroundColor = UIColor.clear
        
        self.webView.scrollView.delegate = self
        self.webView.navigationDelegate = self
        
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        longPressGesture.minimumPressDuration = 0.45
        longPressGesture.allowableMovement = 100.0
        
        
        if let subview = self.webView.scrollView.subviews.first {
            subview.addGestureRecognizer(longPressGesture)
        }
        
        webViewContainer.addSubview(self.webView)
        constrain(self.webView) { webView in
            webView.edges == webView.superview!.edges
        }
        
        didLoadView = true
        loadCurrentLocalPageURL()
        
        self.webView.rx.observe(String.self, "title")
            .map { $0 == nil ? "" : $0! }
            .map { Fonts.PageTitleFont.AttributedPageTitleWithString($0) }
            .bind(to: titleLabel.rx.attributedText)
            .disposed(by: rx_disposeBag)
        
        if self.navigationController?.viewControllers.first == self {
            self.backButton.setImage(UIImage(named: "close"), for: .normal)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.webView.isUserInteractionEnabled = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.webView.isUserInteractionEnabled = true
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    @objc func handleLongPress(recognizer: UILongPressGestureRecognizer) {
    }
    
    private func loadCurrentLocalPageURL() {
        guard didLoadView else { return }
        guard let currentLocalPage = localPageURL else { return }
        
        self.webViewContainer.alpha = 0.0
        self.loadingIndicator.alpha = 0.0
        UIView.animate(withDuration: 0.15, delay: 0.25, options: [.beginFromCurrentState], animations: { [weak self] in
            self?.loadingIndicator.alpha = 1.0
        })
        self.loadingIndicator.rotate()
        webView.loadFileURL(currentLocalPage, allowingReadAccessTo: currentLocalPage.deletingLastPathComponent())
        
        self.webView.rx.observe(Bool.self, "loading")
            .filter { $0 == false }
            .take(1)
            .unwrapOptional()
            .subscribe(weak: self, onNext: LocalWebViewController.didFinishInitialLoad)
            .disposed(by: rx_disposeBag)
    }
    
    @IBAction func onBack(_ sender: AnyObject) {
        self.delegate?.localWebViewControllerDidRequestBack(self)
    }
}

extension LocalWebViewController: WKNavigationDelegate {
    func didFinishInitialLoad(isLoading: Bool) -> Void {
        DispatchQueue.main.async { [weak self] in
            UIView.animate(withDuration: 0.15, delay: 0.0, options: [.beginFromCurrentState], animations: {
                self?.webViewContainer.alpha = 1.0
                self?.loadingIndicator.alpha = 0.0
            }, completion: { done in
                self?.loadingIndicator.stopRotation()
            })
        }
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.webView.evaluateJavaScript("document.body.style.backgroundColor = 'transparent'", completionHandler: nil)
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        decisionHandler(.allow)
    }
}

extension LocalWebViewController: WKScriptMessageHandler {
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        guard let pageName = message.body as? String else { return }
        self.delegate?.localWebViewControllerDidRequestLoadPageNamed(pageName, localWebViewController: self)
        self.webView.isUserInteractionEnabled = false
    }
}

extension LocalWebViewController: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return nil
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        separatorView.alpha = max(0,min(1.0,scrollView.contentOffset.y/22.0))
    }
}
