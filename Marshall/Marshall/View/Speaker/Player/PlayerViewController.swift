//
//  NowPlayingViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 24/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit
import RxSwift
import AlamofireImage
import Cartography
import RxCocoa
import Toast_Swift
import MarqueeLabel.Swift
import MediaPlayer
import MinuetSDK
import ZoundCommon
import UIColor_Hex_Swift

protocol PlayerViewControllerDelegate: class {
    func playerViewControllerDidRequestDismiss(_ playerViewController: PlayerViewController)
    func playerViewControllerDidRequestVolume(_ playerViewController: PlayerViewController, fromView: UIView)
    func playerViewControllerDidRequestAddPreset(_ playerViewController: PlayerViewController, fromView: UIView)
    func playerViewControllerDidRequestBrowse(_ playerViewController: PlayerViewController, fromView:UIView)
    func playerViewControllerDidRequestOpenPlaylistInfo(_ playerViewController: PlayerViewController)
}

class PlayerViewController: UIViewController {
    @IBOutlet weak var addPresetsBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var browseStationsBottomConstraint : NSLayoutConstraint!
    @IBOutlet weak var progressBarBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var dismissButton: UIButton!
    @IBOutlet weak var buttonsContainer: UIView!
    @IBOutlet var browseIRButton: UIButton!
    @IBOutlet var addNewPresetsButton: UIButton!
    @IBOutlet weak var titleLine1Label: MarqueeLabel!
    @IBOutlet weak var titleLine2Label: MarqueeLabel!
    @IBOutlet weak var spotifyLogo: UIImageView!
    @IBOutlet weak var modeNameLabel: UILabel!
    @IBOutlet weak var artworkImageView: UIImageView!
    @IBOutlet weak var playPauseButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var prevButton: UIButton!
    @IBOutlet weak var loadingIndicator: UIImageView!
    @IBOutlet weak var shuffleButton: UIButton!
    @IBOutlet weak var repeatButton: UIButton!
    @IBOutlet weak var progressContainer: UIView!
    @IBOutlet weak var currentTimeLabel: UILabel!
    @IBOutlet weak var totalTimeLabel: UILabel!
    @IBOutlet weak var progressSlider: EasyDragSlider!
    @IBOutlet weak var volumeBUtton: UIButton!
    @IBOutlet weak var sourceButton: UIButton!
    @IBOutlet weak var speakerNameLabel: UILabel!
    @IBOutlet weak var notificationBackgroun: UIView!
    @IBOutlet weak var notificationContainer: UIView!
    @IBOutlet weak var notificationContentContainer: UIView!
    @IBOutlet weak var notificationIcon: UIImageView!
    @IBOutlet weak var notificationLabel: UILabel!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    
    weak var delegate: PlayerViewControllerDelegate?
    
    var timerDisposable: Disposable? = nil
    var constraintGroup = ConstraintGroup()
    var viewModel: PlayerViewModel!
    
    var isSmallScreen: Bool {
        return UIScreen.main.bounds.height < 500
    }
    
    typealias Capability = (canBrowse: Bool, canAddPreset: Bool, hasProgress: Bool)
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    override func viewDidLoad() {
        hideNotification(false)
        let textColor = UIColor(red:242.0/255.0, green:242.0/255.0, blue:242.0/255.0, alpha:255.0/255.0)
        browseIRButton.setAttributedTitle(Fonts.UrbanEars.Bold(15).AttributedTextWithString(Localizations.Player.NowPlaying.BrowseStations, color: textColor, letterSpacing: 0.325, shadow: true), for: .normal)
        addNewPresetsButton.setAttributedTitle(Fonts.UrbanEars.Bold(15).AttributedTextWithString(Localizations.Player.NowPlaying.SavePreset, color: textColor, letterSpacing: 0.325, shadow: true), for: .normal)
        
        titleLine1Label.font = Fonts.UrbanEars.Bold(18)
        titleLine1Label.type = .continuous
        titleLine1Label.speed = .rate(70.0)
        titleLine1Label.animationCurve = .linear
        titleLine1Label.fadeLength = 5.0
        titleLine1Label.animationDelay = 4.0
        titleLine1Label.trailingBuffer = 30.0
        
        titleLine2Label.font = Fonts.UrbanEars.Regular(14)
        titleLine2Label.type = .continuous
        titleLine2Label.speed = .rate(70.0)
        titleLine2Label.animationCurve = .linear
        titleLine2Label.fadeLength = 5.0
        titleLine2Label.animationDelay = 4.0
        titleLine2Label.trailingBuffer = 30.0
        
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate ,
            let length = appDelegate.window?.rootViewController?.topLayoutGuide.length {
            topConstraint.constant = length + 10
        } else {
            topConstraint.constant = 30.0
        }
        
        modeNameLabel.font = Fonts.UrbanEars.Bold(17)
        sourceButton.titleLabel?.font = Fonts.UrbanEars.Bold(30)
        speakerNameLabel.font = Fonts.UrbanEars.Bold(12)
        
        self.artworkImageView.image = UIImage(color: UIColor(white: 0.0, alpha: 0.2))
        self.progressSlider.setThumbImage(UIImage(named: "now_playing_slider_thumb"), for: .normal)
        let minimumTrackImage = UIImage(color: UIColor("#AE915B"), size: CGSize(width:1,height:3))
        let maximumTrackImage = UIImage(color: UIColor(white: 1.0, alpha: 0.25), size: CGSize(width:1,height:3))
        self.progressSlider.setMinimumTrackImage(minimumTrackImage, for: .normal)
        self.progressSlider.setMaximumTrackImage(maximumTrackImage, for: .normal)
        
        let progressFont = Fonts.UrbanEars.Regular(12)
        let bodyFontDescriptor = progressFont.fontDescriptor
        let bodyMonospacedNumbersFontDescriptor = bodyFontDescriptor.addingAttributes(
            [
                UIFontDescriptor.AttributeName.featureSettings: [
                    [
                        UIFontDescriptor.FeatureKey.featureIdentifier: kNumberSpacingType,
                        UIFontDescriptor.FeatureKey.typeIdentifier: kMonospacedNumbersSelector
                    ]
                ]
            ])
        let bodyMonospacedNumbersFont = UIFont(descriptor: bodyMonospacedNumbersFontDescriptor, size: 12.0)
        currentTimeLabel.font = bodyMonospacedNumbersFont
        totalTimeLabel.font = bodyMonospacedNumbersFont
        
        if let vm = viewModel {
            vm.line1Text.asObservable()
                .bind(to: titleLine1Label.rx.text)
                .disposed(by: rx_disposeBag)
            vm.line2Text.asObservable()
                .bind(to: titleLine2Label.rx.text)
                .disposed(by: rx_disposeBag)
            
            vm.line1TextVisible.asObservable()
                .not()
                .bind(to: titleLine1Label.rx.isHidden)
                .disposed(by: rx_disposeBag)
            
            vm.line2TextVisible.asObservable()
                .not()
                .bind(to: titleLine2Label.rx.isHidden)
                .disposed(by: rx_disposeBag)
            
            vm.currentTimeText.asObservable()
                .bind(to: currentTimeLabel.rx.text)
                .disposed(by: rx_disposeBag)
            
            vm.totalTimeText.asObservable()
                .bind(to: totalTimeLabel.rx.text)
                .disposed(by: rx_disposeBag)
            
            vm.progress.asObservable()
                .bind(to: progressSlider.rx.value)
                .disposed(by: rx_disposeBag)
            
            let urlImageComparer = {(rhs: (URL?,UIImage?), lhs: (URL?,UIImage?)) in rhs.0 == lhs.0 && rhs.1 == lhs.1}
            
            Observable.combineLatest(vm.artworkURL.asObservable(),
                                     vm.modeArtworkPlaceholder.asObservable()) { ($0,$1) }
                .distinctUntilChanged(urlImageComparer)
                .subscribe(onNext: { [weak self] params in
                    guard let `self` = self else {return}
                    let url = params.0
                    let placeholderImage = params.1
                    self.updateArtworkWithURL(url: url, modePlaceholderImage: placeholderImage) })
                .disposed(by: rx_disposeBag)
            
            vm.miniPlayerLine1Text.asObservable()
                .map { $0?.uppercased() }
                .bind(to: speakerNameLabel.rx.text)
                .disposed(by: rx_disposeBag)
            
            let spotifyPlaying = vm.spotifyPlaying.asObservable()
            let canSeek = vm.playCaps.asObservable()
                .map { $0.canSeek }
                .distinctUntilChanged()
            
            let isPlayingOrBuffering = vm.isPlayingOrBuffering.asObservable()
            let canStop = vm.canStop.asObservable()
            
            Observable.combineLatest(isPlayingOrBuffering, canStop) { ($0,$1) }
                .subscribe(onNext: { [weak self] isPlayingOrBuffering, canStop in
                    self?.updatePlayPauseButtonForIsPlaying(isPlayingOrBuffering, canStop: canStop) })
                .disposed(by: rx_disposeBag)
            
            vm.shuffleOn
                .asObservable()
                .subscribe(weak: self, onNext: PlayerViewController.updateShuffleButtonForShuffle, onError: nil, onCompleted: nil, onDisposed: nil)
                .disposed(by: rx_disposeBag)
            
            vm.repeatOn.asObservable()
                .subscribe(weak: self, onNext: PlayerViewController.updateRepeatButtonForRepeat)
                .disposed(by: rx_disposeBag)
            
            let canPauseOrStop = vm.playCaps.asObservable()
                .map { $0.canPause || $0.canStop }
            
            let isNotChromecast = vm.chromecastPlaying.asObservable()
                .not()
            
            let showPlayPauseButton = Observable.combineLatest(canPauseOrStop,
                                                               vm.isBuffering.asObservable(),
                                                               isNotChromecast) { ($0 || $1) && $2 }
                .distinctUntilChanged()
            
            showPlayPauseButton
                .subscribe(weak: playPauseButton, onNext: UIView.setAlphaForVisiblity)
                .disposed(by: rx_disposeBag)
            
            let showBuffering = Observable.combineLatest(vm.isBuffering.asObservable(), isNotChromecast) { $0 && $1}
            showBuffering.asObservable()
                .subscribe(weak: self, onNext: PlayerViewController.updateBufferingIndicatorForIsBuffering)
                .disposed(by: rx_disposeBag)
            
            let hasProgressCapability = vm.hasProgress.asObservable()
                .distinctUntilChanged()
            let hasProgress = Observable.combineLatest(hasProgressCapability,isNotChromecast) { $0 && $1 }
            
            Observable.combineLatest(spotifyPlaying, hasProgress, canSeek) { ($0, $1, $2) }
                .subscribe(onNext: { [weak self] spotifyPlaying, hasProgress, canSeek in
                    self?.updateForSpotifyPlaying(spotifyPlaying, hasProgress: hasProgress, canSeek: canSeek) })
                .disposed(by: rx_disposeBag)
            
            vm.playCaps.asObservable()
                .map { $0.canSkipNext }
                .distinctUntilChanged()
                .subscribe(weak: nextButton, onNext:UIView.setAlphaForVisiblity).disposed(by: rx_disposeBag)
            
            vm.playCaps.asObservable()
                .map { $0.canSkipPrevious }
                .distinctUntilChanged()
                .subscribe(weak: prevButton, onNext:UIView.setAlphaForVisiblity).disposed(by: rx_disposeBag)
            
            vm.playCaps.asObservable()
                .map { $0.canShuffle }
                .distinctUntilChanged()
                .subscribe(weak: shuffleButton, onNext:UIView.setAlphaForVisiblity).disposed(by: rx_disposeBag)
            
            vm.playCaps.asObservable()
                .map { $0.canRepeat }
                .distinctUntilChanged()
                .subscribe(weak: repeatButton, onNext:UIView.setAlphaForVisiblity).disposed(by: rx_disposeBag)
            
            vm.playCaps.asObservable()
                .map { $0.canSeek }
                .distinctUntilChanged()
                .subscribe(onNext: { [weak self]canSeek in
                    self?.progressSlider.isEnabled = canSeek
                    if canSeek {
                        self?.progressSlider.setThumbImage(UIImage(named: "now_playing_slider_thumb"), for: .normal)
                    } else {
                        let placeholderImage = UIImage(color: UIColor.clear, size: CGSize(width:2,height:22.5))
                        self?.progressSlider.setThumbImage(placeholderImage, for: .normal)
                    } })
                .disposed(by: rx_disposeBag)
            
            if viewModel.isGuestSpeaker {
                self.browseIRButton.isHidden = true
                self.addNewPresetsButton.isHidden = true
            }
            
            vm.currentSelectionText.asObservable()
                .map { $0?.uppercased() }
                .bind(to:modeNameLabel.rx.text)
                .disposed(by: rx_disposeBag)
            
            vm.clientState.asObservable()
                .map { $0?.currentPlayableItem }
                .subscribe(weak: self, onNext: PlayerViewController.updateForCurrentlyPlayingItem)
                .disposed(by: rx_disposeBag)
            
            let canAddPreset = vm.playCaps.asObservable()
                .map { $0.canAddPreset }
                .distinctUntilChanged()
            let canBrowse = vm.masterState.asObservable()
                .map { $0?.currentMode?.id == "IR" }
                .distinctUntilChanged()
            
            Observable.combineLatest(canBrowse, canAddPreset, hasProgress) { canBrowse, canAddPreset, hasProgress -> Capability in
                return Capability(canBrowse: canBrowse, canAddPreset: canAddPreset, hasProgress: hasProgress) }
                .subscribe(weak: self, onNext: PlayerViewController.updateButtonsWithCapability)
                .disposed(by: rx_disposeBag)
            
            vm.savedNotification
                .subscribe(weak: self, onNext: PlayerViewController.showNotificationForSavedSuccessFully)
                .disposed(by: rx_disposeBag)
            
            setupPlayPauseButton()
        }
    }
    
    func updateButtonsWithCapability(_ capability: Capability) {
        let buttonHeight:CGFloat = 40
        let progressHeight:CGFloat = 31
        
        if capability.hasProgress {
            progressBarBottomConstraint.constant = 0
        } else {
            progressBarBottomConstraint.constant = -progressHeight
        }
        
        if capability.canBrowse {
            addPresetsBottomConstraint .constant = capability.canBrowse ? buttonHeight + 10 : 0
        } else {
            addPresetsBottomConstraint .constant = 0
        }
        
        UIView.animate(withDuration: 0.25, delay: 0, options: [.beginFromCurrentState], animations: { [weak self] in
            self?.progressSlider.alpha = capability.hasProgress ? 1.0 : 0.0
            self?.totalTimeLabel.alpha = capability.hasProgress ? 0.5 : 0.0
            self?.currentTimeLabel.alpha = capability.hasProgress ? 0.5 : 0.0
            
            self?.browseIRButton.alpha = capability.canBrowse ? 1.0 : 0.0
            self?.addNewPresetsButton.alpha = capability.canAddPreset ? 1.0 : 0.0
            self?.view.layoutIfNeeded()
            }, completion: nil)
    }
    
    func updatePlayPauseButtonForIsPlaying(_ isPlaying: Bool, canStop: Bool) {
        var buttonImage: UIImage! = nil
        
        if isPlaying {
            if canStop {
                buttonImage = UIImage(named: "nowplaying_stop_button")
            } else {
                buttonImage = UIImage(named: "nowplaying_pause_button")
            }
        } else {
            buttonImage = UIImage(named: "nowplaying_play_button")
        }
        
        self.playPauseButton.setImage(buttonImage, for: .normal)
    }
    
    func updateShuffleButtonForShuffle(_ shuffle: Bool) {
        let buttonImage =  shuffle ? UIImage(named: "nowplaying_shuffle_on")! : UIImage(named: "nowplaying_shuffle_off")!
        self.shuffleButton.setImage(buttonImage, for: .normal)
    }
    
    func updateRepeatButtonForRepeat(_ repeatState: Bool) {
        let buttonImage =  repeatState ? UIImage(named: "nowplaying_repeat_on")! : UIImage(named: "nowplaying_repeat_off")!
        self.repeatButton.setImage(buttonImage, for: .normal)
    }
    
    func updateBufferingIndicatorForIsBuffering(_ isBuffering: Bool) {
        if isBuffering {
            self.loadingIndicator.rotate(0.5)
        }
        
        UIView.animate(withDuration: 0.2, delay: 0.0, options: [.beginFromCurrentState], animations: { [weak self] in
            self?.loadingIndicator.alpha = isBuffering  ? 1.0 : 0.0
            }, completion: { [weak self] _ in
                if !isBuffering {
                    self?.loadingIndicator.stopRotation()
                }
        })
    }
    
    func updateForCurrentlyPlayingItem(_ playableItem: PlayableItem?) {
        sourceButton.setTitle(nil, for: .normal)
        sourceButton.setImage(nil, for: .normal)
        
        if let item = playableItem {
            switch item.selectableType {
            case .preset(let number): sourceButton.setTitle(String(number), for: .normal)
            default: sourceButton.setImage(UIImage(named:item.selectableType.smallIconName!)?.withRenderingMode(.alwaysTemplate), for: .normal)
            }
        }
    }
    
    func updateForSpotifyPlaying(_ spotifyPlaying: Bool, hasProgress: Bool, canSeek: Bool) {
        
        let defaultSpace: CGFloat = 0.0
        let progressSpace: CGFloat = 3.0
        let canSeekSpace: CGFloat = 15.0
        let spotifyPlaylistSpace: CGFloat = 22.0
        let showSpotifyPlaylistName = spotifyPlaying && !isSmallScreen
        //I think this does not fit on iPhone 4/so I chose to remove it in this case and feature the artwork better
        
        var playPauseTopSpace = defaultSpace
        if showSpotifyPlaylistName {
            playPauseTopSpace = playPauseTopSpace + spotifyPlaylistSpace
        }
        
        if hasProgress {
            playPauseTopSpace = playPauseTopSpace + progressSpace
            
            if canSeek {
                playPauseTopSpace = playPauseTopSpace + canSeekSpace
            }
        }
        
        //playPauseButtonTopConstraint.constant = playPauseTopSpace
        UIView.animate(withDuration: 0.25, delay: 0, options: [.beginFromCurrentState], animations: { [weak self] in
            self?.spotifyLogo.alpha = spotifyPlaying ? 1.0 : 0.0
            self?.modeNameLabel.alpha = spotifyPlaying ? 0.0 : 1.0
            
            self?.view.layoutIfNeeded()
            }, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        viewModel?.startUpdatingProgress()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        viewModel?.stopUpdatingProgress()
    }
    
    
    func updateArtworkWithURL(url: URL?, modePlaceholderImage: UIImage?) {
        var placeholderImage: UIImage? = UIImage(named: "presets_unavailable_placeholder")
        let contentMode = UIView.ContentMode.scaleAspectFit
        
        if modePlaceholderImage != nil {
            placeholderImage = modePlaceholderImage
            artworkImageView.image = placeholderImage
            artworkImageView.contentMode = contentMode
        } else {
            if let url = url {
                artworkImageView.af_cancelImageRequest()
                artworkImageView.af_setImage(withURL:url,
                                             placeholderImage: artworkImageView.image,
                                             imageTransition: UIImageView.ImageTransition.crossDissolve(0.4),
                                             runImageTransitionIfCached: true,
                                             completion: { [weak self] response in
                                                if let _ = response.result.value {
                                                    //do any updates of other elements
                                                } else
                                                {
                                                    self?.artworkImageView.image = placeholderImage
                                                    self?.artworkImageView.contentMode = contentMode
                                                }
                })
            } else {
                artworkImageView.af_cancelImageRequest()
                UIView.transition(
                    with: artworkImageView,
                    duration: 0.4,
                    options: [.transitionCrossDissolve,.beginFromCurrentState],
                    animations: { [weak self] in
                        self?.artworkImageView.image = placeholderImage
                        self?.artworkImageView.contentMode = contentMode
                    },
                    completion: nil)
            }
        }
    }
    
    @IBAction func onDismiss(_ sender: AnyObject) {
        self.delegate?.playerViewControllerDidRequestDismiss(self)
    }
    
    @IBAction func onSpotifyLogo(_ sender: Any) {
        self.delegate?.playerViewControllerDidRequestOpenPlaylistInfo(self)
    }
    
    @IBAction func onPlaylistName(_ sender: Any) {
        self.delegate?.playerViewControllerDidRequestOpenPlaylistInfo(self)
    }
    
    @IBAction func onNext(_ sender: AnyObject) {
        viewModel?.next()
    }
    
    @IBAction func onPrev(_ sender: AnyObject) {
        viewModel?.prev()
    }
    
    @IBAction func onRepeat(_ sender: AnyObject) {
        viewModel?.repeatToggle()
    }
    
    @IBAction func onShuffle(_ sender: AnyObject) {
        viewModel?.shuffleToggle()
    }
    
    @IBAction func onBrowse(_ sender: UIButton) {
        self.delegate?.playerViewControllerDidRequestBrowse(self, fromView: sender)
    }
    
    @IBAction func onAddPreset(_ sender: UIButton) {
        self.delegate?.playerViewControllerDidRequestAddPreset(self, fromView: sender)
    }
    
    @IBAction func onProgressStartEdit(_ sender: AnyObject) {
        viewModel?.seekToProgress(progressSlider.value)
    }
    
    @IBAction func onProgressEndEdit(_ sender: AnyObject) {
        viewModel?.commitProgress(progressSlider.value)
    }
    
    @IBAction func onChangedProgress(_ sender: AnyObject) {
        viewModel?.seekToProgress(progressSlider.value)
    }
    
    @IBAction func onVolume(_ sender: UIButton) {
        self.delegate?.playerViewControllerDidRequestVolume(self, fromView: sender)
    }
}

extension PlayerViewController {
    func showNotificationForSavedSuccessFully(_ success: Bool) {
        if success {
            if let vm = viewModel {
                if vm.masterState.value?.currentMode?.isSpotify == true {
                    showPresetNotification(.addPlaylistSuccess)
                } else if vm.masterState.value?.currentMode?.isIR == true {
                    showPresetNotification(.addRadioStationSuccess)
                } else {
                    showPresetNotification(.addPreset)
                }
            }
        } else {
            if let vm = viewModel {
                if vm.masterState.value?.currentMode?.isSpotify == true {
                    showPresetNotification(.addPlaylistFail)
                } else if vm.masterState.value?.currentMode?.isIR == true {
                    showPresetNotification(.addRadioStationFail)
                } else {
                    showPresetNotification(.addPresetFail)
                }
            }
        }
    }
    
    func hideNotification(_ animated: Bool) {
        if animated {
            UIView.animate(withDuration: 0.25, delay: 0.0, options: [.beginFromCurrentState], animations: { [weak self] in
                guard let `self` = self else { return }
                self.notificationBackgroun.alpha = 0.0
                self.notificationContentContainer.alpha = 0.0
                self.notificationContentContainer.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
                }, completion: nil)
        } else {
            notificationBackgroun.alpha = 0.0
            notificationContentContainer.alpha = 0.0
        }
    }
    
    func hideNotificationInTimeInterval(_ timeInterval: TimeInterval) {
        timerDisposable?.dispose()
        timerDisposable = Observable<Int>
            .interval(timeInterval, scheduler: MainScheduler.instance)
            .take(1)
            .subscribe(onNext:{ [weak self] _ in
                guard let `self` = self else { return }
                self.hideNotification(true) })
    }
    
    func showPresetNotification(_ notification: PresetNotification, animated: Bool = true) {
        notificationLabel.text = notification.message
        notificationIcon.image = UIImage(named: notification.iconName)
        notificationContentContainer.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        notificationContentContainer.alpha = 0.0
        
        UIView.animate(withDuration: 0.25, delay: 0.0, options: [.beginFromCurrentState] , animations: { [weak self] in
            guard let `self` = self else { return }
            self.notificationBackgroun.alpha = 1.0
            self.notificationContentContainer.alpha = 1.0
            self.notificationContentContainer.transform = CGAffineTransform.identity
            }, completion: { [weak self] _ in
                guard let `self` = self else { return }
                self.hideNotificationInTimeInterval(2.0)
        })
    }
}

extension PlayerViewController {
    func setupPlayPauseButton() {
        playPauseButton.rx.tap
            .debounce(0.5, scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] _ in
                self?.viewModel.playPause() })
            .disposed(by: rx_disposeBag)
    }
}
