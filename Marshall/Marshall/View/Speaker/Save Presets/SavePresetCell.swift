//
//  SavePresetCell.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 28/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import AlamofireImage
import MinuetSDK
import ZoundCommon

class SavePresetCell: UITableViewCell {
    @IBOutlet weak var presetNameLabel: UILabel!
    @IBOutlet weak var presetDescriptionLabel: UILabel!
    @IBOutlet weak var presetNumberLabel: UILabel!
    @IBOutlet weak var artworkImageView: UIImageView!
    @IBOutlet weak var notificationBackgroundView: UIView!
    @IBOutlet weak var notificationIconImageView: UIImageView!
    
    fileprivate let viewModelVariable: Variable<PresetCellViewModel?> = Variable(nil)
    var timerDisposable: Disposable? = nil
    
    var viewModel: PresetCellViewModel? {
        get { return viewModelVariable.value }
        set { viewModelVariable.value = newValue
            if let presetViewModel = newValue {
                updateForPreset(presetViewModel.preset, animated: false)
            }
        }
    }
    
    override func awakeFromNib() {
        self.contentView.backgroundColor = UIColor.clear
        self.backgroundColor = UIColor.clear
        
        notificationIconImageView.alpha = 0.0
        notificationBackgroundView.alpha = 0.0
        
        let customColorView = UIView()
        customColorView.backgroundColor = UIColor(white: 0.0, alpha: 0.24);
        self.selectedBackgroundView =  customColorView;
        
        presetNameLabel.font = Fonts.UrbanEars.Bold(15)
        presetDescriptionLabel.font = Fonts.UrbanEars.Regular(14)
        presetNumberLabel.font = Fonts.UrbanEars.Bold(48)
        
        let presetObservable = viewModelVariable.asObservable()
            .unwrapOptional()
            .map { $0.presetVariable.asObservable() }
            .switchLatest()
        
        presetObservable
            .skip(1)
            .subscribe(weak: self, onNext: SavePresetCell.updateForPreset)
            .disposed(by: rx_disposeBag)
        
        viewModelVariable.asObservable()
            .unwrapOptional()
            .map{ $0.playlistArtworkURL.asObservable() }
            .switchLatest()
            .subscribe(weak: self, onNext:SavePresetCell.updateArtworkWithImageURL)
            .disposed(by: rx_disposeBag)
        
        viewModelVariable.asObservable()
            .unwrapOptional()
            .map { $0.savedSuccessfullyNotification }
            .switchLatest()
            .subscribe(weak: self, onNext:SavePresetCell.didSavePreset)
            .disposed(by: rx_disposeBag)
    }
    
    func updateForPreset(_ preset: MinuetSDK.Preset) {
        updateForPreset(preset, animated: true)
    }
    
    func updateForPreset(_ preset: MinuetSDK.Preset, animated: Bool) {
        print("update for preset")
        if animated {
            UIView.transition(with: self.presetNameLabel, duration: 0.5, options: [.transitionCrossDissolve,.beginFromCurrentState], animations: { [weak self] in
                self?.presetNameLabel.text = preset.name },
                              completion: nil)
            
            UIView.transition(with: self.presetDescriptionLabel, duration: 0.5, options: [.transitionCrossDissolve,.beginFromCurrentState], animations: { [weak self] in
                self?.presetDescriptionLabel.text = preset.type?.displayName ?? "" },
                              completion: nil)
        } else {
            self.presetNameLabel.text = preset.name
            self.presetDescriptionLabel.text = preset.type?.displayName ?? ""
        }
        self.presetNumberLabel.text = String(preset.number)
    }
    
    func didSavePreset(successfully success: Bool) {
        showPresetNotification(forSuccess: success)
    }
    
    func showPresetNotification(forSuccess success: Bool, animated: Bool = true) {
        notificationIconImageView.image = UIImage(named: success ? "preset_cell_added_icon" : "preset_cell_failed_icon")
        notificationIconImageView.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        notificationIconImageView.alpha = 0.0
        notificationBackgroundView.alpha = 0.0
        
        UIView.animate(withDuration: 0.25, delay: 0.0, options: [.beginFromCurrentState], animations: { [weak self] in
            guard let `self` = self else { return }
            self.notificationBackgroundView.alpha = 1.0
            self.notificationIconImageView.alpha = 1.0
            self.notificationIconImageView.transform = CGAffineTransform.identity
            }, completion: { [weak self] _ in
                guard let `self` = self else { return }
                self.hideNotificationInTimeInterval(2.0)
        })
    }
    
    func hideNotification(_ animated: Bool) {
        if animated {
            UIView.animate(withDuration: 0.25, delay: 0.0, options: [.beginFromCurrentState], animations: { [weak self] in
                guard let `self` = self else { return }
                self.notificationBackgroundView.alpha = 0.0
                self.notificationIconImageView.alpha = 0.0
                self.notificationIconImageView.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
                }, completion: nil)
        } else {
            notificationBackgroundView.alpha = 0.0
            notificationIconImageView.alpha = 0.0
        }
    }
    
    func hideNotificationInTimeInterval(_ timeInterval: TimeInterval) {
        timerDisposable?.dispose()
        timerDisposable = Observable<Int>.interval(timeInterval, scheduler: MainScheduler.instance).take(1).subscribe(onNext:{ [weak self] _ in
            guard let `self` = self else { return }
            self.hideNotification(true)
        })
        timerDisposable?.disposed(by: rx_disposeBag)
    }
    
    func updateArtworkWithImageURL(_ url: URL?) {
        guard let imageURL  = url else {
            artworkImageView?.image = UIImage(color: UIColor(white: 1.0, alpha: 0.05))
            return
        }
        artworkImageView?.af_setImage(withURL: imageURL,
                                      placeholderImage: artworkImageView?.image,
                                      filter: nil,
                                      progress: nil,
                                      progressQueue: DispatchQueue.main,
                                      imageTransition: UIImageView.ImageTransition.crossDissolve(0.15),
                                      runImageTransitionIfCached: false,
                                      completion: nil)
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        let color = self.notificationBackgroundView!.backgroundColor // Store the color
        super.setHighlighted(highlighted, animated: animated)
        self.notificationBackgroundView?.backgroundColor = color
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        let color = self.notificationBackgroundView!.backgroundColor // Store the color
        super.setSelected(selected, animated: animated)
        self.notificationBackgroundView?.backgroundColor = color
    }
}
