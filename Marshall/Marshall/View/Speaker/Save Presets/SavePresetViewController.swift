//
//  SavePresetViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 28/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

protocol SavePresetViewControllerDelegate: class {
    func savePresetViewControllerDidRequestClose(_ savePresetViewController: SavePresetViewController)
}

class SavePresetViewController: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var separatorViewHeightConstraint: NSLayoutConstraint!
    
    weak var delegate:SavePresetViewControllerDelegate?
    
    var viewModel:SavePresetViewModel?
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.attributedText = Fonts.PageTitleFont.AttributedPageTitleWithString(Localizations.SavePreset.Title)
        
        separatorViewHeightConstraint.constant = 0.5
        _ = tableView.rx.setDelegate(self)
        
        UIView.setAnimationsEnabled(false)
        
        cancelButton.setAttributedTitle(Fonts.SecondaryButtonFont.AttributedSecondaryButtonWithString(Localizations.Appwide.Cancel), for: .normal)
        
        cancelButton.layoutIfNeeded()
        UIView.setAnimationsEnabled(true)
        
        if let vm = viewModel {
            vm.presetsViewModelsVariable.asObservable()
                .bind(to: tableView.rx.items(cellIdentifier:"presetItem", cellType: SavePresetCell.self)) { (row, element, cell) in
                    cell.viewModel = element }
                .disposed(by: rx_disposeBag)
        }
    }
    
    @IBAction func onClose(_ sender: AnyObject) {
        self.delegate?.savePresetViewControllerDidRequestClose(self)
    }
}

extension SavePresetViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let vm = viewModel {
            let presetViewModel = vm.presetsViewModels[indexPath.row]
            presetViewModel.savePreset()
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    private func tableView(_ tableView: UITableView, editingStyleForRowAt editingStyleForRowAtIndexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return UITableViewCellEditingStyle.none
    }
}
