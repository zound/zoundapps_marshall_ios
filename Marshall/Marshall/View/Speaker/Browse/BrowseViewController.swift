//
//  BrowseViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 28/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import Cartography
import MinuetSDK

protocol BrowseViewControllerDelegate: class {
    func browseViewControllerDidRequestClose(_ browseViewController: BrowseViewController)
}

class BrowseViewController: UIViewController {
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var backActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var separatorHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var listActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var titleTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var searchBarToTitleConstraint: NSLayoutConstraint!
    @IBOutlet weak var searchBarRightConstraint: NSLayoutConstraint!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var searchPlaceholderLabel: UILabel!
    
    weak var delegate: BrowseViewControllerDelegate?
    
    let currentPageIsRoot: Variable<Bool> = Variable(false)
    
    var previousPage: SpeakerNavPage?
    var viewModel: BrowseViewModel?
    
    enum TableViewReloadTransitionDiraction {
        case left
        case right
        case top
        case bottom
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.attributedText = Fonts.PageTitleFont.AttributedPageTitleWithString(Localizations.BrowseRadio.Title)
        searchPlaceholderLabel.text = Localizations.BrowseRadio.SearchPlaceholder
        searchPlaceholderLabel.font = Fonts.UrbanEars.Regular(14)
        
        cancelButton.setAttributedTitle(Fonts.UrbanEars.Regular(13).AttributedSecondaryButtonWithString(Localizations.Appwide.Cancel), for: .normal)
        
        searchTextField.font = Fonts.UrbanEars.Regular(14)
        
        separatorHeightConstraint.constant = 0.5
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorStyle = .none
        searchTextField.delegate = self
        
        let paddingView = UIView(frame:CGRect(x: 0, y: 0, width: 33, height: 20))
        searchTextField.leftView = paddingView
        searchTextField.leftViewMode = UITextField.ViewMode.always
        
        updateSeparatorViewVisibility()                  
        
        if let vm = viewModel {
            vm.delegate = self
            let currentPage = vm.isSearching.asObservable()
                .flatMapLatest { $0 ? vm.currentSearchPage.asObservable(): vm.currentPage.asObservable() }
            
            let currentPageIsRoot = currentPage.asObservable()
                .map { page -> Int in
                    guard let depth = page?.depth else { return 1}
                    return depth }
                .do(onNext: { print("depth: \($0)") })
                .map { $0 == 0 }
            
            Observable.combineLatest(currentPageIsRoot,
                                     vm.loadingNextPage.asObservable(),
                                     vm.loadingSearchPage.asObservable(),
                                     vm.loadingPreviousPage.asObservable(),
                                     vm.currentPage.asObservable()) { currentPageIsRoot, loadingNext, loadingSearch, loadingPrevious, currentPage -> Bool in
                                        return currentPageIsRoot || loadingPrevious || loadingSearch || (currentPage == nil) }
                .bind(to: backButton.rx.isHidden)
                .disposed(by: rx_disposeBag)
            
            vm.isSearching.asObservable()
                .distinctUntilChanged()
                .subscribe(weak: self, onNext: BrowseViewController.updateForIsSearching)
                .disposed(by: rx_disposeBag)
            
            vm.loadingPreviousPage.asObservable()
                .distinctUntilChanged()
                .subscribe(weak: self, onNext: BrowseViewController.updateForIsLoadingPrevious)
                .disposed(by: rx_disposeBag)
            
            Observable.combineLatest(vm.loadingNextPage.asObservable(), vm.loadingSearchPage.asObservable()) { nextPage, searchPage in nextPage || searchPage }
                .distinctUntilChanged()
                .subscribe(weak: self, onNext: BrowseViewController.updateForIsLoadingNextPage)
                .disposed(by: rx_disposeBag)
        }
        
        self.searchTextField.rx.text
            .subscribe(onNext:{ [weak self] _ in self?.updateSearchPlaceholder() })
            .disposed(by: rx_disposeBag)
    }
    
    func updateSeparatorViewVisibility() {
        separatorView.alpha = max(0, min(1.0, tableView.contentOffset.y / 22.0))
    }
    
    func updateForIsLoadingNextPage(_ loading: Bool) {
        var visible = false
        
        let hasItems = self.currentPage != nil
        if loading && !hasItems {
            visible = true
        }
        
        self.listActivityIndicator.isHidden = !visible
        if visible {
            self.listActivityIndicator.startAnimating()
        } else {
            self.listActivityIndicator.stopAnimating()
        }
    }
    
    func updateForIsLoadingPrevious(_ loading: Bool) {
        self.backActivityIndicator.isHidden = !loading
        if loading {
            self.backActivityIndicator.startAnimating()
        } else {
            self.backActivityIndicator.stopAnimating()
        }
    }
    
    func updateSearchPlaceholder() {
        searchPlaceholderLabel.isHidden = ((self.searchTextField.text != nil &&
            self.searchTextField.text != "") ||
            self.searchTextField.isFirstResponder)
    }
    
    func updateForIsSearching(_ searching: Bool) {
        var topSpacing: CGFloat = 20
        
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate ,
            let length = appDelegate.window?.rootViewController?.topLayoutGuide.length {
            topSpacing = length
        }
        
        let titleHeight = titleLabel.bounds.size.height
        titleTopConstraint.constant = searching ? -(titleHeight+1) : (topSpacing + 20)
        searchBarToTitleConstraint.constant = searching ? (topSpacing+20) : 20
        searchBarRightConstraint.constant = searching ? (cancelButton.frame.size.width + 16) : 20
        
        if searching {
            self.reloadTableViewWithTransitionDirection(.bottom)
            //self.removeAllRowsWithRowAnimation(UITableViewRowAnimation.top)
            self.previousPage = nil
            
            if !self.searchTextField.isFirstResponder {
                self.searchTextField.becomeFirstResponder()
            }
        } else {
            self.reloadTableViewWithTransitionDirection(.top)
            //self.addRowsFromPage(SpeakerNavPage(items: [], depth: 0, numItems: 0, canLoadMore: false), withRowAnimAtion: UITableViewRowAnimation.automatic)
        }
        
        UIView.animate(withDuration: 0.25, delay: 0.0, options: [.beginFromCurrentState], animations: { [weak self] in
            if !searching {
                self?.searchTextField.textAlignment = NSTextAlignment.center
                self?.searchTextField.resignFirstResponder()
            } else {
                self?.searchTextField.textAlignment = NSTextAlignment.left
            }
            self?.cancelButton.alpha = searching ? 1.0 : 0.0
            self?.view.layoutIfNeeded()
            }, completion: nil)
    }
    
    func removeAllRowsWithRowAnimation(_ rowAnimation: UITableView.RowAnimation) {
        let indexPathsToRemove = (0..<tableView.numberOfRows(inSection: 0)).map{ IndexPath(row: $0, section: 0) }
        
        if indexPathsToRemove.count > 0 {
            self.tableView.deleteRows(at: indexPathsToRemove, with: rowAnimation)
        }
    }
    
    func addRowsFromPage(_ page: SpeakerNavPage, withRowAnimAtion rowAnimation: UITableView.RowAnimation) {
        let indexPathsToAdd = (0..<page.numItems).map{ IndexPath(row: $0, section: 0) }
        
        if indexPathsToAdd.count > 0 {
            self.tableView.insertRows(at: indexPathsToAdd, with:  rowAnimation)
        }
    }
    
    func reloadTableViewWithTransitionDirection(_ transitionDirection: TableViewReloadTransitionDiraction) {
        var subtype = CATransitionSubtype.fromRight
        switch transitionDirection {
        case .right: subtype = CATransitionSubtype.fromRight
        case .left: subtype =  CATransitionSubtype.fromLeft
        case .top: subtype = CATransitionSubtype.fromTop
        case .bottom: subtype = CATransitionSubtype.fromBottom
        }
        let transition = CATransition()
        transition.type = CATransitionType.push
        transition.subtype = subtype
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeIn)
        transition.fillMode = CAMediaTimingFillMode.both
        transition.duration = 0.25
        
        // Make any view changes
        self.tableView.contentOffset = CGPoint.zero
        self.tableView.reloadData()
        
        // Add the transition
        self.tableView.layer.add(transition, forKey: "transition")
    }
    
    @IBAction func onCancelSearch(_ sender: AnyObject) {
        searchTextField.text = ""
        self.updateSearchPlaceholder()
        
        if let vm = viewModel {
            vm.cancelSearch()
            self.reloadTableViewWithTransitionDirection(.top)
        }
    }
    
    @IBAction func onBack(_ sender: AnyObject) {
        viewModel!.back()
    }
    
    @IBAction func onClose(_ sender: AnyObject) {
        self.delegate?.browseViewControllerDidRequestClose(self)
    }
}

extension BrowseViewController: BrowseViewModelDelegate {
    func didLoadMoreInCurrentPageInBrowseViewModel(_ browseViewModel: BrowseViewModel) {
        self.tableView.reloadData()
    }
    
    func didNavigateToPage(_ page: SpeakerNavPage, inBrowseViewModel: BrowseViewModel) {
        if self.previousPage != nil {
            self.reloadTableViewWithTransitionDirection(.right)
        } else {
            self.reloadTableViewWithTransitionDirection(.top)
            //self.addRowsFromPage(page, withRowAnimAtion: UITableViewRowAnimation.top)
        }
        
        self.previousPage = page
    }
    
    func didNavigateToParentPage(_ page: SpeakerNavPage, inBrowseViewModel: BrowseViewModel) {
        self.reloadTableViewWithTransitionDirection(.left)
        self.previousPage = page
    }
    
    func didNavigateToSearch(_ page: SpeakerNavPage, inBrowseViewModel: BrowseViewModel) {
        //self.tableView.beginUpdates()
        //self.removeAllRowsWithRowAnimation(UITableViewRowAnimation.top)
        self.reloadTableViewWithTransitionDirection(.top)
        //self.addRowsFromPage(page, withRowAnimAtion: UITableViewRowAnimation.top)
        //self.tableView.endUpdates()
        self.previousPage = nil
    }
}

extension BrowseViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let vm = viewModel {
            if let text = textField.text {
                vm.search(text)
                self.reloadTableViewWithTransitionDirection(.bottom)
                //self.removeAllRowsWithRowAnimation(UITableViewRowAnimation.top)
            }
        }
        
        textField.resignFirstResponder()
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.selectedTextRange = textField.textRange(from: textField.beginningOfDocument, to: textField.endOfDocument)
        if let vm = viewModel {
            vm.startSearch()
        }
    }
}

extension BrowseViewController: UITableViewDataSource {
    var currentPage: SpeakerNavPage? {
        if let vm = viewModel {
            if vm.isSearching.value {
                if let currentPage = vm.currentSearchPage.value {
                    return currentPage
                }
            } else {
                if let currentPage = vm.currentPage.value {
                    return currentPage
                }
            }
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let currentPageItems = self.currentPage?.numItems {
            return currentPageItems
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "browseItem", for: indexPath) as? BrowseCell {
            var item: NavListItem?
            if let page = self.currentPage {
                if page.items.count > (indexPath as NSIndexPath).row {
                    item = page.items[(indexPath as NSIndexPath).row]
                }
            }
            
            cell.accessoryType = .none
            cell.accessoryView = nil
            
            if item == nil {
                viewModel?.loadMore()
                cell.item.value = NavListItem(name: "...", type: NavListType.unknown, subtype: NavListSubType.none, key: UInt32(indexPath.row))
            } else {
                cell.item.value = item
                if item!.type == NavListType.directory {
                    cell.accessoryView = UIImageView(image: UIImage(named:"disclosure_indicator"))
                }
            }
            
            return cell
        }
        
        return UITableViewCell()
    }
}

extension BrowseViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let page = self.currentPage {
            if let vm = viewModel {
                if page.items.count > indexPath.row {
                    let itemSelected = page.items[indexPath.row]
                    vm.selectItem(itemSelected)
                    if let cell = tableView.cellForRow(at: indexPath) as? BrowseCell {
                        
                        if itemSelected.type == NavListType.directory {
                            let spinner = UIActivityIndicatorView()
                            spinner.style = .white
                            spinner.startAnimating()
                            cell.accessoryView = spinner
                        }
                        if itemSelected.type == NavListType.playableItem {
                            tableView.visibleCells.forEach{ cell in cell.accessoryType = .none }
                            cell.tintColor = UIColor.white
                            cell.accessoryType = .checkmark
                            runAfterDelay(0.25, block: {[weak self] in
                                
                                self?.delegate?.browseViewControllerDidRequestClose(self!)
                            })
                        }
                    }
                }
            }
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        updateSeparatorViewVisibility()
    }
}
