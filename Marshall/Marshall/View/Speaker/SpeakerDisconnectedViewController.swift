//
//  SpeakerDisconnectedViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 21/06/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit
import MinuetSDK

protocol SpeakerDisconnectedViewControllerDelegate: class {
    func speakerDisconnectedDidRequestReconnect(_ viewController: SpeakerDisconnectedViewController)
    func speakerDisconnectedDidRequestDisconnect(_ viewController: SpeakerDisconnectedViewController)
}

class SpeakerDisconnectedViewController: UIViewController {
    @IBOutlet weak var disconnectedTitleLabel: UILabel!
    @IBOutlet weak var disconnectedTextLabel: UILabel!
    @IBOutlet weak var reconnectButton: UIButton!
    @IBOutlet weak var homescreenButton: UIButton!
    
    var delegate: SpeakerDisconnectedViewControllerDelegate?
    
    var viewModel: SpeakerViewModel?
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIView.setAnimationsEnabled(false)
        reconnectButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.Player.SessionLost.Buttons.Reconnect), for: .normal)
        homescreenButton.setAttributedTitle(Fonts.SecondaryButtonFont.AttributedSecondaryButtonWithString(Localizations.Appwide.Back), for: .normal)
        reconnectButton.layoutIfNeeded()
        homescreenButton.layoutIfNeeded()
        UIView.setAnimationsEnabled(true)
        
        disconnectedTextLabel.font = Fonts.MainContentFont
        disconnectedTitleLabel.font = Fonts.UrbanEars.Bold(23)
        
        if let vm = viewModel {
            vm.connectionStatus.asObservable().subscribe(weak: self, onNext: SpeakerDisconnectedViewController.updateForConnectionStatus).disposed(by: rx_disposeBag)
        }
    }
    
    @IBAction func onGotoHomescreen(_ sender: AnyObject) {
        self.delegate?.speakerDisconnectedDidRequestDisconnect(self)
    }
    
    @IBAction func onReconnect(_ sender: AnyObject) {
        self.delegate?.speakerDisconnectedDidRequestReconnect(self)
    }
    
    func messageForSpeakerNotifierError(_ error: SpeakerNotifierError?) -> String? {
        guard let friendlyName = viewModel?.clientSpeaker.friendlyName else { return nil }
        
        if let notifierError = error {
            switch  notifierError {
            case .networkConnectionLost:
                return Localizations.Player.SessionLost.NetworkConnectionLostToSpeaker(friendlyName)
            case .sessionLost:
                return Localizations.Player.SessionLost.UserIsControllingSpeaker(friendlyName)
            case .unknownError:
                return Localizations.Player.SessionLost.GenericErrorToSpeaker(friendlyName)
            case .canceledByUser:
                return Localizations.Player.SessionLost.GenericErrorToSpeaker(friendlyName)
            case .timeout:
                return Localizations.Player.SessionLost.Timeout(friendlyName)
            case .paused:
                return nil
            }
        } else {
            return  Localizations.Player.SessionLost.SpeakerNotConnected
        }
    }
    
    func titleForSpeakerNotifierError(_ error: SpeakerNotifierError?) -> String? {
        guard let notifierError = error else { return nil }
        
        switch  notifierError {
        case .networkConnectionLost:
            return Localizations.Player.SessionLost.NetworkConnectionLostToSpeakerTitle
        case .sessionLost:
            return Localizations.Player.SessionLost.UserIsControllingSpeakerTitle
        default:
            return nil
        }
    }
    
    func updateForConnectionStatus(_ status: SpeakerConnectionStatus) {
        var message: String? = nil
        var title: String? = nil
        
        switch status {
        case .disconnected(let error):
            message = messageForSpeakerNotifierError(error)
            title = titleForSpeakerNotifierError(error)
        default: break
        }
        
        disconnectedTextLabel.text = message
        disconnectedTitleLabel.text = title
    }
}
