//
//  SpeakerConnectingViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 22/06/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit

protocol SpeakerConnectingViewControllerDelegate: class {
    func speakerConnectingDidRequestCancel(_ viewController: SpeakerConnectingViewController)
}

class SpeakerConnectingViewController: UIViewController {
    @IBOutlet weak var connectingLabel: UILabel!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var spinnerImage: UIImageView!
    
    weak var delegate: SpeakerConnectingViewControllerDelegate?
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        connectingLabel.text = Localizations.Player.Connecting.Reconnecting
        connectingLabel.font = Fonts.UrbanEars.Regular(18)
        
        UIView.setAnimationsEnabled(false)
        cancelButton.setAttributedTitle(Fonts.ButtonFont.AttributedSecondaryButtonWithString(Localizations.Appwide.Cancel), for: .normal)
        cancelButton.layoutIfNeeded()
        UIView.setAnimationsEnabled(true)
        
        self.view.backgroundColor = UIColor.clear
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        spinnerImage.rotate()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        spinnerImage.stopRotation()
    }
    
    @IBAction func cancelButton(_ sender: AnyObject) {
        delegate?.speakerConnectingDidRequestCancel(self)
    }
}
