//
//  PlayerCoordinator.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 24/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import STKWebKitViewController
import SafariServices

import Toast_Swift
import Cartography
import MinuetSDK
import ZoundCommon

protocol PlayerCoordinatorDelegate: class {
    func playerCoordinatorDidStartControllingVolume(_ coordinator: PlayerCoordinator)
    func playerCoordinatorDidFinishControllingVolume(_ coordinator: PlayerCoordinator)
    func playerCoordinatorDidRequestVolumeFromViewController(_ viewController: UIViewController)
    func playerCoordinatorDidFinishCoordinating(_ coordinator: PlayerCoordinator)
}

class PlayerCoordinator: NSObject, Coordinator {
    weak var delegate: PlayerCoordinatorDelegate?
    
    let fadeTransitioningDelegate = FadeTransitioningDelegate()
    let spotifyProvider = SpotifyProvider(isUrbanearsApp: false)
    let disposeBag = DisposeBag()
    let connectionManager: SpeakerConnectionManager
    
    var navController: UINavigationController!
    var appStoreCoordinator: AppStoreCoordinator?
    public var speakerViewController: SpeakerViewController!
    
    var isShowingBrowseViewController: Bool = false
    var browseViewController: BrowseViewController?
    var isStopping = false
    
    var presetsHelpNavController: UENavigationController?
    
    var isBrowseViewVisible: Bool {
        return self.isShowingBrowseViewController
    }
    
    var isVolumeViewVisible: Bool {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return false }
        return appDelegate.appCoordinator.isShowingVolumeViewController
    }
    
    init(connectionManager: SpeakerConnectionManager, navigationController: UINavigationController) {
        self.connectionManager = connectionManager
        self.navController = navigationController
        
        super.init()
        
        self.connectionManager.connectionStatus.asObservable()
            .distinctUntilChanged()
            .subscribe(onNext: { [weak self] status in
                if case .disconnected(_) = status {
                    self?.updateForDisconnected()
                }
            }).disposed(by: disposeBag)
    }
    
    func updateForDisconnected() {
        guard isShowingBrowseViewController, let vc = browseViewController else { return }
        browseViewControllerDidRequestClose(vc)
    }
    
    func start() {
        start(animated: true)
    }
    
    func start(animated: Bool) {
        self.connectionManager.connect()
        
        speakerViewController = UIStoryboard.player.speakerViewController
        speakerViewController.delegate = self
        
        let speakerViewModel = SpeakerViewModel(connectionManager: connectionManager, spotifyProvider: spotifyProvider)
        speakerViewController.viewModel = speakerViewModel
        
        self.navController.pushViewController(speakerViewController, animated: animated)
    }
    
    func stop() {
        if let spViewController = speakerViewController {
            if !isStopping {
                isStopping = true
                if spViewController.presentedViewController != nil {
                    spViewController.dismiss(animated: true, completion: { [weak self] in
                        self?.delegate?.playerCoordinatorDidFinishCoordinating(self!)
                    })
                } else if spViewController.playerViewController?.presentingViewController != nil {
                    spViewController.playerViewController?.presentingViewController?.dismiss(animated: true, completion: { [weak self] in
                        self?.delegate?.playerCoordinatorDidFinishCoordinating(self!)
                    })
                }
                else {
                    self.delegate?.playerCoordinatorDidFinishCoordinating(self)
                }
            }
        }
    }
    
    func showPresetsDisabledAlert() {
        guard let rootVC = self.navController.parent as? RootViewController else { return }
        
        let presetDisabled = UIStoryboard.player.presetDisabledInfoController
        rootVC.definesPresentationContext = true
        
        presetDisabled.providesPresentationContextTransitionStyle = true
        presetDisabled.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        presetDisabled.transitioningDelegate = fadeTransitioningDelegate
        presetDisabled.delegate = self
        
        rootVC.present(presetDisabled, animated: true, completion: nil)
    }
}

extension PlayerCoordinator {
    func showAirplay() {
        let airplay = UIStoryboard.setup.airplaySourceController
        airplay.delegate = self
        self.navController.present(airplay, animated: true, completion: nil)
    }
    
    func showGoogleCast() {
        let googleCast = UIStoryboard.setup.googleSourceViewController
        googleCast.delegate = self
        self.navController.present(googleCast, animated: true, completion: nil)
    }
    
    func showSpotify() {
        let spotify = UIStoryboard.setup.spotifySourceViewController
        spotify.delegate = self
        self.navController.present(spotify, animated: true, completion: nil)
    }
    
    func showAddPresetFromViewController(_ viewController: UIViewController, view: UIView) {
        let addPreset = UIStoryboard.player.savePresetController
        let addPresetViewModel = SavePresetViewModel(speaker: connectionManager.clientSpeaker, clientState: connectionManager.clientState, provider: connectionManager.provider, notifier: connectionManager.clientNotifier, spotifyProvider: spotifyProvider)
        addPreset.viewModel = addPresetViewModel
        addPreset.modalPresentationCapturesStatusBarAppearance = false
        addPreset.delegate = self
        
        if UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad {
            addPreset.modalPresentationStyle = UIModalPresentationStyle.formSheet
        } else {
            addPreset.modalPresentationStyle = .overFullScreen
        }
        
        viewController.present(addPreset, animated: true, completion: nil)
    }
    
    func showBrowseRadioFromViewController(_ viewController: UIViewController, view: UIView) {
        let browseRadio = UIStoryboard.player.browseRadioViewController
        browseRadio.delegate = self
        browseRadio.viewModel = BrowseViewModel(speaker: connectionManager.clientSpeaker, speakerNotifier: connectionManager.clientNotifier, provider: connectionManager.provider)
        browseRadio.modalPresentationCapturesStatusBarAppearance = false
        
        if UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad {
            browseRadio.modalPresentationStyle = UIModalPresentationStyle.formSheet
        } else {
            browseRadio.modalPresentationStyle = .overFullScreen
        }
        
        self.isShowingBrowseViewController = true
        browseViewController = browseRadio
        viewController.present(browseRadio, animated: true, completion: nil)
    }
    
    func showHelpWebPage(withURL url: URL, animated: Bool = true) {
        let localWeb = UIStoryboard.help.localWebViewController
        localWeb.localPageURL = url
        localWeb.delegate = self
        
        if presetsHelpNavController == nil {
            localWeb.isRoot = true
            presetsHelpNavController = UENavigationController(rootViewController: localWeb)
            presetsHelpNavController!.navigationBar.isHidden = true
            presetsHelpNavController!.modalPresentationStyle = .formSheet
            self.navController.present(presetsHelpNavController!, animated: true, completion: nil)
        } else {
            DispatchQueue.main.async {
                self.presetsHelpNavController?.pushViewController(localWeb, animated: true)
            }
        }
    }
}


extension PlayerCoordinator: LocalWebViewControllerDelegate {
    func localWebViewControllerDidRequestBack(_ localWebViewController: LocalWebViewController) {
        if let _ = localWebViewController.presentingViewController {
            if presetsHelpNavController?.viewControllers.first == localWebViewController {
                localWebViewController.dismiss(animated: true, completion: nil)
                presetsHelpNavController = nil
            } else {
                presetsHelpNavController?.popViewController(animated: true)
            }
        }
    }
    
    func localWebViewControllerDidRequestLoadPageNamed(_ pageName: String, localWebViewController: LocalWebViewController) {
        var subdirectory = "QuickGuide/en"
        
        if let language = NSLocale.current.languageCode {
            subdirectory = "QuickGuide/\(language)"
        }
        
        if let pageURL = Bundle.main.url(forResource: pageName, withExtension: "html", subdirectory: subdirectory) {
            self.showHelpWebPage(withURL: pageURL)
        }
    }
}


//# MARK:- PresetDisabledInfoViewControllerDelegate
extension PlayerCoordinator: PresetDisabledInfoViewControllerDelegate {
    func presetDisabledAlertDidRequestContinue(_ alertViewController: PresetDisabledInfoViewController) {
        guard let vc = alertViewController.presentingViewController else { return }
        vc.dismiss(animated: true, completion: nil)
    }
    
    func presetDisabledAlertDidRequestLearnMore(_ alertViewController: PresetDisabledInfoViewController) {
        guard let vc = alertViewController.presentingViewController else { return }
        
        vc.dismiss(animated: true, completion: {
            if let pageURL = QuickGuideItem.presets.localWebPageURL {
                self.showHelpWebPage(withURL: pageURL)
            }
        })
    }
}

//# MARK:- SavePresetViewControllerDelegate
extension PlayerCoordinator: SavePresetViewControllerDelegate {
    func savePresetViewControllerDidRequestClose(_ savePresetViewController: SavePresetViewController) {
        savePresetViewController.presentingViewController?.dismiss(animated: true, completion: nil)
    }
}

//# MARK:- BrowseViewControllerDelegate
extension PlayerCoordinator: BrowseViewControllerDelegate {
    func browseViewControllerDidRequestClose(_ browseViewController: BrowseViewController) {
        self.isShowingBrowseViewController = false
        self.browseViewController = nil
        browseViewController.presentingViewController?.dismiss(animated: true, completion: nil)
    }
}

//# MARK:- AirplaySourceViewControllerDelegate
extension PlayerCoordinator: AirplaySourceViewControllerDelegate {
    func airplaySourceDidRequestBack(_ viewController: AirplaySourceViewController) {
        self.navController.dismiss(animated: true, completion: nil)
    }
    
    func airplaySourceDidRequestOpenApp(_ viewController: AirplaySourceViewController) {
        self.navController.dismiss(animated: true) { [weak self] in
            self?.openMusicApp()
        }
        
    }
    
    func openMusicApp() {
        let appleMusicAppLink = "music://"
        let appleMusicAppURL = URL(string:appleMusicAppLink)!
        
        UIApplication.shared.openURL(appleMusicAppURL)
    }
}

//# MARK:- GoogleCastSourceViewControllerDelegate
extension PlayerCoordinator: GoogleCastSourceViewControllerDelegate {
    func googleCastSourceDidRequestBack(_ viewController: GoogleCastSourceViewController) {
        self.navController.dismiss(animated: true, completion: nil)
    }
    
    func googleCastSourceDidRequestFindCastApps(_ viewController: GoogleCastSourceViewController) {
        self.navController.dismiss(animated: true) { 
            if let castAppsLink = URL(string: "https://www.google.com/intl/en_us/chromecast/apps/?utm_source=chromecast.com"),
                let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                if let rootViewController = appDelegate.window?.rootViewController {
                    
                    if #available(iOS 9, *) {
                        let safari = SFSafariViewController(url: castAppsLink)
                        safari.modalPresentationStyle = .overFullScreen
                        safari.modalPresentationCapturesStatusBarAppearance = true
                        rootViewController.present(safari, animated: true, completion: nil)
                    } else {
                        let webViewController = STKWebKitModalViewController(url: castAppsLink)
                        rootViewController.present(webViewController!, animated: true, completion: nil)
                    }
                    
                }
            }
        }
    }
    
    func googleCastSourceDidRequestOpenApp(_ viewController: GoogleCastSourceViewController, appId: String, appURL: String) {
        viewController.presentingViewController?.dismiss(animated: true, completion: { [weak self] in
            guard let `self` = self else { return }
            
            if let url  = URL(string: appURL) {
                if UIApplication.shared.canOpenURL(url) == true  {
                    UIApplication.shared.openURL(url)
                } else {
                    if self.appStoreCoordinator == nil {
                        self.appStoreCoordinator = AppStoreCoordinator(parentViewController: self.navController, itunesItemIdentifier: appId)
                        self.appStoreCoordinator?.delegate = self
                        self.appStoreCoordinator?.start()
                    }
                }
            }
        })
    }
}

extension PlayerCoordinator: AppStoreCoordinatorDelegate {
    func appStoreCoordinatorDidFinishCoordinating(_ coordinator: AppStoreCoordinator) {
        appStoreCoordinator = nil
    }
}

//# MARK:- SpotifySourceViewControllerDelegate
extension PlayerCoordinator: SpotifySourceViewControllerDelegate {
    func spotifySourceDidRequestBack(_ viewController: SpotifySourceViewController) {
        self.navController.dismiss(animated: true, completion: nil)
    }
    
    func spotifySourceDidRequestSpeakerName(_ viewController: SpotifySourceViewController) -> String {
        return self.connectionManager.masterSpeaker.friendlyName
    }
}

//# MARK:- SpeakerViewControllerDelegate
extension PlayerCoordinator: SpeakerViewControllerDelegate {
    func speakerViewControllerDidRequestBack(_ speakerViewController: SpeakerViewController) {
        self.delegate?.playerCoordinatorDidFinishCoordinating(self)
    }
    
    func viewControllerDidRequestVolume(_ viewController: UIViewController, fromView: UIView) {
        self.delegate?.playerCoordinatorDidRequestVolumeFromViewController(viewController)
    }
    
    func speakerViewControllerDidRequestAirplay(_ speakerViewController: SpeakerViewController) {
        showAirplay()
    }
    
    func speakerViewControllerDidRequestGoogleCast(_ speakerViewController: SpeakerViewController) {
        showGoogleCast()
    }
    
    func speakerViewControllerDidRequestPresetDisabledInfo(_ speakerViewController: SpeakerViewController) {
        showPresetsDisabledAlert()
    }
    
    func speakerViewControllerDidRequestSpotifyConnect(_ speakerViewController: SpeakerViewController) {
        showSpotify()
    }
    
    func speakerViewControllerDidRequestAddPreset(_ speakerViewController: SpeakerViewController, fromViewController: UIViewController, fromView: UIView) {
        showAddPresetFromViewController(fromViewController, view: fromView)
    }
    
    func speakerViewControllerDidRequestShowSpotifyPlaylistWithId(_ spotifyPlaylistId: String, speakerViewController: SpeakerViewController) {
        guard let firstRangeOfSpotify = spotifyPlaylistId.range(of: "spotify:") else { return }
        
        if let url = URL(string: spotifyPlaylistId) {
            if UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.openURL(url)
            } else {
                let webUriString = spotifyPlaylistId.replacingCharacters(in: firstRangeOfSpotify, with: "").replacingOccurrences(of: ":", with: "/")
                let spotifyAppUriString = "https://open.spotify.com/\(webUriString)"
                if let url = URL(string: spotifyAppUriString) {
                    
                    UIApplication.shared.openURL(url)
                }
            }
        }
    }
    
    func speakerViewControllerDidRequestBrowseRadio(_ speakerViewController: SpeakerViewController, fromViewController: UIViewController, fromView: UIView) {
        let needToSwitchMode = connectionManager.clientState.value?.currentMode == nil || (connectionManager.clientState.value?.currentMode != nil && connectionManager.clientState.value!.currentMode!.isIR == false)
        
        if needToSwitchMode {
            if let radioMode = connectionManager.clientState.value?.modes.find({$0.isIR}) {
                let waitToSwitchToIR = connectionManager.clientNotifier.notificationsForNode(ScalarNode.Mode)
                    .map { radioMode.key == Int($0) }
                    .take(1)
                
                let switchToIRMode = connectionManager.provider.setNode(ScalarNode.Mode, value: String(radioMode.key), forSpeaker: connectionManager.clientSpeaker)
                switchToIRMode
                    .flatMapLatest{ done in return waitToSwitchToIR }
                    .subscribe(onNext: { [weak self] done in
                        self?.showBrowseRadioFromViewController(fromViewController, view: fromView) })
                    .disposed(by: disposeBag)
            }
        } else {
            showBrowseRadioFromViewController(fromViewController, view: fromView)
        }
    }
    
    func speakerViewControllerDidRequestReconnect(_ speakerViewController: SpeakerViewController) {
        connectionManager.reconnect()
    }
    
    func speakerViewControllerDidRequestCancelReconnect(_ speakerViewController: SpeakerViewController) {
        connectionManager.cancelReconnect()
    }
}
