//
//  PresetsViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 24/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import AlamofireImage

protocol PresetsViewControllerDelegate: class {
    func presetsViewControllerDidRequestSpotifyConnect(_ viewController: PresetsViewController)
    func presetsViewControllerDidRequestGoogleCast(_ viewController: PresetsViewController)
    func presetsViewControllerDidRequestAirplay(_ viewController: PresetsViewController)
    func presetsViewControllerDidRequestPresetsDisabledInfo(_ viewController: PresetsViewController)
    func presetsViewControllerDidRequestPlayRadio(_ viewController: PresetsViewController, view: UIView)
    func presetsViewControllerDidRequestShowSpotifyPlaylist(_ spotifyPlaylistId: String)
}

class PresetsViewController: UIViewController {
    @IBOutlet weak var presetTypeLabel: UILabel!
    @IBOutlet weak var presetNumbersScrollView: UIScrollView!
    @IBOutlet weak var presetsScrollView: UIScrollView!
    
    weak var delegate : PresetsViewControllerDelegate?
    
    var viewControllerCache = [Int: SelectableViewController]()
    var selectableViewControllers: [SelectableViewController] = []
    
    var viewModel: PresetsViewModel?
    var didSetInitialContentOffset = false
    var lastPresetBoundsDidLayout: CGRect = CGRect.zero
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.clear
        
        presetTypeLabel.font = Fonts.UrbanEars.Bold(18)
        if let view = self.view as? PresetsView {
            view.dataSource = self
            if let vm = viewModel {
                view.reloadViews(vm.currentSelectedViewModelIndex.value)
                
                vm.selectableItems.asObservable()
                    .skip(1)
                    .subscribe(onNext:{[ weak self] presets in
                        self?.updateViewModelsInSelectableViewControllers() })
                    .disposed(by: rx_disposeBag)
            }
        }
    }
    
    func updateViewModelsInSelectableViewControllers() {
        for viewController in self.viewControllerCache.map({ (key, value) in return value }) {
            
            if let presetViewController = viewController as? PresetSelectableViewController {
                if let vm = viewModel {
                    if let newSelectableItem = vm.selectableItems.value.find({ selectableViewModel in
                        if let presetViewModel = presetViewController.viewModel as? PresetViewModel {
                            if let selectableViewModel = (selectableViewModel as? PresetViewModel) {
                                if presetViewModel.preset.number == selectableViewModel.preset.number {
                                    return true
                                }
                            }
                        }
                        return false
                    }) {
                        if let oldPresetViewModel = (presetViewController.viewModel as? PresetViewModel),
                            let newPresetViewModel = (newSelectableItem as? PresetViewModel) {
                            if oldPresetViewModel.preset != newPresetViewModel.preset {
                                if oldPresetViewModel.willBeReplacedByUndonePreset {
                                    newPresetViewModel.presetIsUndone = true
                                }
                                if !oldPresetViewModel.preset.isEmpty && !oldPresetViewModel.willBeReplacedByUndonePreset {
                                    newPresetViewModel.previousPreset = oldPresetViewModel.preset
                                }
                            }
                        }
                        
                        presetViewController.viewModel = newSelectableItem
                    }
                }
            }
            
            if let bluetoothViewController = viewController as? BluetoothSelectableViewController {
                if let vm = viewModel {
                    if let newSelectableItem = vm.selectableItems.value.find({ selectableViewModel in
                        if let _ = (selectableViewModel as? BluetoothViewModel),
                            let _ = (bluetoothViewController.viewModel as? BluetoothViewModel) {
                            
                            return true
                        }
                        return false
                        
                    }) {
                        bluetoothViewController.viewModel = newSelectableItem
                        
                    }
                }
            }
            
            if let auxViewController = viewController as? AuxSelectableViewController {
                if let vm = viewModel {
                    if let newSelectableItem = vm.selectableItems.value.find({ selectableViewModel in
                        if let _ = (selectableViewModel as? AuxViewModel),
                            let _ = (auxViewController.viewModel as? AuxViewModel) {
                            return true
                        }
                        return false
                        
                    }) {
                        auxViewController.viewModel = newSelectableItem
                    }
                }
            }
            
            if let rcaViewController = viewController as? RCASelectableViewController {
                if let vm = viewModel {
                    if let newSelectableItem = vm.selectableItems.value.find({ selectableViewModel in
                        if let _ = (selectableViewModel as? RCAViewModel),
                            let _ = (rcaViewController.viewModel as? RCAViewModel) {
                            return true
                        }
                        return false
                    }) {
                        rcaViewController.viewModel = newSelectableItem
                    }
                }
            }
        }
    }
    
    func selectableItemIndexFromPage(_ page: Int) -> Int {
        guard let vm = viewModel else { return -1 }
        
        let totalPages = vm.selectableItems.value.count
        let modIndex = page % totalPages
        let index = modIndex >= 0 ? modIndex : totalPages-abs(modIndex)-1
        return index
    }
    
    func selectableViewModelForPage(_ page: Int) -> SelectableViewModel?{
        guard let vm = viewModel else { return nil }
        
        let index = selectableItemIndexFromPage(page)
        let selectableViewModel = vm.selectableItems.value[index]
        return selectableViewModel
    }
    
    
    func numberViewForPage(_ page: Int) -> UIView {
        let numberButton = UIButton(type: .system)
        numberButton.alpha = 0.3
        numberButton.addTarget(self, action: #selector(onNumberSelected), for: .touchUpInside)
        numberButton.translatesAutoresizingMaskIntoConstraints = true
        numberButton.autoresizingMask = []
        numberButton.tag = page
        
        let selectableViewModel = selectableViewModelForPage(page)
        
        if let text = selectableViewModel?.selectableText {
            numberButton.setTitle(text, for: .normal)
            numberButton.titleLabel!.font = Fonts.UrbanEars.Bold(35)
            //numberButton.setTitleColor(UIColor.white, for: .normal)
            numberButton.contentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        } else if let imageName = selectableViewModel?.selectableImageName {
            numberButton.setImage(UIImage(named:imageName), for: .normal)
            numberButton.tintColor = UIColor.white
        }
        
        return numberButton
    }
    
    func selectableViewControllerForSelectableViewModel(_ viewModel: SelectableViewModel) -> SelectableViewController {
        switch viewModel.selectableType {
        case .aux:
            return UIStoryboard.player.auxSelectableViewController
        case .rca:
            return UIStoryboard.player.rcaSelectableViewController
        case .bluetooth:
            return UIStoryboard.player.bluetoothSelectableViewController
        case .cloud,.internetRadio:
            let cloudViewController = UIStoryboard.player.cloudSelectableViewController
            cloudViewController.delegate = self
            return cloudViewController
        case .preset:
            let presetViewController = UIStoryboard.player.presetViewController
            presetViewController.delegate = self
            return presetViewController
        }
    }
    
    @objc func onNumberSelected(_ sender: UIButton) {
        let center = sender.center
        let size = self.presetNumbersScrollView.frame.size
        let untransformedFrame = CGRect(x: center.x-size.width/2,
                                        y: center.y-size.height/2,
                                        width: size.width,
                                        height: size.height)
        
        self.presetNumbersScrollView.scrollRectToVisible(untransformedFrame, animated: true)
    }
    
    @IBAction func onPlayPause(_ sender: AnyObject) {
    }
}

extension PresetsViewController: PresetSelectableViewControllerDelegate {
    func presetSelectableViewControllerDidRequestPresetDisabledInfo(_ viewController: PresetSelectableViewController) {
        self.delegate?.presetsViewControllerDidRequestPresetsDisabledInfo(self)
    }
    
    func presetSelectableViewControllerDidRequestShowPlaylistDetails(_ viewController: PresetSelectableViewController) {
        guard let vm = viewController.viewModel as? PresetViewModel,
            let spotifyURI = vm.preset.spotifyURI else { return }
        
        self.delegate?.presetsViewControllerDidRequestShowSpotifyPlaylist(spotifyURI)
    }
}

extension PresetsViewController: CloudSelectableViewControllerDelegate {
    func cloudViewControllerDidRequestAirplay(_ viewController: CloudSelectableViewController) {
        self.delegate?.presetsViewControllerDidRequestAirplay(self)
    }
    
    func cloudViewControllerDidRequestGoogleCast(_ viewController: CloudSelectableViewController) {
        self.delegate?.presetsViewControllerDidRequestGoogleCast(self)
    }
    
    func cloudViewControllerDidRequestSpotifyConnect(_ viewController: CloudSelectableViewController) {
        self.delegate?.presetsViewControllerDidRequestSpotifyConnect(self)
    }
    
    func cloudViewControllerDidRequestBrowseRadio(_ viewController: CloudSelectableViewController, view: UIView) {
        self.delegate?.presetsViewControllerDidRequestPlayRadio(self, view: view)
    }
}

extension PresetsViewController: PresetsViewDataSource {
    func numberOfSelectableViewsInPresetsView(_ presetsView: PresetsView) -> Int {
        return self.viewModel?.selectableItems.value.count ?? 0
    }
    
    func viewControllerForPage(_ page: Int) -> SelectableViewController? {
        let index = selectableItemIndexFromPage(page)
        return viewControllerCache[index]
    }
    
    func presetsView(_ presetsView: PresetsView, selectableViewForIndex page:Int) -> UIView {
        if let viewModel = self.selectableViewModelForPage(page) {
            var viewController = viewControllerForPage(page)
            if viewController == nil {
                viewController = self.selectableViewControllerForSelectableViewModel(viewModel)
                viewController!.viewModel = viewModel
                let index = selectableItemIndexFromPage(page)
                viewControllerCache[index] = viewController!
            }
            
            selectableViewControllers.append(viewController!)
            return viewController!.view
        }
        return UIView()
    }
    
    func presetsView(_ presetsView: PresetsView, numberViewForIndex index:Int) -> UIView {
        return numberViewForPage(index)
    }
    
    func presetsView(_ presetsView: PresetsView, titleForPage page:Int) -> String? {
        let selectableViewModel = selectableViewModelForPage(page)
        return selectableViewModel?.selectableType.typeName.uppercased()
    }
    
    func presetsView(_ presetsView: PresetsView, shouldUseFullWidthForViewAtIndex page:Int) -> Bool {
        return true
    }
    
    func presetsView(_ presetsView: PresetsView, willShowView view:UIView) {
        guard let vc = selectableViewControllers.filter({ $0.view == view }).first else { return }
        
        self.addChild(vc)
    }
    
    func presetsView(_ presetsView: PresetsView, didShowView view:UIView) {
        guard let vc = selectableViewControllers.filter({$0.view == view}).first else { return }
        
        vc.relativeDistanceFromCenter = 0
        vc.didMove(toParent: self)
    }
    
    func presetsView(_ presetsView: PresetsView, willHideView view:UIView) {
        guard let vc = selectableViewControllers.filter({$0.view == view}).first else { return }
        
        vc.willMove(toParent: nil)
    }
    
    func presetsView(_ presetsView: PresetsView, didHideView view:UIView) {
        guard let vc = selectableViewControllers.filter({$0.view == view}).first else { return }
        
        vc.removeFromParent()
        selectableViewControllers.removeObject(vc)
    }
    
    func presetsView(_ presetsView: PresetsView, didUpdateRelativeDistanceFromCenter relativeDistanceFromCenter:Double, forView view: UIView) {
        view.isUserInteractionEnabled = fabs(relativeDistanceFromCenter) < 0.1
        
        guard let vc = selectableViewControllers.filter({$0.view == view}).first else { return }
        
        vc.nextFillsScreen = false
        vc.prevFillsScreen = false
        
        vc.relativeDistanceFromCenter = relativeDistanceFromCenter
    }
}
