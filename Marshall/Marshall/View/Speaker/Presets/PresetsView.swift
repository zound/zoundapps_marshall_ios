//
//  PresetsView.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 14/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import ZoundCommon

@objc protocol PresetsViewDataSource: class {
    func numberOfSelectableViewsInPresetsView(_ presetsView: PresetsView) -> Int
    func presetsView(_ presetsView: PresetsView, selectableViewForIndex:Int) -> UIView
    func presetsView(_ presetsView: PresetsView, numberViewForIndex:Int) -> UIView
    func presetsView(_ presetsView: PresetsView, shouldUseFullWidthForViewAtIndex:Int) -> Bool
    func presetsView(_ presetsView: PresetsView, titleForPage page:Int) -> String?
    
    func presetsView(_ presetsView: PresetsView, willShowView:UIView)
    func presetsView(_ presetsView: PresetsView, didShowView:UIView)
    
    func presetsView(_ presetsView: PresetsView, willHideView:UIView)
    func presetsView(_ presetsView: PresetsView, didHideView:UIView)
    
    func presetsView(_ presetsView: PresetsView, didUpdateRelativeDistanceFromCenter relativeDistanceFromCenter:Double, forView: UIView)
}

class PresetsView: UIView, UIScrollViewDelegate {
    @IBOutlet weak var dataSource: PresetsViewDataSource?
    @IBOutlet weak var presetTypeLabel: UILabel!
    @IBOutlet weak var presetNumbersScrollView: UIScrollView!
    @IBOutlet weak var presetsScrollView: UIScrollView!
    
    var globalCurrentPage: CGFloat = 0
    var bindingsDisposable: CompositeDisposable?
    var numbersScrollSizeUsedForLayout: CGSize?
    var scrollSizeUsedForLayout: CGSize?
    
    var lastLeftPage: Int = 0
    var lastCenterPage: Int = 0
    
    override func awakeFromNib() {
        self.backgroundColor = UIColor.clear
    }
    
    func reloadViews(_ initialSelectedPage: Int) {
        self.presetsScrollView.subviews.forEach{ [weak self] selectableView in
            self?.dataSource?.presetsView(self!, willHideView: selectableView)
            selectableView.removeFromSuperview()
            self?.dataSource?.presetsView(self!, didHideView: selectableView)
        }
        
        self.presetNumbersScrollView.subviews.forEach{ selectableView in
            selectableView.removeFromSuperview()
        }
        
        updateGlobalCurrentPageFromCurrentPage(initialSelectedPage)
        
        recreateBindings()
        
        let presetNumbersBounds = presetNumbersScrollView.bounds
        let contentOffset = presetNumbersScrollView.contentOffset
        let currentLeftPage = Int(floor(contentOffset.x/presetNumbersBounds.size.width))
        let currentPage = contentOffset.x/presetNumbersBounds.size.width
        
        self.updateVisibleViewsForPage(currentLeftPage)
        self.updateScrollingViewsForCurrentPage(currentPage)
    }
    
    func updateGlobalCurrentPageFromCurrentPage(_ currentPage: Int) {
        let pagesCount = self.dataSource?.numberOfSelectableViewsInPresetsView(self) ?? 0
        globalCurrentPage =  1000*CGFloat(pagesCount) + CGFloat(currentPage)
    }
    
    func recreateBindings() {
        self.bindingsDisposable?.dispose()
        self.bindingsDisposable = CompositeDisposable()
        self.bindingsDisposable?.disposed(by: rx_disposeBag)
        
        presetsScrollView.delegate = self
        presetNumbersScrollView.delegate = self
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == presetNumbersScrollView {
            numbersScrollViewDidScroll()
        }
        
        if scrollView == presetsScrollView {
            presetsScrollViewDidScroll()
        }
        
        updateVisibleAndScrollingViews()
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        self.didEndScrolling()
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.didEndScrolling()
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if (!decelerate) {
            self.didEndScrolling()
        }
    }
    
    func didEndScrolling() {
        let presetNumbersBounds = presetNumbersScrollView.bounds
        let contentOffset = presetNumbersScrollView.contentOffset
        let currentPage = contentOffset.x/presetNumbersBounds.size.width
        let currentPageInt = Int(round(currentPage))
        let totalPages = self.dataSource?.numberOfSelectableViewsInPresetsView(self) ?? 0
        let normalizedPageInt = currentPageInt % totalPages
        updateGlobalCurrentPageFromCurrentPage(normalizedPageInt)
    }
    
    func updateVisibleAndScrollingViews() {
        let presetNumbersBounds = presetNumbersScrollView.bounds
        let contentOffset = presetNumbersScrollView.contentOffset
        let currentPage = contentOffset.x/presetNumbersBounds.size.width
        
        self.updateScrollingViewsForCurrentPage(currentPage)
        let currentLeftPage = Int(floor(contentOffset.x/presetNumbersBounds.size.width))
        
        if lastLeftPage != currentLeftPage {
            lastLeftPage = currentLeftPage
            self.updateVisibleViewsForPage(currentLeftPage)
        }
        
        let currentCenterPage = Int(round(contentOffset.x/presetNumbersBounds.size.width))
        if lastCenterPage != currentCenterPage {
            
            lastCenterPage = currentCenterPage
            self.updateTitleForPage(currentCenterPage)
        }
        
    }
    
    func presetsScrollViewDidScroll() {
        let presetNumbersBounds = presetNumbersScrollView.bounds
        let presetsBounds = presetsScrollView.bounds
        let scrollRatio = presetsBounds.size.width/presetNumbersBounds.size.width
        let contentOffset = presetsScrollView.contentOffset
        let scrollPoint = CGPoint(x: contentOffset.x/scrollRatio, y: contentOffset.y)
        
        var newBounds = presetNumbersBounds
        newBounds.origin = scrollPoint
        presetNumbersScrollView.bounds = newBounds
    }
    
    func numbersScrollViewDidScroll() {
        let presetNumbersBounds = presetNumbersScrollView.bounds
        let presetsBounds = presetsScrollView.bounds
        let scrollRatio = presetsBounds.size.width/presetNumbersBounds.size.width
        let contentOffset = presetNumbersScrollView.contentOffset
        let scrollPoint = CGPoint(x: scrollRatio*contentOffset.x, y: contentOffset.y)
        
        var newBounds = presetsBounds
        newBounds.origin = scrollPoint
        presetsScrollView.bounds = newBounds
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let newNumbersSize = presetNumbersScrollView.frame.size
        if numbersScrollSizeUsedForLayout != newNumbersSize {
            numbersScrollSizeUsedForLayout = newNumbersSize
            self.updateNumbersLayout(newNumbersSize)
        }
        
        let newScrollSize  = presetsScrollView.frame.size
        if scrollSizeUsedForLayout != newScrollSize {
            scrollSizeUsedForLayout = newScrollSize
            self.updateSelectableViewsLayout(newScrollSize)
        }
        
        updateVisibleAndScrollingViews()
    }
    
    
    func onPlaylistSelected(_ gesture: UIGestureRecognizer) {
        let frameToCenter = gesture.view!.frame
        let scrollSize = self.presetsScrollView.bounds.size
        let frameToScrollTo = CGRect(x: frameToCenter.minX+(frameToCenter.size.width-scrollSize.width)/2,
                                     y: frameToCenter.minY,
                                     width: scrollSize.width,
                                     height: scrollSize.height)
        self.presetsScrollView.scrollRectToVisible(frameToScrollTo, animated: true)
    }
    
    func selectableViewForPage(_ page: Int) -> UIView {
        if let selectableView = dataSource?.presetsView(self, selectableViewForIndex: page) {
            selectableView.translatesAutoresizingMaskIntoConstraints = true
            selectableView.autoresizingMask = UIView.AutoresizingMask()
            
            selectableView.backgroundColor = UIColor.clear//(page % 2 == 0) ? UIColor(white: 1.0, alpha: 0.1) : UIColor(white: 1.0, alpha: 0.3)
            selectableView.tag = page
            
            return selectableView
        }
        
        return UIView()
    }
    
    func updateTitleForPage(_ currentPage: Int) {
        let newTitle = self.dataSource?.presetsView(self, titleForPage: currentPage)
        UIView.transition(with: presetTypeLabel,
                          duration: 0.4,
                          options: [.transitionCrossDissolve,.beginFromCurrentState],
                          animations: { [weak self] in
                            self?.presetTypeLabel.text = newTitle
            },
                          completion: nil)
    }
    
    func updateVisibleViewsForPage(_ currentPage: Int) {
        let visiblePages = presetNumbersScrollView.superview!.bounds.size.width/presetNumbersScrollView.bounds.size.width
        let currentPageCount = 1
        let totalPagesToLoad = visiblePages-CGFloat(currentPageCount)
        let pagesToLoadInEachDirection = Int(ceil(totalPagesToLoad/2))
        
        let currentIndexes = Set(presetsScrollView.subviews.map { $0.tag })
        let nextIndexes = Set((currentPage-pagesToLoadInEachDirection)...(currentPage+pagesToLoadInEachDirection+1))
        let addedIndexes = nextIndexes.subtracting(currentIndexes)
        let removedIndexes = currentIndexes.subtracting(nextIndexes)
        
        let selectableViewsToRemove = presetsScrollView.subviews.filter{ removedIndexes.contains($0.tag) }
        selectableViewsToRemove.forEach{ [weak self] selectableView in
            self?.dataSource?.presetsView(self!, willHideView: selectableView)
            selectableView.removeFromSuperview()
            self?.dataSource?.presetsView(self!, didHideView: selectableView)
        }
        
        let numbersToRemove = presetNumbersScrollView.subviews.filter { removedIndexes.contains($0.tag) }
        numbersToRemove.forEach{
            $0.removeFromSuperview()
        }
        
        for page in  addedIndexes {
            if let numberView = self.dataSource?.presetsView(self, numberViewForIndex: page) {
                presetNumbersScrollView.addSubview(numberView)
                numberView.autoresizingMask = []
                numberView.tag = page
                layoutNumberView(numberView)
            }
            
            if let selectableView =  self.dataSource?.presetsView(self, selectableViewForIndex: page) {
                self.dataSource?.presetsView(self, willShowView: selectableView)
                presetsScrollView.addSubview(selectableView)
                selectableView.autoresizingMask = []
                selectableView.tag = page
                self.dataSource?.presetsView(self, didShowView: selectableView)
                layoutSelectableView(selectableView)
            }
        }
        
        presetsScrollView.subviews.filter { $0.tag == currentPage }.forEach {
            $0.superview?.bringSubviewToFront($0)
        }
    }
    
    func updateScrollingViewsForCurrentPage(_ currentPage: CGFloat) {
        let minAlpha = CGFloat(0.3)
        let maxAlpha = CGFloat(1.0)
        let alphaDelta = maxAlpha-minAlpha
        
        let minScale = CGFloat(0.71)
        let maxScale = CGFloat(1.00)
        let scaleDelta = maxScale-minScale
        
        let baseColor = UIColor(red:155.0/255.0, green:155.0/255.0, blue:155.0/255.0, alpha:255.0/255.0)
        let highlightedColor = UIColor(red:174.0/255.0, green:145.0/255.0, blue:91.0/255.0, alpha:255.0/255.0)
        
        self.presetNumbersScrollView.subviews.forEach{ $0.alpha = minAlpha }
        
        //handle number views
        for (_, subview) in self.presetNumbersScrollView.subviews.enumerated() {
            let point = subview.superview!.convert(subview.center, to: self.presetNumbersScrollView.superview)
            let distanceFromCenter = self.presetNumbersScrollView.center.x - point.x
            let scrollingWidth = self.presetNumbersScrollView.bounds.size.width
            let relativeDistanceFromCenter = (scrollingWidth - distanceFromCenter)/(scrollingWidth) - 1
            
            let percentage = fmin(abs(CGFloat(relativeDistanceFromCenter)),1.0)
            subview.alpha = maxAlpha - alphaDelta * percentage
            let scale = maxScale - scaleDelta * percentage
            subview.transform = CGAffineTransform(scaleX: scale, y: scale)
            subview.tintColor = baseColor.interpolateRGBColorTo(end: highlightedColor, fraction: 1-percentage)
        }

        //handle large views
        for (_, view) in self.presetsScrollView.subviews.enumerated() {
            let point = view.superview!.convert(view.center, to: self.presetsScrollView.superview)
            let distanceFromCenter = self.presetsScrollView.center.x - point.x
            let scrollingWidth = self.presetsScrollView.bounds.size.width
            let relativeDistanceFromCenter = (scrollingWidth - distanceFromCenter)/(scrollingWidth) - 1
            
            self.dataSource?.presetsView(self, didUpdateRelativeDistanceFromCenter: Double(relativeDistanceFromCenter), forView: view)
        }
    }
    
    func layoutSelectableView(_ selectableView: UIView) {
        let presetsBounds = self.presetsScrollView.frame
        let superviewFrame = self.presetsScrollView.superview!.frame
        let presetsContainerBounds = CGRect(x: presetsBounds.origin.x, y: presetsBounds.origin.y, width: superviewFrame.size.width, height: presetsBounds.size.height)
        
        let pageNumber = selectableView.tag
        let shouldLayoutToScreenWidth = self.dataSource?.presetsView(self, shouldUseFullWidthForViewAtIndex: pageNumber)
        
        if shouldLayoutToScreenWidth == false {
            selectableView.frame = CGRect(x: presetsBounds.size.width*CGFloat(selectableView.tag),
                                          y: 0,
                                          width: presetsBounds.size.width,
                                          height: presetsBounds.size.height)
        } else {
            selectableView.frame = CGRect(x: presetsBounds.size.width*CGFloat(selectableView.tag)-(presetsContainerBounds.size.width-presetsBounds.size.width)/2,
                                          y: 0,
                                          width: presetsContainerBounds.size.width,
                                          height: presetsContainerBounds.size.height)
        }
    }
    
    func layoutNumberView(_ numberView:UIView) {
        let presetNumbersBounds = self.presetNumbersScrollView.frame
        
        numberView.frame = CGRect(x: presetNumbersBounds.size.width*CGFloat(numberView.tag),
                                  y: 0,
                                  width: presetNumbersBounds.size.width,
                                  height: presetNumbersBounds.size.height)
        
    }
    
    func updateSelectableViewsLayout(_ size: CGSize) {
        presetsScrollView.subviews.forEach(layoutSelectableView)
        self.presetsScrollView.contentSize = CGSize(width: CGFloat.greatestFiniteMagnitude, height: size.height)
    }
    
    func updateNumbersLayout(_ size: CGSize) {
        presetNumbersScrollView.subviews.forEach(layoutNumberView)
        
        self.presetNumbersScrollView.contentSize = CGSize(width: CGFloat.greatestFiniteMagnitude,  height: size.height)
        self.presetNumbersScrollView.contentOffset = CGPoint(x: size.width*self.globalCurrentPage, y: 0)
    }
}
