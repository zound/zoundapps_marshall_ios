//
//  PresetViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 04/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit
import AlamofireImage
import RxSwift
import MinuetSDK

protocol PresetSelectableViewControllerDelegate: class {
    func presetSelectableViewControllerDidRequestPresetDisabledInfo(_ viewController: PresetSelectableViewController)
    func presetSelectableViewControllerDidRequestShowPlaylistDetails(_ viewController: PresetSelectableViewController)
}

enum PresetNotification {
    case addPreset
    case addPlaylistSuccess
    case addRadioStationSuccess
    case addPresetFail
    case addPlaylistFail
    case addRadioStationFail
    case play
    case empty
    case deletePreset
    case deletePlaylist
    case deleteRadioStation
    
    var message: String {
        switch self {
        case .addPreset: return Localizations.Player.Preset.Notification.AddPlaylist
        case .addPlaylistSuccess: return Localizations.Player.Preset.Notification.AddPlaylist
        case .addRadioStationSuccess: return Localizations.Player.Preset.Notification.AddRadioStation
        case .addPresetFail: return Localizations.Player.Preset.Notification.Addfailed
        case .addPlaylistFail: return Localizations.Player.Preset.Notification.Addfailedplaylist
        case .addRadioStationFail: return Localizations.Player.Preset.Notification.Addfailedinternetradio
        case .deletePreset: return Localizations.Player.Preset.Notification.Delete
        case .deletePlaylist: return Localizations.Player.Preset.Notification.DeletePlaylist
        case .deleteRadioStation: return Localizations.Player.Preset.Notification.DeleteInternetRadio
        case .play: return Localizations.Player.Preset.Notification.Play
        case .empty: return Localizations.Player.Preset.Notification.Empty
        }
    }
    
    var iconName: String {
        switch self {
        case .addPreset: return "preset_added_icon"
        case .addPlaylistSuccess: return "preset_added_icon"
        case .addRadioStationSuccess: return "preset_added_icon"
        case .addPresetFail: return "preset_failed_icon"
        case .addPlaylistFail: return "preset_failed_icon"
        case .addRadioStationFail: return "preset_failed_icon"
        case .deletePreset: return "preset_failed_icon"
        case .deletePlaylist: return "preset_failed_icon"
        case .deleteRadioStation: return "preset_failed_icon"
        case .play: return "preset_playing_icon"
        case .empty: return "preset_failed_icon"
        }
    }
}

class PresetSelectableViewController: SelectableViewController {
    @IBOutlet weak var playPauseButton: UIButton!
    @IBOutlet weak var presetTypeLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var infoButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var artworkImageView: UIImageView!
    @IBOutlet var controlsViews: [UIView]!
    @IBOutlet weak var spotifyLogo: UIImageView!
    @IBOutlet weak var notificationBackgroun: UIView!
    @IBOutlet var notificationContainer: UIView!
    @IBOutlet weak var notificationContentContainer: UIView!
    @IBOutlet weak var notificationIcon: UIImageView!
    @IBOutlet weak var notificationLabel: UILabel!
    @IBOutlet weak var loginSpotifyButton: UIButton!
    @IBOutlet weak var loginSpotifyContainer: UIView!
    @IBOutlet var notificationContainerConstraints: [NSLayoutConstraint]!
    @IBOutlet weak var presetTypeToTitleLabelConstraint: NSLayoutConstraint!
    
    weak var delegate: PresetSelectableViewControllerDelegate?
    
    let downloader = ImageDownloader()
    let minImageAlpha:CGFloat = 0.25
    let maxImageAlpha:CGFloat = 1.0
    let minControlsAlpha:CGFloat = 0.0
    let maxConstrolsAlpha:CGFloat = 1.0
    
    var spotifyLogoVisible = false
    var presetTypeVisible = false
    var currentDownloadReceipt: RequestReceipt?
    var imageURLObserverDisposable = DisposeBag()
    var timerDisposable: Disposable? = nil
    
    private var isInFocus = false
    
    var imageAlphaDelta: CGFloat {
        return maxImageAlpha - minImageAlpha
    }
    
    var controlsAlphaDelta: CGFloat {
        return maxConstrolsAlpha - minControlsAlpha
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loginSpotifyButton.titleLabel?.numberOfLines = 0
        loginSpotifyButton.setTitle(Localizations.Player.Preset.LoginSpotifyAdvice, for: .normal)
        loginSpotifyButton.titleLabel?.font = Fonts.UrbanEars.Regular(12)
        artworkImageView.image = nil
        
        titleLabel.font = Fonts.UrbanEars.Bold(18)
        notificationLabel.font = Fonts.UrbanEars.Regular(20.5)
        
        self.view.backgroundColor = UIColor.clear
        
        viewModelVariable.asObservable()
            .map{ ($0 as! PresetViewModel).preset.type }
            .take(1)
            .subscribe(onNext: { [weak self] presetType in self?.updatePresetTypeWith(presetType, animated: false) })
            .disposed(by: rx_disposeBag)
        
        viewModelVariable.asObservable()
            .map{ ($0 as! PresetViewModel).preset.type }
            .skip(1)
            .subscribe(weak: self, onNext: PresetSelectableViewController.updatePresetTypeWith)
            .disposed(by: rx_disposeBag)
        
        typealias PresetAndCounter = (preset: Preset?, count: Int?)
        let initialPresetAndCounter: PresetAndCounter = (preset: nil, count: nil)
        viewModelVariable.asObservable()
            .map { ($0 as! PresetViewModel).preset }
            .distinctUntilChanged()
            .scan(initialPresetAndCounter, accumulator: { (previous, newPreset) -> PresetAndCounter in
                var next = previous
                next.preset = newPreset
                next.count = (previous.count != nil) ? previous.count! + 1 : 0
                return next })
            .subscribe(onNext: { [weak self] presetAndCounter in
                self?.updateCardForPreset(presetAndCounter.preset!, animated: presetAndCounter.count! != 0 ) })
            .disposed(by: rx_disposeBag)
        
        viewModelVariable.asObservable()
            .map { viewModel in
                let vm = viewModel as! PresetViewModel
                return vm.preset.isEmpty ? Localizations.Player.Preset.EmptyPlaceholder : vm.preset.name }
            .subscribe(weak: self, onNext: PresetSelectableViewController.updateTitleText)
            .disposed(by: rx_disposeBag)
        
        let canDeletePreset = viewModelVariable.asObservable()
            .map { viewModel -> Bool in
                let vm = viewModel as! PresetViewModel
                return !vm.preset.isEmpty
        }
        
        let canAddPreset = viewModelVariable.asObservable()
            .map { viewModel -> Observable<Bool> in
                let vm = viewModel as! PresetViewModel
                return vm.canAddPreset.asObservable() }
            .switchLatest()
        
        let canUndoSave = viewModelVariable.asObservable()
            .map { viewModel -> Observable<Bool> in
                let vm = viewModel as! PresetViewModel
                return vm.canUndoSave.asObservable() }
            .switchLatest()
        
        let canUndoDelete = viewModelVariable.asObservable()
            .map { viewModel -> Observable<Bool> in
                let vm = viewModel as! PresetViewModel
                return vm.canUndoDelete.asObservable() }
            .switchLatest()
        
        Observable.combineLatest(canAddPreset, canUndoSave) { canAddPreset, canUndo in return canAddPreset && !canUndo }
            .not()
            .bind(to: saveButton.rx.isHidden)
            .disposed(by: rx_disposeBag)
        
        Observable.combineLatest(canAddPreset, canUndoSave) { canAddPreset, canUndo in return !canAddPreset && !canUndo }
            .not()
            .bind(to: infoButton.rx.isHidden)
            .disposed(by: rx_disposeBag)
        
        canUndoDelete
            .bind(to: deleteButton.rx.isHidden)
            .disposed(by: rx_disposeBag)
        
        canDeletePreset
            .bind(to: deleteButton.rx.isEnabled)
            .disposed(by: rx_disposeBag)
        
        canDeletePreset
            .bind(to: playPauseButton.rx.isEnabled)
            .disposed(by: rx_disposeBag)
        
        viewModelVariable.asObservable().map{ $0 as! PresetViewModel }
            .map { presetViewModel in
                presetViewModel.savedNotification
                    .map { return (didSave: $0, mode:presetViewModel.masterState.value?.currentMode) }}
            .switchLatest()
            .subscribe(onNext: { [weak self] didSaveNotification in
                if !didSaveNotification.didSave {
                    var presetType = PresetType.unknown
                    if didSaveNotification.mode?.isIR == true {
                        presetType = .internetRadio
                    }
                    if didSaveNotification.mode?.isSpotify == true {
                        presetType = .spotify
                    }
                    self?.showNotificationForSavedSuccessFully(didSaveNotification.didSave, presetType: presetType)
                } })
            .disposed(by: rx_disposeBag)
        
        let isSelected = viewModelVariable.asObservable()
            .map { ($0 as! PresetViewModel).isSelected.asObservable() }
            .switchLatest()
        
        let isPlaying = viewModelVariable.asObservable()
            .map { ($0 as! PresetViewModel).isPlayingOrBuffering }
            .switchLatest()
        
        let canStop = viewModelVariable.asObservable()
            .map { ($0 as! PresetViewModel).canStop }
            .switchLatest()
        
        Observable.combineLatest(isSelected, isPlaying, canStop) { isSelected, isPlaying, canStop -> UIImage? in
            if isSelected && isPlaying {
                if canStop {
                    return UIImage(named: "playlist_stop_button")
                } else {
                    return UIImage(named: "playlist_pause_button")
                }
            } else  {
                return UIImage(named: "playlist_play_button") }}
            .subscribe(onNext:{ [weak self] image in self?.updatePlayPauseButtonImage(image) })
            .disposed(by: rx_disposeBag)
        
        let needsSpotifyLogin: Observable<Bool> = viewModelVariable.asObservable()
            .map { ($0 as! PresetViewModel).spotifyLoginNeeded.asObservable() }
            .switchLatest()
        
        needsSpotifyLogin
            .map { !$0 }.bind(to:loginSpotifyContainer.rx.isHidden)
            .disposed(by: rx_disposeBag)
        
        hideNotification(false)
        uninstallNotificationContainer()
    }
    
    func installNotificationContainer() {
        notificationContainer.alpha = 1.0
        notificationContainer.transform = CGAffineTransform.identity
        artworkImageView.insertSubview(notificationContainer, aboveSubview: artworkImageView)
        artworkImageView.superview!.addConstraints(notificationContainerConstraints)
    }
    
    func uninstallNotificationContainer() {
        artworkImageView.superview!.removeConstraints(notificationContainerConstraints)
        notificationContainer.removeFromSuperview()
    }
    
    func updateTitleText(title: String) {
        UIView.transition(with: titleLabel, duration: 0.25, options: [.beginFromCurrentState, .transitionCrossDissolve], animations: { [weak self] in
            self?.titleLabel.text = title
            }, completion: nil)
    }
    
    func cachedImageForImageWithURL(_ imageURL: URL) -> UIImage? {
        let cache = UIImageView.af_sharedImageDownloader.imageCache
        let req = URLRequest(url: imageURL)
        guard let image = cache?.image(for: req, withIdentifier: nil) else { return nil }
        return image
    }
    
    func updateCardForPreset(_ preset: Preset, animated: Bool) {
        unobserveImageURLChanges()
        
        var undoAnimation = false
        if let vm = (self.viewModel as? PresetViewModel) {
            undoAnimation = vm.presetIsUndone
        }
        
        let emptyPlaceholder = UIImage(named: "presets_empty_placeholder")
        let unavailablePlaceholder = UIImage(named: "presets_unavailable_placeholder")
        let loadingPlaceholder = UIImage(named: "presets_unavailable_placeholder")
        var imageToShow: UIImage? = loadingPlaceholder
        var loadAfterTransition = false
        
        if preset.isEmpty {
            imageToShow = emptyPlaceholder
        } else {
            if let imageURL = (self.viewModel as? PresetViewModel)?.playlistArtworkURL.value,
                imageURL.absoluteString != "" {
                
                if let cachedImage = cachedImageForImageWithURL(imageURL) {
                    imageToShow = cachedImage
                } else {
                    imageToShow = loadingPlaceholder
                    self.downloadImageWithURL(imageURL)
                    loadAfterTransition = true
                }
            } else {
                imageToShow = unavailablePlaceholder
                self.observeImageURLChanges(justDownloadIfChanged: true)
                loadAfterTransition = true
            }
        }
        
        hideNotification(false)
        
        DispatchQueue.main.async { [weak self] in
            let flipAnimation: UIView.AnimationOptions = undoAnimation ? .transitionFlipFromLeft : .transitionFlipFromRight
            if animated {
                UIView.transition(
                    with: (self?.artworkImageView)!,
                    duration: 0.7,
                    options: [flipAnimation],
                    animations: { [weak self] in
                        self?.artworkImageView.image = imageToShow
                    },
                    completion: { [weak self] done in
                        if done {
                            if loadAfterTransition {
                                if let imageURL = (self?.viewModel as? PresetViewModel)?.playlistArtworkURL.value {
                                    self?.updateArtworkImageWithURL(imageURL)
                                } else {
                                    self?.observeImageURLChanges(justDownloadIfChanged: false)
                                }
                            }
                            
                            if !preset.isEmpty {
                                self?.showNotificationForSavedSuccessFully(true, presetType: preset.type ?? PresetType.unknown)
                            } else {
                                self?.showNotificationForDeleted(presetType: preset.type ?? PresetType.unknown)
                            }
                        }
                })
            } else {
                self?.artworkImageView.image = imageToShow
                
                if loadAfterTransition {
                    if let imageURL = (self?.viewModel as? PresetViewModel)?.playlistArtworkURL.value {
                        self?.updateArtworkImageWithURL(imageURL)
                    } else {
                        self?.observeImageURLChanges(justDownloadIfChanged: false)
                    }
                }
            }
        }
    }
    
    func unobserveImageURLChanges() {
        imageURLObserverDisposable = DisposeBag()
    }
    
    func observeImageURLChanges(justDownloadIfChanged: Bool) {
        imageURLObserverDisposable = DisposeBag()
        let artworkURL = viewModelVariable.asObservable()
            .map { ($0 as! PresetViewModel).playlistArtworkURL.asObservable() }
            .switchLatest()
        
        artworkURL.subscribe(weak: self,
                             onNext:justDownloadIfChanged ?
                                PresetSelectableViewController.downloadImageWithURL:
                                PresetSelectableViewController.updateArtworkImageWithURL)
            .disposed(by: imageURLObserverDisposable)
    }
    
    func downloadImageWithURL(_ imageURL: URL?) {
        guard let imageURL = imageURL else { return }
        
        let urlRequest = URLRequest(url: imageURL)
        
        if let receipt = currentDownloadReceipt {
            downloader.cancelRequest(with: receipt)
        }
        
        currentDownloadReceipt = downloader.download(urlRequest) { _ in }
    }
    
    func updateArtworkImageWithURL(_ imageURL: URL?) {
        guard let imageURL = imageURL else { return }
        
        let loadingPlaceholder = artworkImageView.image ?? UIImage(named: "presets_unavailable_placeholder")
        artworkImageView.af_setImage(withURL: imageURL,
                                     placeholderImage: loadingPlaceholder,
                                     filter: nil,
                                     progress: nil,
                                     progressQueue: DispatchQueue.main,
                                     imageTransition: UIImageView.ImageTransition.crossDissolve(0.25),
                                     runImageTransitionIfCached: true,
                                     completion: nil)
    }
    
    func showNotificationForDeleted(presetType: PresetType) {
        switch presetType {
        case .internetRadio:
            showPresetNotification(.deleteRadioStation)
        case .spotify:
            showPresetNotification(.deletePlaylist)
        default:
            showPresetNotification(.deletePreset)
        }
    }
    
    func showNotificationForSavedSuccessFully(_ success: Bool, presetType: PresetType) {
        if fabs(relativeDistanceFromCenter) < 0.1 {
            if success {
                switch presetType {
                case .internetRadio: showPresetNotification(.addRadioStationSuccess)
                case .spotify: showPresetNotification(.addPlaylistSuccess)
                case .unknown: showPresetNotification(.addPreset)
                }
            } else {
                switch presetType {
                case .internetRadio: showPresetNotification(.addRadioStationFail)
                case .spotify: showPresetNotification(.addPlaylistFail)
                case .unknown: showPresetNotification(.addPresetFail)
                }
            }
        }
    }
    
    func hideNotification(_ animated: Bool) {
        if animated {
            UIView.animate(withDuration: 0.25, delay: 0.0, options: [.beginFromCurrentState], animations: { [weak self] in
                guard let `self` = self else { return }
                self.notificationBackgroun.alpha = 0.0
                self.notificationContentContainer.alpha = 0.0
                self.notificationContentContainer.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
                self.loginSpotifyContainer.alpha = 1.0
                }, completion: { [weak self] _ in
                    self?.notificationContentContainer.transform = CGAffineTransform.identity
                    self?.notificationContainer.isHidden = true
                    self?.uninstallNotificationContainer()
            })
        } else {
            notificationBackgroun.alpha = 0.0
            notificationContentContainer.alpha = 0.0
            self.notificationContentContainer.transform = CGAffineTransform.identity
            self.notificationContainer.isHidden = true
            self.uninstallNotificationContainer()
        }
        
        timerDisposable?.dispose()
    }
    
    func hideNotificationInTimeInterval(_ timeInterval: TimeInterval) {
        timerDisposable?.dispose()
        timerDisposable = Observable<Int>.interval(timeInterval, scheduler: MainScheduler.instance).take(1).subscribe(onNext:{ [weak self] _ in
            guard let `self` = self else { return }
            self.hideNotification(true)
        })
    }
    
    func showPresetNotification(_ notification: PresetNotification, animated: Bool = true) {
        installNotificationContainer()
        
        notificationLabel.text = notification.message
        notificationIcon.image = UIImage(named: notification.iconName)
        
        if animated {
            self.notificationContainer.isHidden = false
            notificationContentContainer.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
            notificationContentContainer.alpha = 0.0
            UIView.animate(withDuration: 0.25, delay: 0.0, options: [.beginFromCurrentState], animations: { [weak self] in
                guard let `self` = self else { return }
                self.notificationBackgroun.alpha = 1.0
                self.notificationContentContainer.alpha = 1.0
                self.notificationContentContainer.transform = CGAffineTransform.identity
                self.loginSpotifyContainer.alpha = 0.0
                }, completion: { [weak self] _ in
                    guard let `self` = self else { return }
                    self.hideNotificationInTimeInterval(2.0)
            })
        } else {
            self.notificationContainer.isHidden = false
            self.notificationBackgroun.isHidden = false
            self.notificationBackgroun.alpha = 1.0
            self.notificationContentContainer.alpha = 1.0
            self.loginSpotifyContainer.alpha = 0.0
            self.hideNotificationInTimeInterval(2.0)
        }
    }
    
    func updatePresetTypeWith(_ presetType: PresetType?) {
        updatePresetTypeWith(presetType, animated: true)
    }
    
    func updatePresetTypeWith(_ presetType: PresetType?, animated: Bool) {
        if let vm = (self.viewModel as? PresetViewModel) {
            if vm.preset.number == 2 {
                print()
            }
        }
        
        //this looks more complicated than it should - it just sets the alphas in different states
        if let type = presetType {
            self.presetTypeToTitleLabelConstraint.constant = 2
            switch type {
            case PresetType.internetRadio, PresetType.unknown:
                UIView.setAlphaOfView(spotifyLogo, visible: false, animated: animated)
                spotifyLogoVisible = false
                if presetTypeLabel.text == type.displayName {
                    if isInFocus {
                        UIView.setAlphaOfView(presetTypeLabel, visible: true, animated: animated)
                    }
                    
                    presetTypeVisible = true
                    presetTypeLabel.isHidden = false
                    self.presetTypeLabel.text = type.displayName
                } else {
                    presetTypeLabel.text = type.displayName
                    
                    if isInFocus {
                        UIView.setAlphaOfView(presetTypeLabel, visible: true, animated: animated)
                    }
                    
                    presetTypeVisible = true
                    presetTypeLabel.isHidden = false
                }
            case PresetType.spotify :
                if isInFocus {
                    UIView.setAlphaOfView(spotifyLogo, visible: true, animated: animated)
                    UIView.setAlphaOfView(presetTypeLabel, visible: false, animated: animated)
                }
                presetTypeVisible = false
                spotifyLogoVisible = true
            }
        } else {
            self.presetTypeToTitleLabelConstraint.constant = -18
            
            if isInFocus {
                UIView.setAlphaOfView(spotifyLogo, visible: false, animated: animated)
                UIView.setAlphaOfView(presetTypeLabel, visible: false, animated: animated)
            }
            
            presetTypeVisible = false
            spotifyLogoVisible = false
        }
        
        
        self.titleLabel.layoutIfNeeded()
        UIView.animate(withDuration: 0.25, animations: { [weak self] in
            guard let `self` = self else { return }
            self.titleLabel.superview?.layoutIfNeeded()
        })
        
    }
    
    func updatePlayPauseButtonImage(_ image:UIImage?) {
        playPauseButton.setImage(image, for: .normal)
    }
    
    @IBAction func onPlayPause(_ sender: AnyObject) {
        guard let vm = viewModel as? PresetViewModel else { return }
        
        if vm.preset.isEmpty {
            showPresetNotification(.empty)
        } else {
            vm.select(vm.preset)
            if let currentPresetIndex = vm.clientState.value?.presetIndex {
                if currentPresetIndex != (vm.preset.number - 1) {
                    showPresetNotification(.play)
                }
            }
        }
    }
    
    @IBAction func onPresetDisabledInfo(_ sender: AnyObject) {
        self.delegate?.presetSelectableViewControllerDidRequestPresetDisabledInfo(self)
    }
    
    @IBAction func onAdd(_ sender: AnyObject) {
        guard let vm = viewModel as? PresetViewModel else { return }
        
        vm.savePreset()
    }
    
    @IBAction func onUndo(_ sender: AnyObject) {
        guard let vm = viewModel as? PresetViewModel else { return }
        
        vm.undoPreset()
    }
    
    @IBAction func onPlaylistName(_ sender: Any) {
        self.delegate?.presetSelectableViewControllerDidRequestShowPlaylistDetails(self)
    }
    
    @IBAction func onDelete(_ sender: AnyObject) {
        guard let vm = viewModel as? PresetViewModel else { return }
        
        vm.deletePreset()
    }
    
    @IBAction func onLoginSpotify(_ sender: AnyObject) {
        loginSpotify(forceWebLogin: false)
    }
    
    func loginSpotify(forceWebLogin: Bool) {
        guard let vm = viewModel as? PresetViewModel else { return }
        
        vm.spotifyProvider.tokenManager.login(forceWebLogin: forceWebLogin)
            .subscribe(onError: { [weak self] error in
                self?.spotifyLoginFailed(with: error) })
            .disposed(by: rx_disposeBag)
    }
    
    func spotifyLoginFailed(with error: Swift.Error) {
        guard let loginError = error as? SpotifyLoginError else { return }
        
        switch loginError {
        case .authorizationFailed(let spotifyError):
            if spotifyError == "unknown_error" {
                showSpotifyLoginError()
            }
            break
        default: break
        }
    }
    
    func showSpotifyLoginError() {
        let alertController = UIAlertController(title: Localizations.Player.Preset.SpotifyLoginError.Title,
                                                message: Localizations.Player.Preset.SpotifyLoginError.Content, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: Localizations.Appwide.Cancel, style: .cancel, handler: nil))
        
        alertController.addAction(UIAlertAction(title: Localizations.Player.Preset.SpotifyLoginError.RetryButton, style: .default, handler: { [weak self] action in
            self?.loginSpotify(forceWebLogin: true)
        }))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    override var relativeDistanceFromCenter : Double {
        didSet {
            let superViewWidth = self.view.superview!.frame.size.width
            let percentInFocus = CGFloat(fmax(-1.0, fmin(CGFloat(relativeDistanceFromCenter), 1.0)))
            
            isInFocus = abs(percentInFocus) < 0.2
            
            let distanceFromCenter = superViewWidth * percentInFocus
            
            controlsViews.forEach{ control in
                let transform = CGAffineTransform(translationX: -distanceFromCenter, y: 0)
                control.transform = transform
                deleteButton.transform = transform
                saveButton.transform = transform
                infoButton.transform = transform
                spotifyLogo.transform = transform
                presetTypeLabel.transform = transform
            }
            
            let minScale: CGFloat = 0.9
            let maxScale: CGFloat = 1.0
            let scale: CGFloat = minScale + (maxScale-minScale)*CGFloat(1.0-abs(percentInFocus))
            
            artworkImageView.transform = CGAffineTransform(scaleX: scale, y: scale)
            
            //coverflow  👌
            /*let minAngle: CGFloat = 0.0
             let maxAngle: CGFloat = 45.0
             
             var rotationAndPerspectiveTransform = CATransform3DIdentity
             rotationAndPerspectiveTransform.m34 = 1.0 / -500
             let angleInDegrees: CGFloat = minAngle + (minAngle-maxAngle)*percentInFocus
             let angleInRadians: CGFloat = (angleInDegrees*CGFloat.pi/180.0)
             rotationAndPerspectiveTransform = CATransform3DRotate(rotationAndPerspectiveTransform, angleInRadians, 0.0, 1.0, 0.0)
             artworkImageView.layer.transform = rotationAndPerspectiveTransform*/
            
            
            var artworkAlpha: CGFloat = 0.0
            var controlsAlpha: CGFloat = 0.0
            if (percentInFocus < 0 && nextFillsScreen) || (percentInFocus > 0 && prevFillsScreen) {
                
                artworkAlpha = maxImageAlpha * (1.0-abs(percentInFocus))
                controlsAlpha = maxConstrolsAlpha * (1.0-abs(percentInFocus))
            } else {
                artworkAlpha = minImageAlpha + imageAlphaDelta*(1.0-abs(percentInFocus))
                controlsAlpha = minControlsAlpha +  controlsAlphaDelta * (1.0-abs(percentInFocus))
            }
            
            artworkImageView.alpha = CGFloat(artworkAlpha)
            
            if !notificationContainer.isHidden {
                notificationContainer.alpha = artworkImageView.alpha
                notificationContainer.transform = artworkImageView.transform
            }
            
            if !loginSpotifyContainer.isHidden {
                loginSpotifyContainer.alpha = artworkImageView.alpha
                loginSpotifyContainer.transform = artworkImageView.transform
            }
            
            controlsViews.forEach{ view in
                view.alpha = CGFloat(controlsAlpha)
            }
            
            deleteButton.alpha = CGFloat(controlsAlpha)
            saveButton.alpha = CGFloat(controlsAlpha)
            infoButton.alpha = CGFloat(controlsAlpha)
            spotifyLogo.alpha = spotifyLogoVisible ? CGFloat(controlsAlpha) : 0
            presetTypeLabel.alpha = presetTypeVisible ? CGFloat(controlsAlpha) : 0
            
        }
    }
}
