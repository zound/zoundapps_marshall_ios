//
//  CloudSelectableViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 06/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

struct CloudSource {
    let name: String
    let imageName: String
}

protocol CloudSelectableViewControllerDelegate: class {
    func cloudViewControllerDidRequestSpotifyConnect(_ viewController: CloudSelectableViewController)
    func cloudViewControllerDidRequestGoogleCast(_ viewController: CloudSelectableViewController)
    func cloudViewControllerDidRequestAirplay(_ viewController: CloudSelectableViewController)
    func cloudViewControllerDidRequestBrowseRadio(_ viewController: CloudSelectableViewController, view: UIView)
}

class CloudSelectableViewController: SelectableViewController {
    @IBOutlet var controlsViews: [UIView]!
    @IBOutlet weak var spotifyConnectButton: UIButton!
    @IBOutlet weak var playInternetRadioButton: UIButton!
    @IBOutlet weak var googleCastButton: UIButton!
    @IBOutlet weak var airplayButton: UIButton!
    @IBOutlet weak var orPlayButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    
    weak var delegate: CloudSelectableViewControllerDelegate?
    
    let minImageAlpha:CGFloat = 0.1
    let maxImageAlpha:CGFloat = 1.0
    let minControlsAlpha:CGFloat = 0.0
    let maxConstrolsAlpha:CGFloat = 1.0
    let minScale: CGFloat = 0.9
    let maxScale: CGFloat = 1.0
    
    var imageAlphaDelta: CGFloat {
        return maxImageAlpha - minImageAlpha
    }
    
    var controlsAlphaDelta: CGFloat {
        return maxConstrolsAlpha - minControlsAlpha
    }
    
    var scaleDelta: CGFloat {
        return maxScale - minScale
    }
    
    fileprivate var _isInFocus: Bool = false
    fileprivate var isInFocus: Bool {
        get { return _isInFocus }
        set {
            guard newValue != isInFocus else { return }
            _isInFocus = newValue
            
            if newValue == false {
                scrollView.setContentOffset(CGPoint.zero, animated: true)
            }
        }
    }
    
    override var relativeDistanceFromCenter : Double {
        didSet {
            let percentInFocus = CGFloat(fmax(-1.0, fmin(CGFloat(relativeDistanceFromCenter), 1.0)))
            
            self.isInFocus = abs(percentInFocus) < 0.1
            var controlsAlpha: CGFloat = 0.0
            
            if (percentInFocus < 0 && nextFillsScreen) || (percentInFocus > 0 && prevFillsScreen) {
                controlsAlpha = maxConstrolsAlpha * (1.0 - abs(percentInFocus))
            } else {
                controlsAlpha = minControlsAlpha +  controlsAlphaDelta * (1.0 - abs(percentInFocus))
            }
            
            controlsViews.forEach{ control in
                control.alpha = CGFloat(controlsAlpha)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UIView.setAnimationsEnabled(false)
        playInternetRadioButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.Player.Cloud.PlayInternetRadio), for: .normal)
        
        orPlayButton.setTitle(Localizations.Player.Cloud.OrPlayMobileApp, for: .normal)
        orPlayButton.titleLabel?.font = Fonts.UrbanEars.Regular(17)
        orPlayButton.titleLabel?.numberOfLines = 2
        orPlayButton.titleLabel?.textAlignment = .center
        
        spotifyConnectButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.Player.Cloud.SpotifyConnect), for: .normal)
        googleCastButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.Player.Cloud.GoogleCast), for: .normal)
        airplayButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.Player.Cloud.Airplay), for: .normal)
        
        playInternetRadioButton.layoutIfNeeded()
        orPlayButton.layoutIfNeeded()
        spotifyConnectButton.layoutIfNeeded()
        googleCastButton.layoutIfNeeded()
        airplayButton.layoutIfNeeded()
        UIView.setAnimationsEnabled(true)
        
        self.view.backgroundColor = UIColor.clear
        
        self.view.rx.observe(CGRect.self, "frame").subscribe(onNext: { [weak self] frame  in
            self?.scrollView.setContentOffset(CGPoint.zero, animated: false)
        }).disposed(by: rx_disposeBag)
    }
    
    @IBAction func onPlayRadio(_ sender: AnyObject) {
        self.delegate?.cloudViewControllerDidRequestBrowseRadio(self, view: sender as! UIButton)
    }
    
    @IBAction func onSpotifyConnect(_ sender: AnyObject) {
        self.delegate?.cloudViewControllerDidRequestSpotifyConnect(self)
    }
    
    @IBAction func onGoogleCast(_ sender: AnyObject) {
        self.delegate?.cloudViewControllerDidRequestGoogleCast(self)
    }
    
    @IBAction func onAirplay(_ sender: AnyObject) {
        self.delegate?.cloudViewControllerDidRequestAirplay(self)
    }
}

