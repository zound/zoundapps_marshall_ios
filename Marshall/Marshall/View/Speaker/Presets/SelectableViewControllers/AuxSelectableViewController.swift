//
//  AuxSelectableViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 06/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit

class AuxSelectableViewController: SelectableViewController {
    @IBOutlet weak var controlsContainer: UIView!
    @IBOutlet weak var auxinActiveContainer: UIView!
    @IBOutlet weak var activateButton: UIButton!
    @IBOutlet weak var auxActiveLabel: UILabel!
    
    let minImageAlpha:CGFloat = 0.1
    let maxImageAlpha:CGFloat = 1.0
    let minScale: CGFloat = 0.9
    let maxScale: CGFloat = 1.0
    let minControlsAlpha:CGFloat = 0.0
    let maxConstrolsAlpha:CGFloat = 1.0
    
    var imageAlphaDelta: CGFloat {
        return maxImageAlpha - minImageAlpha
    }

    var controlsAlphaDelta: CGFloat {
        return maxConstrolsAlpha - minControlsAlpha
    }
    
    var scaleDelta: CGFloat {
        return maxScale-minScale
    }
    
    override var relativeDistanceFromCenter : Double {
        didSet {
            let percentInFocus = CGFloat(fmax(-1.0,fmin(CGFloat(relativeDistanceFromCenter),1.0)))
            var controlsAlpha: CGFloat = 0.0
            
            if (percentInFocus < 0 && nextFillsScreen) || (percentInFocus > 0 && prevFillsScreen) {
                controlsAlpha = maxConstrolsAlpha * (1.0-abs(percentInFocus))
            } else {
                controlsAlpha = minControlsAlpha +  controlsAlphaDelta * (1.0-abs(percentInFocus))
            }
 
            controlsContainer.alpha = CGFloat(controlsAlpha)
        }
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activateButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.Player.Aux.Activate), for: .normal)
        auxActiveLabel.attributedText = Fonts.ButtonFont.AttributedSecondaryButtonWithString(Localizations.Player.Aux.Activated)
        
        self.view.backgroundColor = UIColor.clear
        
        let isSelected = viewModelVariable.asObservable()
            .map { ($0 as! AuxViewModel).isSelected.asObservable() }
            .switchLatest()
        
        isSelected
            .distinctUntilChanged()
            .skip(1)
            .subscribe(weak: self, onNext: AuxSelectableViewController.updateForActive)
            .disposed(by: rx_disposeBag)
        
        isSelected
            .take(1)
            .subscribe(onNext: { [weak self] active in
                self?.updateForActive(active, animated: false) })
            .disposed(by: rx_disposeBag)
    }
    
    func updateForActive(_ active: Bool) {
        updateForActive(active, animated: true)
    }
    
    func updateForActive(_ active: Bool, animated: Bool) {
        if active {
            UIView.setAlphaOfView(auxinActiveContainer, visible: true, animated: animated)
            UIView.setAlphaOfView(activateButton, visible: false, animated: animated)
        } else {
            
            UIView.setAlphaOfView(auxinActiveContainer, visible: false, animated: animated)
            UIView.setAlphaOfView(activateButton, visible: true, animated: animated)
        }
    }
    
    @IBAction func onActivate(_ sender: AnyObject) {
        guard let vm = viewModel as? AuxViewModel else { return }
        
        vm.select(vm.mode)
    }
}
