//
//  BluetoothSelectableViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 06/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import MinuetSDK

class BluetoothSelectableViewController: SelectableViewController {
    @IBOutlet weak var controlsContainer: UIView!
    @IBOutlet weak var bluetoothActiveContainer: UIView!
    @IBOutlet weak var activateButton: UIButton!
    @IBOutlet weak var pairingModeContainer: UIView!
    @IBOutlet weak var enterPairingButton: UIButton!
    @IBOutlet weak var pairingActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var pairingModeLabel: UILabel!
    @IBOutlet weak var bluetoothActiveLabe: UILabel!
    @IBOutlet weak var connectedToContainer: UIView!
    @IBOutlet weak var connectedToTitleLabel: UILabel!
    @IBOutlet weak var connectedToDeviceLabel: UILabel!
    @IBOutlet weak var bluetoothOnAdvice: UILabel!
    
    let minImageAlpha:CGFloat = 0.1
    let maxImageAlpha:CGFloat = 1.0
    let minControlsAlpha:CGFloat = 0.0
    let maxConstrolsAlpha:CGFloat = 1.0
    let minScale: CGFloat = 0.9
    let maxScale: CGFloat = 1.0
    
    var imageAlphaDelta: CGFloat {
        return maxImageAlpha - minImageAlpha
    }
    
    var controlsAlphaDelta: CGFloat {
        return maxConstrolsAlpha - minControlsAlpha
    }
    
    var scaleDelta: CGFloat {
        return maxScale - minScale
    }
    
    override var relativeDistanceFromCenter : Double {
        didSet {
            let percentInFocus = CGFloat(fmax(-1.0,fmin(CGFloat(relativeDistanceFromCenter),1.0)))
            var controlsAlpha: CGFloat = 0.0
            
            if (percentInFocus < 0 && nextFillsScreen) || (percentInFocus > 0 && prevFillsScreen) {
                controlsAlpha = maxConstrolsAlpha * (1.0-abs(percentInFocus))
            } else {
                controlsAlpha = minControlsAlpha +  controlsAlphaDelta * (1.0-abs(percentInFocus))
            }
            
            controlsContainer.alpha = CGFloat(controlsAlpha)
            connectedToContainer.alpha = CGFloat(controlsAlpha)
            bluetoothOnAdvice.alpha = CGFloat(controlsAlpha)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activateButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.Player.Bluetooth.Activate), for: .normal)
        enterPairingButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.Player.Bluetooth.EnterPairing), for: .normal)
        
        bluetoothActiveLabe.attributedText = Fonts.ButtonFont.AttributedSecondaryButtonWithString(Localizations.Player.Bluetooth.Activated)
        
        pairingModeLabel.attributedText = Fonts.ButtonFont.AttributedSecondaryButtonWithString(Localizations.Player.Bluetooth.WaitingToPair)
        
        connectedToTitleLabel.text = Localizations.Player.Bluetooth.ConnectedTo
        connectedToTitleLabel.font = Fonts.UrbanEars.Regular(13)
        
        connectedToDeviceLabel.font = Fonts.UrbanEars.Regular(13)
        
        let model = UIDevice.current.model
        bluetoothOnAdvice.text = Localizations.Player.Bluetooth.ConnectDeviceAdvice(model)
        bluetoothOnAdvice.font = Fonts.UrbanEars.Regular(13)
        
        self.view.backgroundColor = UIColor.clear
        
        let isSelected = viewModelVariable.asObservable()
            .map { ($0 as! BluetoothViewModel).isSelected.asObservable() }
            .switchLatest()
        
        let isPairing = viewModelVariable.asObservable()
            .map { ($0 as! BluetoothViewModel).isPairing.asObservable() }
            .switchLatest()
        
        let bluetoothDevices = viewModelVariable.asObservable()
            .map { ($0 as! BluetoothViewModel).bluetoothDevices.asObservable() }
            .switchLatest()
        
        isSelected.distinctUntilChanged()
            .skip(1)
            .subscribe(weak: self, onNext: BluetoothSelectableViewController.updateForActive)
            .disposed(by: rx_disposeBag)
        
        isSelected
            .take(1)
            .subscribe(onNext: { [weak self] bluetoothActive in
                self?.updateForActive(bluetoothActive, animated: false) })
            .disposed(by: rx_disposeBag)
        
        isPairing
            .distinctUntilChanged()
            .skip(1)
            .subscribe(weak: self, onNext: BluetoothSelectableViewController.updateForIsPairing)
            .disposed(by: rx_disposeBag)
        
        isPairing
            .take(1)
            .subscribe(onNext: { [weak self] isPairing in
                self?.updateForIsPairing(isPairing, animated: false) })
            .disposed(by: rx_disposeBag)
        
        let bluetoothDeviceComparer = {(rhs: [BluetoothDevice], lhs: [BluetoothDevice]) in rhs == lhs }
        bluetoothDevices.distinctUntilChanged(bluetoothDeviceComparer)
            .subscribe(weak: self, onNext: BluetoothSelectableViewController.updateForBluetoothDevices)
            .disposed(by: rx_disposeBag)
        
        let anyBluetoothDevicesConnected = bluetoothDevices.map { $0.count > 0 }.distinctUntilChanged()
        let isActive = isSelected
        let adviceVisible = Observable.combineLatest(anyBluetoothDevicesConnected, isActive) { !$0 && $1 }
        adviceVisible
            .not()
            .bind(to: bluetoothOnAdvice.rx.isHidden)
            .disposed(by: rx_disposeBag)
    }
    
    @IBAction func onActivate(_ sender: AnyObject) {
        guard let vm = viewModel as? BluetoothViewModel else { return }
        
        vm.select(vm.mode)
    }
    
    @IBAction func onEnterPairingMode(_ sender: AnyObject) {
        guard let vm = viewModel as? BluetoothViewModel else { return }
        
        if vm.clientState.value?.bluetoothDiscoverable == false {
            vm.startPairing()
        } else {
            vm.endPairing()
        }
    }
    
    @IBAction func onBluetoothSettings(_ sender: AnyObject) {
        guard let settingsURL = URL(string: UIApplication.openSettingsURLString) else { return }
        guard UIApplication.shared.canOpenURL(settingsURL) else { return }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(settingsURL)
        } else {
            UIApplication.shared.openURL(settingsURL)
        }
    }
    
    func updateForActive(_ active: Bool) {
        updateForActive(active, animated: true)
    }
    
    func updateForActive(_ active: Bool, animated: Bool) {
        if active {
            UIView.setAlphaOfView(bluetoothActiveContainer, visible: true, animated: animated)
            UIView.setAlphaOfView(activateButton, visible: false, animated: animated)
        } else {
            UIView.setAlphaOfView(bluetoothActiveContainer, visible: false, animated: animated)
            UIView.setAlphaOfView(activateButton, visible: true, animated: animated)
        }
    }
    
    func updateForIsPairing(_ isPairing: Bool) {
        updateForIsPairing(isPairing, animated: true)
    }
    
    func updateForIsPairing(_ isPairing: Bool, animated: Bool) {
        if isPairing {
            UIView.setAlphaOfView(pairingModeContainer, visible: true, animated: animated)
            UIView.setAlphaOfView(enterPairingButton, visible: false, animated: animated)
            pairingActivityIndicator.startAnimating()
        } else {
            UIView.setAlphaOfView(pairingModeContainer, visible: false, animated: animated)
            UIView.setAlphaOfView(enterPairingButton, visible: true, animated: animated)
            pairingActivityIndicator.stopAnimating()
        }
    }
    
    func updateForBluetoothDevices(_ bluetoothDevices: [BluetoothDevice]) {
        if bluetoothDevices.count == 0 {
            connectedToContainer.isHidden = true
        } else {
            connectedToContainer.isHidden = false
        }
        
        connectedToDeviceLabel.text = bluetoothDevices.map { $0.name }.joined(separator: "\n")
    }
}
