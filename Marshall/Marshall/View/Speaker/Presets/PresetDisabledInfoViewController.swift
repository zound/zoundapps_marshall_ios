//
//  PresetDisabledInfoViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 05/10/2016.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit
import ZoundCommon

protocol PresetDisabledInfoViewControllerDelegate: class {
    func presetDisabledAlertDidRequestContinue(_ alertViewController: PresetDisabledInfoViewController)
    func presetDisabledAlertDidRequestLearnMore(_ alertViewController: PresetDisabledInfoViewController)
}

class PresetDisabledInfoViewController: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var readMoreButton: UIButton!
    
    weak var delegate: PresetDisabledInfoViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.text = Localizations.PresetsDisabled.Title
        titleLabel.font = Fonts.UrbanEars.Bold(23)
        
        let contentFont = Fonts.MainContentFont
        let contentColor = UIColor("#FFFFFF")
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        paragraphStyle.lineSpacing = 5
        
        let termsAttributes = [NSAttributedString.Key.font:contentFont,
                               NSAttributedString.Key.foregroundColor:contentColor,
                               NSAttributedString.Key.paragraphStyle: paragraphStyle]
        
        let contentText = Localizations.PresetsDisabled.Content
        let contentAttributedString = NSMutableAttributedString(string: contentText, attributes: termsAttributes)
        let imageTokens = ["[cloud_image]": UIImage (named: "preset_disabled_cloud_icon"),
                           "[plus_image]": UIImage (named: "preset_disabled_add_preset_icon")]
        
        for token in imageTokens.keys {
            let tokenRange = (contentAttributedString.string as NSString).range(of: token)
            if tokenRange.length != 0 {
                let replaceImage = imageTokens[token]!
                
                let textAttachment = NSTextAttachment()
                textAttachment.image = replaceImage
                let attrStringWithImage = NSAttributedString(attachment: textAttachment)
                contentAttributedString.replaceCharacters(in: tokenRange, with: attrStringWithImage)
            }
        }
        
        contentLabel.attributedText = contentAttributedString
        contentLabel.font = Fonts.MainContentFont
        
        backButton.setAttributedTitle(Fonts.SecondaryButtonFont.AttributedSecondaryButtonWithString(Localizations.Appwide.Back), for: .normal)
        readMoreButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.PresetsDisabled.Buttons.ReadMore), for: .normal)
    }

    @IBAction func onBack(_ sender: Any) {
        self.delegate?.presetDisabledAlertDidRequestContinue(self)
    }
    
    @IBAction func onReadMore(_ sender : Any) {
        self.delegate?.presetDisabledAlertDidRequestLearnMore(self)
    }
}
