//
//  GoogleCastSourceViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 06/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit
import STKWebKitViewController

protocol GoogleCastSourceViewControllerDelegate: class {
    
    func googleCastSourceDidRequestBack(_ viewController: GoogleCastSourceViewController)
    func googleCastSourceDidRequestFindCastApps(_ viewController: GoogleCastSourceViewController)
    func googleCastSourceDidRequestOpenApp(_ viewController: GoogleCastSourceViewController, appId: String, appURL: String)
}

class GoogleCastSourceViewController: UIViewController {

    
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var getGoogleCastButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    
    weak var delegate: GoogleCastSourceViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        contentLabel.text = Localizations.GoogleCastSource.Content
        contentLabel.font = Fonts.UrbanEars.Regular(16)
        
        UIView.setAnimationsEnabled(false)
        
        getGoogleCastButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.GoogleCastSource.GetCastApps), for: .normal)
        
        backButton.setAttributedTitle(Fonts.SecondaryButtonFont.AttributedSecondaryButtonWithString(Localizations.Appwide.Back), for: .normal)
        
        getGoogleCastButton.layoutIfNeeded()
        backButton.layoutIfNeeded()
        UIView.setAnimationsEnabled(true)
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    @IBAction func onDeezer(_ sender: AnyObject) {
        
        self.delegate?.googleCastSourceDidRequestOpenApp(self, appId: "292738169", appURL: "deezer://")
    }
    
    @IBAction func onPandora(_ sender: AnyObject) {
        
        self.delegate?.googleCastSourceDidRequestOpenApp(self, appId: "284035177", appURL: "pandora://")
    }
    
    @IBAction func onTidal(_ sender: AnyObject) {
        
        self.delegate?.googleCastSourceDidRequestOpenApp(self, appId: "913943275", appURL: "tidal://")
    }
    
    //open chromecast app url scheme: Chromecast://
    @IBAction func onBack(_ sender: AnyObject) {
        
        self.delegate?.googleCastSourceDidRequestBack(self)
        
    }
    @IBAction func onGetGoogleCast(_ sender: AnyObject) {
        
        self.delegate?.googleCastSourceDidRequestFindCastApps(self)
    }
}
