//
//  SpotifySourceViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 06/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit
import StoreKit

protocol SpotifySourceViewControllerDelegate: class {
    
    func spotifySourceDidRequestBack(_ viewController: SpotifySourceViewController)
    func spotifySourceDidRequestSpeakerName(_ viewController: SpotifySourceViewController) -> String
}

class SpotifySourceViewController: UIViewController {

    @IBOutlet weak var instructionsLabel: UILabel!
    @IBOutlet weak var introLabel: UILabel!
    @IBOutlet var openOrGetSpotify: UIButton!
    var openSpotifyOnDissapear: Bool = false
    
    @IBOutlet weak var backButton: UIButton!
    weak var delegate: SpotifySourceViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let speakerName = self.delegate?.spotifySourceDidRequestSpeakerName(self) ?? ""
        introLabel.text = Localizations.SpotifySource.Intro(speakerName)
        introLabel.font = Fonts.UrbanEars.Regular(16)
        
        instructionsLabel.text = Localizations.SpotifySource.Instructions
        instructionsLabel.font = Fonts.UrbanEars.Regular(16)
        
        UIView.setAnimationsEnabled(false)
        
        if canOpenSpotify() {
            openOrGetSpotify.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.SpotifySource.Buttons.OpenSpotify), for: .normal)
        } else {
            openOrGetSpotify.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.SpotifySource.Buttons.GetSpotify), for: .normal)
        }
        
        backButton.setAttributedTitle(Fonts.SecondaryButtonFont.AttributedSecondaryButtonWithString(Localizations.Appwide.Back), for: .normal)
        
        
        openOrGetSpotify.layoutIfNeeded()
        backButton.layoutIfNeeded()
        UIView.setAnimationsEnabled(true)
    }
    
    func openSpotifyApp() {
        
        let spotifyURLString = "spotify://"
        let spotifyURL = URL(string:spotifyURLString)!
        
        if canOpenSpotify() {
            UIApplication.shared.openURL(spotifyURL)
            
        } else {
            
            let iTunesLinkString = "itms-apps://itunes.apple.com/us/app/apple-store/id324684580?mt=8"
            let iTunesURL = URL(string: iTunesLinkString)!
            
            if UIApplication.shared.canOpenURL(iTunesURL) {
                
                UIApplication.shared.openURL(iTunesURL)
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        if openSpotifyOnDissapear {
            
            openSpotifyApp()
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    @IBAction func onOpenOrGetSpotify(_ sender: AnyObject) {
        
        openSpotifyOnDissapear = true
        delegate?.spotifySourceDidRequestBack(self)
    }

    @IBAction func onBack(_ sender: AnyObject) {
        
        self.delegate?.spotifySourceDidRequestBack(self)
    }
    
    func updateForLoading(_ loading: Bool) {
        
    }
    
    func canOpenSpotify() -> Bool {
        let spotifyURLString = "spotify://"
        let spotifyURL = URL(string:spotifyURLString)!
        
        if UIApplication.shared.canOpenURL(spotifyURL) {
            
            return true
        }
        return false
    }
}

extension SpotifySourceViewController: SKStoreProductViewControllerDelegate {
    
    func productViewControllerDidFinish(_ viewController: SKStoreProductViewController) {
        
        self.dismiss(animated: true, completion: nil)
    }
}
