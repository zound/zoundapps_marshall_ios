//
//  UIStoryboar+SpeakerControllers.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 21/06/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit

extension UIStoryboard {
    static var player: UIStoryboard {
        return UIStoryboard(name: "Player", bundle: nil)
    }
}

extension UIStoryboard {
    var speakerViewController: SpeakerViewController {
        guard let vc = UIStoryboard.player.instantiateViewController(withIdentifier: "speaker") as? SpeakerViewController else {
            fatalError("SpeakerViewController couldn't be found in Storyboard file")
        }
        return vc
    }
    
    var speakerDisconnectedViewController: SpeakerDisconnectedViewController {
        guard let vc = UIStoryboard.player.instantiateViewController(withIdentifier: "speakerDisconnected") as? SpeakerDisconnectedViewController else {
            fatalError("SpeakerDisconnectedViewController couldn't be found in Storyboard file")
        }
        return vc
    }
    
    var speakerConnectingViewController: SpeakerConnectingViewController {
        guard let vc = UIStoryboard.player.instantiateViewController(withIdentifier: "speakerConnecting") as? SpeakerConnectingViewController else {
            fatalError("SpeakerConnectingViewController couldn't be found in Storyboard file")
        }
        return vc
    }
    
    var presetsViewController: PresetsViewController {
        guard let vc = UIStoryboard.player.instantiateViewController(withIdentifier: "presets") as? PresetsViewController else {
            fatalError("PresetsViewController couldn't be found in Storyboard file")
        }
        return vc
    }
    
    var presetViewController: PresetSelectableViewController {
        guard let vc = UIStoryboard.player.instantiateViewController(withIdentifier: "preset") as? PresetSelectableViewController else {
            fatalError("PresetSelectableViewController couldn't be found in Storyboard file")
        }
        return vc
    }
    
    var auxSelectableViewController: AuxSelectableViewController {
        guard let vc = UIStoryboard.player.instantiateViewController(withIdentifier: "auxSelectable") as? AuxSelectableViewController else {
            fatalError("AuxSelectableViewController couldn't be found in Storyboard file")
        }
        return vc
    }
    
    var rcaSelectableViewController: RCASelectableViewController {
        guard let vc = UIStoryboard.player.instantiateViewController(withIdentifier: "rcaSelectable") as? RCASelectableViewController else {
            fatalError("RCASelectableViewController couldn't be found in Storyboard file")
        }
        return vc
    }
    
    var bluetoothSelectableViewController: BluetoothSelectableViewController {
        guard let vc = UIStoryboard.player.instantiateViewController(withIdentifier: "bluetoothSelectable") as? BluetoothSelectableViewController else {
            fatalError("BluetoothSelectableViewController couldn't be found in Storyboard file")
        }
        return vc
    }
    
    var cloudSelectableViewController: CloudSelectableViewController {
        guard let vc = UIStoryboard.player.instantiateViewController(withIdentifier: "cloudSelectable") as? CloudSelectableViewController else {
            fatalError("CloudSelectableViewController couldn't be found in Storyboard file")
        }
        return vc
    }
    
    var nowPlayingViewController: PlayerViewController {
        guard let vc = UIStoryboard.player.instantiateViewController(withIdentifier: "player") as? PlayerViewController else {
            fatalError("PlayerViewController couldn't be found in Storyboard file")
        }
        return vc
    }
    
    var spotifySourceViewController: SpotifySourceViewController {
        guard let vc = UIStoryboard.player.instantiateViewController(withIdentifier: "sourceSpotify") as? SpotifySourceViewController else {
            fatalError("SpotifySourceViewController couldn't be found in Storyboard file")
        }
        return vc
    }
    
    var googleSourceViewController: GoogleCastSourceViewController {
        guard let vc = UIStoryboard.player.instantiateViewController(withIdentifier: "sourceGoogleCast") as? GoogleCastSourceViewController else {
            fatalError("GoogleCastSourceViewController couldn't be found in Storyboard file")
        }
        return vc
    }
    
    var airplaySourceController: AirplaySourceViewController {
        guard let vc = UIStoryboard.player.instantiateViewController(withIdentifier: "sourceAirplay") as? AirplaySourceViewController else {
            fatalError("AirplaySourceViewController couldn't be found in Storyboard file")
        }
        return vc
    }
    
    var presetDisabledInfoController: PresetDisabledInfoViewController {
        
        guard let vc = UIStoryboard.player.instantiateViewController(withIdentifier: "presetDisabledInfoAlert") as? PresetDisabledInfoViewController else {
            fatalError("PresetDisabledInfoViewController couldn't be found in Storyboard file")
        }
        return vc   
    }
    
    var savePresetController: SavePresetViewController {
        guard let vc = UIStoryboard.player.instantiateViewController(withIdentifier: "savePreset") as? SavePresetViewController else {
            fatalError("SavePresetViewController couldn't be found in Storyboard file")
        }
        return vc
    }
    
    var browseRadioViewController: BrowseViewController {
        guard let vc = UIStoryboard.player.instantiateViewController(withIdentifier: "browseRadio") as? BrowseViewController else {
            fatalError("BrowseViewController couldn't be found in Storyboard file")
        }
        return vc
    }
}
