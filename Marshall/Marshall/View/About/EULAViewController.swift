//
//  EULAViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 16/02/2017.
//  Copyright © 2017 Zound Industries. All rights reserved.
//

import Foundation
import UIKit

protocol EULAViewControllerDelegate: class {
    func eulaViewControllerDidRequestBack(_ eulaViewController: EULAViewController)
    func eulaViewControllerDidRequestGotoWebsite(_ website: String, eulaViewController: EULAViewController)
}

class EULAViewController: UIViewController {
    weak var delegate: EULAViewControllerDelegate?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var gotoWebsiteButton: UIButton!
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    override func viewDidLoad() {
        titleLabel.attributedText = Fonts.PageTitleFont.AttributedPageTitleWithString(Localizations.About.Eula.Title.uppercased())
        
        contentLabel.text = Localizations.About.Eula.Content
        contentLabel.font = Fonts.MainContentFont
        
        UIView.setAnimationsEnabled(false)
        
        gotoWebsiteButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.Appwide.GoToWebsite), for: .normal)
        
        gotoWebsiteButton.layoutIfNeeded()
        UIView.setAnimationsEnabled(true)
    }
    
    @IBAction func onGotoWebsite(_ sender: Any) {
        self.delegate?.eulaViewControllerDidRequestGotoWebsite(Localizations.About.Eula.Website, eulaViewController: self)
    }
    
    @IBAction func onBack(_ sender: Any) {
        self.delegate?.eulaViewControllerDidRequestBack(self)
    }
}
