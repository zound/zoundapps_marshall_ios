//
//  AboutViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 09/09/2016.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

protocol AboutViewControllerDelegate: class {
    func aboutViewControllerDidRequestBack(_ aboutViewController: AboutViewController)
    func aboutViewControllerDidRequestAboutItem(_ helpItem: AboutItem, _ aboutViewController: AboutViewController)
}

enum AboutItem {
    case endUserLicense
    case openSource
    case privacyPolicy
    
    var localizedDisplayName: String {
        switch  self {
        case .endUserLicense: return Localizations.About.MenuItem.Eula
        case .openSource: return Localizations.About.MenuItem.Foss
        case .privacyPolicy: return Localizations.About.PrivacyPolicy.Title
        }
    }
}

class AboutViewController: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableTopSeparatorHeight: NSLayoutConstraint!
    @IBOutlet weak var tableBottomSeparatorHeight: NSLayoutConstraint!
    
    weak var delegate: AboutViewControllerDelegate?
    
    let menuItems: [AboutItem]
    let disposeBag = DisposeBag()
    
    required init?(coder aDecoder: NSCoder) {
        self.menuItems = [.endUserLicense, .openSource, .privacyPolicy]
        super.init(coder: aDecoder)
    }
    
    deinit {
        print(#function, String(describing: self))
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    override func viewDidLoad() {
        titleLabel.attributedText = Fonts.PageTitleFont.AttributedPageTitleWithString(Localizations.About.Title.uppercased())
        self.tableTopSeparatorHeight.constant = 0.5
        self.tableBottomSeparatorHeight.constant = 0.5
        versionLabel.font = Fonts.UrbanEars.Regular(15)
        
        if let version = Bundle.main.releaseVersionNumber, let build = Bundle.main.buildVersionNumber {
            versionLabel.text = Localizations.About.Version(version, build)
        }
        
        self.tableView.backgroundColor = UIColor.clear
        setupTableView()
    }
    
    @IBAction func onBack(_ sender: AnyObject) {
        self.delegate?.aboutViewControllerDidRequestBack(self)
    }
}

// MARK: - Rx bindings for UITableView
extension AboutViewController {
    func setupTableView() {
        Observable.just(menuItems)
            .bind(to: tableView.rx.items(cellIdentifier: "aboutItem", cellType: AboutCell.self)) { (row, element, cell) in
                cell.aboutItem = element }
            .disposed(by: disposeBag)
        
        tableView.rx.itemSelected
            .debounce(0.1, scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] indexPath in
                guard let `self` = self else { return }
                
                let menuItem = self.menuItems[(indexPath as NSIndexPath).row]
                self.delegate?.aboutViewControllerDidRequestAboutItem(menuItem, self)
                self.tableView.deselectRow(at: indexPath, animated: true) })
            .disposed(by: disposeBag)
    }
}
