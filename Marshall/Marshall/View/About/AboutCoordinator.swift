//
//  AboutCoordinator.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 16/02/2016.
//  Copyright © 2017 Zound Industries. All rights reserved.
//

import Foundation
import STKWebKitViewController
import SafariServices
import ZoundCommon

protocol AboutCoordinatorDelegate: class {
    func aboutCoordinatorDidFinishCoordinating(_ coordinator: AboutCoordinator)
}

class AboutCoordinator: Coordinator, RunAfterDelay {
    weak var delegate: AboutCoordinatorDelegate?
    
    var navController: UINavigationController!
    let parentViewController: UIViewController
    
    init(parentViewController: UIViewController) {
        self.parentViewController = parentViewController
    }
    
    func start() {
        self.navController = UENavigationController()
        self.navController.navigationBar.isHidden = true
        self.navController.modalPresentationStyle = .formSheet
        
        showAboutRoot(animated: false)
        self.parentViewController.present(navController, animated: true, completion: nil)
    }
    
    func showAboutRoot(animated: Bool = true) {
        let aboutController = UIStoryboard.main.aboutViewController
        aboutController.delegate = self
        aboutController.modalPresentationStyle = .formSheet
        navController.pushViewController(aboutController, animated: animated)
    }
    
    func showEULA(animated: Bool = true) {
        let eula = UIStoryboard.main.eulaViewController
        eula.delegate = self
        navController.pushViewController(eula, animated: animated)
    }
    
    func showFOSS(animated: Bool = true) {
        let foss = UIStoryboard.main.fossViewController
        foss.delegate = self
        navController.pushViewController(foss, animated: animated)
    }
    
    func showBrowserForURL(_ url: URL) {
        if let rootVC:UIViewController = self.navController {
            if #available(iOS 9, *) {
                let safari = SFSafariViewController(url: url)
                safari.modalPresentationStyle = .overFullScreen
                safari.modalPresentationCapturesStatusBarAppearance = true
                rootVC.present(safari, animated: true, completion: nil)
            } else {
                let webViewController = STKWebKitModalViewController(url: url)
                rootVC.present(webViewController!, animated: true, completion: nil)
            }
        }
    }
}

extension SFSafariViewController {
    open override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
}

extension AboutCoordinator: EULAViewControllerDelegate {
    func eulaViewControllerDidRequestBack(_ eulaViewController: EULAViewController) {
        self.navController.popViewController(animated: true)
    }
    
    func eulaViewControllerDidRequestGotoWebsite(_ website: String, eulaViewController: EULAViewController) {
        guard let url = URL(string: website) else  { return }
        showBrowserForURL(url)
    }
}

extension AboutCoordinator: FOSSViewControllerDelegate {
    func fossViewControllerDidRequestBack(_ fossViewController: FOSSViewController) {
        self.navController.popViewController(animated: true)
    }
    
    func fossViewControllerDidRequestGotoWebsite(_ website: String, fossVewController: FOSSViewController) {
        guard let url = URL(string: website) else { return }
        showBrowserForURL(url)
    }
}

extension AboutCoordinator: AboutViewControllerDelegate {
    func aboutViewControllerDidRequestBack(_ aboutViewController: AboutViewController) {
        self.delegate?.aboutCoordinatorDidFinishCoordinating(self)
    }
    
    func aboutViewControllerDidRequestAboutItem(_ aboutItem: AboutItem, _ helpViewController: AboutViewController) {
        switch aboutItem {
        case .openSource:
            showFOSS()
        case .endUserLicense:
            showEULA()
        case .privacyPolicy:
            guard let url = URL(string: Localizations.About.PrivacyPolicy.Website) else { return }
            showBrowserForURL(url)
        }
    }
}
