//
//  FOSSViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 16/02/2017.
//  Copyright © 2017 Zound Industries. All rights reserved.
//

import Foundation
import UIKit

protocol FOSSViewControllerDelegate: class {
    func fossViewControllerDidRequestBack(_ fossViewController: FOSSViewController)
    func fossViewControllerDidRequestGotoWebsite(_ website: String, fossVewController: FOSSViewController)
}

class FOSSViewController: UIViewController {
    weak var delegate: FOSSViewControllerDelegate?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var gotoWebsiteButton: UIButton!
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    override func viewDidLoad() {
        
        titleLabel.attributedText = Fonts.PageTitleFont.AttributedPageTitleWithString(Localizations.About.Foss.Title.uppercased())
        
        contentLabel.text = Localizations.About.Foss.Content
        contentLabel.font = Fonts.MainContentFont
        
        UIView.setAnimationsEnabled(false)
        
        gotoWebsiteButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.Appwide.GoToWebsite), for: .normal)
        
        gotoWebsiteButton.layoutIfNeeded()
        UIView.setAnimationsEnabled(true)
    }
    
    @IBAction func onGotoWebsite(_ sender: Any) {
        self.delegate?.fossViewControllerDidRequestGotoWebsite(Localizations.About.Foss.Website, fossVewController: self)
    }
    
    @IBAction func onBack(_ sender: Any) {
        self.delegate?.fossViewControllerDidRequestBack(self)
    }
}
