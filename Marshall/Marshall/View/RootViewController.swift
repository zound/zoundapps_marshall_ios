//
//  RootViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 11/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit
import Cartography
import ZoundCommon

protocol RootViewControllerDelegate: class {
    func rootViewControllerdidShowMenuViewController(_ rootViewController: RootViewController)
    func rootViewControllerdidHideMenuViewController(_ rootViewController: RootViewController)
}

class RootViewController: UIViewController, UIGestureRecognizerDelegate, MenuViewInteractiveDelegate {
    weak var delegate: RootViewControllerDelegate?
    
    var displayedViewController:UIViewController?
    var menuVisible = false
    var currentMenuViewController: UIViewController?
    var currentMenuConstraints:ConstraintGroup = ConstraintGroup()
    
    let menuTransitionDelegate: MenuTransitioningDelegate = MenuTransitioningDelegate()
    let menuAnimationTime = 0.35
    
    override var childForStatusBarHidden: UIViewController? {
        guard forcedHiddenStatusBar else { return self.children.first }
        return nil
    }
    
    override var childForStatusBarStyle: UIViewController? { return self.children.first }
    
    override var prefersStatusBarHidden: Bool { return true }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        menuTransitionDelegate.dismissInteractor.delegate = self
        menuTransitionDelegate.presentInteractor.delegate = self
    }
    
    var forcedHiddenStatusBar: Bool = false {
        didSet {
            UIView.animate(withDuration: 0.15, animations: { [weak self] in
                self?.setNeedsStatusBarAppearanceUpdate()
            })
        }
    }
    
    func showChildViewController(_ viewController: UIViewController?, animated:Bool) {
        if let vc = self.displayedViewController {
            vc.willMove(toParent: nil)
            vc.view.removeFromSuperview()
            vc.removeFromParent()
        }
        
        if let newVC = viewController {
            self.addChild(newVC)
            newVC.view.translatesAutoresizingMaskIntoConstraints = false
            self.view.addSubview(newVC.view)
            
            constrain(newVC.view) { view in
                view.edges == view.superview!.edges
            }
            
            newVC.didMove(toParent: self)
            self.displayedViewController = newVC
            self.setNeedsStatusBarAppearanceUpdate()
        }
    }
    
    var menuViewController: UIViewController? {
        didSet {
            if let menuViewController = menuViewController {
                menuViewController.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
                menuViewController.transitioningDelegate = menuTransitionDelegate
                
                menuTransitionDelegate.attachDismissToViewController(menuViewController, withView: menuViewController.view, presentViewController: nil)
                //menuTransitionDelegate.attachPresentToViewController(self, withView: self.view, presentViewController: menuViewController)
            }
        }
    }
    
    func showMenuViewController(with completion: @escaping () -> Void) {
        guard let menuViewController = menuViewController else { return }
        
        menuTransitionDelegate.disableInteractivePlayerTransitioning = true
        menuVisible = true
        self.present(menuViewController, animated: true, completion: { [weak self] in
            self?.menuTransitionDelegate.disableInteractivePlayerTransitioning = false
            self?.delegate?.rootViewControllerdidShowMenuViewController(self!)
            completion()
        })
    }
    
    func hideMenuViewController(with completion: @escaping () -> Void) {
        menuTransitionDelegate.disableInteractivePlayerTransitioning = true
        self.dismiss(animated: true, completion: {[weak self] in
            self?.menuVisible = false
            self?.menuTransitionDelegate.disableInteractivePlayerTransitioning = false
            self?.delegate?.rootViewControllerdidHideMenuViewController(self!)
            completion()
        })
    }
    
    func menuViewInteractiveDidInteractivelyDismiss() {
        self.menuVisible = false
    }
    
    func menuViewInteractiveDidInteractivelyPresent() {
        self.menuVisible = true
    }
}
