//
//  SetupFailedSetupViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 16/06/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit
import YYImage

protocol SetupFailedSetupViewControllerDelegate: class {
    func setupFailedSetupDidConfirmSetupMode(_ viewController: SetupFailedSetupViewController)
    func setupFailedSetupDidDenySetupMode(_ viewController: SetupFailedSetupViewController)
}

class SetupFailedSetupViewController: UIViewController {
    @IBOutlet weak var dialImage: YYAnimatedImageView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var yesButton: UIButton!
    @IBOutlet weak var noButton: UIButton!
    
    weak var delegate: SetupFailedSetupViewControllerDelegate?
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        messageLabel.text = Localizations.Setup.FailedSetup.Message
        messageLabel.font = Fonts.MainContentFont
        
        UIView.setAnimationsEnabled(false)
        
        yesButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.Setup.FailedSetup.Buttons.Yes), for: .normal)
        noButton.setAttributedTitle(Fonts.SecondaryButtonFont.AttributedSecondaryButtonWithString(Localizations.Setup.FailedSetup.Buttons.No), for: .normal)
        
        yesButton.layoutIfNeeded()
        noButton.layoutIfNeeded()
        UIView.setAnimationsEnabled(true)
        
        self.view.backgroundColor = UIColor.clear
        
        let image = YYImage(named: "failed_setup.apng")
        dialImage.image = image
        dialImage.backgroundColor = UIColor.clear
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        dialImage.startAnimating()
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        dialImage.stopAnimating()
        super.viewWillDisappear(animated)
    }

    
    @IBAction func onYes(_ sender: AnyObject) {
        self.delegate?.setupFailedSetupDidConfirmSetupMode(self)
    }
    
    @IBAction func onNo(_ sender: AnyObject) {
        self.delegate?.setupFailedSetupDidDenySetupMode(self)
    }
}
