//
//  SetupLoadingViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 04/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit

protocol SetupLoadingViewControllerDelegate: class {
    func setupLoadingDidRequestCancelSetup(_ setupLoading:SetupLoadingViewController)
}

class SetupLoadingViewController: UIViewController {
    @IBOutlet weak var loadingIndicatorImage: UIImageView!
    @IBOutlet weak var loadingLabel: UILabel!
    
    var viewModel: SetupNetworkViewModel?
    weak var delegate: SetupLoadingViewControllerDelegate?
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }

    override func viewDidLoad() {
        
        super.viewDidLoad()
        loadingLabel.text = Localizations.Setup.Loading.LookingForSpeakers
        loadingLabel.font = Fonts.UrbanEars.Regular(18)
        loadingIndicatorImage.rotate()
        
    }
    
    @IBAction func onCloseButton(_ sender: AnyObject) {
        self.delegate?.setupLoadingDidRequestCancelSetup(self)
    }
}
