//
//  SetupFailedBootViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 16/06/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit
import YYImage

protocol SetupFailedBootViewControllerDelegate: class {
    func setupFailedBootDidRequestNext(_ viewController: SetupFailedBootViewController)
    func setupFailedBootDidRequestCancel(_ viewController: SetupFailedBootViewController)
}

class SetupFailedBootViewController: UIViewController {
    @IBOutlet weak var dialImage: YYAnimatedImageView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    weak var delegate: SetupFailedBootViewControllerDelegate?
    
    deinit {
        dialImage = nil
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.clear
        
        messageLabel.text = Localizations.Setup.FailedBoot.Message
        messageLabel.font = Fonts.MainContentFont
        
        UIView.setAnimationsEnabled(false)
        
        nextButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.Appwide.Done), for: .normal)
        cancelButton.setAttributedTitle(Fonts.SecondaryButtonFont.AttributedSecondaryButtonWithString(Localizations.Appwide.Cancel), for: .normal)
        
        nextButton.layoutIfNeeded()
        cancelButton.layoutIfNeeded()
        
        UIView.setAnimationsEnabled(true)

        let image = YYImage(named: "failed_boot.apng")
        dialImage.image = image
        dialImage.backgroundColor = UIColor.clear
    }
    
    override func viewDidAppear(_ animated: Bool) {
        dialImage.startAnimating()
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        dialImage.stopAnimating()
        super.viewWillDisappear(animated)
    }
    
    @IBAction func onNext(_ sender: AnyObject) {
        self.delegate?.setupFailedBootDidRequestNext(self)
    }
    
    @IBAction func onCancel(_ sender: AnyObject) {
        self.delegate?.setupFailedBootDidRequestCancel(self)
    }
}
