//
//  SetupPresetsLoadingProgress.swift
//  Marshall
//
//  Created by Raul Andrisan on 23/05/2017.
//  Copyright © 2017 Zound Industries. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import ZoundCommon
import YYImage

protocol  SetupPresetsLoadingProgressDelegate: class {
    func setupPresetsLoadingProgressDidFinishLoadingAnimation(_ viewcontroller:SetupPresetsLoadingProgress)
}

class SetupPresetsLoadingProgress: UIViewController {
    @IBOutlet weak var dialImage: YYAnimatedImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var statusLabelsContainer: UIView!
    @IBOutlet weak var statusLabelNumber1: UILabel!
    @IBOutlet weak var statusLabel1: UILabel!
    @IBOutlet weak var statusLabelNumber2: UILabel!
    @IBOutlet weak var statusLabel2: UILabel!
    @IBOutlet weak var statusLabel1VerticalPositionConstraint: NSLayoutConstraint!
    @IBOutlet weak var statusLabel2VerticalPositionConstraint: NSLayoutConstraint!
    
    weak var delegate: SetupPresetsLoadingProgressDelegate?
    
    var viewModel: SetupPresetsViewModel?
    
    var currentLabel: UILabel?
    var otherLabel: UILabel? { return currentLabel != nil ? (currentLabel == statusLabel1 ? statusLabel2 : statusLabel1) : nil }
    var constraintForLabel: [UILabel:NSLayoutConstraint]?
    var numberLabelForLabel: [UILabel:UILabel]?
    var disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        titleLabel.text = Localizations.Setup.PresetsLoading.Title.uppercased()
        titleLabel.font = Fonts.UrbanEars.Bold(18)
        
        subtitleLabel.text = self.viewModel?.spotifyUsername.value
        subtitleLabel.font = Fonts.UrbanEars.Regular(17)
        
        statusLabel1.font = Fonts.UrbanEars.Regular(16)
        statusLabel2.font = Fonts.UrbanEars.Regular(16)
        
        statusLabelNumber1.font = Fonts.UrbanEars.Bold(75)
        statusLabelNumber2.font = Fonts.UrbanEars.Bold(75)
        
        constraintForLabel = [statusLabel1 : statusLabel1VerticalPositionConstraint,
                              statusLabel2: statusLabel2VerticalPositionConstraint]
        
        numberLabelForLabel = [statusLabel1 : statusLabelNumber1,
                               statusLabel2: statusLabelNumber2]
        
        statusLabel1.alpha = 0.0
        statusLabel2.alpha = 0.0
        statusLabelNumber1.alpha = 0.0
        statusLabelNumber2.alpha = 0.0
        
        let image = YYImage(named: "presets_loading.apng")
        
        dialImage.image = image
        dialImage.backgroundColor = UIColor.clear
        dialImage.stopAnimating()
        dialImage.rx.observe(Int.self, "currentAnimatedImageIndex")
            .subscribeNext(weak: self, SetupPresetsLoadingProgress.didUpdateAnimatedImageIndexTo)
            .disposed(by: disposeBag)
        
        runAfterDelay(0.5, block: { [weak self] in
            self?.dialImage.startAnimating()
        })
    }
    
    func didUpdateAnimatedImageIndexTo(_ index: Int?) {
        if let index = index, UInt(index) + 1 == (dialImage.image as? YYImage)?.animatedImageFrameCount() {
            self.dialImage.stopAnimating()
        }
    }
    
    func runPresetsSettingAnimation() {
        let ticks = Observable<Int>
            .interval(0.8, scheduler: MainScheduler.instance)
            .take(7)
        
        guard let presets = viewModel?.presets.value else { return }
        let presetNames = presets.map { $0.name }
        
        ticks
            .map { currentTick in
                let statuses = presetNames
                let index = currentTick % statuses.count
                
                return (index, Localizations.Setup.PresetsLoading.AddingPresetTitle(statuses[index])) }
            .subscribe(onNext: { [weak self] (index: Int, status: String) in
                self?.showStatus(index, status: status)
                self?.selectLedWithIndex(index)
                }, onCompleted: { [weak self] in
                    guard let `self` = self else {return}
                    
                    self.runAfterDelay(1.0, block: {
                        self.finishedLoadingAnimation()
                    })
            })
            .disposed(by: disposeBag)
    }
    
    func reportUploadingFinished() {
        self.showStatus(-1, status: Localizations.Setup.PresetsLoading.Done)
        self.selectLedWithIndex(-1)
        runAfterDelay(1.0, block: { [weak self] in
            self?.delegate?.setupPresetsLoadingProgressDidFinishLoadingAnimation(self!)
        })
    }
    
    func finishedLoadingAnimation() {
        reportUploadingFinished()
    }
    
    func selectLedWithIndex(_ index: Int) {}
    
    func showStatus(_ index: Int, status: String) {
        let scrollSize = CGFloat(50.0)
        let animationDuration = 0.35
        
        if currentLabel != nil {
            let currentLabelConstraint = constraintForLabel![currentLabel!]!
            let otherLabelConstraint = constraintForLabel![otherLabel!]!
            
            let currentNumberLabel = numberLabelForLabel![currentLabel!]
            let otherNumberLabel = numberLabelForLabel![otherLabel!]
            
            currentLabelConstraint.constant = 0
            self.currentLabel!.alpha = 1.0
            currentNumberLabel?.alpha = 1.0
            
            otherLabelConstraint.constant = scrollSize
            self.otherLabel!.alpha = 0.0
            otherNumberLabel?.alpha = 0.0
            
            self.otherLabel!.text = status
            
            if index >= 0 {
                otherNumberLabel?.text = String(index+1)
            } else {
                otherNumberLabel?.text = ""
            }
            
            self.statusLabelsContainer.layoutIfNeeded()
            
            currentLabelConstraint.constant = -scrollSize
            otherLabelConstraint.constant = 0
            UIView.animate(withDuration: animationDuration, animations: { [weak self] in
                self?.statusLabelsContainer.layoutIfNeeded()
                
                self?.currentLabel?.alpha = 0.0
                currentNumberLabel?.alpha = 0.0
                
                self?.otherLabel?.alpha = 1.0
                otherNumberLabel?.alpha = 1.0
            })
            
            self.currentLabel = self.otherLabel
        } else {
            currentLabel = statusLabel1
            
            let currentLabelConstraint = constraintForLabel![currentLabel!]!
            let currentNumberLabel = numberLabelForLabel![currentLabel!]
            
            currentLabelConstraint.constant = scrollSize
            self.currentLabel!.alpha = 0.0
            currentNumberLabel?.alpha = 0.0
            
            self.currentLabel!.text = status
            
            if index >= 0 {
                currentNumberLabel?.text = String(index+1)
            } else {
                currentNumberLabel?.text = ""
            }
            
            self.statusLabelsContainer.layoutIfNeeded()
            currentLabelConstraint.constant = 0
            
            UIView.animate(withDuration: animationDuration, animations: { [weak self] in
                self?.statusLabelsContainer.layoutIfNeeded()
                self?.currentLabel?.alpha = 1.0
                currentNumberLabel?.alpha = 1.0
            })
        }
    }
    
    func resetLoader() {
        statusLabel1.alpha = 0.0
        statusLabel2.alpha = 0.0
        statusLabelNumber1.alpha = 0.0
        statusLabelNumber2.alpha = 0.0
        selectLedWithIndex(0)
    }
}
