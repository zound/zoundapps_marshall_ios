//
//  SetupPresetsIntroViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 18/06/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation

import UIKit

protocol SetupPresetsIntroViewControllerDelegate:class {
    func setupPresetsIntroDidReqestSkip(_ viewController: SetupPresetsIntroViewController)
    func setupPresetsIntroDidReqestNext(_ viewController: SetupPresetsIntroViewController)
}

class SetupPresetsIntroViewController: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var skipButton: UIButton!
    
    weak var delegate: SetupPresetsIntroViewControllerDelegate?
    
    var viewModel: SetupTutorialViewModel?
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.clear
        
        titleLabel.numberOfLines = 0
        titleLabel.text = Localizations.Setup.PresetsInitial.Title.uppercased()
        titleLabel.font = Fonts.UrbanEars.Bold(45)
        
        contentLabel.numberOfLines = 0
        contentLabel.text = Localizations.Setup.PresetsInitial.Content
        contentLabel.font = Fonts.MainContentFont
        
        UIView.setAnimationsEnabled(false)
        
        nextButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.Setup.PresetsInitial.Buttons.Next), for: .normal)
        skipButton.setAttributedTitle(Fonts.SecondaryButtonFont.AttributedSecondaryButtonWithString(Localizations.Appwide.Skip), for: .normal)
        
        nextButton.layoutIfNeeded()
        skipButton.layoutIfNeeded()
        
        UIView.setAnimationsEnabled(true)
    }
    
    @IBAction func onSkip(_ sender: AnyObject) {
        self.delegate?.setupPresetsIntroDidReqestSkip(self)
    }
    
    @IBAction func onNext(_ sender: AnyObject) {
        self.delegate?.setupPresetsIntroDidReqestNext(self)
    }
}
