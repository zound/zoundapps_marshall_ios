//
//  SetupPresetsLoadingPreparing.swift
//  Marshall
//
//  Created by Raul Andrisan on 23/05/2017.
//  Copyright © 2017 Zound Industries. All rights reserved.
//

import Foundation
import UIKit

class SetupPresetsLoadingPreparing: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var loadingIndicator: UIImageView!
    @IBOutlet weak var preparingPresetsLabel: UILabel!
 
    var viewModel: SetupPresetsViewModel?
    
    override func viewDidLoad() {
        titleLabel.text = Localizations.Setup.PresetsLoading.Title.uppercased()
        titleLabel.font = Fonts.UrbanEars.Bold(18)
        
        preparingPresetsLabel.text = Localizations.Setup.PresetsLoading.PreparingPresets
        preparingPresetsLabel.font = Fonts.UrbanEars.Regular(16)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loadingIndicator.rotate()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        loadingIndicator.stopRotation()
    }
}
