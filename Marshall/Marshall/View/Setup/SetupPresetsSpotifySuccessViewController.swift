//
//  SetupPresetsIntroViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 16/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

protocol SetupPresetsSpotifySuccessViewControllerDelegate: class {
    func setupPresetsSpotifyDidRequestNext(_ viewController:SetupPresetsSpotifySuccessViewController)
}

class SetupPresetsSpotifySuccessViewController: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var accountNameLabel: UILabel!
    @IBOutlet weak var sentenceBeginLabel: UILabel!
    @IBOutlet weak var continueButton: UIButton!
    
    weak var delegate: SetupPresetsSpotifySuccessViewControllerDelegate?
    
    var viewModel: SetupPresetsViewModel?
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.text = Localizations.Setup.SpotifySuccess.Title.uppercased()
        titleLabel.font = Fonts.UrbanEars.Bold(23)
        
        sentenceBeginLabel.text = Localizations.Setup.SpotifySuccess.YourSpotifyAccount
        sentenceBeginLabel.font = Fonts.MainContentFont
        
        accountNameLabel.font = Fonts.MainContentFont
        
        UIView.setAnimationsEnabled(false)
        
        continueButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.Appwide.Done), for: .normal)
        
        continueButton.layoutIfNeeded()
        UIView.setAnimationsEnabled(true)
        
        if let vm = viewModel {
            vm.spotifyUsername.asObservable()
                .map { $0 ?? "" }
                .bind(to: accountNameLabel.rx.text)
                .disposed(by: rx_disposeBag)
        }
    }
    
    @IBAction func onContinue(_ sender: AnyObject) {
        self.delegate?.setupPresetsSpotifyDidRequestNext(self)
    }
}
