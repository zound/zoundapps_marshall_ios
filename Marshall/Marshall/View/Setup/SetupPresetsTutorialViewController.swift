//
//  SetupPresetsIntroViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 16/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit
import YYImage

protocol SetupPresetsTutorialViewControllerDelegate: class {
    func setupPresetsTutorialDidFinish(_ viewController:SetupPresetsTutorialViewController)
    func setupPresetsTutorialDidRequestBack(_ viewController:SetupPresetsTutorialViewController)
}

class SetupPresetsTutorialViewController: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentTopLabel: UILabel!
    @IBOutlet weak var contentBottomLabel: UILabel!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var dialImage: YYAnimatedImageView!
    
    weak var delegate: SetupPresetsTutorialViewControllerDelegate?
    
    var viewModel: SetupPresetsViewModel?
    
    deinit {
        dialImage = nil
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.text = Localizations.Setup.PresetsTutorial.Title.uppercased()
        titleLabel.font = Fonts.UrbanEars.Bold(18)
        
        contentTopLabel.text = Localizations.Setup.PresetsTutorial.Content
        contentTopLabel.font = Fonts.MainContentFont
        
        UIView.setAnimationsEnabled(false)
        
        nextButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.Appwide.Done), for: .normal)
        nextButton.layoutIfNeeded()
        UIView.setAnimationsEnabled(true)
        
        let image = YYImage(named: "presets_tutorial.apng")
        dialImage.image = image
        dialImage.backgroundColor = UIColor.clear
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        dialImage.startAnimating()
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        dialImage.stopAnimating()
        super.viewWillDisappear(animated)
    }
    
    @IBAction func onDone(_ sender: AnyObject) {
        self.delegate?.setupPresetsTutorialDidFinish(self)
    }
    
    @IBAction func onBack(_ sender: AnyObject) {
        self.delegate?.setupPresetsTutorialDidRequestBack(self)
    }
}
