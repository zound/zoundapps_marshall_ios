//
//  AppStoreCoordinator.swift
//  Marshall
//
//  Created by Raul Andrisan on 31/05/2017.
//  Copyright © 2017 Zound Industries. All rights reserved.
//

import UIKit
import Foundation
import StoreKit
import RxSwift
import Toast_Swift

protocol AppStoreCoordinatorDelegate: class {
    func appStoreCoordinatorDidFinishCoordinating(_ coordinator: AppStoreCoordinator)
}

class AppStoreCoordinator: NSObject,Coordinator {
    weak var delegate: AppStoreCoordinatorDelegate?
    
    let parentViewController: UIViewController
    var openiTunesPageDisposable: Disposable?
    var storeViewController: SKStoreProductViewController!
    let disposeBag = DisposeBag()
    let identifier: String
    
    init(parentViewController: UIViewController, itunesItemIdentifier: String) {
        self.parentViewController = parentViewController
        self.identifier = itunesItemIdentifier
    }
    
    func start() {
        openStoreProductWithiTunesItemIdentifier(with: identifier, in: parentViewController)
    }
   
    func openStoreProductWithiTunesItemIdentifier(with indentifier: String, in viewController: UIViewController) {
        let rootViewController = viewController
        storeViewController = SKStoreProductViewController()
        storeViewController.delegate = self
        
        let parameters = [ SKStoreProductParameterITunesItemIdentifier : identifier]
        startOpenProductPageTimeout()
        
        ToastManager.shared.isTapToDismissEnabled = false
        rootViewController.view.makeToastActivity(.center)
        
        storeViewController.loadProduct(withParameters: parameters, completionBlock: { [weak self, weak rootViewController] (loaded, error) in
            guard let storeViewController = self?.storeViewController else { return }
            self?.stopOpenProductPageTimeout()
            rootViewController?.view.hideToastActivity()
            
            if loaded {
                // Parent class of self is UIViewContorller
                rootViewController?.present(storeViewController, animated: true, completion: nil)
            } else {
                if let error = error {
                    print("not loaded because \(error)")
                }
            }
        })
    }
    
    func showFailedOpenProductPage() {
        storeViewController = nil
        let alert = UIAlertController(title: "Open AppStore Page Error",
                                      message: "The app you are trying to open is probably not available in your country",
                                      preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK",
                                      style: .default,
                                      handler: { [weak alert] action in
            alert?.dismiss(animated: true, completion: nil)
        }))
        
        parentViewController.view.hideToastActivity()
        parentViewController.present(alert, animated: true, completion: nil)
        delegate?.appStoreCoordinatorDidFinishCoordinating(self)
    }
    
    func startOpenProductPageTimeout() {
        stopOpenProductPageTimeout()
        openiTunesPageDisposable = Observable<Int>
            .timer(5.0, scheduler: MainScheduler.instance)
            .take(1)
            .subscribe(onNext: { [weak self] _ in self?.showFailedOpenProductPage() })
        
        openiTunesPageDisposable?.disposed(by: disposeBag)
    }
    
    func stopOpenProductPageTimeout() {
        openiTunesPageDisposable?.dispose()
        openiTunesPageDisposable = nil
    }
}

//# MARK:- SKStoreProductViewControllerDelegate
extension AppStoreCoordinator: SKStoreProductViewControllerDelegate {
    func productViewControllerDidFinish(_ viewController: SKStoreProductViewController) {
        viewController.presentingViewController?.dismiss(animated: true, completion: nil)
        storeViewController = nil
        delegate?.appStoreCoordinatorDidFinishCoordinating(self)
    }
}
