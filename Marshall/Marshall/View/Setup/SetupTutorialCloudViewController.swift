//
//  SetupTutorialCloudViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 18/06/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation

import UIKit

class SetupTutorialCloudViewController: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var spotifyConnectLabel: UILabel!
    @IBOutlet weak var airplayLabel: UILabel!
    @IBOutlet weak var googleCastLabel: UILabel!
    @IBOutlet weak var cloudServicesContainer: UIView!
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.clear
        
        titleLabel.numberOfLines = 0
        titleLabel.text = Localizations.Setup.Tutorial.Cloud.Title
        titleLabel.font = Fonts.UrbanEars.Bold(45)
        
        
        contentLabel.numberOfLines = 0
        contentLabel.text = Localizations.Setup.Tutorial.Cloud.Content
        contentLabel.font = Fonts.MainContentFont
        
        spotifyConnectLabel.text = Localizations.Setup.Tutorial.Cloud.SpotifyConnect
        spotifyConnectLabel.font = Fonts.UrbanEars.Regular(12)
        
        airplayLabel.text = Localizations.Setup.Tutorial.Cloud.Airplay
        airplayLabel.font = Fonts.UrbanEars.Regular(12)
        
        googleCastLabel.text = Localizations.Setup.Tutorial.Cloud.GoogleCast
        googleCastLabel.font = Fonts.UrbanEars.Regular(12)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
}
