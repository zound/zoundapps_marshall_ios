//
//  SetupListViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 15/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit
import RxSwift
import MinuetSDK

protocol SetupListViewControllerDelegate:class {
    func setupListViewControllerDidPickSetupSpeaker(_ speaker:SetupSpeaker, setupListViewController: SetupListViewController)
    func setupListViewControllerRequestCancel(_ setupListViewController:SetupListViewController)
}

enum SetupListState {
    case loading
    case list
}

class SetupListViewController: UIViewController,UITableViewDelegate {
    @IBOutlet weak var setupListContainerView: UIView!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var speakerCountLabel: UILabel!
    @IBOutlet weak var separatorViewHeight: NSLayoutConstraint!
    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    var viewModel : SetupNetworkViewModel!
    
    weak var delegate: SetupListViewControllerDelegate?
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        skipButton.setAttributedTitle(Fonts.SecondaryButtonFont.AttributedSecondaryButtonWithString(Localizations.Appwide.Skip), for: .normal)
        separatorViewHeight.constant = 0.5
        
        speakerCountLabel.font = Fonts.UrbanEars.Regular(17)
        
        tableView.rx.setDelegate(self).disposed(by: rx_disposeBag)
        tableView.separatorStyle = .none
        
        setupListContainerView.isHidden = false
        
        updateSeparatorViewVisibility()
        
        if let vm = viewModel {
            //populate unconfigured speakers in the list
            vm.visibleUnconfiguredSpeakers.asObservable()
                .bind(to: tableView.rx.items(cellIdentifier:"setupSpeakerItem", cellType: SetupSpeakerCell.self)) { (row, element, cell) in
                cell.setupSpeaker = element }
                .disposed(by: rx_disposeBag)
            
            //bind number of speakers text to "Found x speakers label"
            vm.visibleUnconfiguredSpeakers.asObservable()
                .map { [weak self] e in return self?.displayTextForSpeakerCount(e.count) ?? "" }
                .bind(to: speakerCountLabel.rx.text)
                .disposed(by: rx_disposeBag)
        }
    }
    
    @IBAction func onClose(_ sender: AnyObject) {
        
        self.delegate?.setupListViewControllerRequestCancel(self)
    }
    
    func displayTextForSpeakerCount(_ count:Int)->String {
        return Localizations.Setup.List.FoundSpeakersWithCount(count)
    }
    
    func updateSeparatorViewVisibility() {
        separatorView.alpha = max(0,min(1.0,tableView.contentOffset.y/22.0))
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let speaker = viewModel.visibleUnconfiguredSpeakers.value[indexPath.row]
        delegate?.setupListViewControllerDidPickSetupSpeaker(speaker, setupListViewController: self)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    private func tableView(_ tableView: UITableView, editingStyleForRowAt editingStyleForRowAtIndexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return UITableViewCell.EditingStyle.none
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        updateSeparatorViewVisibility()
    }
}
