//
//  SetupNoSpeakersViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 17/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit
import Cartography

protocol SetupTutorialViewControllerDelegate: class {
    func setupTutorialControllerDidRequestNext(_ setupTutorialViewController:SetupTutorialViewController)
    func setupTutorialControllerDidRequestCancel(_ setupTutorialViewController:SetupTutorialViewController)
}

class SetupTutorialViewController: UIViewController, UIScrollViewDelegate {
    @IBOutlet weak var pageIndicator: UIPageControl!
    @IBOutlet weak var pageViewContainer: UIView!
    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    
    weak var delegate: SetupTutorialViewControllerDelegate?
    
    var viewModel: SetupTutorialViewModel?
    var done = false
    var pageViewController: UIPageViewController!
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        skipButton.setAttributedTitle(Fonts.SecondaryButtonFont.AttributedSecondaryButtonWithString(Localizations.Appwide.Skip), for: .normal)
        nextButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.Appwide.Next), for: .normal)
        
        pageViewController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        pageViewController.delegate = self
        pageViewController.dataSource = self
        
        self.addChild(pageViewController)
        self.pageViewContainer.addSubview(pageViewController.view)
        pageViewController.view.translatesAutoresizingMaskIntoConstraints = false
        
        constrain(pageViewController.view) { view in
            view.edges == view.superview!.edges
        }
        
        pageViewController.didMove(toParent: self)
        pageViewController.setViewControllers([UIStoryboard.setup.setupTutorialCastViewController],
                                              direction: .forward,
                                              animated: false,
                                              completion: nil)
        updateButtonsForCurrentPage(animated: false)
    }
    
    @IBAction func onPageChanged(_ sender: AnyObject) {
    }
    
    @IBAction func onSkipButton(_ sender: AnyObject) {
        self.delegate?.setupTutorialControllerDidRequestNext(self)
    }
    
    @IBAction func onNextButton(_ sender: AnyObject) {
        self.delegate?.setupTutorialControllerDidRequestNext(self)
    }
    
    @IBAction func onClose(_ sender: AnyObject) {
        self.delegate?.setupTutorialControllerDidRequestCancel(self)
    }
    
    func indexForViewController(_ viewController: UIViewController) -> Int? {
        if viewController is SetupTutorialCastViewController {
            return 0
        } else if viewController is SetupTutorialRadioViewController {
            return 1
        } else if viewController is SetupTutorialMultiViewController {
            return 2
        } else if viewController is  SetupTutorialCloudViewController{
            return 3
        }
        return nil
    }
}

extension SetupTutorialViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        if viewController is SetupTutorialCastViewController {
            return UIStoryboard.setup.setupTutorialRadioViewController
        } else if viewController is SetupTutorialRadioViewController {
            return UIStoryboard.setup.setupTutorialMultiViewController
        } else if viewController is SetupTutorialMultiViewController {
            return UIStoryboard.setup.setupTutorialCloudViewController
        }
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        if viewController is SetupTutorialCloudViewController {
            return UIStoryboard.setup.setupTutorialMultiViewController
        } else if viewController is SetupTutorialMultiViewController {
            return UIStoryboard.setup.setupTutorialRadioViewController
        } else if viewController is SetupTutorialRadioViewController {
            return UIStoryboard.setup.setupTutorialCastViewController
        }
        
        return nil
    }
    
    func updateButtonsForCurrentPage(animated: Bool) {
        let nextVisible = pageIndicator.currentPage == (pageIndicator.numberOfPages - 1)
        UIView.setAlphaOfView(skipButton, visible: !nextVisible, animated: animated)
        UIView.setAlphaOfView(nextButton, visible: nextVisible, animated: animated)
    }
}


extension SetupTutorialViewController: UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {}
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        guard let currentViewController = pageViewController.viewControllers?.last,
            let indexForCurrent = indexForViewController(currentViewController) else { return }
        
        pageIndicator.currentPage = indexForCurrent
        updateButtonsForCurrentPage(animated: true)
    }
}

