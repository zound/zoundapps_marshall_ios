//
//  SetupFailedResetViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 16/06/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit

protocol SetupFailedResetViewControllerDelegate: class {
    func setupFailedResetDidRequestNext(_ viewController: SetupFailedResetViewController)
    func setupFailedResetDidRequestCancel(_ viewController: SetupFailedResetViewController)
}

class SetupFailedResetViewController: UIViewController {
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var nextButton: UIButton!
    
    weak var delegate: SetupFailedResetViewControllerDelegate?
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        messageLabel.text = Localizations.Setup.FailedReset.Message
        messageLabel.font = Fonts.MainContentFont
        
        UIView.setAnimationsEnabled(false)
        
        nextButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.Appwide.Done), for: .normal)
        nextButton.layoutIfNeeded()
        UIView.setAnimationsEnabled(true)
        
        self.view.backgroundColor = UIColor.clear
    }
 
    @IBAction func onNext(_ sender: AnyObject) {
        self.delegate?.setupFailedResetDidRequestCancel(self)
    }
}
