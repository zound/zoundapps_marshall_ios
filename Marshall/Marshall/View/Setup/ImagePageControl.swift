//
//  ImagePageControl.swift
//  Marshall
//
//  Created by Raul Andrisan on 05/07/2017.
//  Copyright © 2017 Zound Industries. All rights reserved.
//

import Foundation
import UIKit

class ImagePageControl: UIPageControl {
    var activeimage: UIImage? = UIImage(named: "page_control_bullet_active")
    var inactiveImage: UIImage? = UIImage(named: "page_control_bullet_inactive")
    
    override var currentPage: Int {
        didSet { updateDots() }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func updateDots() {
        var i = 0
        
        for view in self.subviews {
            view.backgroundColor = UIColor.clear
            
            if let imageView = self.imageForSubview(view) {
                if i == self.currentPage {
                    imageView.image = activeimage
                } else {
                    imageView.image = inactiveImage
                }
                i = i + 1
            } else {
                var dotImage = self.inactiveImage
                if i == self.currentPage {
                    dotImage = activeimage
                }
                view.clipsToBounds = false
                view.addSubview(UIImageView(image:dotImage))
                
                i = i + 1
            }
        }
    }
    
    fileprivate func imageForSubview(_ view:UIView) -> UIImageView? {
        var dot: UIImageView?
        
        if let dotImageView = view as? UIImageView {
            dot = dotImageView
        } else {
            for foundView in view.subviews {
                if let imageView = foundView as? UIImageView {
                    dot = imageView
                    break
                }
            }
        }
        
        return dot
    }
}
