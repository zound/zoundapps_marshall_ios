//
//  SetupCoordinator.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 11/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit
import RxSwift
import STKWebKitViewController
import SafariServices
import MessageUI
import Crashlytics
import Fabric
import MinuetSDK

protocol SetupCoordinatorDelegate: class {
    func setupCoordinatorDidFinishSetup(_ coordinator: SetupCoordinator)
    func setupCoordinatorDidCancelSetup(_ coordinator: SetupCoordinator)
}

class SetupCoordinator: NSObject, Coordinator {
    weak var delegate: SetupCoordinatorDelegate?
    
    var displayViewController: UIViewController
    var setupViewController: SetupViewController
    var appStoreCoordinator: AppStoreCoordinator?
    var wacService: WACServiceType
    var spotifyProvider: SpotifyProvider!
    var setupNetworkViewModel: SetupNetworkViewModel?
    
    var disposeBag = DisposeBag()
    
    let provider:SpeakerProvider
    let audioSystem: AudioSystem
    
    init(displayViewController: UIViewController, moyaProvider:SpeakerProvider, audioSystem: AudioSystem, wacService: WACServiceType) {
        self.displayViewController = displayViewController
        self.provider = moyaProvider
        self.wacService = wacService
        self.audioSystem = audioSystem
        setupViewController = UIStoryboard.setup.setupViewController
        
        super.init()
        
        let willEnterForeground = NotificationCenter.default.rx.notification(UIApplication.willEnterForegroundNotification)
        willEnterForeground
            .subscribe(onNext: { [weak self] _ in
                self?.audioSystem.refresh(clear: false) })
            .disposed(by: rx_disposeBag)
        
        setupViewController.delegate = self
    }
    
    func start(_ speaker: Speaker?) {
        UIApplication.shared.isIdleTimerDisabled = true
        //SpotifyTokenStore.clearToken()
        //SpotifyArtworkCache.sharedInstace.clearData()
        if speaker != nil {
            let tutorialVM = SetupTutorialViewModel(speaker: speaker!)
            showTutorial(tutorialVM)
            //showSetupUpdate(speaker!)
            //showRenameForSpeaker(speaker!)
            //let setupPresetsVieModel = SetupPresetsViewModel(speaker: speaker!, speakerProvider: provider)
            //setupPresetsVieModel.delegate = self
            //showPresetsPicker(setupPresetsVieModel)
        } else {
            self.setupNetworkViewModel = SetupNetworkViewModel(wacService: wacService, audioSystem: audioSystem, speakerProvider: provider)
            self.setupNetworkViewModel?.delegate = self
            self.setupNetworkViewModel?.searchForSpeakers()
            /*self.setupNetworkViewModel = SetupNetworkViewModel(speakerProvider: provider, wacService: wacService, discoveryService: discoveryService)
             self.setupNetworkViewModel?.delegate = self
             let unconfiguredSpeaker = UnconfiguredSpeaker(model: "Baggen Plant Green", ssid: "Baggen_Plant_Green_1f:32", wacMac: "0:22:61:1f:1f:32")
             setupNetworkViewModel?.setupNetworkState = .configured(speaker: unconfiguredSpeaker, configureError: SetupNetworkError.other)
             showLostConnection(setupNetworkViewModel!)*/
        }
        
        self.setupViewController.modalPresentationStyle = .formSheet
        displayViewController.present(setupViewController, animated: true, completion: {})
    }
    
    func start() {
        start(nil)
    }
    
    func showSpeakerList(_ viewModel: SetupNetworkViewModel) {
        let setupList = UIStoryboard.setup.setupListViewController
        setupList.viewModel = viewModel
        setupList.delegate = self
        
        setupViewController.resetWithViewController(setupList, animated: false)
    }
    
    func showTutorial(_ viewModel: SetupTutorialViewModel) {
        let setupTutorial = UIStoryboard.setup.setupTutorialViewController
        setupTutorial.viewModel = viewModel
        setupTutorial.delegate = self
        
        setupViewController.resetWithViewController(setupTutorial, animated: true)
    }
    
    func showPresetsIntro(_ viewModel: SetupTutorialViewModel) {
        let presetsIntro = UIStoryboard.setup.setupPresetsIntroViewController
        presetsIntro.viewModel = viewModel
        presetsIntro.delegate = self
        
        setupViewController.resetWithViewController(presetsIntro, animated: true)
    }
    
    func showPresetsPicker(_ viewModel: SetupPresetsViewModel) {
        let presetsPicker = UIStoryboard.setup.setupPresetsPickerViewController
        presetsPicker.viewModel = viewModel
        presetsPicker.delegate = self
        
        setupViewController.resetWithViewController(presetsPicker, animated: true)
    }
    
    func showPresetsLoading(_ viewModel: SetupPresetsViewModel) {
        let presetsLoading = UIStoryboard.setup.setupPresetsLoadingViewController
        presetsLoading.viewModel = viewModel
        presetsLoading.delegate = self
        
        setupViewController.resetWithViewController(presetsLoading, animated: true)
    }
    
    func showPresetsFinal(_ viewModel: SetupPresetsViewModel) {
        let presetsFinal = UIStoryboard.setup.setupPresetsFinalViewController
        presetsFinal.viewModel = viewModel
        presetsFinal.delegate = self
        
        setupViewController.resetWithViewController(presetsFinal, animated: true)
    }
    
    func showPresetsTutorial(_ viewModel: SetupPresetsViewModel) {
        let presetsTutorial = UIStoryboard.setup.setupPresetsTutorialViewController
        presetsTutorial.viewModel = viewModel
        presetsTutorial.delegate = self
        
        setupViewController.resetWithViewController(presetsTutorial, animated: true)
    }
    
    func showPresetsSpotify(_ viewModel: SetupPresetsViewModel) {
        let setupSpotify = UIStoryboard.setup.setupPresetsSpotifyViewController
        setupSpotify.viewModel = viewModel
        setupSpotify.delegate = self
        
        setupViewController.pushViewController(setupSpotify, animated: true)
    }
    
    func showPresetsSpotifySuccess(_ viewModel: SetupPresetsViewModel) {
        let setupSpotifySuccess = UIStoryboard.setup.setupPresetsSpotifySuccessViewController
        setupSpotifySuccess.viewModel = viewModel
        setupSpotifySuccess.delegate = self
        
        setupViewController.pushViewController(setupSpotifySuccess, animated: true)
    }
    
    func showSetupDone() {
        let setupDone = UIStoryboard.setup.setupDoneViewController
        setupDone.delegate = self
        
        setupViewController.resetWithViewController(setupDone, animated: true)
    }
    
    func showSetupFailed(_ viewModel: SetupNetworkViewModel) {
        let setupFailed = UIStoryboard.setup.setupFailedViewController
        setupFailed.viewModel = viewModel
        setupFailed.delegate = self
        
        setupViewController.resetWithViewController(setupFailed, animated: false)
    }
    
    func showSetupLoading(_ viewModel: SetupNetworkViewModel) {
        let setupLoading = UIStoryboard.setup.setupLoadingViewController
        setupLoading.viewModel = viewModel
        setupLoading.delegate = self
        
        setupViewController.resetWithViewController(setupLoading, animated: false)
    }
    
    func closeSetup() {
        delegate?.setupCoordinatorDidCancelSetup(self)
        UIApplication.shared.isIdleTimerDisabled = false
        wacService.active = false
    }
}

extension SetupCoordinator: SetupViewControllerDelegate {
    func setupViewContontrollerDidCancel(_ setupViewController: SetupViewController) {
        delegate?.setupCoordinatorDidCancelSetup(self)
        UIApplication.shared.isIdleTimerDisabled = false
        wacService.active = false
    }
}

extension SetupCoordinator: SetupTutorialViewControllerDelegate {
    func setupTutorialControllerDidRequestNext(_ setupTutorialViewController: SetupTutorialViewController) {
        guard let tutorialViewModel = setupTutorialViewController.viewModel else { return }
        showPresetsIntro(tutorialViewModel)
    }
    
    func setupTutorialControllerDidRequestCancel(_ setupTutorialViewController: SetupTutorialViewController) {
        delegate?.setupCoordinatorDidCancelSetup(self)
        UIApplication.shared.isIdleTimerDisabled = false
        wacService.active = false
    }
}

extension SetupCoordinator: SetupPresetsIntroViewControllerDelegate {
    func setupPresetsIntroDidReqestNext(_ setupIntroViewController: SetupPresetsIntroViewController) {
        guard let tutorialViewModel = setupIntroViewController.viewModel else { return }
        
        let setupPresetsViewModel = SetupPresetsViewModel(speaker: tutorialViewModel.speaker, speakerProvider: provider)
        setupPresetsViewModel.delegate = self
        showPresetsPicker(setupPresetsViewModel)
    }
    
    func setupPresetsIntroDidReqestSkip(_ viewController: SetupPresetsIntroViewController) {
        if let speaker = viewController.viewModel?.speaker {
            provider.setDidSetupInUserDataStoreForSpeaker(speaker, withValue: true).subscribe(weak: self).disposed(by: disposeBag)
        }
        
        showSetupDone()
    }
}

extension SetupCoordinator: SetupFailedViewControllerDelegate {
    func setupFailedDidRequestCancelSetup(_ viewcontroller: SetupFailedViewController) {
        delegate?.setupCoordinatorDidCancelSetup(self)
        UIApplication.shared.isIdleTimerDisabled = false
        wacService.active = false
    }
    
    func setupFailedDidFinishTroubleshooting(_ viewcontroller: SetupFailedViewController) {
        guard let setupNetworkViewModel = viewcontroller.viewModel else { return }
        
        setupNetworkViewModel.searchForSpeakers()
    }
}

extension SetupCoordinator: SetupLoadingViewControllerDelegate {
    func setupLoadingDidRequestCancelSetup(_ setupLoading: SetupLoadingViewController) {
        delegate?.setupCoordinatorDidCancelSetup(self)
        UIApplication.shared.isIdleTimerDisabled = false
        wacService.active = false
    }
}

extension SetupCoordinator: AppStoreCoordinatorDelegate {
    func appStoreCoordinatorDidFinishCoordinating(_ coordinator: AppStoreCoordinator) {
        appStoreCoordinator = nil
    }
}

extension SetupCoordinator: SetupListViewControllerDelegate {
    func setupListViewControllerDidPickSetupSpeaker(_ speaker: SetupSpeaker, setupListViewController: SetupListViewController) {
        
        if !speaker.isConnected {
            let appURL = "https://itunes.apple.com/us/app/google-home/id680819774?mt=8"
            let appId = "680819774"
            if let url  = URL(string: appURL) {
                if UIApplication.shared.canOpenURL(url) == true  {
                    UIApplication.shared.openURL(url)
                } else {
                    if self.appStoreCoordinator == nil {
                        self.appStoreCoordinator = AppStoreCoordinator(parentViewController: self.setupViewController, itunesItemIdentifier: appId)
                        self.appStoreCoordinator?.delegate = self
                        self.appStoreCoordinator?.start()
                    }
                }
            }
        } else {
            if let speaker = speaker.configuredSpeaker {
                let tutorialVM = SetupTutorialViewModel(speaker: speaker)
                showTutorial(tutorialVM)
            }
        }
    }
    
    func setupListViewControllerRequestCancel(_ setupListViewController: SetupListViewController) {
        delegate?.setupCoordinatorDidCancelSetup(self)
        UIApplication.shared.isIdleTimerDisabled = false
        wacService.active = false
    }
}

extension SetupCoordinator: SetupNetworkViewModelDelegate {
    func setupNetworkViewModelDidChangeConfigurationState(_ setupViewModel: SetupNetworkViewModel, state: SetupConfigurationState) {
        switch state {
        case .idle: break
        case .loading: showSetupLoading(setupViewModel)
        case .noSpeakersFound: showSetupFailed(setupViewModel)
        case .foundSpeakers: showSpeakerList(setupViewModel)
        }
    }
}

extension SetupCoordinator: SetupPresetsViewModelDelegate {
    func setupViewModelDidLoginSpotify(_ setupViewModel: SetupPresetsViewModel) {
        showPresetsSpotifySuccess(setupViewModel)
    }
    
    func showSpotifyLoginErrorAlert(_ setupViewModel: SetupPresetsViewModel) {
        let alertController = UIAlertController(title: Localizations.Player.Preset.SpotifyLoginError.Title,
                                                message: Localizations.Player.Preset.SpotifyLoginError.Content, preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: Localizations.Appwide.Cancel, style: .cancel, handler: { [weak self] action in
            guard let `self` = self else { return }
            self.setupViewController.popViewController(true)
        }))
        
        alertController.addAction(UIAlertAction(title: Localizations.Player.Preset.SpotifyLoginError.RetryButton, style: .default, handler: { action in
            setupViewModel.loginSpotify(forceWebLogin: true)
        }))
        
        setupViewController.present(alertController, animated: true, completion: nil)
    }
    
    func setupViewModelDidNotLoginSpotify(_ setupViewModel: SetupPresetsViewModel, error: Swift.Error) {
        if error is SpotifyLoginError {
            if let loginError = error as? SpotifyLoginError {
                switch loginError {
                case .cancelled: break
                case .authorizationFailed(let spotifyError):
                    if ["access_denied","user_canceled"].contains(spotifyError)  {
                        //just do nothing since the whole process stops
                    } else {
                        showSpotifyLoginErrorAlert(setupViewModel)
                    }
                default:
                    showSpotifyLoginErrorAlert(setupViewModel)
                }
            }
        } else if error is SpotifyProductError {
            let alertController = UIAlertController(title: Localizations.Player.Preset.SpotifyPremiumError.Title,
                                                    message: Localizations.Player.Preset.SpotifyPremiumError.Content, preferredStyle: .alert)
            
            alertController.addAction(UIAlertAction(title: Localizations.Player.Preset.SpotifyPremiumError.DismissButton, style: .cancel, handler: { [weak self] action in
                
                guard let `self` = self else {return}
                let setupPresetsViewModel = SetupPresetsViewModel(speaker: setupViewModel.speaker, speakerProvider: self.provider)
                setupPresetsViewModel.delegate = self
                self.showPresetsPicker(setupPresetsViewModel)
            }))
            
            setupViewController.present(alertController, animated: true, completion: nil)
        }
    }
}

extension SetupCoordinator: SetupPresetsTutorialViewControllerDelegate {
    func setupPresetsTutorialDidFinish(_ viewController: SetupPresetsTutorialViewController) {
        if let speaker = viewController.viewModel?.speaker {
            provider.setDidSetupInUserDataStoreForSpeaker(speaker, withValue: true).subscribe(weak: self).disposed(by: disposeBag)
        }
        
        showSetupDone()
    }
    
    func setupPresetsTutorialDidRequestBack(_ viewController: SetupPresetsTutorialViewController) {
        setupViewController.popViewController(true)
    }
}


extension SetupCoordinator: SetupPresetsPickerViewControllerDelegate {
    func setupPresetsPickerDidRequestRadio(_ viewController: SetupPresetsPickerViewController) {
        guard let setupPresetsViewModel = viewController.viewModel else { return }
        showPresetsLoading(setupPresetsViewModel)
    }
    
    func setupPresetsPickerDidRequestSpotify(_ viewController: SetupPresetsPickerViewController) {
        guard let setupPresetsViewModel = viewController.viewModel else { return }
        showPresetsSpotify(setupPresetsViewModel)
    }
    
    func setupPresetsPickerDidRequestSpotifyAndRadio(_ viewController: SetupPresetsPickerViewController) {
        guard let setupPresetsViewModel = viewController.viewModel else { return }
        showPresetsSpotify(setupPresetsViewModel)
    }
    
    func setupPresetsPickerDidRequestSkip(_ viewController: SetupPresetsPickerViewController) {
        if let speaker = viewController.viewModel?.speaker {
            provider.setDidSetupInUserDataStoreForSpeaker(speaker, withValue: true)
                .subscribe(weak: self)
                .disposed(by: disposeBag)
        }
        
        showSetupDone()
    }
    
    func setupPresetsPickerDidRequestBack(_ viewController: SetupPresetsPickerViewController) {
        setupViewController.popViewController(true)
    }
}

extension SetupCoordinator: SetupPresetsSpotifyViewControllerDelegate {
    func setupPresetsSpotifyDidRequestLogin(_ viewController: SetupPresetsSpotifyViewController) {
        guard let setupPresetsViewModel = viewController.viewModel else { return }
        setupPresetsViewModel.loginSpotify(clearExistingLogin: true)
    }
    
    func setupPresetsSpotifyDidRequestBack(_ viewController: SetupPresetsSpotifyViewController) {
        setupViewController.popViewController(true)
    }
}

extension SetupCoordinator: SetupPresetsSpotifySuccessViewControllerDelegate {
    func setupPresetsSpotifyDidRequestNext(_ viewController: SetupPresetsSpotifySuccessViewController) {
        guard let setupPresetsViewModel = viewController.viewModel else { return }
        showPresetsLoading(setupPresetsViewModel)
    }
}

extension SetupCoordinator: SetupPresetsLoadingViewControllerDelegate {
    func setupPresetsLoadingDidFinishLoading(_ viewController: SetupPresetsLoadingViewController) {
        guard let setupPresetsViewModel = viewController.viewModel else { return }
        showPresetsFinal(setupPresetsViewModel)
    }
    
    func setupPresetsLoadingDidRequestBackOnError(_ viewController: SetupPresetsLoadingViewController) {
        guard let vm = viewController.viewModel else { return }
        showPresetsPicker(vm)
    }
}

extension SetupCoordinator: SetupPresetsFinalViewControllerDelegate {
    func setupPresetsFinalDidFinish(_ viewController: SetupPresetsFinalViewController) {
        guard let setupPresetsViewModel = viewController.viewModel else { return }
        showPresetsTutorial(setupPresetsViewModel)
    }
}

extension SetupCoordinator: SetupDoneViewControllerDelegate {
    func setupDidFinish(_ viewController: SetupDoneViewController) {
        self.delegate?.setupCoordinatorDidFinishSetup(self)
        UIApplication.shared.isIdleTimerDisabled = false
        wacService.active = false
    }
}
