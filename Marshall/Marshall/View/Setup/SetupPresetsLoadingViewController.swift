//
//  SetupPresetsLoadingViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 16/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit
import RxSwift
import AlamofireImage
import Cartography

protocol SetupPresetsLoadingViewControllerDelegate: class {
    func setupPresetsLoadingDidFinishLoading(_ viewcontroller:SetupPresetsLoadingViewController)
    func setupPresetsLoadingDidRequestBackOnError(_ viewcontroller:SetupPresetsLoadingViewController)
}

class SetupPresetsLoadingViewController: UIViewController {
    @IBOutlet weak var pageContainerView: UIView!
    
    weak var delegate: SetupPresetsLoadingViewControllerDelegate?
    
    var viewModel: SetupPresetsViewModel?
    var disposeBag = DisposeBag()
    var loadingAnimationDone = false
    var loadingPresetsDone = false
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let vm = viewModel {
            vm.presetUploadStateVariable.asObservable()
                .distinctUntilChanged()
                .subscribe(weak: self, onNext: SetupPresetsLoadingViewController.updateForUploadState)
                .disposed(by: rx_disposeBag)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        guard let vm = viewModel else { return }
        vm.loadPresets()
    }
    
    fileprivate func updateForUploadState(_ state: PresetUploadState) {
        let preparing = (state == .downloadingPresets)
        let error = (state == .error)
        let uploading = (state == .uploadingPresets)
        let done = (state == .done)
        
        if preparing {
            showPreparing()
        }
        if error {
            showError()
        }
        if uploading {
            showProgress()
        }
        
        if done {
            loadingPresetsDone = true
            reportLoadingFinished()
        }
    }
    
    func reportLoadingFinished() {
        if loadingAnimationDone && loadingPresetsDone {
            self.delegate?.setupPresetsLoadingDidFinishLoading(self)
        }
    }
    
    func showPreparing() {
        let preparingViewController = UIStoryboard.setup.setupPresetsLoadingPreparingViewController
        preparingViewController.viewModel = viewModel
        transitionToViewController(preparingViewController)
    }
    
    func showError() {
        let errorViewController = UIStoryboard.setup.setupPresetsLoadingErrorViewController
        errorViewController.delegate = self
        errorViewController.viewModel = viewModel
        transitionToViewController(errorViewController)
    }
    
    func showProgress() {
        let progressViewController = UIStoryboard.setup.setupPresetsLoadingProgressViewController
        progressViewController.delegate = self
        progressViewController.viewModel = viewModel
        progressViewController.runPresetsSettingAnimation()
        transitionToViewController(progressViewController)
    }
    
    func transitionToViewController(_ viewController: UIViewController) {
        if self.children.count == 0 {
            self.addChild(viewController)
            pageContainerView.addSubview(viewController.view)
            constrain(viewController.view) { view in
                if let superview = view.superview {
                    view.edges == superview.edges
                }
            }
            viewController.didMove(toParent: self)
        } else {
            if let currentViewController = self.children.first {
                self.addChild(viewController)
                currentViewController.willMove(toParent: nil)
                self.transition(from: currentViewController,
                                to: viewController,
                                duration: 0.15,
                                options: [.beginFromCurrentState, .transitionCrossDissolve],
                                animations: {
                                    viewController.view.translatesAutoresizingMaskIntoConstraints = false;
                                    constrain(viewController.view) { view in
                                        if let superview = view.superview {
                                            view.edges == superview.edges
                                        }
                                    }
                },
                                completion: { finished in
                                    currentViewController.removeFromParent()
                                    viewController.didMove(toParent: self)
                                    
                })
            }
        }
    }
}

extension SetupPresetsLoadingViewController: SetupPresetsLoadingProgressDelegate {
    func setupPresetsLoadingProgressDidFinishLoadingAnimation(_ viewcontroller: SetupPresetsLoadingProgress) {
        loadingAnimationDone = true
        reportLoadingFinished()
    }
}

extension SetupPresetsLoadingViewController: SetupPresetsLoadingErrorDelegate {
    func setupPresetsLoadingErrorDidRequestBackOnError(_ viewcontroller: SetupPresetsLoadingError) {
        self.delegate?.setupPresetsLoadingDidRequestBackOnError(self)
    }
    
    func setupPresetsLoadingErrorDidRequestRetry(_ viewcontroller: SetupPresetsLoadingError) {
        guard let vm = self.viewModel else { return }
        self.disposeBag = DisposeBag()
        vm.loadPresets()
    }
}


