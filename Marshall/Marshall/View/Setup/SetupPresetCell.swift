//
//  SetupPresetCell.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 16/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit

class SetupPresetCell: UITableViewCell {

    @IBOutlet weak var menuItemImageView: UIImageView!
    @IBOutlet weak var menuItemLabel: UILabel!
    @IBOutlet weak var menuItemNumberLabel: UILabel!
    @IBOutlet weak var menuItemDescriptionLabe: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.backgroundColor = UIColor.clear
        self.backgroundColor = UIColor.clear
        
        menuItemLabel.font = Fonts.UrbanEars.Bold(15)
        menuItemNumberLabel.font = Fonts.UrbanEars.Bold(48)
        menuItemDescriptionLabe.font = Fonts.UrbanEars.Regular(14)
        
        let customColorView = UIView()
        customColorView.backgroundColor = UIColor(white: 0.0, alpha: 0.24)
        self.selectedBackgroundView =  customColorView
    }
}
