//
//  SetupTutorialCloudViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 18/06/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation

import UIKit

class SetupTutorialCastViewController: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentTopLabel: UILabel!
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.clear
       
        titleLabel.text = Localizations.Setup.Tutorial.Cast.Title.uppercased()
        titleLabel.font = Fonts.UrbanEars.Bold(45)
        
        contentTopLabel.numberOfLines = 0
        contentTopLabel.text = Localizations.Setup.Tutorial.Cast.ContentTop
        contentTopLabel.font = Fonts.MainContentFont
    }
}
