//
//  MenuTableViewCell.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 09/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit
import RxSwift

class SetupSpeakerCell: UITableViewCell {
    @IBOutlet weak var menuItemImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var modelLabel: UILabel!
    @IBOutlet weak var connectionStatusLabel: UILabel!
    @IBOutlet weak var menuItemIndicatorImageView: UIImageView!
    
    let setupSpeakerVariable: Variable<SetupSpeaker?> = Variable(nil)
    var setupSpeaker: SetupSpeaker? {
        get { return setupSpeakerVariable.value }
        set { setupSpeakerVariable.value = newValue }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.contentView.backgroundColor = UIColor.clear
        self.backgroundColor = UIColor.clear
        
        let customColorView = UIView()
        customColorView.backgroundColor = UIColor(white: 0.0, alpha: 0.24);
        self.selectedBackgroundView =  customColorView;
        
        nameLabel.font = Fonts.UrbanEars.Bold(17)
        modelLabel.font = Fonts.UrbanEars.Regular(14)
        connectionStatusLabel.font = Fonts.UrbanEars.Light(12)
        
        setupSpeakerVariable.asObservable()
            .unwrapOptional()
            .map { "\($0.name.uppercased())" }
            .bind(to: nameLabel.rx.text)
            .disposed(by: rx_disposeBag)
        
        setupSpeakerVariable.asObservable()
            .unwrapOptional()
            .map { $0.model }
            .bind(to: modelLabel.rx.text)
            .disposed(by: rx_disposeBag)
        
        setupSpeakerVariable.asObservable()
            .unwrapOptional()
            .map { $0.isConnectedVariable.asObservable() }
            .switchLatest()
            .map { isConnected in return isConnected ? Localizations.Setup.List.Connected : Localizations.Setup.List.ConnectToWifi }
            .bind(to: connectionStatusLabel.rx.text)
            .disposed(by: rx_disposeBag)
        
        setupSpeakerVariable.asObservable()
            .unwrapOptional().map { UIImage(named:$0.imageName) }
            .bind(to: menuItemImageView.rx.image)
            .disposed(by: rx_disposeBag)
    }
}
