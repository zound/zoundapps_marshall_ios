//
//  SetupFailedViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 31/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit
import Cartography

protocol SetupFailedViewControllerDelegate: class {
    func setupFailedDidFinishTroubleshooting(_ viewcontroller:SetupFailedViewController)
    func setupFailedDidRequestCancelSetup(_ viewcontroller:SetupFailedViewController)
}

class SetupFailedViewController: UIViewController {
    @IBOutlet weak var pageContainerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    
    var viewModel: SetupNetworkViewModel?
    weak var delegate: SetupFailedViewControllerDelegate?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.attributedText = Fonts.PageTitleFont.AttributedPageTitleWithString(Localizations.Setup.Failed.Title)
        showFailedConnect()
    }
    
    @IBAction func onCancel(_ sender: AnyObject) {
        cancelSetup()
    }
    
    func showFailedConnect() {
        let failedConnectViewController = UIStoryboard.setup.setupFailedConnectViewController
        failedConnectViewController.delegate = self
        transitionToViewController(failedConnectViewController)
    }
    
    func showFailedBoot() {
        let failedBootViewController = UIStoryboard.setup.setupFailedBootViewController
        failedBootViewController.delegate = self
        transitionToViewController(failedBootViewController)
    }
    
    func showFailedSetup() {
        let failedSetupViewController = UIStoryboard.setup.setupFailedSetupViewController
        failedSetupViewController.delegate = self
        transitionToViewController(failedSetupViewController)
    }
    
    func showFailedReset() {
        let failedResetViewController = UIStoryboard.setup.setupFailedResetViewController
        failedResetViewController.delegate = self
        transitionToViewController(failedResetViewController)
    }
    
    func transitionToViewController(_ viewController: UIViewController) {
        if self.children.count == 0 {
            self.addChild(viewController)
            pageContainerView.addSubview(viewController.view)
            constrain(viewController.view) { view in
                if let superview = view.superview {
                    view.edges == superview.edges
                }
            }
            viewController.didMove(toParent: self)
        } else {
            if let currentViewController = self.children.first {
                self.addChild(viewController)
                currentViewController.willMove(toParent: nil)
                
                self.transition(from: currentViewController,
                                to: viewController,
                                duration: 0.15,
                                options: [.beginFromCurrentState, .transitionCrossDissolve],
                                animations: {
                                    viewController.view.translatesAutoresizingMaskIntoConstraints = false;
                                    constrain(viewController.view) { view in
                                        if let superview = view.superview {
                                            view.edges == superview.edges
                                        }
                                    }
                },
                                completion: { finished in
                                    currentViewController.removeFromParent()
                                    viewController.didMove(toParent: self)
                                    
                })
            }
        }
    }
    
    func cancelSetup() {
        self.delegate?.setupFailedDidRequestCancelSetup(self)
    }
    func didFinishSetup() {
        self.delegate?.setupFailedDidFinishTroubleshooting(self)
    }
    
    
}



extension SetupFailedViewController: SetupFailedConnectViewControllerDelegate {
    func setupFailedConnectDidRequestNext(_ viewController: SetupFailedConnectViewController) {
        showFailedBoot()
    }
    
    func setupFailedConnectDidRequestCancel(_ viewController: SetupFailedConnectViewController) {
        cancelSetup()
    }
}

extension SetupFailedViewController: SetupFailedBootViewControllerDelegate {
    func setupFailedBootDidRequestNext(_ viewController: SetupFailedBootViewController) {
        showFailedSetup()
    }
    
    func setupFailedBootDidRequestCancel(_ viewController: SetupFailedBootViewController) {
        cancelSetup()
    }
}

extension SetupFailedViewController: SetupFailedSetupViewControllerDelegate {
    func setupFailedSetupDidConfirmSetupMode(_ viewController: SetupFailedSetupViewController) {
        didFinishSetup()
    }
    
    func setupFailedSetupDidDenySetupMode(_ viewController: SetupFailedSetupViewController) {
        showFailedReset()
    }
}

extension SetupFailedViewController: SetupFailedResetViewControllerDelegate {
    func setupFailedResetDidRequestNext(_ viewController: SetupFailedResetViewController) {
        showFailedSetup()
    }
    
    func setupFailedResetDidRequestCancel(_ viewController: SetupFailedResetViewController) {
        cancelSetup()
    }
}

