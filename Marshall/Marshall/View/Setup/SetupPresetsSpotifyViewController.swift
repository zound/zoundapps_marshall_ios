//
//  SetupPresetsIntroViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 16/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit

protocol SetupPresetsSpotifyViewControllerDelegate: class {
    func setupPresetsSpotifyDidRequestLogin(_ viewController:SetupPresetsSpotifyViewController)
    func setupPresetsSpotifyDidRequestBack(_ viewController:SetupPresetsSpotifyViewController)
}

class SetupPresetsSpotifyViewController: UIViewController {
    @IBOutlet weak var content1Label: UILabel!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var loadingIndicator: UIImageView!
    
    weak var delegate: SetupPresetsSpotifyViewControllerDelegate?
    
    var viewModel: SetupPresetsViewModel?
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        content1Label.text = Localizations.Setup.ConnectSpotify.Paragraph1.Content
        content1Label.font = Fonts.UrbanEars.Regular(17)
        
        UIView.setAnimationsEnabled(false)
        
        loginButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.Setup.ConnectSpotify.Buttons.ConnectToSpotify), for: .normal)
        loginButton.layoutIfNeeded()
        UIView.setAnimationsEnabled(true)
        
        loadingIndicator.rotate()
        
        if let vm = viewModel {
            vm.spotifyLoggingInVariable.asObservable()
                .subscribe(weak: self, onNext: SetupPresetsSpotifyViewController.updateForSpotifyLoggingIn)
                .disposed(by: rx_disposeBag)
        }
    }
    
    func updateForSpotifyLoggingIn(_ loggingIn: Bool) {
        UIView.setAlphaOfView(loadingIndicator, visible: loggingIn, animated: true)
        UIView.setAlphaOfView(loginButton, visible: !loggingIn, animated: true)
    }

    @IBAction func onConnectSpotify(_ sender: AnyObject) {
        self.delegate?.setupPresetsSpotifyDidRequestLogin(self)
    }
    
    @IBAction func onBack(_ sender: AnyObject) {
        self.delegate?.setupPresetsSpotifyDidRequestBack(self)
    }
}
