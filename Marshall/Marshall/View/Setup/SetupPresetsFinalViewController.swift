//
//  SetupPresetsFinalViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 16/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit
import AlamofireImage

protocol SetupPresetsFinalViewControllerDelegate: class {
    func setupPresetsFinalDidFinish(_ viewcontroller:SetupPresetsFinalViewController)
}

class SetupPresetsFinalViewController: UIViewController {
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var topSeparator: UIView!
    @IBOutlet weak var separatorViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    
    weak var delegate: SetupPresetsFinalViewControllerDelegate?
    
    var viewModel: SetupPresetsViewModel?
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleLabel.text = Localizations.Setup.PresetsList.Title.uppercased()
        titleLabel.font = Fonts.UrbanEars.Bold(18)
        
        UIView.setAnimationsEnabled(false)
        self.nextButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.Appwide.Done), for: .normal)
        nextButton.layoutIfNeeded()
        UIView.setAnimationsEnabled(true)
        
        self.separatorViewHeightConstraint.constant = 0.5
        
        tableView.rx.setDelegate(self).disposed(by: rx_disposeBag)
        tableView.separatorStyle = .none
        
        updateTopSeparatorAlpha()
        
        if let vm = viewModel {
            vm.presets.asObservable()
                .bind(to: tableView.rx.items(cellIdentifier:"presetItem", cellType: SetupPresetCell.self)) { (row, element, cell) in
                    cell.menuItemLabel?.text = element.name
                    cell.menuItemDescriptionLabe?.text = element.type?.displayName
                    cell.menuItemNumberLabel.text = String(element.number)
                    
                    if let imageURL  = element.imageURL {
                        cell.menuItemImageView?.af_setImage(withURL:imageURL,
                                                            placeholderImage: UIImage(),
                                                            filter: nil,
                                                            progress: nil,
                                                            progressQueue: DispatchQueue.main,
                                                            imageTransition: UIImageView.ImageTransition.crossDissolve(0.15),
                                                            runImageTransitionIfCached: false,
                                                            completion: nil)
                    } else {
                        cell.menuItemImageView.image = UIImage(color: UIColor(white: 1.0, alpha: 0.05)) }
                }.disposed(by: rx_disposeBag)
        }
    }
    
    func updateTopSeparatorAlpha() {
        topSeparator.alpha = max(0, min(1.0, tableView.contentOffset.y / 22.0))
    }
    
    @IBAction func onOK(_ sender: AnyObject) {
        self.delegate?.setupPresetsFinalDidFinish(self)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        updateTopSeparatorAlpha()
    }
}

extension SetupPresetsFinalViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    private func tableView(_ tableView: UITableView, editingStyleForRowAt editingStyleForRowAtIndexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return UITableViewCell.EditingStyle.none
    }
}
