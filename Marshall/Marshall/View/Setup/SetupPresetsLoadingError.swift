//
//  SetupPresetsLoadingError.swift
//  Marshall
//
//  Created by Raul Andrisan on 23/05/2017.
//  Copyright © 2017 Zound Industries. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

protocol SetupPresetsLoadingErrorDelegate: class {
    func setupPresetsLoadingErrorDidRequestRetry(_ viewcontroller:SetupPresetsLoadingError)
    func setupPresetsLoadingErrorDidRequestBackOnError(_ viewcontroller:SetupPresetsLoadingError)
}

class SetupPresetsLoadingError: UIViewController {
    @IBOutlet weak var errorContainerTitleLabel: UILabel!
    @IBOutlet weak var errorContainerContentLabel: UILabel!
    @IBOutlet weak var tryAgainButton: UIButton!
    @IBOutlet weak var skipButton: UIButton!

    weak var delegate: SetupPresetsLoadingErrorDelegate?
    var viewModel: SetupPresetsViewModel?
    
    override func viewDidLoad() {
        errorContainerTitleLabel.text = Localizations.Setup.PresetsFail.Title
        errorContainerTitleLabel.font = Fonts.UrbanEars.Bold(23)
        
        errorContainerContentLabel.text = Localizations.Setup.PresetsFail.Content
        errorContainerContentLabel.font = Fonts.UrbanEars.Regular(17)
        
        UIView.setAnimationsEnabled(false)
        
        tryAgainButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.Setup.PresetsFail.Buttons.TryAgain), for: .normal)
        skipButton.setAttributedTitle(Fonts.SecondaryButtonFont.AttributedSecondaryButtonWithString(Localizations.Appwide.Back), for: .normal)
        
        tryAgainButton.layoutIfNeeded()
        skipButton.layoutIfNeeded()
        UIView.setAnimationsEnabled(true)
    }
    
    @IBAction func errorContainerOnTryAgain(_ sender: Any) {
        self.delegate?.setupPresetsLoadingErrorDidRequestRetry(self)
    }
    
    @IBAction func errorContainerOnSkip(_ sender: Any) {
        self.delegate?.setupPresetsLoadingErrorDidRequestBackOnError(self)
    }
}
