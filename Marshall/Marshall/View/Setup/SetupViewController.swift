//
//  SetupViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 11/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit
import Cartography
import ZoundCommon

protocol SetupViewControllerDelegate: class {
    func setupViewContontrollerDidCancel(_ setupViewController:SetupViewController)
}

class SetupViewController : UIViewController {
    weak var delegate:SetupViewControllerDelegate?
    
    var setupNavigation: UINavigationController!
    var navigationStack: [UIViewController]

    required init?(coder aDecoder: NSCoder) {
        navigationStack = []
        super.init(coder: aDecoder)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    override var prefersStatusBarHidden: Bool { return false }
    
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation { return .fade }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavigation = UENavigationController()
        setupNavigation.isNavigationBarHidden = true
        self.addChild(setupNavigation)
        setupNavigation.view.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(setupNavigation.view)
        constrain(setupNavigation.view) { view in
            view.edges == view.superview!.edges
        }
        setupNavigation.didMove(toParent: self)

        if navigationStack.count > 0 {
            setupNavigation.setViewControllers(navigationStack, animated: false)
        }
    }
    
    @IBAction func onClose(_ sender: AnyObject) {
        delegate?.setupViewContontrollerDidCancel(self)
    }
    
    func resetWithViewController(_ viewController: UIViewController, animated: Bool) {
        navigationStack.removeAll()
        navigationStack.append(viewController)
        
        if let navigation = setupNavigation {
            navigation.setViewControllers([viewController], animated: animated)
        }
    }
    
    func pushViewController(_ viewController:UIViewController ,animated:Bool) {
        navigationStack.append(viewController)
        
        if let navigation = setupNavigation {
            navigation.pushViewController(viewController, animated: animated)
        }
    }
    
    func popViewController(_ animated:Bool) {
        guard navigationStack.count > 0 else { return }
        
        navigationStack.removeLast()
        if let navigation = setupNavigation {
            navigation.popViewController(animated: animated)
        }
    }
}
