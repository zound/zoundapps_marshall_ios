//
//  SetupDoneViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 16/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit

protocol SetupDoneViewControllerDelegate: class {
    func setupDidFinish(_ viewController:SetupDoneViewController)
}

class SetupDoneViewController: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var doneButton: UIButton!
    
    weak var delegate: SetupDoneViewControllerDelegate?
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.text = Localizations.Setup.Done.Title.uppercased()
        titleLabel.font = Fonts.UrbanEars.Bold(23)
        
        contentLabel.text = Localizations.Setup.Done.Content
        contentLabel.font = Fonts.MainContentFont
        
        UIView.setAnimationsEnabled(false)
        
        doneButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.Appwide.Done), for: .normal)
        doneButton.layoutIfNeeded()
        UIView.setAnimationsEnabled(true)
        
    }
    
    @IBAction func onDone(_ sender: AnyObject) {
        self.delegate?.setupDidFinish(self)
    }
}
