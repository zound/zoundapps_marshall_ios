//
//  SettingsSettingCell.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 16/05/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

class SettingsMenuCell: UITableViewCell {
    @IBOutlet weak var settingLabel: UILabel!
    
    let settingsItemVariable: Variable<SettingsItem?> = Variable(nil)
    var settingsItem: SettingsItem? {
        get { return settingsItemVariable.value }
        set { settingsItemVariable.value = newValue }
    }
    
    override func awakeFromNib() {
        self.contentView.backgroundColor = UIColor.clear
        self.backgroundColor = UIColor.clear
        
        settingLabel.font = Fonts.ListItemFont
        
        let customColorView = UIView()
        customColorView.backgroundColor = UIColor(white: 0.0, alpha: 0.24)
        self.selectedBackgroundView =  customColorView;
        
        settingsItemVariable.asObservable()
            .map { $0 != nil ? $0!.localizedDisplayName : "" }
            .bind(to: settingLabel.rx.text)
            .disposed(by: rx_disposeBag)
    }
}
