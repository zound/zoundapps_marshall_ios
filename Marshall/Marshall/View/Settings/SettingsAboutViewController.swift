//
//  SettingsAbout.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 16/05/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import MinuetSDK

protocol SettingsAboutViewControllerDelegate: class {
    func settingsAboutViewControllerDidRequestBack(_ viewController: SettingsAboutViewController)
    func settingsAboutViewControllerDidRequestUpdate(_ viewController: SettingsAboutViewController, for speaker: Speaker)
}

class SettingsAboutViewController: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var aboutLabel: UILabel!
    @IBOutlet weak var updateButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var topSeparator: UIView!
    @IBOutlet weak var topSeparatorHeightConstraint: NSLayoutConstraint!
    
    weak var delegate: SettingsAboutViewControllerDelegate?
    
    var viewModel: SettingsAboutViewModel?
    let disposeBag = DisposeBag()
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.attributedText = Fonts.PageTitleFont.AttributedPageTitleWithString(Localizations.Settings.About.Title.uppercased())
        aboutLabel.font = Fonts.UrbanEars.Light(15)
        scrollView.delegate = self
        scrollView.alwaysBounceVertical = true
        
        self.topSeparatorHeightConstraint.constant = 0.5
        self.updateButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.Settings.About.Update.Buttonlabel), for: .normal)
        
        updateTopSeparatorAlpha()
        setupLabels()
        setupUpdateButton()
    }
    
    @IBAction func onBack(_ sender: AnyObject) {
        self.delegate?.settingsAboutViewControllerDidRequestBack(self)
    }
    
    @IBAction func onUpdate(_ sender: AnyObject) {
        guard let speaker = viewModel?.speaker else { return }
        self.delegate?.settingsAboutViewControllerDidRequestUpdate(self, for: speaker)
    }
    
    func updateTopSeparatorAlpha() {
        topSeparator.alpha = max(0, min(1.0, scrollView.contentOffset.y / 22.0))
    }
}

extension SettingsAboutViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        updateTopSeparatorAlpha()
    }
}

extension SettingsAboutViewController {
    func setupLabels() {
        guard let vm = viewModel else { return }
        
        Observable.combineLatest(vm.infoDict.asObservable(),
                                 vm.wifiSignal.asObservable())
        { (infoDict, wifiSignal) in
            let wifiNameLabel = Localizations.Settings.About.WiFiNetwork
            let ipLabel = Localizations.Settings.About.Ip
            let nameLabel = Localizations.Settings.About.Name
            let macLabel = Localizations.Settings.About.Mac
            let modelLabel = Localizations.Settings.About.Model
            let fwVersionLabel = Localizations.Settings.About.BuildVersion
            let castVersionLabel = Localizations.Settings.About.CastVersion
            
            let labels = [nameLabel,
                          modelLabel,
                          wifiNameLabel,
                          ipLabel,
                          macLabel,
                          fwVersionLabel,
                          castVersionLabel].map { NSAttributedString(string: $0, attributes: [NSAttributedString.Key.foregroundColor: UIColor.white]) }
            
            let empty = ""
            
            let wifiName = infoDict[vm.wifiName_key] ?? empty
            let name = infoDict[vm.name_key] ?? empty
            let ip = infoDict[vm.ip_key] ?? empty
            let mac = infoDict[vm.mac_key]?.formatToMAC() ?? empty
            let model = infoDict[vm.model_key] ?? empty
            let buildVersion = infoDict[vm.buildVersion_key] ?? empty
            let castVersion = infoDict[vm.castVersion_key] ?? empty
            
            let wifi = wifiName + " (\(wifiSignal)%)"
            let values = [name,
                          model,
                          wifi,
                          ip,
                          mac,
                          buildVersion,
                          castVersion].map { NSAttributedString(string: $0, attributes: [NSAttributedString.Key.foregroundColor: UIColor("#CDCDCD")]) }
            
            let spacer = NSAttributedString(string: " ")
            let newLine = NSAttributedString(string: "\n")
            let finalString = NSMutableAttributedString()
            
            zip(labels, values).forEach { label, value in
                finalString.append(label)
                finalString.append(spacer)
                finalString.append(value)
                finalString.append(newLine)
            }
            
            return finalString }
            .bind(to: aboutLabel.rx.attributedText)
            .disposed(by: disposeBag)
    }
    
    func setupUpdateButton() {
        guard let vm = viewModel else { return }
        
        vm.updateAvailable
            .asDriver(onErrorJustReturn: false)
            .drive(updateButton.rx.visible)
            .disposed(by: disposeBag)
    }
    
}
