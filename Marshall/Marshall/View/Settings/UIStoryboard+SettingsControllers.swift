//
//  UIStoryboard+SettingsControllers.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 21/06/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit

extension UIStoryboard {
    static var settings: UIStoryboard {
        return UIStoryboard(name: "Settings", bundle: nil)
    }
}

extension UIStoryboard {
    var settingsMenuViewController: SettingsMenuViewController {
        guard let vc = UIStoryboard.settings.instantiateViewController(withIdentifier: "settingsMenu") as? SettingsMenuViewController else {
            fatalError("SettingsMenuViewController couldn't be found in Storyboard file")
        }
        return vc
    }
    
    var settingsSpeakerListViewController: SettingsSpeakerListViewController {
        guard let vc = UIStoryboard.settings.instantiateViewController(withIdentifier: "settingsSpeakerList") as? SettingsSpeakerListViewController else {
            fatalError("SettingsSpeakerListViewController couldn't be found in Storyboard file")
        }
        return vc
    }
    
    var settingsSpeakerViewController: SettingsSpeakerViewController {
        guard let vc = UIStoryboard.settings.instantiateViewController(withIdentifier: "settingsSpeaker") as? SettingsSpeakerViewController else {
            fatalError("SettingsSpeakerViewController couldn't be found in Storyboard file")
        }
        return vc
    }
    
    var settingsAboutViewController: SettingsAboutViewController {
        guard let vc = UIStoryboard.settings.instantiateViewController(withIdentifier: "settingsAbout") as? SettingsAboutViewController else {
            fatalError("SettingsAboutViewController couldn't be found in Storyboard file")
        }
        return vc
    }
    
    var settingsSpeakerUpdateViewController: SettingsSpeakerUpdateViewController {
        guard let vc = UIStoryboard.settings.instantiateViewController(withIdentifier: "settingsUpdate") as? SettingsSpeakerUpdateViewController else {
            fatalError("SettingsSpeakerUpdateViewController couldn't be found in Storyboard file")
        }
        return vc
    }
    
    var settingsLedIntensityViewController: SettingsLedIntensityViewController {
        guard let vc = UIStoryboard.settings.instantiateViewController(withIdentifier: "settingsLEDAdjuster") as? SettingsLedIntensityViewController else {
            fatalError("SettingsLedIntensityViewController couldn't be found in Storyboard file")
        }
        return vc
    }
    
    var settingsSoundsViewController: SettingsSoundsViewController {
        guard let vc = UIStoryboard.settings.instantiateViewController(withIdentifier: "settingsSounds") as? SettingsSoundsViewController else {
            fatalError("SettingsSoundsViewController couldn't be found in Storyboard file")
        }
        return vc
    }
    
    var settingsStreamingQualityViewController: SettingsStreamingQualityViewController {
        guard let vc = UIStoryboard.settings.instantiateViewController(withIdentifier: "settingsStreamingQuality") as? SettingsStreamingQualityViewController else {
            fatalError("SettingsStreamingQualityViewController couldn't be found in Storyboard file")
        }
        return vc
    }
}
