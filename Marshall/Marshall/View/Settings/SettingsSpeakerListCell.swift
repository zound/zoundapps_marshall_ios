//
//  SettingsSpeakerListCell.swift
//  UrbanEars
//
//  Created by Robert Sandru on 06/12/2016.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit
import RxSwift
import MinuetSDK

enum SettingsSpeakerListCell {
    case speakerCell(SettingsSpeakerCell)
    case streamingQualityCell(SettingsStreamingQualityCell)
}

class SettingsStreamingQualityCell: UITableViewCell {
    @IBOutlet weak var settingLabel: UILabel!
    @IBOutlet weak var streamingQualityImageView: UIImageView!
    @IBOutlet weak var disclosureIndicatorImageView: UIImageView!
    
    override func awakeFromNib() {
        settingLabel.text = Localizations.Settings.StreamingQuality.Celllabel.uppercased()
        settingLabel.font = Fonts.UrbanEars.Bold(17)
        self.contentView.backgroundColor = UIColor.clear
        self.backgroundColor = UIColor.clear
        let customColorView = UIView()
        customColorView.backgroundColor = UIColor(white: 0.0, alpha: 0.24)
        self.selectedBackgroundView =  customColorView
    }
}

class SettingsSpeakerCell: UITableViewCell {
    @IBOutlet weak var speakerImageView: UIImageView!
    @IBOutlet weak var speakerNameLabel: UILabel!
    @IBOutlet weak var disclosureIndicatorImageView: UIImageView!
    
    fileprivate let viewModelVariable: Variable<Speaker?> = Variable(nil)
    
    var viewModel: Speaker? {
        set { viewModelVariable.value = newValue }
        get { return viewModelVariable.value }
    }
    
    override func awakeFromNib() {
        self.contentView.backgroundColor = UIColor.clear
        self.backgroundColor = UIColor.clear
        
        speakerNameLabel.font = Fonts.UrbanEars.Bold(17)
        
        let customColorView = UIView()
        customColorView.backgroundColor = UIColor(white: 0.0, alpha: 0.24)
        self.selectedBackgroundView =  customColorView
        
        viewModelVariable.asObservable()
            .map { $0 != nil ? $0!.friendlyNameWithIndex.uppercased() : "" }
            .bind(to: speakerNameLabel.rx.text)
            .disposed(by: rx_disposeBag)
        
        viewModelVariable.asObservable()
            .map { ExternalConfig.sharedInstance.speakerImageForColor($0?.color)?.listItemImageName }
            .map { ($0 != nil ? UIImage(named: $0!) : UIImage(named: "list_item_placeholder")) }
            .bind(to: speakerImageView.rx.image).disposed(by: rx_disposeBag)
        
        viewModelVariable.asObservable()
            .map { ExternalConfig.sharedInstance.speakerImageForColor($0?.color)?.connectable }
            .map { $0 != nil ? $0! : false }.map(!)
            .bind(to: disclosureIndicatorImageView.rx.isHidden)
            .disposed(by: rx_disposeBag)
        
    }
}
