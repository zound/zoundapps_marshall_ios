//
//  SettingsSpeakerList.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 16/05/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit
import MinuetSDK

protocol SettingsSpeakerListViewControllerDelegate: class {
    func settingsSpeakerListViewControllerDidRequestBack(_ viewController: SettingsSpeakerListViewController)
    func settingsSpeakerListViewControllerDidSelectSpeaker(_ speaker: Speaker)
    func settingsSpeakerListViewControllerDidSelectStreamingQuality(_ cell: SettingsStreamingQualityCell, speakers: [Speaker])
}

class SettingsSpeakerListViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var separatorView: UIView!
    
    weak var delegate: SettingsSpeakerListViewControllerDelegate?
    
    var viewModel: SettingsSpeakerListViewModel?
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.attributedText = Fonts.PageTitleFont.AttributedPageTitleWithString(Localizations.Settings.List.Title)
        tableView.tableFooterView = UIView()
        
        self.tableView.backgroundColor = UIColor.clear
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 105.0
        tableView.rowHeight = UITableView.automaticDimension
        tableView.separatorStyle = .none
        
        if let count = self.navigationController?.viewControllers.count, count > 1 {
            self.backButton.setImage(UIImage(named: "back"), for: .normal)
        } else {
            self.backButton.setImage(UIImage(named: "close"), for: .normal)
        }
        
        updateSeparatorViewVisibility()
        
        if let vm = viewModel {
            vm.cells.asObservable()
                .subscribe(onNext: { [weak self] cells in
                self?.tableView.reloadData()
            }).disposed(by: rx_disposeBag)
        }
    }
    
    @IBAction func onBack(_ sender: AnyObject) {
        self.delegate?.settingsSpeakerListViewControllerDidRequestBack(self)
    }
    
    func updateSeparatorViewVisibility() {
        separatorView.alpha = max(0,min(1.0,tableView.contentOffset.y/22.0))
    }
}

extension SettingsSpeakerListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let vm = viewModel {
            switch vm.cells.value[indexPath.row] {
            case .speakerCell(let speakerCell):
                if let speaker = speakerCell.viewModel {
                    if let connectable = ExternalConfig.sharedInstance.speakerImageForColor(speaker.color)?.connectable,
                        connectable == true {
                        delegate?.settingsSpeakerListViewControllerDidSelectSpeaker(speaker)
                    }
                }
            case .streamingQualityCell(_):
                let cell = tableView.cellForRow(at: indexPath) as! SettingsStreamingQualityCell
                delegate?.settingsSpeakerListViewControllerDidSelectStreamingQuality(cell, speakers: vm.audioSystem.speakers.value)
            }
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    private func tableView(_ tableView: UITableView, editingStyleForRowAt editingStyleForRowAtIndexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return UITableViewCell.EditingStyle.none
    }
}

extension SettingsSpeakerListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let vm = viewModel {
            return vm.cells.value.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        
        if let vm = viewModel {
            switch vm.cells.value[indexPath.row] {
            case .speakerCell(let speakerCell):
                let cell = self.tableView.dequeueReusableCell(withIdentifier: "settingsSpeakerCell", for: indexPath) as! SettingsSpeakerCell
                cell.viewModel = speakerCell.viewModel
                return cell
            case .streamingQualityCell(_):
                let cell = self.tableView.dequeueReusableCell(withIdentifier: "settingsStreamingQualityCell", for: indexPath) as! SettingsStreamingQualityCell
                return cell
            }
        }

        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        updateSeparatorViewVisibility()
    }
}
