//
//  SettingsLedIntensityViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 12/06/17.
//  Copyright © 2017 Zound Industries. All rights reserved.
//

import Foundation
import UIKit
import RxCocoa
import RxSwift

protocol SettingsLedIntensityViewControllerDelegate: class {
    func settingsLedIntensityViewControllerDidRequestBack(_ viewController: SettingsLedIntensityViewController)
}

class SettingsLedIntensityViewController: UIViewController {
    @IBOutlet weak var intensitySlider: UISlider!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var adjustLedInstructionLabel: UILabel!
    @IBOutlet weak var ledIntensityImage: UIImageView!
    
    weak var delegate: SettingsLedIntensityViewControllerDelegate?
    
    var viewModel: SettingsLedIntensityViewModel?
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    override func viewDidLoad() {
        intensitySlider.setMinimumTrackImage(UIImage(named:"eq_slider_track_min"), for: .normal)
        intensitySlider.setMaximumTrackImage(UIImage(named:"eq_slider_track_max"), for: .normal)
        intensitySlider.setThumbImage(UIImage(named: "master_slider_thumb"), for: .normal)
        
        titleLabel.attributedText = Fonts.PageTitleFont.AttributedPageTitleWithString(Localizations.Settings.Speaker.LedIntensity.uppercased())
        
        adjustLedInstructionLabel.text = Localizations.Settings.LedAdjuster.AdjustInstruction
        adjustLedInstructionLabel.font = Fonts.UrbanEars.Regular(17)
        
        if let vm = viewModel {
            vm.intensityNormalizedVariable.asObservable()
                .bind(to: intensitySlider.rx.value)
                .disposed(by: rx_disposeBag)
            vm.intensityNormalizedVariable.asObservable()
                .subscribe(weak: self, onNext: SettingsLedIntensityViewController.updateLedIndicatorForIntensityNormalized)
                .disposed(by: rx_disposeBag)
        }
    }
    
    func updateLedIndicatorForIntensityNormalized(_ intensity: Float) {
        let imageSteps = 14
        let step = Int(Float(imageSteps-1) * intensity)
        let stepWithLeadingZero = String(format:"%02d",step)
        ledIntensityImage.image = UIImage(named: "led_intensity_\(stepWithLeadingZero)")
    }
    
    @IBAction func onUpdatedIntensityValue(_ sender: AnyObject) {
        guard let vm = viewModel, let slider = sender as? UISlider else { return }
        vm.intensityNormalizedVariable.value = Float(slider.value)
    }
    
    @IBAction func onBack(_ sender: AnyObject) {
        self.delegate?.settingsLedIntensityViewControllerDidRequestBack(self)
    }
}
