//
//  SettingsSpeakerUpdateViewController.swift
//  Marshall
//
//  Created by Grzegorz Kiel on 09/01/2018.
//  Copyright © 2018 Zound Industries. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import ZoundCommon

protocol SettingsSpeakerUpdateViewControllerDelegate: class {
    func settingsSpeakerUpdateViewControllerDidRequestContinue(_ updateViewController: SettingsSpeakerUpdateViewController)
    func settingsSpeakerUpdateViewControllerDidRequestCancel(_ updateViewController: SettingsSpeakerUpdateViewController)
}

class SettingsSpeakerUpdateViewController: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    weak var delegate: SettingsSpeakerUpdateViewControllerDelegate?
    
    var speakerUpdateViewModel: SettingsSpeakerUpdateViewModel?
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    deinit {
        print("Deallocated \(self)")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.text = Localizations.Appwide.Note
        titleLabel.font = Fonts.UrbanEars.Bold(23)
        
        contentLabel.text = Localizations.Settings.About.Update.Content((speakerUpdateViewModel?.speaker.friendlyName)!)
        contentLabel.font = Fonts.MainContentFont
        
        UIView.setAnimationsEnabled(false)
        
        continueButton.setAttributedTitle(Fonts.ButtonFont.AttributedPrimaryButtonWithString(Localizations.Appwide._Continue), for: .normal)
        cancelButton.setAttributedTitle(Fonts.SecondaryButtonFont.AttributedSecondaryButtonWithString(Localizations.Appwide.Cancel), for: .normal)
        
        continueButton.layoutIfNeeded()
        cancelButton.layoutIfNeeded()
        UIView.setAnimationsEnabled(true)
    }
    
    @IBAction func onCancel(_ sender: AnyObject) {
        self.delegate?.settingsSpeakerUpdateViewControllerDidRequestCancel(self)
    }
    
    @IBAction func onContinue(_ sender: AnyObject) {
        speakerUpdateViewModel?.startUpdate()
        self.delegate?.settingsSpeakerUpdateViewControllerDidRequestContinue(self)
    }
}
