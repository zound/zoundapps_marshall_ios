//
//  SettingsCoordinator.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 16/05/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import RxSwift
import STKWebKitViewController
import SafariServices
import MinuetSDK
import ZoundCommon

protocol SettingsCoordinatorDelegate: class {
    func settingsCoordinatorDidFinishCoordinating(_ coordinator: SettingsCoordinator)
}

class SettingsCoordinator: Coordinator, RunAfterDelay {
    weak var delegate: SettingsCoordinatorDelegate?
    
    var navController: UINavigationController!
    var appStoreCoordinator: AppStoreCoordinator?
    
    var currentSpeaker: Speaker? = nil
    var disposeBag = DisposeBag()
    
    let provider: SpeakerProvider
    let audioSystem: AudioSystem
    let parentViewController: UIViewController
    let discoveryService: DiscoveryServiceType
    
    init(viewController: UIViewController, audioSystem: AudioSystem, provider: SpeakerProvider, discoveryService: DiscoveryServiceType) {
        self.audioSystem = audioSystem
        self.parentViewController = viewController
        self.provider = provider
        self.discoveryService = discoveryService
    }
    
    deinit {
        print(#function, String(describing: self))
    }
    
    func start() {
        showMultiSettingsMenu()
    }
    
    func startForSpeaker(_ speaker: Speaker) {
        self.showSettingsForSpeaker(speaker)
    }
    
    func showSpeakerList() {
        let listViewController = UIStoryboard.settings.settingsSpeakerListViewController
        listViewController.delegate = self
        let listViewModel  = SettingsSpeakerListViewModel(audioSystem: audioSystem)
        listViewController.viewModel = listViewModel
        
        if let navController = self.navController {
            navController.pushViewController(listViewController, animated: true)
        }
    }
    
    func showSettingsForSpeaker(_ speaker: Speaker) {
        let settingsSpeakerViewController = UIStoryboard.settings.settingsSpeakerViewController
        settingsSpeakerViewController.delegate = self
        
        self.navController = UENavigationController(rootViewController: settingsSpeakerViewController)
        self.navController.navigationBar.isHidden = true
        self.navController.modalPresentationStyle = .formSheet
        
        let settingsSpeakerViewModel = SettingsSpeakerViewModel(speaker: speaker, provider: provider)
        settingsSpeakerViewController.viewModel = settingsSpeakerViewModel
        
        self.currentSpeaker = speaker
        self.parentViewController.present(navController, animated: true, completion: nil)
    }
    
    func showMultiSettingsMenu() {
        let menuViewController = UIStoryboard.settings.settingsMenuViewController
        menuViewController.delegate = self
        menuViewController.menuItems = [.multiStreamingQuality]
        menuViewController.menuTitle = Localizations.Settings.RootMenu.MultiSpeakers
        
        self.navController = UENavigationController(rootViewController: menuViewController)
        self.navController.navigationBar.isHidden = true
        self.navController.modalPresentationStyle = .formSheet
        
        self.parentViewController.present(navController, animated: true, completion: nil)
    }
    
    func showAboutForSpeaker(_ speaker: Speaker, viewModel: SettingsSettingCellViewModel) {
        viewModel.loading.value = true
        
        provider.getAboutStateForSpeaker(speaker)
            .subscribe(onNext: { [weak self] state in
                guard let `self` = self else { return }
                
                self.showAboutWithAboutState(state, for: speaker)
                viewModel.loading.value = false
                }, onError: { error in
                    viewModel.loading.value = false })
            .disposed(by: disposeBag)
    }
    
    func showAboutWithAboutState(_ state: AboutState, for speaker: Speaker) {
        let settingsAboutViewController = UIStoryboard.settings.settingsAboutViewController
        settingsAboutViewController.delegate = self
        
        let aboutViewModel = SettingsAboutViewModel(aboutState: state, speaker: speaker, provider: self.provider)
        settingsAboutViewController.viewModel = aboutViewModel
        
        self.navController.pushViewController(settingsAboutViewController, animated: true)
    }
    
    func showUpdateNote(forSpeaker speaker: Speaker) {
        let speakerUpdateViewController = UIStoryboard.settings.settingsSpeakerUpdateViewController
        speakerUpdateViewController.delegate = self
        
        let updateViewModel = SettingsSpeakerUpdateViewModel(speaker: speaker, provider: self.provider)
        speakerUpdateViewController.speakerUpdateViewModel = updateViewModel
        
        self.navController.pushViewController(speakerUpdateViewController, animated: true)
    }
    
    func navigateToRenameInGoogleHomeApp(forSpeaker speaker: Speaker) {
        let appURL = "chromecast://devicesettings?ip=\(speaker.ipAddress)"
        let appId = "680819774"
        
        guard let url  = URL(string: appURL) else { return }
        
        if UIApplication.shared.canOpenURL(url) == true  {
            UIApplication.shared.openURL(url)
        } else {
            if self.appStoreCoordinator == nil {
                self.appStoreCoordinator = AppStoreCoordinator(parentViewController: self.navController, itunesItemIdentifier: appId)
                self.appStoreCoordinator?.delegate = self
                self.appStoreCoordinator?.start()
            }
        }
    }
    
    func showLedIntensity(_ speaker: Speaker, viewModel: SettingsSettingCellViewModel) {
        viewModel.loading.value = true
        
        provider.getLedIntensityStateForSpeaker(speaker)
            .subscribe(onNext: { [weak self] intensity in
                guard let `self` = self else { return }
                
                self.showLedIntensityWithIntensity(intensity, forSpeaker: speaker)
                viewModel.loading.value = false
                }, onError: { error in
                    viewModel.loading.value = false
            }).disposed(by: disposeBag)
    }
    
    func showLedIntensityWithIntensity(_ intensity:Int, forSpeaker speaker: Speaker) {
        let ledIntensityViewController = UIStoryboard.settings.settingsLedIntensityViewController
        ledIntensityViewController.delegate = self
        
        let ledIntensityViewModel = SettingsLedIntensityViewModel(intensity: intensity, speaker: speaker, provider: self.provider)
        ledIntensityViewController.viewModel = ledIntensityViewModel
        
        self.navController.pushViewController(ledIntensityViewController, animated: true)
    }
    
    func showStreamingQuality(audioSystem: AudioSystem) {
        let streamingQualityViewController = UIStoryboard.settings.settingsStreamingQualityViewController
        streamingQualityViewController.delegate = self
        streamingQualityViewController.viewModel = SettingsStreamingQualityViewModel(audioSystem: audioSystem)
        self.navController.pushViewController(streamingQualityViewController, animated: true)
    }
    
    // TODO: Should be removed after testing
    func showStreamingQualityForSpeakers(_ speakers: [Speaker]) {
        fatalError("")
    }
    
    func showBrowserForURL(_ link: URL) {
        if #available(iOS 9, *) {
            let safari = SFSafariViewController(url: link)
            safari.modalPresentationStyle = .overFullScreen
            safari.modalPresentationCapturesStatusBarAppearance = true
            navController.present(safari, animated: true, completion: nil)
        } else {
            let webViewController = STKWebKitModalViewController(url: link)
            navController.present(webViewController!, animated: true, completion: nil)
        }
    }
}

extension SettingsCoordinator: SettingsMenuViewControllerDelegate {
    func settingsMenuViewControllerDidRequestBack(_ settingsMenuViewController: SettingsMenuViewController) {
        if navController.viewControllers.count > 1 {
            navController.popViewController(animated: true)
        } else {
            self.parentViewController.dismiss(animated: true, completion: {[weak self] in
                self?.delegate?.settingsCoordinatorDidFinishCoordinating(self!)
            })
        }
    }
    
    func settingsMenuViewControllerDidRequestSettingsItem(_ settingsItem: SettingsItem, _ settingsMenuViewController: SettingsMenuViewController) {
        disposeBag = DisposeBag()
        switch settingsItem {
        case .multiSpeakers: showMultiSettingsMenu()
        case .multiStreamingQuality:
            showStreamingQuality(audioSystem: audioSystem)
        case .soloSpeakers: showSpeakerList()
        case .stereoPairing: break
        }
    }
}

extension SettingsCoordinator: AppStoreCoordinatorDelegate {
    func appStoreCoordinatorDidFinishCoordinating(_ coordinator: AppStoreCoordinator) {
        appStoreCoordinator = nil
    }
}

extension SettingsCoordinator: SettingsAboutViewControllerDelegate {
    func settingsAboutViewControllerDidRequestBack(_ viewController: SettingsAboutViewController) {
        self.navController.popViewController(animated: true)
    }
    
    func settingsAboutViewControllerDidRequestUpdate(_ viewController: SettingsAboutViewController, for speaker: Speaker) {
        self.showUpdateNote(forSpeaker: speaker)
    }
}

extension SettingsCoordinator: SettingsSpeakerUpdateViewControllerDelegate {
    func settingsSpeakerUpdateViewControllerDidRequestContinue(_ updateViewController: SettingsSpeakerUpdateViewController) {
        //dismiss everything and go to homescreen
        self.parentViewController.dismiss(animated: true, completion: {[weak self] in
            self?.delegate?.settingsCoordinatorDidFinishCoordinating(self!)
        })
    }
    
    func settingsSpeakerUpdateViewControllerDidRequestCancel(_ updateViewController: SettingsSpeakerUpdateViewController) {
        self.navController.popViewController(animated: true)
    }
}


extension SettingsCoordinator: SettingsLedIntensityViewControllerDelegate {
    func settingsLedIntensityViewControllerDidRequestBack(_ viewController: SettingsLedIntensityViewController) {
        self.navController.popViewController(animated: true)
    }
}

extension SettingsCoordinator: SettingsStreamingQualityViewControllerDelegate {
    func settingsStreamingQualityControllerDidRequestBack(_ viewController: SettingsStreamingQualityViewController) {
        self.navController.popViewController(animated: true)
    }
}

extension SettingsCoordinator: SettingsSpeakerViewControllerDelegate {
    func settingsSpeakerViewControllerDidRequestBack(_ viewController: SettingsSpeakerViewController) {
        if self.navController.viewControllers.count > 1 {
            self.currentSpeaker = nil
            self.navController.popViewController(animated: true)
        } else {
            self.parentViewController.dismiss(animated: true, completion: {[weak self] in
                self?.currentSpeaker = nil
                self?.delegate?.settingsCoordinatorDidFinishCoordinating(self!)
            })
        }
    }
    
    func settingsSpeakerViewControllerDidSelectSpeakerSettingViewModel(_ speakerSettingViewModel: SettingsSettingCellViewModel, viewController: SettingsSpeakerViewController) {
        disposeBag = DisposeBag()
        viewController.menuItems.forEach{ $0.loading.value = false }
        if let speaker = viewController.viewModel?.speaker {
            switch speakerSettingViewModel.setting {
            case .about: showAboutForSpeaker(speaker, viewModel: speakerSettingViewModel)
            case .rename:
                if let speaker = self.currentSpeaker {
                    navigateToRenameInGoogleHomeApp(forSpeaker: speaker)
                }
            case .ledIntensity: showLedIntensity(speaker, viewModel: speakerSettingViewModel)
            }
        }
    }
}

extension SettingsCoordinator: SettingsSpeakerListViewControllerDelegate {
    func settingsSpeakerListViewControllerDidRequestBack(_ viewController: SettingsSpeakerListViewController) {
        if self.navController.viewControllers.count > 1 {
            self.navController.popViewController(animated: true)
        } else {
            self.parentViewController.dismiss(animated: true, completion: {[weak self] in
                self?.delegate?.settingsCoordinatorDidFinishCoordinating(self!)
            })
        }
    }
    
    func settingsSpeakerListViewControllerDidSelectSpeaker(_ speaker: Speaker) {
        showSettingsForSpeaker(speaker)
    }
    
    func settingsSpeakerListViewControllerDidSelectStreamingQuality(_ cell: SettingsStreamingQualityCell, speakers: [Speaker]) {
        showStreamingQualityForSpeakers(speakers)
    }
}
