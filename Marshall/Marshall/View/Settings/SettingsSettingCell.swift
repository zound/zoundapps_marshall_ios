//
//  SettingsSettingCell.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 16/05/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

class SettingsSettingCell: UITableViewCell {
    @IBOutlet weak var disclosureIndicator: UIImageView!
    @IBOutlet weak var loadingIndicator: UIImageView!
    @IBOutlet weak var settingLabel: UILabel!
    
    let settingVariable: Variable<SpeakerSetting?> = Variable(nil)
    let viewModelVariable: Variable<SettingsSettingCellViewModel?> = Variable(nil)
    
    var viewModel: SettingsSettingCellViewModel? {
        get { return self.viewModelVariable.value }
        set { self.viewModelVariable.value = newValue }
    }
    
    override func awakeFromNib() {
        self.contentView.backgroundColor = UIColor.clear
        self.backgroundColor = UIColor.clear
        
        settingLabel.font = Fonts.ListItemFont
        
        let customColorView = UIView()
        customColorView.backgroundColor = UIColor(white: 0.0, alpha: 0.24)
        self.selectedBackgroundView =  customColorView
        
        settingVariable.asObservable().map{ $0 != nil ?  $0!.localizedDisplayName : "" }.bind(to:settingLabel.rx.text).disposed(by: rx_disposeBag)
        
        viewModelVariable.asObservable()
            .subscribe(onNext: { [weak self] vm in
                if let vm = vm {
                    vm.loading.asObservable()
                        .subscribe(onNext: { [weak self] isLoading in
                            if isLoading {
                                self?.disclosureIndicator.isHidden = true
                                self?.loadingIndicator.isHidden = false
                                self?.loadingIndicator.rotate()
                            } else {
                                self?.disclosureIndicator.isHidden = false
                                self?.loadingIndicator.isHidden = true
                                self?.loadingIndicator.stopRotation()
                            }
                        }).disposed(by: self!.rx_disposeBag)
                }
            }).disposed(by: rx_disposeBag)
    }
}
