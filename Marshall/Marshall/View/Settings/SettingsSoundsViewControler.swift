//
//  SettingsLedIntensityViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 12/06/17.
//  Copyright © 2017 Zound Industries. All rights reserved.
//

import Foundation
import UIKit
import RxCocoa
import RxSwift

protocol SettingsSoundsViewControllerDelegate: class {
    func settingsSoundsViewControllerDidRequestBack(_ viewController: SettingsSoundsViewController)
}

class SettingsSoundsViewController: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bluetoothConnectLabel: UILabel!
    @IBOutlet weak var networkConnectLabel: UILabel!
    @IBOutlet weak var presetStoreLabel: UILabel!
    @IBOutlet weak var presetFailLabel: UILabel!
    
    @IBOutlet weak var bluetoothConnectSwitch: UISwitch!
    @IBOutlet weak var networkConnectSwitch: UISwitch!
    @IBOutlet weak var presetStoreSwitch: UISwitch!
    @IBOutlet weak var presetFailSwitch: UISwitch!
    
    weak var delegate: SettingsSoundsViewControllerDelegate?
    
    var viewModel: SettingsSoundsViewModel?
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    override func viewDidLoad() {
        titleLabel.attributedText = Fonts.PageTitleFont.AttributedPageTitleWithString(Localizations.Settings.Sounds.Title)
        bluetoothConnectLabel.font = Fonts.UrbanEars.Regular(17)
        bluetoothConnectLabel.text = Localizations.Settings.Sounds.BluetoothConnect
        
        networkConnectLabel.font = Fonts.UrbanEars.Regular(17)
        networkConnectLabel.text = Localizations.Settings.Sounds.NetworkConnect
        
        presetStoreLabel.font = Fonts.UrbanEars.Regular(17)
        presetStoreLabel.text = Localizations.Settings.Sounds.PresetStore
        
        presetFailLabel.font = Fonts.UrbanEars.Regular(17)
        presetFailLabel.text = Localizations.Settings.Sounds.PresetFail
    }
    
    @IBAction func onBluetoothConnectChanged(_ sender: Any) {
    }
    
    @IBAction func onNetworkConnectChanged(_ sender: Any) {
    }
    
    @IBAction func onPresetStoreChanged(_ sender: Any) {
    }
    
    @IBAction func onPresetFailChanged(_ sender: Any) {
    }
    
    @IBAction func onBack(_ sender: AnyObject) {
        self.delegate?.settingsSoundsViewControllerDidRequestBack(self)
    }
}
