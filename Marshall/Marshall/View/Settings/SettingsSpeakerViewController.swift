//
//  SettingsSpeaker.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 16/05/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import MinuetSDK
import ZoundCommon

fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l > r
    default:
        return rhs < lhs
    }
}


protocol SettingsSpeakerViewControllerDelegate: class {
    func settingsSpeakerViewControllerDidRequestBack(_ viewController: SettingsSpeakerViewController)
    func settingsSpeakerViewControllerDidSelectSpeakerSettingViewModel(_ speakerSettingViewModel: SettingsSettingCellViewModel, viewController: SettingsSpeakerViewController)
}


class SettingsSpeakerViewController: UIViewController {
    @IBOutlet weak var speakerImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableTopSeparatorHeight: NSLayoutConstraint!
    @IBOutlet weak var topSeparator: UIView!
    @IBOutlet weak var closeButton: UIButton!
    
    weak var delegate: SettingsSpeakerViewControllerDelegate?
    
    var viewModel: SettingsSpeakerViewModel?
    let menuItems: [SettingsSettingCellViewModel]
    
    required init?(coder aDecoder: NSCoder) {
        self.menuItems = [SettingsSettingCellViewModel(setting: .about),
                          SettingsSettingCellViewModel(setting: .rename),
                          SettingsSettingCellViewModel(setting: .ledIntensity)]
        super.init(coder: aDecoder)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = 65.0
        tableView.rowHeight = UITableView.automaticDimension
        tableView.sizeHeaderToFit(preferredWidth: self.view.bounds.size.width)
        tableView.separatorStyle = .none
        
        updateTopSeparatorAlpha()
        self.tableTopSeparatorHeight.constant = 0.5
        self.tableView.backgroundColor = UIColor.clear
        tableView.rx.setDelegate(self).disposed(by: rx_disposeBag)
        
        if let vm = viewModel {
            vm.speakerVariable.asObservable()
                .subscribe(weak: self, onNext: SettingsSpeakerViewController.updateWithSpeaker)
                .disposed(by: rx_disposeBag)
            Observable
                .just(menuItems)
                .bind(to: tableView.rx.items(cellIdentifier:"settingsSetting", cellType: SettingsSettingCell.self)) { (row, element, cell) in
                    cell.viewModel = element
                    cell.settingLabel.text = element.setting.localizedDisplayName
                }.disposed(by: rx_disposeBag)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.reloadData()
        
        if self.navigationController?.viewControllers.count > 1 {
            closeButton.setImage(UIImage(named:"back"), for: .normal)
        } else {
            closeButton.setImage(UIImage(named:"close"), for: .normal)
        }
    }
    
    @IBAction func onBack(_ sender: AnyObject) {
        self.delegate?.settingsSpeakerViewControllerDidRequestBack(self)
    }
    
    func updateWithSpeaker(_ speaker: Speaker) {
        if let speakerName = ExternalConfig.sharedInstance.speakerImageForColor(speaker.color)?.heroImageName {
            speakerImageView.image = UIImage(named: speakerName)
        } else {
            speakerImageView.image = UIImage(named: "hero_placeholder")
        }
        
        titleLabel.attributedText = Fonts.PageTitleFont.AttributedPageTitleWithString(speaker.friendlyName.uppercased())
    }

    func updateTopSeparatorAlpha() {
        topSeparator.alpha = max(0,min(1.0,tableView.contentOffset.y/22.0))
    }
}

extension SettingsSpeakerViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let settingViewModel = menuItems[(indexPath as NSIndexPath).row]
        self.delegate?.settingsSpeakerViewControllerDidSelectSpeakerSettingViewModel(settingViewModel, viewController: self)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    private func tableView(_ tableView: UITableView, editingStyleForRowAt editingStyleForRowAtIndexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return UITableViewCellEditingStyle.none
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        updateTopSeparatorAlpha()
    }
}
