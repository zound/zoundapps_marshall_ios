//
//  SettingsStreamingQualitySettingCell.swift
//  UrbanEars
//
//  Created by Robert Sandru on 24/11/2016.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import MinuetSDK

class SettingsStreamingQualitySettingCell: UITableViewCell {
    @IBOutlet weak var settingLabel: UILabel!
    @IBOutlet weak var radioButtonCircle: UIImageView!
    @IBOutlet weak var radioButtonInside: UIImageView!
    
    let settingVariable: Variable<StreamingQuality?> = Variable(nil)
    let viewModel = SettingsStreamingQualitySettingCellViewModel()
    var cellStreamingQuality: StreamingQuality? = nil
    var setting: StreamingQuality? {
        get { return settingVariable.value }
        set { settingVariable.value = newValue }
    }
    
    override func awakeFromNib() {
        self.contentView.backgroundColor = UIColor.clear
        self.backgroundColor = UIColor.clear
        
        settingLabel.font = Fonts.ListItemFont
        
        let customColorView = UIView()
        customColorView.backgroundColor = UIColor(white: 0.0, alpha: 0.24)
        self.selectedBackgroundView =  customColorView
        
        settingVariable
            .asDriver()
            .flatMap { Driver.from(optional: $0) }
            .map { $0.localizedDisplayName() }
            .drive(settingLabel.rx.text)
            .disposed(by: rx_disposeBag)
        
        viewModel.isSelected.asObservable()
            .subscribe(onNext: self.setRadioButtonState)
            .disposed(by: rx_disposeBag)
    }
    
    func setRadioButtonState(selected: Bool) {
        radioButtonInside.isHidden = !selected
    }
}
