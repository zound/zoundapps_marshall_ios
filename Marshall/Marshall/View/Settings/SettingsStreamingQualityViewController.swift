//
//  SettingsStreamingQualityViewController.swift
//  UrbanEars
//
//  Created by Robert Sandru on 24/11/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import MinuetSDK

protocol SettingsStreamingQualityViewControllerDelegate: class {
    func settingsStreamingQualityControllerDidRequestBack(_ viewController: SettingsStreamingQualityViewController)
}

class SettingsStreamingQualityViewController: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tableTopSeparatorHeight: NSLayoutConstraint!
    @IBOutlet weak var topSeparator: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    weak var delegate: SettingsStreamingQualityViewControllerDelegate?
    
    var viewModel: SettingsStreamingQualityViewModel?
    
    let menuItems: [StreamingQuality]
    
    required init?(coder aDecoder: NSCoder) {
        self.menuItems = [.normal, .high]
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let vm = viewModel else { return }
        
        titleLabel.attributedText = Fonts.PageTitleFont.AttributedPageTitleWithString(Localizations.Settings.StreamingQuality.Title)
        self.tableTopSeparatorHeight.constant = 0.5
        
        self.tableView.dataSource = self
        self.tableView.backgroundColor = UIColor.clear
        self.tableView.delegate = self
        
        descriptionLabel.attributedText = Fonts.UrbanEars.Regular(15).AttributedTextWithString(Localizations.Settings.StreamingQuality.Description, color: UIColor.white)
        
        updateSeparatorViewVisibility()
        
        if vm.streamingQuality.value == .high {
            self.tableView.selectRow(at: IndexPath(row: 1, section: 0), animated: false, scrollPosition: .none)
        } else {
            self.tableView.selectRow(at: IndexPath(row: 0, section: 0), animated: false, scrollPosition: .none)
        }
    }
    
    @IBAction func onBack(_ sender: AnyObject) {
        self.delegate?.settingsStreamingQualityControllerDidRequestBack(self)
    }
}

extension SettingsStreamingQualityViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let vm = viewModel else { return UITableViewCell() }
        let menuItem = menuItems[(indexPath as NSIndexPath).row]
        
        switch menuItem {
        case .high:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "settingsSettingHigh", for: indexPath) as? SettingsStreamingQualitySettingCell {
                cell.setting = menuItem
                cell.cellStreamingQuality = .high
                if vm.streamingQuality.value == .high {
                    cell.viewModel.isSelected.value = true
                } else {
                    cell.viewModel.isSelected.value = false
                }
                return cell
            }
            break
        case .normal:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "settingsSettingLow", for: indexPath) as? SettingsStreamingQualitySettingCell {
                cell.setting = menuItem
                cell.cellStreamingQuality = .normal
                if vm.streamingQuality.value == .normal {
                    cell.viewModel.isSelected.value = true
                } else {
                    cell.viewModel.isSelected.value = false
                }
                return cell
            }
            break
        }
        return UITableViewCell()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    func updateSeparatorViewVisibility() {
        topSeparator.alpha = max(0,min(1.0,tableView.contentOffset.y/22.0))
    }
}

extension SettingsStreamingQualityViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let vm = viewModel else { return }
        switch menuItems[(indexPath as NSIndexPath).row] {
        case .high: vm.change(quality: .high)
        case .normal: vm.change(quality: .normal)
        }
        
        let cell = tableView.cellForRow(at: indexPath) as! SettingsStreamingQualitySettingCell
        for cell in self.tableView.visibleCells {
            if let cell = cell as? SettingsStreamingQualitySettingCell {
                cell.viewModel.isSelected.value = false
            }
        }

        cell.viewModel.isSelected.value = true
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        updateSeparatorViewVisibility()
    }
}

extension StreamingQuality {
    func localizedDisplayName() -> String {
        switch self {
        case .high:
            return Localizations.Settings.StreamingQuality.High
        case .normal:
            return Localizations.Settings.StreamingQuality.NormalRecommended
        }
    }
}
