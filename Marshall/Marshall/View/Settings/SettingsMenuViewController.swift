//
//  AboutViewController.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 09/09/2016.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit

protocol SettingsMenuViewControllerDelegate: class {
    func settingsMenuViewControllerDidRequestBack(_ settingsMenuViewController: SettingsMenuViewController)
    func settingsMenuViewControllerDidRequestSettingsItem(_ settingsItem: SettingsItem, _ settingsMenuViewController: SettingsMenuViewController)
}

enum SettingsItem {
    case soloSpeakers
    case multiSpeakers
    case stereoPairing
    case multiStreamingQuality
    
    var localizedDisplayName: String {
        switch  self {
        case .soloSpeakers: return Localizations.Settings.RootMenu.SoloSpeakers
        case .multiSpeakers: return Localizations.Settings.RootMenu.MultiSpeakers
        case .stereoPairing: return Localizations.Settings.MultiMenu.StereoPairing
        case .multiStreamingQuality: return Localizations.Settings.MultiMenu.MultiStreamingQuality
        }
    }
}

class SettingsMenuViewController: UIViewController {
    @IBOutlet weak var titleLabel: UILabel?
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableTopSeparatorHeight: NSLayoutConstraint!

    weak var delegate: SettingsMenuViewControllerDelegate?
    
    var menuItems: [SettingsItem]
    var menuTitle: String? {
        didSet {
            updateTitle()
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
    
    required init?(coder aDecoder: NSCoder) {
        self.menuItems = []
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        updateTitle()
        self.tableTopSeparatorHeight.constant = 0.5
        
        self.tableView.dataSource = self
        self.tableView.backgroundColor = UIColor.clear
        self.tableView.delegate = self
        
        self.tableView.separatorStyle = .none
        
        updateSeparatorViewVisibility()
        
        if let count = self.navigationController?.viewControllers.count, count > 1 {
            self.backButton.setImage(UIImage(named: "back"), for: .normal)
        } else {
            self.backButton.setImage(UIImage(named: "close"), for: .normal)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.reloadData()
    }
    
    @IBAction func onBack(_ sender: AnyObject) {
        self.delegate?.settingsMenuViewControllerDidRequestBack(self)
    }
    
    func updateTitle() {
        if let title = menuTitle {
            titleLabel?.attributedText = Fonts.PageTitleFont.AttributedPageTitleWithString(title.uppercased())
        } else {
            titleLabel?.attributedText = Fonts.PageTitleFont.AttributedPageTitleWithString(Localizations.Settings.RootMenu.Title.uppercased())
        }
    }
    
    func updateSeparatorViewVisibility() {
        separatorView.alpha = max(0,min(1.0,tableView.contentOffset.y/22.0))
    }
}



extension SettingsMenuViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let settingsItem = menuItems[(indexPath as NSIndexPath).row]

        if let cell = tableView.dequeueReusableCell(withIdentifier: "settingsMenuItem", for: indexPath) as? SettingsMenuCell {
            cell.settingsItem = settingsItem
            return cell
        }
        
        return UITableViewCell()
    }
    
}
extension SettingsMenuViewController: UITableViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        updateSeparatorViewVisibility()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let menuItem = menuItems[(indexPath as NSIndexPath).row]
        self.delegate?.settingsMenuViewControllerDidRequestSettingsItem(menuItem, self)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
