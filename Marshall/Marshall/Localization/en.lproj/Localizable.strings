/* Localizable.strings
  UrbanEars

  Created by Raul Andrisan on 11/07/16.
  Copyright © 2016 Zound Industries. All rights reserved. */

"appwide.note" = "NOTE";

// since "continue" is reserved keyword in Swift, then Laurine adds "_" in front of the property
"appwide.continue" = "CONTINUE";

"appwide.done" = "DONE";

"appwide.back" = "BACK";

"appwide.next" = "NEXT";

"appwide.cancel" = "CANCEL";

"appwide.skip" = "SKIP";

"appwide.go_to_website" = "GO TO WEBSITE";

"about.eula.content" = "The full version of the End User License Agreement can be viewed at www.marshallheadphones.com.";

"about.eula.title" = "End User License Agreement";

"about.foss.content" = "The full version of the Free and Open Source Software can be viewed at www.marshallheadphones.com.";

"about.foss.title" = "FOSS";

"about.privacy_policy.title" = "Privacy Policy";

"about.menu_item.eula" = "End User License Agreement";

"about.menu_item.foss" = "Free and Open Source Software";

"about.title" = "ABOUT";

"about.version" = "Marshall Multi-Room\nVersion %@ (%@)";

"airplay_source.content" = "With AirPlay you can stream any music source directly to your speaker via the Control Center of any iOS device.";

"browse_radio.search_placeholder" = "Search radio stations";

"browse_radio.title" = "BROWSE RADIO";

"cast_info.buttons.get_google_cast" = "GET GOOGLE HOME APP";

"cast_info.buttons.learn_more" = "LEARN MORE";

"cast_info.content" = "This speaker is playing via Chromecast built-in.\n\nPlease use the Google Home app to set up groups for Cast Multi-room\n\nYour multi-room groups will show up under the Cast button in your music app.";

"cast_switch.content" = "Moving this speaker to Multi Mode will end this Chromecast built-in Session. \n\nPlease use the Google Home app to set up groups for Cast Multi-Room";

"unlocked_module.content" = "%@ is a prototype unit and will therefore stop working by the end of 2017. \n\nPlease get in touch with your point of contact at Zound Industries.";

"CFBundleDisplayName" = "Multi-Room";

"CFBundleName" = "Multi-Room";

"google_cast_source.content" = "Your speaker is available under the Cast icon in various apps.";

"google_cast_source.get_cast_apps" = "FIND CAST ENABLED APPS";

"google_cast_source.note" = "NOTE\nTo play music from multiple speakers together via Chromecast built-in, you need to set up a Multi group in the Google Home app.";

"group_full.content" = "You are trying to add a sixth speaker \ninto Multi Mode. \n\nThough we are happy you have bought \nso many speakers, at the moment the Marshall Multi-Room system only support Multi Mode between five speakers in the same Wi-Fi network.";

"help.contact.button.send_email" = "CONTACT SUPPORT";

"help.contact.content" = "You can visit our website at www.marshallheadphones.com \nor contact our support.";

"help.contact.title" = "CONTACT";

"help.menu_item.contact" = "Contact";

"help.menu_item.online_manual" = "Online Manual";

"help.menu_item.quick_guide" = "Quick Guide";

"help.online_manual_content" = "User manuals for Acton, Stanmore and Woburn are available in several languages on the Marshall website.";

"help.online_manual.title" = "ONLINE MANUAL";

"help.quick_guide.menu_item.presets" = "Presets";

"help.quick_guide.menu_item.solo_multi" = "Single/Multi";

"help.quick_guide.menu_item.speaker_knobs" = "Speaker Controls";

"help.quick_guide.title" = "QUICK GUIDE";

"help.title" = "HELP";

"home.speaker.playing_source" = "%@";

"home.speaker.switch_label.multi" = "MULTI";

"home.speaker.switch_label.solo" = "SINGLE";

"loading.loading_speakers" = "Searching for Multi-Room speakers…";

"menu.about" = "About";

"menu.add_speaker" = "Setup";

"menu.help" = "Help";

"menu.shop" = "Shop";

"menu.speaker_settings" = "Settings";

"no_speakers.buttons.refresh" = "REFRESH";

"no_speakers.buttons.set_up_speakers" = "SETUP";

"no_speakers.description" = "It appears that there are no speakers installed on this Wi-Fi network.";

"no_wifi.buttons.go_to_settings" = "GO TO SETTINGS";

"no_wifi.description" = "There's no Wi-Fi connection available \non your device. Check your connection and then try again.";

"no_wifi.title" = "SORRY, NO SIGNAL";

"player.aux.activate" = "ACTIVATE";

"player.aux.activated" = "ACTIVATED";

"player.aux.audio_active" = "Audio Active";

"player.bluetooth.activate" = "ACTIVATE";

"player.bluetooth.activated" = "ACTIVATED";

"player.bluetooth.connected" = "Bluetooth - Connected";

"player.bluetooth.connected_to" = "Connected to";

"player.bluetooth.enter_pairing" = "PAIRING MODE";

"player.bluetooth.idle" = "Idle";

"player.bluetooth.waiting_to_pair" = "WAITING TO PAIR";

"player.cloud.airplay" = "AIRPLAY";

"player.cloud.google_cast" = "CHROMECAST BUILT-IN";

"player.cloud.or_play_mobile_app" = "Play through your mobile app";

"player.cloud.play_internet_radio" = "INTERNET RADIO";

"player.cloud.spotify_connect" = "SPOTIFY";

"player.connecting.reconnecting" = "Reconnecting…";

"player.now_playing.browse_stations" = "BROWSE RADIO";

"player.now_playing.save_preset" = "ADD TO PRESET";

"airplay_source.open_airplay" = "OPEN APPLE MUSIC";

"player.preset.empty_placeholder" = "Empty Preset";

"player.preset.notification.add" = "Preset added";

"player.preset.notification.add_playlist" = "Playlist added";

"player.preset.notification.add_radio_station" = "Radio station added";

"player.preset.notification.addfailed" = "Adding preset failed";

"player.preset.notification.addfailedinternetradio" = "Adding radio station failed";

"player.preset.notification.addfailedplaylist" = "Adding playlist failed";

"player.preset.notification.delete" = "Preset deleted";

"player.preset.notification.delete_internet_radio" = "Radio station deleted";

"player.preset.notification.delete_playlist" = "Playlist deleted";

"player.preset.notification.empty" = "Preset is empty";

"player.preset.notification.play" = "Playing preset";

"player.preset.spotify_login_error.content" = "We could not connect your speaker\nto your Spotify account. \n\nPlease try again or go back.";

"player.preset.spotify_login_error.retry_button" = "Try Again";

"player.preset.spotify_login_error.title" = "SOMETHING WENT WRONG";

"player.preset.spotify_premium_error.content" = "Your account is not premium. A premium account is required to stream Spotify on your speaker.";

"player.preset.spotify_premium_error.dismiss_button" = "OK";

"player.preset.spotify_premium_error.title" = "Spotify Error";

"player.session_lost.buttons.reconnect" = "RECONNECT";

"player.session_lost.generic_error_to_speaker" = "An unknown error occurred while communicating with %@. Try reconnecting by pressing \"Reconnect\"";

"player.session_lost.network_connection_lost_to_speaker" = "%@ has lost Wi-Fi connection. Press \"Reconnect\" and get back to grooving.";

"player.session_lost.speaker_not_connected" = "Speaker is not yet connected";

"player.session_lost.timeout" = "Connection to %@ has timed out. Press \"Reconnect\" to try again.";

"player.session_lost.user_is_controlling_speaker" = "%@ is being controlled by another user.  Ready to be in charge? \nPress \"Reconnect\" to take over control of the speaker.";

"presets_disabled.buttons.read_more" = "LEARN MORE";

"presets_disabled.content" = "To add a preset you have to play either:\n\n- Music via Spotify Connect\n- Internet Radio (found under  [cloud_image] )\n\nWhen one of these sources are playing, the add button ( [plus_image] ) will be enabled and by tapping it you will add the playlist/radio station that is currently playing.";

"presets_disabled.title" = "HOW TO ADD A PRESET";

"save_preset.title" = "ADD TO PRESET";

"settings.about.cast_version" = "Chromecast built-in version:";

"settings.about.ip" = "IP:";

"settings.about.mac" = "MAC:";

"settings.about.name" = "Name:";

"settings.about.model" = "Model:";

"settings.about.title" = "ABOUT THIS SPEAKER";

"settings.about.wi_fi_network" = "Wi-Fi Network:";

"settings.list.title" = "SINGLE SPEAKERS";

"settings.speaker.about" = "About This Speaker";

"settings.speaker.rename" = "Rename Speaker";

"settings.speaker.streaming_quality" = "Multi Streaming Quality";

"settings.streaming_quality.cellLabel" = "Multi Streaming Quality";

"settings.streaming_quality.title" = "MULTI STREAMING QUALITY";

"settings.streaming_quality.description" = "The normal setting dynamically adjust quality to the network conditions and is optimal if you experience interuptions. The high setting always uses the maximum quality.\n\nThe multi streaming quality settings only affect Multi Mode.";

"settings.streaming_quality.high" = "High";

"settings.streaming_quality.normal_recommended" = "Normal (Recommended)";

"setup.connect_spotify.buttons.connect_to_spotify" = "CONNECT TO SPOTIFY";

"setup.connect_spotify.paragraph1.content" = "It’s easy to get Spotify playing on \nyour speaker.\n\nOnce you’re connected, open the Spotify app to play and control music wirelessly.";

"setup.connect_spotify.paragraph1.title" = "Connect to Spotify";

"setup.done.content" = "You're all ready to enjoy the Marshall Wireless Multi-Room System.";

"setup.done.title" = "All Finished";

"setup.failed_boot.message" = "When both LED lights are steady \nthe speaker is booting up. This will take around 20 seconds.";

"setup.failed_connect.message" = "Please make sure your speaker \nis connected to the mains.";

"setup.failed_reset.message" = "Still experiencing issues? Find the quick guide in the help section to learn more about the setup.";

"setup.failed_setup.buttons.no" = "NO";

"setup.failed_setup.buttons.yes" = "YES";

"setup.failed_setup.message" = "When all the LED lights on the \nSource Knob are blinking, \nthe speaker is in setup mode.\n\nAre the LED lights blinking?";

"setup.list.found_speakers_with_count" = "Alright. %d speaker(s) were found. \nSelect one to continue.";

"setup.loading.looking_for_speakers" = "Searching for Multi-Room speakers…";

"setup.pick_presets.content" = "Connect to Spotify and internet radio to populate your presets.";

"setup.pick_presets.internet_radio_description" = "Over 30,000 stations from the whole planet";

"setup.pick_presets.internet_radio_title" = "Internet Radio";

"setup.pick_presets.spotify_description" = "Playlists, Albums, Artists, Genres & Podcasts";

"setup.pick_presets.title" = "Adding Presets";

"setup.presets_fail.content" = "There seemed to be some problem when adding the presets to your speaker. \n\nPlease try again or go back. You can always add presets later.";

"setup.presets_list.title" = "Meet Your Presets";

"setup.presets_loading.adding_preset_title" = "Adding %@";

"setup.presets_loading.done" = "Done";

"setup.presets_loading.title" = "Adding Presets";

"setup.presets_tutorial.content" = "To play a preset, turn the knob to a number and press.\n\nTo save a preset, turn the knob to a number and long-press while listening to music via Spotify or internet radio.";

"setup.presets_tutorial.title" = "Using Presets";

"setup.spotify_success.title" = "All done";

"setup.spotify_success.your_spotify_account" = "You are now signed in to Spotify";

"setup.tutorial.cast.content_top" = "Chromecast built-in allows you to stream your music, internet radio or podcasts from your favorite music apps to your speakers.";

"setup.tutorial.cast.title" = "Chromecast built-in";

"setup.tutorial.cloud.airplay" = "AirPlay";

"setup.tutorial.cloud.content" = "Look out for one of these symbols \nin your favourite music app. \nTap the symbol when it appears and \nselect your speaker. You’ll be rocking \nout in a matter of seconds.";

"setup.tutorial.cloud.google_cast" = "Chromecast built-in";

"setup.tutorial.cloud.spotify_connect" = "Spotify Connect";

"setup.tutorial.cloud.title" = "Wi-Fi";

"setup.tutorial.internet_radio.content" = "Browse and play over 30,000 internet radio stations from inside this app.\nInternet radio stations are located under\nthe Wi-Fi category of this app.";

"setup.tutorial.internet_radio.title" = "Internet Radio";

"setup.tutorial.multi.content" = "You can group Multi-Room Speakers together for a synchronized playdate. Set a speaker to Multi Mode via this app or by pressing the Single/Multi button on the speakers top panel. ";

"setup.tutorial.multi.title" = "MULTI MODE";

"spotify_source.buttons.get_spotify" = "GET SPOTIFY";

"spotify_source.buttons.open_spotify" = "OPEN SPOTIFY";

"spotify_source.instructions" = "1. \tOpen up the Spotify app on your phone, tablet or laptop.\n2. \tPlay a song and select Devices Available.\n3.\tSelect your device and start listening.";

"spotify_source.intro" = "Ready to play some music?\nListen to Spotify on %@ using the spotify app as a remote.";

"volume.buttons.mute_all" = "MUTE";

"volume.buttons.unmute_all" = "UNMUTE";

"volume.headers.master_volume_label" = "Multi Volume";

"volume.title" = "VOLUME";

"welcome.title" = "WELCOME";

"about.eula.website" = "https://www.marshallheadphones.com/multi-room-speaker-eula-en";

"about.privacy_policy.website" = "https://www.marshallheadphones.com/app-privacy-policy-en";

"welcome.buttons.accept" = "START";

"setup.presets_fail.title" = "PRESET NOT SAVED";

"setup.presets_fail.buttons.try_again" = "TRY AGAIN";

"help.contact.website" = "https://www.marshallheadphones.com";

"help.online_manual.website" = "https://www.marshallheadphones.com/multi-room-speaker-support";

"about.foss.website" = "https://www.marshallheadphones.com/multi-room-speaker-foss";

"volume.role.multi" = "MULTI";

"volume.role.solo" = "SINGLE";

"player.aux.mode_name" = "AUX";

"player.bluetooth.connect_device_advice" = "Do not forget to make sure the Bluetooth on your %@ is activated.";

"setup.presets_loading.preparing_presets" = "Retrieving presets";

"player.preset.login_spotify_advice" = "Tap here to connect to Spotify and the preset artwork will be updated.";

"player.cloud.internet_radio_mode_name" = "Internet Radio";

"player.cloud.google_cast_mode_name" = "Chromecast Built-in";

"player.preset.spotify_playlist" = "Spotify Playlist";

"player.carousel.playable_source.aux" = "AUX";

"player.carousel.playable_source.preset" = "Preset";

"player.carousel.playable_source.bluetooth" = "Bluetooth";

"player.carousel.playable_source.cloud" = "Wi-Fi";

"welcome.subtitle" = "Your Marshall Wireless system taps \ninto over 50 years of experience to bring home that live feeling.";

"help.contact.support_website" = "https://www.marshallheadphones.com/multi-room-speaker-contact";

"no_speakers.title" = "NO SPEAKERS FOUND";

"player.rca.mode_name" = "RCA";

"player.carousel.playable_source.rca" = "RCA";

"player.rca.activate" = "ACTIVATE";

"player.rca.activated" = "ACTIVATED";

"player.rca.audio_active" = "Audio Active";

"volume.volume_slider_label" = "Volume";

"volume.bass_slider_label" = "Bass";

"volume.treble_slider_label" = "Treble";

"volume.eq_button_show_text" = "EQ";

"volume.eq_button_hide_text" = "HIDE";

"settings.speaker.led_intensity" = "Light";

"setup.failed.title" = "SETUP GUIDE";

"setup.pick_presets.content_bottom" = "Let us add presets to your speaker \nso you can get started. You can always edit them later.";

"settings.led_adjuster.adjust_instruction" = "Adjust LED intensity on your speaker";

"settings.sounds.title" = "Sound";

"settings.sounds.bluetooth_connect" = "Bluetooth connect";

"settings.sounds.network_connect" = "Network connect";

"settings.sounds.preset_store" = "Preset store";

"settings.sounds.preset_fail" = "Preset fail";

"reconnecting.reconnecting_to" = "Reconnecting to %@";

"settings.about.build_version" = "System firmware version:";

"setup.list.multi_room" = "Multi-Room";

"setup.list.connected" = "Add Presets";

"setup.list.connect_to_wifi" = "Connect to Wi-Fi";

"setup.presets_initial.title" = "PRESETS";

"setup.presets_initial.content" = "Seven presets gives you immediate access to your favourite internet radio stations or artists and playlists on Spotify.";

"setup.presets_initial.buttons.next" = "ADDING PRESETS";

"settings.root_menu.title" = "SETTINGS";

"settings.about.update_speaker" = "UPDATE SPEAKER";

"settings.root_menu.solo_speakers" = "Single Speakers";

"settings.root_menu.multi_speakers" = "Multi Speakers";

"settings.multi_menu.stereo_pairing" = "Stereo Pairing";

"settings.multi_menu.multi_streaming_quality" = "Multi Streaming Quality";

"player.session_lost.user_is_controlling_speaker_title" = "USER TAKEOVER";

"player.session_lost.network_connection_lost_to_speaker_title" = "NO Wi-Fi CONNECTION";

"updateavailable_module.content" = "%@ is not using the latest update for Multi-Room Speakers. \n\nUpdate it manually by going into the speaker\'s settings and select \"About this Speaker\". Alternatively, it will update automatically during the night.";

"settings.about.update.content" = "%@ is selected to be updated. \n\nWhen updating a Multi-Room Speaker it will temporarily be unavailable. Once done, it will appear again in the home screen.";

"settings.about.update.buttonLabel" = "UPDATE SPEAKER";





