//
//  Fonts.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 20/07/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {
    
    public func AttributedTextWithString(_ text: String, color: UIColor = UIColor.black, letterSpacing: CGFloat = 0, lineSpacing: CGFloat = 0, lineHeightMultiple: CGFloat = 0.0, shadow: Bool = false) -> NSAttributedString {
        
        var attributes:[NSAttributedString.Key : Any] = [NSAttributedString.Key(rawValue: NSAttributedString.Key.font.rawValue): self,
                                                         NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue): color]
        if letterSpacing != 0 {
            
            attributes[NSAttributedString.Key.kern] = letterSpacing
        }
        if lineSpacing != 0 {
            
            
            let paragraphStyle = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
            paragraphStyle.lineSpacing = lineSpacing
            if lineHeightMultiple != 0.0 {
                paragraphStyle.lineHeightMultiple = lineHeightMultiple
            }
            
            attributes[NSAttributedString.Key.paragraphStyle] = paragraphStyle
            
        }
        if shadow {
            let shadowObject = NSShadow()
            shadowObject.shadowOffset = CGSize(width: 0.5, height: 1.0)
            shadowObject.shadowBlurRadius = 1.0
            shadowObject.shadowColor = UIColor(white: 0.0, alpha: 0.5)
            attributes[NSAttributedString.Key.shadow] = shadowObject
        }
        
        return NSAttributedString(string: text, attributes: attributes)
    }
    
    public func AttributedPrimaryButtonWithString(_ text: String) -> NSAttributedString {
        
        let textColor = UIColor("#F2F2F2")
        return AttributedTextWithString(text, color: textColor, letterSpacing: 0.5, lineSpacing: 0, shadow: true)
    }
    
    public func AttributedSecondaryButtonWithString(_ text: String) -> NSAttributedString {
        
        let textColor = UIColor("#CDCDCD")
        return AttributedTextWithString(text, color: textColor, letterSpacing: 1.5, lineSpacing: 0)
    }
    
    
    public func AttributedPageTitleWithString(_ text: String) -> NSAttributedString {
     
        return AttributedTextWithString(text, color: UIColor.white, letterSpacing: 1.0, lineSpacing: 0)
    }
    
}


public struct Fonts {
    
}

public extension Fonts {
 
    public static var ButtonFont: UIFont = UrbanEars.Bold(20)
    public static var SecondaryButtonFont: UIFont = UrbanEars.Regular(13)
    public static var PageTitleFont: UIFont = UrbanEars.Bold(18)
    public static var MainContentFont: UIFont = UrbanEars.Regular(17)
    public static var ListItemFont: UIFont = UrbanEars.Regular(17)
    
    public struct UrbanEars {
        
        
        public static func ExtraLight(_ fontSize: CGFloat) -> UIFont {
            
            return UIFont(name: "RobotoCondensed-Light", size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)
        }
        
        public static func Light(_ fontSize: CGFloat) -> UIFont {
            
            return UIFont(name: "RobotoCondensed-Light", size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)
        }
        
        public static func Regular(_ fontSize: CGFloat) -> UIFont {
            
            return UIFont(name: "RobotoCondensed-Regular", size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)
        }
        
        public static func Medium(_ fontSize: CGFloat) -> UIFont {
            
            return UIFont(name: "RobotoCondensed-Regular", size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)
        }
        
        public static func Bold(_ fontSize: CGFloat) -> UIFont {
            
            return UIFont(name: "RobotoCondensed-Bold", size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)
        }
        
        public static func ExtraBold(_ fontSize: CGFloat) -> UIFont {
            
            return UIFont(name: "RobotoCondensed-Bold", size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)
        }
        
        public static func Black(_ fontSize: CGFloat) -> UIFont {
            
            return UIFont(name: "RobotoCondensed-Bold", size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)
        }
        
    }
}

