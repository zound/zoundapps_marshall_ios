//
//  ExternalConfig.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 10/06/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import ObjectMapper
import MinuetSDK

public class ExternalConfig {
    public static let sharedInstance  = ExternalConfig()
    public var speakerImages: SpeakerImages?
    public var presets: SpeakerInitialPresets?
    
    private init() {
        guard let localSpeakerImagesJSONLocation = Bundle.main.url(forResource: "speaker_images", withExtension: "json"),
            let speakerImagesJSONString = try? String(contentsOf: localSpeakerImagesJSONLocation) else { return }
        
        guard let localPresetsJSONLocation = Bundle.main.url(forResource: "marshall_radio_presets", withExtension: "json"),
            let localPresetsJSONString = try? String(contentsOf: localPresetsJSONLocation) else { return }
        
        speakerImages = Mapper<SpeakerImages>()
            .map(JSONString: speakerImagesJSONString)
        
        presets = Mapper<SpeakerInitialPresets>()
            .map(JSONString: localPresetsJSONString)
    }
    
    public func speakerImageForColor(_ color: String?) -> SpeakerImage? {
        return ExternalConfig.sharedInstance.speakerImages?.speakerImages?.filter({ $0.color == color }).first
    }
}
