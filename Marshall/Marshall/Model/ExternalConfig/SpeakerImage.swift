//
//  SpeakerImage.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 10/06/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import ObjectMapper
import Foundation

public struct SpeakerImage: Mappable {
    public var ssid: String?
    public var color: String?
    public var name: String?
    public var displayModel: String?
    public var displayColor: String?
    public var listItemImageName: String?
    public var listItemSmallImageName: String?
    public var heroImageName: String?
    public var connectable: Bool = false
    
    public init?(map: Map) {}
    public mutating func mapping(map: Map) {
        ssid                   <- map["ssid"]
        color                  <- map["color"]
        name                   <- map["name"]
        displayModel           <- map["displayModel"]
        displayColor           <- map["displayColor"]
        listItemImageName      <- map["list_item"]
        listItemSmallImageName <- map["list_item_small"]
        heroImageName          <- map["hero"]
        connectable            <- map["connectable"]
        
    }
}

