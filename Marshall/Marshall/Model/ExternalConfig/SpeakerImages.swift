//
//  SpeakerImages.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 10/06/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import ObjectMapper

public struct SpeakerImages: Mappable {
    public var speakerImages: [SpeakerImage]?
    
    public init?(map: Map) {}
    public mutating func mapping(map: Map) {
        speakerImages <- map["speakers"]
    }
}
