//
//  NowPlayingState+CurrentlyPlaying.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 26/04/2017.
//  Copyright © 2017 Zound Industries. All rights reserved.
//

import Foundation
import MinuetSDK

extension NowPlayingState {
    var currentNowPlaying: String? {
        var currentNowPlaying: String?
        if self.presetIndex == nil && self.modeIndex == nil {
            currentNowPlaying = nil
        }
        if self.presetIndex == nil && self.modeIndex != nil {
            currentNowPlaying = self.modes.filter({$0.key == self.modeIndex!}).first?.playableTitle
        }
        if self.presetIndex != nil {
            
            currentNowPlaying = "\(Localizations.Player.Carousel.PlayableSource.Preset) \(self.presetIndex! + 1)"
        }
        
        return currentNowPlaying
    }
}
