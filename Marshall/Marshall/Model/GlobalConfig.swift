//
//  GlobalConfig.swift
//  Marshall
//
//  Created by Dolewski Bartosz A (Ext) on 26/11/2018.
//  Copyright © 2018 Zound Industries. All rights reserved.
//

import Foundation

struct GlobalConfig {
    private init() {}

    /// Official website URL for Marshall products
    static let officialWebsite = URL(string: "https://www.marshallheadphones.com")!

    struct BitBucket {
        /// Respository for Internet Radio stations stored on remote server (BitBucket)
        static let repo = URL(string: "https://bitbucket.org/zoundindustries/radiopresets/src/master/")!

        /// Path to configuration JSON file
        static let path = "marshall_radio_presets.json"
    }
}
