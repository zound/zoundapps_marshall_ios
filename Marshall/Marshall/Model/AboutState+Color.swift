//
//  AboutState+Color.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 26/04/2017.
//  Copyright © 2017 Zound Industries. All rights reserved.
//

import Foundation
import MinuetSDK

extension AboutState {
    public var modelName: String? {
        guard let color = color, let displayModelName = ExternalConfig.sharedInstance.speakerImageForColor(color)?.displayModel else { return "" }
        return displayModelName
    }
    
    public var colorName: String? {
        guard let displayColorName = ExternalConfig.sharedInstance.speakerImageForColor(color)?.displayColor else { return "" }
        return displayColorName
    }
}
