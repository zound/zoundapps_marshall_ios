//
//  Mode+PlayableTitle.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 26/04/2017.
//  Copyright © 2017 Zound Industries. All rights reserved.
//

import Foundation
import MinuetSDK

extension Mode {
    var playableTitle: String {
        switch id {
        case "IR": return Localizations.Player.Cloud.InternetRadioModeName
        case "Google Cast": return Localizations.Player.Cloud.GoogleCastModeName
        case "Airplay": return "AirPlay"
        case "AUXIN": return "AUX"
        default: return label
        }
    }
}
