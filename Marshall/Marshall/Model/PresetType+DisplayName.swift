//
//  PlayableItem+DisplayName.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 26/04/2017.
//  Copyright © 2017 Zound Industries. All rights reserved.
//

import Foundation
import MinuetSDK

extension PresetType {
    var displayName: String {
        switch self {
        case .spotify:
            return Localizations.Player.Preset.SpotifyPlaylist
        case .internetRadio:
            return Localizations.Player.Cloud.InternetRadioModeName
        case .unknown:
            return "Preset"
        }
    }
}
