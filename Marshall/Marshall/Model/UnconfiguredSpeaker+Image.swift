//
//  UnconfiguredSpeaker+Image.swift
//  UrbanEars
//
//  Created by Raul Andrisan on 26/04/2017.
//  Copyright © 2017 Zound Industries. All rights reserved.
//

import Foundation
import MinuetSDK

extension UnconfiguredSpeaker {
    public var speakerImage: SpeakerImage? {
        return ExternalConfig.sharedInstance.speakerImages?.speakerImages?.filter({ speakerImage in ssid.hasPrefix(speakerImage.ssid!) }).first
    }
    
    public var completeName: String {
        guard let speakerImage = speakerImage else { return ssid }
        return "\(speakerImage.name!) \(nameIndex)"
    }
}
