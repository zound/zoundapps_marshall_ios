//
//  TodayPlayerViewModel.swift
//  Marshall
//
//  Created by Raul Andrisan on 04/07/2017.
//  Copyright © 2017 Zound Industries. All rights reserved.
//

import Foundation
import MinuetSDK
import Zound
import RxSwift

class TodayPlayerViewModel {
    let audioSystem: AudioSystem
    let provider: SpeakerProvider
    let disposeBag = DisposeBag()
    
    var connectedGroup: SpeakerGroup? = nil
    
    init(audioSystem: AudioSystem, speakerProvider: SpeakerProvider) {
        self.audioSystem = audioSystem
        self.provider = speakerProvider
    }
    
    func connect(toSpeakerGroup group: SpeakerGroup) {
    }
}
