//
//  TodayPlayerViewController.swift
//  Marshall
//
//  Created by Raul Andrisan on 04/07/2017.
//  Copyright © 2017 Zound Industries. All rights reserved.
//

import Foundation
import UIKit
import MinuetSDK

class TodayPlayerViewController: UIViewController {
    @IBOutlet weak var tabsStackView: UIStackView!
    @IBOutlet weak var controlsStackView: UIStackView!
    @IBOutlet weak var playInfoStackView: UIStackView!
    @IBOutlet weak var sourceLabel: UILabel!
    @IBOutlet weak var nowPlayingLabel: UILabel!
    @IBOutlet weak var artworkImage: UIImageView!
    
    var buttonToGroup: [UIButton: SpeakerGroup] = [:]
    
    var viewModel: TodayPlayerViewModel!
    
    var compactMode: Bool = true {
        didSet { updateForCompactMode(compactMode) }
    }
    
    override func viewDidLoad() {
        guard let vm = viewModel else { return }
        
        vm.audioSystem.groups.asObservable()
            .subscribe(weak: self, onNext:TodayPlayerViewController.updateForGroups)
            .disposed(by: rx_disposeBag)
    }
    
    func updateForGroups(_ groups: [SpeakerGroup]) {
        buttonToGroup.removeAll()
        
        tabsStackView.arrangedSubviews.for_each { $0.removeFromSuperview() }
        
        for group in groups.sorted(by: { left, right in left.groupName! > right.groupName! }).prefix(3){
            let button = UIButton(type: .system)
            
            if let groupName = group.groupName {
                button.setTitle(withText: groupName, color: UIColor.white)
            }

            button.titleLabel?.adjustsFontSizeToFitWidth = true
            button.titleLabel?.minimumScaleFactor = 0.5
            button.titleLabel?.lineBreakMode = .byTruncatingTail
            button.translatesAutoresizingMaskIntoConstraints = true
            button.addTarget(self, action: #selector(didPressButton), for: .touchUpInside)

            self.tabsStackView.addSubview(button)
            self.tabsStackView.addArrangedSubview(button)

            buttonToGroup[button] = group
        }
        
    }
    
    @objc private func didPressButton(sender: UIButton) {
        for view in self.tabsStackView.arrangedSubviews {
            if let button = view as? UIButton {
                button.setTitle(withText: button.titleLabel?.text, color: UIColor.white)
            }
        }
        
        sender.setTitle(withText: sender.titleLabel?.text, color: UIColor("#AD915C"))
        
        if let group = buttonToGroup[sender] {
            if let vm = viewModel {
                vm.connect(toSpeakerGroup: group)
            }
        }
    }
    
    private func updateForCompactMode(_ compactMode: Bool) {
        if compactMode {
            artworkImage.isHidden = true
            playInfoStackView.spacing = 0
        } else {
            artworkImage.isHidden = false
            playInfoStackView.spacing = 8
        }
    }    
}

extension UIButton {
    func setTitle(withText text: String?, color: UIColor) {
        let text = Fonts.UrbanEars.Light(12).AttributedTextWithString(text?.uppercased() ?? "", color: color, letterSpacing: 0.2,  shadow: true)
        UIView.setAnimationsEnabled(false)
        self.setAttributedTitle(text, for: .normal)
        self.layoutIfNeeded()
        UIView.setAnimationsEnabled(true)
    }
}
