//
//  TodayCoordinatorViewController.swift
//  MarshallTodayExtension
//
//  Created by Raul Andrisan on 03/07/2017.
//  Copyright © 2017 Zound Industries. All rights reserved.
//

import UIKit
import NotificationCenter
import Cartography
import MinuetSDK

class TodayCoordinatorViewController: UIViewController, NCWidgetProviding {
    
    var playerViewController: TodayPlayerViewController? = nil
    
    let provider: SpeakerProvider
    let discoveryService: DiscoveryServiceType
    let audioSystem: AudioSystem
    
    required init?(coder aDecoder: NSCoder) {
        self.provider = SpeakerProvider()
        self.discoveryService = DiscoveryService()
        self.audioSystem = AudioSystem(discoveryService: self.discoveryService, provider: self.provider)
        self.audioSystem.groupUpdatesActive = true
        
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.discoveryService.search()
        
        if #available(iOSApplicationExtension 10.0, *) {
            self.extensionContext?.widgetLargestAvailableDisplayMode = .expanded
        } else {
            self.preferredContentSize.height = 250
        }
        
        guard let vc = UIStoryboard(name: "TodayExtension", bundle: nil).instantiateViewController(withIdentifier: "todayPlayer") as? TodayPlayerViewController else {
            return
        }
        
        vc.viewModel = TodayPlayerViewModel(audioSystem: audioSystem, speakerProvider: provider)
        self.addChildViewController(vc)
        vc.view.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(vc.view)
        constrain(vc.view) { view in
            view.edges == view.superview!.edges
        }
        vc.didMove(toParentViewController: self)
        playerViewController = vc
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func widgetPerformUpdate(completionHandler: (@escaping (NCUpdateResult) -> Void)) {
        // Perform any setup necessary in order to update the view.
        
        // If an error is encountered, use NCUpdateResult.Failed
        // If there's no update required, use NCUpdateResult.NoData
        // If there's an update, use NCUpdateResult.NewData
        
        completionHandler(NCUpdateResult.newData)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        print("view will transition to \(NSStringFromCGSize(size))")
        
        coordinator.animate(alongsideTransition: { [weak self] context in
            if #available(iOSApplicationExtension 10.0, *) {
                if let displayMode = self?.extensionContext?.widgetActiveDisplayMode {
                    self?.playerViewController?.compactMode = ( displayMode == .compact) ? true : false
                } else {
                    self?.playerViewController?.compactMode = false
                }
            } else {
                self?.playerViewController?.compactMode = false
            }
        }) { context in
        }
    }
    
    @available(iOSApplicationExtension 10.0, *)
    func widgetActiveDisplayModeDidChange(_ activeDisplayMode: NCWidgetDisplayMode, withMaximumSize maxSize: CGSize) {
        if activeDisplayMode == .compact {
            // Changed to compact mode
            self.preferredContentSize = maxSize
        } else {
            // Changed to expanded mode
            self.preferredContentSize = CGSize(width: maxSize.width, height: 250)
        }
        print("max size is \(NSStringFromCGSize(maxSize))")
    }
}
