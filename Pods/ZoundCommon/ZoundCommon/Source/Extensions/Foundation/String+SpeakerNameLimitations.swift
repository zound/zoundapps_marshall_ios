//
//  String+SpeakerNameLimitations.swift
//  ZoundCommon
//
//  Created by Raul Andrisan on 10/08/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation


extension String {
    public var allCharactersValidAsSpeakerName: Bool {
        let alphaNumeric = CharacterSet.alphanumerics
        let acceptedInput = (CharacterSet(charactersIn: "[]{}(e)<>!?$£€&%#¤@~^*_-+=|/\\\"'`;:,. ") as NSCharacterSet).mutableCopy() as! NSMutableCharacterSet
        acceptedInput .formUnion(with: alphaNumeric)
        let unacceptedInput = acceptedInput.inverted
        let allCharactersValid = self.components(separatedBy: unacceptedInput).count <= 1
        
        return allCharactersValid
    }
}
