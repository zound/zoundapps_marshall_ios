//
//  NSLocale+Language.swift
//  Zound
//
//  Created by Dolewski Bartosz A (Ext) on 13/11/2018.
//  Copyright © 2018 Zound Industries. All rights reserved.
//

import Foundation

public extension NSLocale {
    public static var language: String {
        var language = NSLocale.preferredLanguages[0]
        if language.count > 2 {
            let index = language.index(language.startIndex, offsetBy: 2)
            language = String(language[..<index])
        }
        
        return language
    }
}
