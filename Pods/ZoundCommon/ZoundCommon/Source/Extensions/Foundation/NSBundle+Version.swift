//
//  NSBundle+Version.swift
//  ZoundCommon
//
//  Created by Raul Andrisan on 09/09/2016.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation

public extension Bundle {
    public var releaseVersionNumber: String? {
        return self.infoDictionary?["CFBundleShortVersionString"] as? String
    }
    
    public var buildVersionNumber: String? {
        return self.infoDictionary?["CFBundleVersion"] as? String
    }
}
