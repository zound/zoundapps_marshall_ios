//
//  NSObject+RunAfterDelay.swift
//  ZoundCommon
//
//  Created by Raul Andrisan on 16/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation

public protocol RunAfterDelay {
    func runAfterDelay(_ delay: TimeInterval, block: @escaping () -> () )
}

public extension RunAfterDelay {
    public func runAfterDelay(_ delay: TimeInterval, block: @escaping () -> () ) {
        let time = DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: time, execute: block)
    }
}

extension NSObject: RunAfterDelay {
    public func runAfterDelay(_ delay: TimeInterval, block: @escaping () -> () ) {
        let time = DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: time, execute: block)
    }
}
