//
//  UIImage+Color.swift
//  ZoundCommon
//
//  Created by Raul Andrisan on 31/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit

public extension UIImage {
    convenience init(color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) {
        let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.init(cgImage: (image?.cgImage!)!, scale: image!.scale, orientation: image!.imageOrientation)
    }  
}
