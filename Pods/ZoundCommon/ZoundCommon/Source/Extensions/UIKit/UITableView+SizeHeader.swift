//
//  UITableView+SizeHeader.swift
//  Zound
//
//  Created by Raul Andrisan on 12/05/2017.
//  Copyright © 2017 Zound Industries. All rights reserved.
//

import UIKit
import CoreGraphics

public extension UITableView {
    public func sizeHeaderToFit(preferredWidth: CGFloat) {
        guard let headerView = self.tableHeaderView else { return }
        
        headerView.translatesAutoresizingMaskIntoConstraints = false
        let layout = NSLayoutConstraint(
            item: headerView,
            attribute: .width,
            relatedBy: .equal,
            toItem: nil,
            attribute:
            .notAnAttribute,
            multiplier: 1,
            constant: preferredWidth)
        
        headerView.addConstraint(layout)
        
        let height = headerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
        headerView.frame = CGRect(x:0, y:0, width:preferredWidth, height:height)
        
        headerView.removeConstraint(layout)
        headerView.translatesAutoresizingMaskIntoConstraints = true
        
        self.tableHeaderView = headerView
    }
}
