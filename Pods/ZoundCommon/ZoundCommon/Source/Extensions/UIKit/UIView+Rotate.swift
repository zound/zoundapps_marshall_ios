//
//  UIView+Rotate.swift
//  ZoundCommon
//
//  Created by Raul Andrisan on 31/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit

extension UIView{
    public func rotate(_ rotationDuration: Double = 1.5) {
        self.transform = CGAffineTransform.identity
        
        let rotation : CABasicAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
        rotation.fromValue = NSNumber(value: 0 as Double)
        rotation.toValue = NSNumber(value: Double.pi * 2 as Double)
    
        rotation.duration = rotationDuration
        rotation.isCumulative = true
        rotation.repeatCount = Float.infinity
        rotation.isRemovedOnCompletion = false
        rotation.fillMode = CAMediaTimingFillMode.forwards
        self.layer.add(rotation, forKey: "rotationAnimation")
    }
    
    public func stopRotation() {
        self.layer.removeAnimation(forKey: "rotationAnimation")
    }
}
