//
//  UIImage+Desaturate.swift
//  Zound
//
//  Created by Raul Andrisan on 12/05/2017.
//  Copyright © 2017 Zound Industries. All rights reserved.
//

import Foundation
import UIKit

public extension UIImage {
    public var desaturated: UIImage? {
        guard let cgImage = self.cgImage else { return nil }
        
        let ciimage = CIImage.init(cgImage: cgImage)
        let filter = CIFilter.init(name: "CIColorControls")
        filter?.setValue(ciimage, forKey: kCIInputImageKey)
        filter?.setValue(0.0, forKey: kCIInputSaturationKey)
        let result = filter?.value(forKey: kCIOutputImageKey) as! CIImage
        let cgimage = CIContext.init(options: nil).createCGImage(result, from: result.extent)
        let image = UIImage(cgImage: cgimage!, scale: self.scale, orientation: self.imageOrientation)
        
        return image
    }
}
