//
//  UIImage+Alpha.swift
//  ZoundCommon
//
//  Created by Robert Sandru on 13/10/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit

extension UIImage {
    func alpha(value: CGFloat) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        draw(at: CGPoint.zero, blendMode: .normal, alpha: value)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}
