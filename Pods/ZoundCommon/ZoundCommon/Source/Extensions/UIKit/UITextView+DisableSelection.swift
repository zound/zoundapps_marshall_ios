//
//  UITextView+DisableSelection.swift
//  ZoundCommon
//
//  Created by Raul Andrisan on 21/07/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit

extension UITextView {
    public func disableTextSelection() {
        //fix for selectable text in textview while maintaining clickable linksr
        let textViewGestureRecognizers = self.gestureRecognizers
        var mutableArrayOfGestureRecognizers:[UIGestureRecognizer] = []
        
        for gestureRecognizer in textViewGestureRecognizers! {
            let tapAndHalfGestureClass: AnyClass? = NSClassFromString("UITapAndAHalf"+"Recognizer")
            
            if gestureRecognizer.isKind(of: UILongPressGestureRecognizer.self) {
                let longPressGestureRecognizer = gestureRecognizer as! UILongPressGestureRecognizer
                if longPressGestureRecognizer.minimumPressDuration < 0.3 {
                    mutableArrayOfGestureRecognizers.append(longPressGestureRecognizer)
                }
            }  else if gestureRecognizer.isKind(of: UITapGestureRecognizer.self) {
                let tapGestureRecognizer = gestureRecognizer as! UITapGestureRecognizer
                if tapGestureRecognizer.numberOfTapsRequired == 1 {
                    mutableArrayOfGestureRecognizers.append(gestureRecognizer)
                }
            } else  {
                if let _ = tapAndHalfGestureClass {
                } else {
                    mutableArrayOfGestureRecognizers.append(gestureRecognizer)
                }
            }
        }
        self.gestureRecognizers = mutableArrayOfGestureRecognizers
    }
}
