//
//  UIApplication+Foreground.swift
//  ZoundCommon
//
//  Created by Raul Andrisan on 27/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

extension UIApplication {
    public static var appIsInForeground: Observable<Bool> {
        let willEnterForeground = NotificationCenter.default.rx.notification(UIApplication.willEnterForegroundNotification).replaceWith(true)
        let didEnterBackground = NotificationCenter.default.rx.notification(UIApplication.didEnterBackgroundNotification).replaceWith(false)
        let applicationIsCurrentlyActive = UIApplication.shared.applicationState != .background
        let appIsInForegroud = Observable.of(willEnterForeground, didEnterBackground).merge().startWith(applicationIsCurrentlyActive)
        
        return appIsInForegroud
    }
}
