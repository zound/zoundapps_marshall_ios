//
//  UIScrollView+CurrentPage.swift
//  ZoundCommon
//
//  Created by Raul Andrisan on 17/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit

extension UIScrollView {
    var currentPage: Int {
        return Int((self.contentOffset.x + (0.5 * self.frame.size.width)) / self.frame.width)
    }
    
    var currentPageFractional: CGFloat {
        return (self.contentOffset.x + (0.5 * self.frame.size.width)) / self.frame.width
    }
    
    var horizontalPageCount: Int {
        return Int(self.contentSize.width / self.frame.size.width)
    }
}
