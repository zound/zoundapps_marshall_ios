//
//  UIView+HideAnimiated.swift
//  ZoundCommon
//
//  Created by Raul Andrisan on 28/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit

public typealias OptionalClosure = (() -> Void)?

extension UIView {
    public func setAlphaForVisiblity(_ visible: Bool) {
        UIView.setAlphaOfView(self, visible: visible, animated: true)
    }
    
    public static func setAlphaOfView(_ view: UIView, visible: Bool, animated:Bool) {
        setAlphaOfView(view, visible: visible, animated: animated, completed: nil)
    }
    
    public static func setAlphaOfView(_ view: UIView, visible: Bool, animated:Bool, completed: OptionalClosure) {
        let animationClosure: ()->() = { [weak view] in
            view?.alpha = visible ? 1.0 : 0.0
        }
        
        if animated {
            UIView.animate(withDuration: 0.25, delay: 0.0, options: [.beginFromCurrentState], animations: animationClosure, completion: {finished in completed?() })
        } else {
            animationClosure()
            completed?()
        }
    }
}
