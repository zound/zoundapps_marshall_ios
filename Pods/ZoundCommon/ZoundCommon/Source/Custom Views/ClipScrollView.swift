//
//  ClipScrollView.swift
//  ZoundCommon
//
//  Created by Raul Andrisan on 05/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//
import UIKit

class ClipScrollView: UIScrollView {
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        var view = super.hitTest(point, with: event)
        
        for subview in self.subviews {
            if view != nil && view!.isUserInteractionEnabled {
                break
            }
            let newPoint = self.convert(point, to: subview)
            view = subview.hitTest(newPoint, with: event)
        }

        return view
    }
}
