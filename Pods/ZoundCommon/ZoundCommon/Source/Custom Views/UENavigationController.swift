//
//  UIViewController+StatusBarStyle.swift
//  ZoundCommon
//
//  Created by Raul Andrisan on 20/02/2017.
//  Copyright © 2017 Zound Industries. All rights reserved.
//

public class UENavigationController: UINavigationController {
    override public var childForStatusBarHidden: UIViewController? {
        return self.topViewController
    }
    
    override public var childForStatusBarStyle: UIViewController? {
        return self.topViewController
    }
}
