//
//  PlayIndicatorView.swift
//  ZoundCommon
//
//  Created by Raul Andrisan on 13/02/2017.
//  Copyright © 2017 Zound Industries. All rights reserved.
//

import Foundation
import UIKit

public struct BarAnimationOptions {
    let minRelativeHeight: CGFloat
    let maxRelativeHeight: CGFloat
    let animationTime: TimeInterval
}

public class PlayIndicatorView: UIView {
    var numberOfBars = 3
    public var barViews = [UIView]()
    var barSpacing: CGFloat = 2.0
    var barAnimationOptions = [BarAnimationOptions]()
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        barAnimationOptions = [BarAnimationOptions(minRelativeHeight: 0.2,
                                                   maxRelativeHeight: 1.0,
                                                   animationTime: 1.0),
                               BarAnimationOptions(minRelativeHeight: 0.2,
                                                   maxRelativeHeight: 1.0,
                                                   animationTime: 0.8),
                               BarAnimationOptions(minRelativeHeight: 0.2,
                                                   maxRelativeHeight: 1.0,
                                                   animationTime: 1.2)]
        
        for _ in 0..<numberOfBars {
            let view = UIView()
            self.addSubview(view)
            barViews.append(view)
        }
    }
    
    public func startAnimation() {
        for barNumber in 0..<numberOfBars {
            let view = barViews[barNumber]
            view.layer.removeAllAnimations()
            let options = barAnimationOptions[barNumber]
            animateBar(barNumber, barAnimationOptions:  options)
        }
    }
    
    override public func layoutSubviews() {
        let barWidth = (self.bounds.size.width - (CGFloat(numberOfBars) - 1.0)*barSpacing) / CGFloat(numberOfBars)
        let barMinHeight = self.bounds.size.height * 0.2
        for barNumber in 0..<numberOfBars {
            let view = barViews[barNumber]
            view.backgroundColor = UIColor(white: 1.0, alpha: 0.5)
            view.frame = CGRect(x: CGFloat(barNumber)*barWidth + CGFloat(barNumber)*barSpacing,
                                y: self.bounds.size.height - barMinHeight,
                                width: barWidth,
                                height: barMinHeight)
            view.layer.anchorPoint = CGPoint(x: 0.0, y: 1.0)
        }
    }
    
    func animateBar(_ barNumber: Int, barAnimationOptions: BarAnimationOptions) {
        let barWidth = (self.bounds.size.width - (CGFloat(numberOfBars) - 1.0) * barSpacing) / CGFloat(numberOfBars)
        let view = barViews[barNumber]
        let barLowHeight = self.bounds.size.height * barAnimationOptions.minRelativeHeight
        let barHighHeight = self.bounds.size.height * barAnimationOptions.maxRelativeHeight
        
        let initialBounds = CGRect(x: CGFloat(barNumber)*barWidth + CGFloat(barNumber)*barSpacing,
                                   y: self.bounds.size.height - barLowHeight,
                                   width: barWidth,
                                   height: barLowHeight)
        
        let finalBounds = CGRect(x: CGFloat(barNumber)*barWidth + CGFloat(barNumber)*barSpacing,
                                 y: self.bounds.size.height - barHighHeight,
                                 width: barWidth,
                                 height: barHighHeight)
        
        let boundsAnimation = CAKeyframeAnimation()
        boundsAnimation.keyPath = "bounds"
        boundsAnimation.values = [NSValue(cgRect:initialBounds) ,
                                  NSValue(cgRect:finalBounds),
                                  NSValue(cgRect:initialBounds)]
        boundsAnimation.keyTimes = [0, 0.5, 1.0]
        boundsAnimation.duration = barAnimationOptions.animationTime
        boundsAnimation.isAdditive = false
        boundsAnimation.fillMode = CAMediaTimingFillMode.forwards
        boundsAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        boundsAnimation.repeatCount = Float.infinity
        
        boundsAnimation.isCumulative = false
        boundsAnimation.isRemovedOnCompletion = false
        
        view.layer.add(boundsAnimation, forKey: "bar\(barNumber)boundsAnimation")
    }
}
