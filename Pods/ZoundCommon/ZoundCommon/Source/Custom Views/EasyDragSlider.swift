//
//  EasyDragSlider.swift
//  ZoundCommon
//
//  Created by Raul Andrisan on 19/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit

public class EasyDragSlider: UISlider {
    var thumbTouchSize = CGSize(width: 50, height: 50)
    
    override public func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        let bounds = self.bounds.insetBy(dx: -thumbTouchSize.width, dy: -thumbTouchSize.height);
        return bounds.contains(point);
    }
    
    override public func beginTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        let thumbPercent = (value - minimumValue) / (maximumValue - minimumValue)
        let thumbSize = thumbImage(for: .normal)!.size.height
        let thumbPos = CGFloat(thumbSize) + (CGFloat(thumbPercent) * (CGFloat(bounds.size.width) - (2 * CGFloat(thumbSize))))
        let touchPoint = touch.location(in: self)
        
        return (touchPoint.x >= (thumbPos - thumbTouchSize.width) &&
            touchPoint.x <= (thumbPos + thumbTouchSize.width))
    }    
}
