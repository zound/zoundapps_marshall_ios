//
//  ClipView.swift
//  ZoundCommon
//
//  Created by Raul Andrisan on 04/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit

class ClipView: UIView {
    @IBOutlet weak var scrollView: UIScrollView?
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        let child: UIView? = super.hitTest(point, with: event)
        if child == self {
            return self.scrollView
        }
        
        return child
    }
}
