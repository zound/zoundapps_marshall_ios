//
//  AnimatedSlider.swift
//  ZoundCommon
//
//  Created by Raul Andrisan on 18/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import UIKit

public class AnimatedSlider: EasyDragSlider {
    public var numberOfSteps: Int = 33
    public var willSetValueFromUserInteraction = false
    public var didSetInitialValue = false
    
    override public func setValue(_ value: Float, animated: Bool) {
        if ((animated && !willSetValueFromUserInteraction) || willSetValueFromUserInteraction == false) && didSetInitialValue == true {
            UIView.animate(withDuration: 0.1, delay: 0.0, options: [.allowUserInteraction, .beginFromCurrentState], animations: {
                super.setValue(value, animated: true)
            }, completion: nil)
        } else {
            didSetInitialValue = true
            super.setValue(value, animated: false)
        }
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(sliderTapped))
        self.addGestureRecognizer(gestureRecognizer)
        
    }
    
    @objc func sliderTapped(_ g: UITapGestureRecognizer) {
        if let s = g.view as? UISlider {
            if s.isHighlighted {
                return
            }
            
            let pt: CGPoint = g.location(in: s)
            let crtSliderPercent = self.value / (s.maximumValue - s.minimumValue)
            let percentage = pt.x / s.bounds.size.width
            let stepVal = (s.maximumValue - s.minimumValue) / Float(numberOfSteps)
            
            var value = self.value
            
            if Float(percentage) > crtSliderPercent {
                value = value + stepVal
            } else {
                value = value - stepVal
            }
            
            let animated = g.isKind(of: UITapGestureRecognizer.self)
            willSetValueFromUserInteraction = true
            s.setValue(value, animated: animated)
            s.sendActions(for: .touchDown)
            s.sendActions(for: UIControl.Event.valueChanged)
            s.sendActions(for: .touchUpInside)
        }
    }
}
