//
//  BlurViewController.swift
//  ZoundCommon
//
//  Created by Raul Andrisan on 11/10/2016.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit

public protocol BlurViewController {
    var view: UIView! { get }
    var blurView: UIVisualEffectView! { get }
}
