//
//  MenuTransitioningDelegate.swift
//  ZoundCommon
//
//  Created by Raul Andrisan on 28/10/2016.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation

public class MenuTransitioningDelegate: NSObject, UIViewControllerTransitioningDelegate {
    public var disableInteractivePlayerTransitioning: Bool = false
    public let presentInteractor = MenuViewInteractive()
    public let dismissInteractor = MenuViewInteractive()
    
    public func attachDismissToViewController(_ viewController: UIViewController, withView view: UIView, presentViewController: UIViewController?) {
        dismissInteractor.attachToViewController(viewController, withView: viewController.view, presentViewController: nil)
    }
    
    public func attachPresentToViewController(_ viewController: UIViewController, withView view: UIView, presentViewController: UIViewController?) {
        presentInteractor.attachToViewController(viewController, withView: viewController.view, presentViewController: presentViewController)
    }
    
    public func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let animator = MenuViewAnimator()
        animator.transitionType = .dismiss
        return animator
    }
    
    public func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let animator = MenuViewAnimator()
        animator.transitionType = .present
        return animator
    }
    
    public func interactionControllerForPresentation(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return disableInteractivePlayerTransitioning ? nil : self.presentInteractor
    }
    
    public func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return disableInteractivePlayerTransitioning ? nil : self.dismissInteractor
    }
}
