//
//  BaseAnimator.swift
//  ZoundCommon
//
//  Created by Raul Andrisan on 19/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit
import Cartography

public protocol MenuAnimatorDataSource: class {
    func transitioningViewForAnimator(_ animator: MenuViewAnimator) -> UIView
}

public class MenuViewAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    public weak var dataSource: MenuAnimatorDataSource?
    public var transitionType: ModalAnimatedTransitioningType = .present
    public var initialY: CGFloat = 0
    
    public func animatePresentingInContext(_ transitioningContext: UIViewControllerContextTransitioning, toViewController toVC: UIViewController, fromViewController fromVC: UIViewController) {
        let container = transitioningContext.containerView
        
        let darkCover = UIView()
        darkCover.backgroundColor = UIColor(white: 0.0, alpha: 0.7)
        darkCover.alpha = 0.0
        darkCover.tag = 555
        
        container.addSubview(darkCover)
        container.addSubview(toVC.view)
        
        darkCover.translatesAutoresizingMaskIntoConstraints = false
        toVC.view.translatesAutoresizingMaskIntoConstraints = false
        
        constrain(darkCover) { view in
            view.edges == view.superview!.edges
        }
        
        constrain(toVC.view) { view in
            view.leading == view.superview!.leading
            view.top == view.superview!.top
            view.height == view.superview!.height
            view.width == view.superview!.width * 0.8 ~ 999
            view.width <= 400
            view.width <= view.superview!.width
            view.width >= 320
        }
        
        container.layoutIfNeeded()
        
        let onScreenFrame = toVC.view.frame
        var outOfScreenFrame = toVC.view.frame
        outOfScreenFrame.origin.x = -outOfScreenFrame.size.width
        toVC.view.frame =  outOfScreenFrame
        
        let options:UIView.AnimationOptions = transitioningContext.isInteractive ? [UIView.AnimationOptions.curveLinear] : UIView.AnimationOptions()
        UIView.animate(withDuration: 0.4,
                       delay:0.0,
                       options: options,
                       animations: {
                        toVC.view.frame = onScreenFrame
                        darkCover.alpha = 1.0 },
                       completion: { finished in
                        if transitioningContext.transitionWasCancelled {
                            transitioningContext.completeTransition(false)
                        } else {
                            transitioningContext.completeTransition(true)
                        }
        })
    }
    
    public func animateDismissingInContext(_ transitioningContext: UIViewControllerContextTransitioning, toViewController toVC: UIViewController, fromViewController fromVC: UIViewController) {
        let initialVCRect = transitioningContext.initialFrame(for: fromVC)
        let container = transitioningContext.containerView
        let darkCover = container.viewWithTag(555) ?? UIView()
        let options: UIView.AnimationOptions = transitioningContext.isInteractive ? [UIView.AnimationOptions.curveLinear] : UIView.AnimationOptions()
        
        UIView.animate(withDuration: 0.4,
                       delay:0.0,
                       options: options,
                       animations: {
                        darkCover.alpha = 0.0
                        var finalVCRect = initialVCRect
                        finalVCRect.origin.x = -finalVCRect.size.width
                        fromVC.view.frame = finalVCRect },
                       completion: { finished in
                        if transitioningContext.transitionWasCancelled {
                            transitioningContext.completeTransition(false)
                        } else {
                            darkCover.removeFromSuperview()
                            transitioningContext.completeTransition(true)
                        }
        })
    }
    
    public func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let to = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)
        let from = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)
        
        if transitionType == .present {
            self.animatePresentingInContext(transitionContext, toViewController: to!, fromViewController: from!)
        } else if transitionType == .dismiss {
            self.animateDismissingInContext(transitionContext, toViewController: to!, fromViewController: from!)
        }
    }
    
    public func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.4
    }
    
    public func animationEnded(_ transitionCompleted: Bool) {
    }
}
