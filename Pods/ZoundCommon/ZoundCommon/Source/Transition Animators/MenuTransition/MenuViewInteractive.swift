//
//  MiniToLargeViewInteractive.swift
//  ZoundCommon
//
//  Created by Raul Andrisan on 19/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation

import UIKit

public protocol MenuViewInteractiveDelegate: class {
    func menuViewInteractiveDidInteractivelyDismiss()
    func menuViewInteractiveDidInteractivelyPresent()
}

public class MenuViewInteractive:  UIPercentDrivenInteractiveTransition {
    public weak var delegate: MenuViewInteractiveDelegate?
    public weak var viewController: UIViewController?
    public weak var presentViewController: UIViewController?
    var shouldComplete: Bool  = false
    
    public func attachToViewController(_ viewController: UIViewController, withView view: UIView, presentViewController: UIViewController?) {
        self.viewController = viewController
        self.presentViewController = presentViewController
        let pan = UIPanGestureRecognizer(target: self, action: #selector(onPan))
        view.addGestureRecognizer(pan)
    }
    
    @objc public func onPan(_ pan:UIPanGestureRecognizer) {
        let translation = pan.translation(in: pan.view!.superview!)
        switch pan.state {
        case .began:
            if self.presentViewController == nil {
                self.viewController?.dismiss(animated: true, completion: { [weak self] in
                    guard let `self` = self, self.shouldComplete else { return }
                    self.delegate?.menuViewInteractiveDidInteractivelyDismiss()
                })
            } else {
                self.viewController?.definesPresentationContext = true
                self.viewController?.present(self.presentViewController!, animated: true, completion: {[weak self] in
                    guard let `self` = self, self.shouldComplete else { return }
                    self.delegate?.menuViewInteractiveDidInteractivelyPresent()
                })
            }
        case .changed:
            let dragViewWidth = self.viewController?.view.bounds.size.width ?? 400
            let dragAmount = self.presentViewController == nil ? -dragViewWidth : dragViewWidth
            let threshold = Float(0.2)
            var percent: Float = Float(translation.x) / Float(dragAmount)
            
            percent = fmaxf(percent, 0.0)
            percent = fminf(percent, 1.0)
            
            self.update(CGFloat(percent))
            self.shouldComplete = percent > threshold
        case .ended, .cancelled:
            if pan.state == .cancelled || !self.shouldComplete {
                self.cancel()
            } else {
                self.finish()
            }
        default: break
        }
    }
}
