//
//  MiniToLargeViewInteractive.swift
//  ZoundCommon
//
//  Created by Raul Andrisan on 19/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit

public class MiniToLargeViewInteractive:  UIPercentDrivenInteractiveTransition {
    weak var viewController: UIViewController?
    weak var presentViewController: UIViewController?
    var shouldComplete: Bool  = false
    
    public func attachToViewController(_ viewController: UIViewController, withView view: UIView, presentViewController: UIViewController?) {
        self.viewController = viewController
        self.presentViewController = presentViewController
        let pan = UIPanGestureRecognizer(target: self, action: #selector(onPan))
        view.addGestureRecognizer(pan)
    }
    
    @objc public func onPan(_ pan:UIPanGestureRecognizer) {
        let translation = pan.translation(in: pan.view!.superview!)
        
        switch pan.state {
        case .began:
            if self.presentViewController == nil {
                self.viewController?.dismiss(animated: true, completion: nil)
            } else {
                self.viewController?.definesPresentationContext = true
                self.viewController?.present(self.presentViewController!, animated: true, completion: nil)
            }
        case .changed:
            let screenHeight = UIScreen.main.bounds.size.height - 50.0
            let dragAmount = self.presentViewController == nil ? screenHeight : -screenHeight
            let threshold = Float(0.4)
            var percent: Float = Float(translation.y) / Float(dragAmount)
            
            percent = fmaxf(percent, 0.0)
            percent = fminf(percent, 1.0)
            
            self.update(CGFloat(percent))
            self.shouldComplete = percent > threshold
        case .ended, .cancelled:
            if pan.state == .cancelled || !self.shouldComplete {
                self.cancel()
            } else {
                self.finish()
                
            }
        default: break
        }
    }
}
