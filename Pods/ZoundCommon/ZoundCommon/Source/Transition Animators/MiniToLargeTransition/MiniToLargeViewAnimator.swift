//
//  BaseAnimator.swift
//  ZoundCommon
//
//  Created by Raul Andrisan on 19/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit

public enum ModalAnimatedTransitioningType {
    case present
    case dismiss
}

public protocol MiniToLargeAnimatorDataSource: class {
    func transitioningViewForAnimator(_ animator: MiniToLargeAnimator) -> UIView
}

public class MiniToLargeAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    public weak var dataSource: MiniToLargeAnimatorDataSource?
    public var transitionType: ModalAnimatedTransitioningType = .present
    public var initialY: CGFloat = 0
    
    public func animatePresentingInContext(_ transitioningContext: UIViewControllerContextTransitioning, toViewController toVC: UIViewController, fromViewController fromVC: UIViewController) {
        let fromVCRect = transitioningContext.initialFrame(for: fromVC)
        var toVCRect = fromVCRect
        toVCRect.origin.y = toVCRect.size.height - self.initialY
        
        toVC.view.frame = toVCRect
        let container = transitioningContext.containerView
        container.layer.masksToBounds = true
        let imageView = self.miniView()
        
        toVC.view.addSubview(imageView)
        //container.addSubview(fromVC.view)
        container.addSubview(toVC.view)
        
        let options:UIView.AnimationOptions = transitioningContext.isInteractive ? [UIView.AnimationOptions.curveLinear] : []
        UIView.animate(withDuration: 0.4,
                       delay: 0.0,
                       options: options,
                       animations: {
                        toVC.view.frame = fromVCRect
                        imageView.alpha = 0.0 },
                       completion: { finished in
                        imageView.removeFromSuperview()
                        if transitioningContext.transitionWasCancelled {
                            transitioningContext.completeTransition(false)
                        } else {
                            transitioningContext.completeTransition(true)
                        }
        })
    }
    
    public func animateDismissingInContext(_ transitioningContext: UIViewControllerContextTransitioning, toViewController toVC: UIViewController, fromViewController fromVC: UIViewController) {
        var fromVCRect = transitioningContext.initialFrame(for: fromVC)
        let toVCRect = transitioningContext.finalFrame(for: toVC)
        fromVCRect.origin.y = fromVCRect.size.height - self.initialY
        
        let imageView = self.miniView()
        fromVC.view.addSubview(imageView)
        let container = transitioningContext.containerView
        //container.addSubview(toVC.view)
        container.addSubview(fromVC.view)
        imageView.alpha = 0.0
        toVC.view.frame = toVCRect
        
        let options:UIView.AnimationOptions = transitioningContext.isInteractive ? [UIView.AnimationOptions.curveLinear] : []
        
        UIView.animate(withDuration: 0.4,
                       delay: 0.0,
                       options: options,
                       animations: {
                        fromVC.view.frame = fromVCRect
                        imageView.alpha = 1.0 },
                       completion: { finished in
                        imageView.removeFromSuperview()
                        if transitioningContext.transitionWasCancelled {
                            transitioningContext.completeTransition(false)
                        } else {
                            transitioningContext.completeTransition(true)
                        }
        })
    }
    
    public func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let to = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)
        let from = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)
        
        if transitionType == .present {
            self.animatePresentingInContext(transitionContext, toViewController: to!, fromViewController: from!)
        } else if transitionType == .dismiss {
            self.animateDismissingInContext(transitionContext, toViewController: to!, fromViewController: from!)
        }
    }
    
    public func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.4
    }
    
    public func animationEnded(_ transitionCompleted: Bool) {
    }
    
    fileprivate func miniView() -> UIView {
        guard let miniView = dataSource?.transitioningViewForAnimator(self) else { return UIView() }
        return miniView
    }
}
