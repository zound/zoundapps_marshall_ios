//
//  BlurPresentationAnimator.swift
//  ZoundCommon
//
//  Created by Raul Andrisan on 11/10/2016.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit

public class BlurPresentationAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    public func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.3
    }
    
    public func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let toController = transitionContext.viewController(forKey: .to) as? BlurViewController else { return }
        
        let container = transitionContext.containerView
        container.addSubview(toController.view)
        
        toController.blurView.effect = nil
        toController.blurView.contentView.alpha = 0.0
        UIView.animate(withDuration: 0.3, animations: {
            toController.blurView.effect = UIBlurEffect(style: .dark)
            toController.blurView.contentView.alpha = 1.0
        }, completion: { finished in
            transitionContext.completeTransition(finished)
        })
    }
}
