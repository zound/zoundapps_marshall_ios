//
//  BlurTransitioningDelegate.swift
//  ZoundCommon
//
//  Created by Raul Andrisan on 11/10/2016.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit

public class BlurTransitioningDelegate: NSObject, UIViewControllerTransitioningDelegate {
    public func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return BlurPresentationAnimator()
    }
    
    public func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return BlurDismissAnimator()
    }
}
