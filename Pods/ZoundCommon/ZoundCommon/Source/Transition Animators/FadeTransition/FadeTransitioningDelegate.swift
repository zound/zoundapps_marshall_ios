//
//  BlurTransitioningDelegate.swift
//  ZoundCommon
//
//  Created by Raul Andrisan on 11/10/2016.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit

public class FadeTransitioningDelegate: NSObject, UIViewControllerTransitioningDelegate {
    public func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return FadePresentationAnimator()
    }
    
    public func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return FadeDismissAnimator()
    }
}
