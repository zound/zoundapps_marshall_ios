//
//  BlurPresentationAnimator.swift
//  ZoundCommon
//
//  Created by Raul Andrisan on 11/10/2016.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit

public class FadePresentationAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    public func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.3
    }
    
    public func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let toController = transitionContext.viewController(forKey: .to) else { return }
        let container = transitionContext.containerView
        container.addSubview(toController.view)
        
        toController.view.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        toController.view.alpha = 0.0
        
        UIView.animate(withDuration: 0.3,
                       delay: 0.0,
                       usingSpringWithDamping: 0.6,
                       initialSpringVelocity: 0.0,
                       options: [.beginFromCurrentState],
                       animations: {
                        toController.view.transform = CGAffineTransform.identity
                        toController.view.alpha = 1.0 },
                       completion: { finished in
                        transitionContext.completeTransition(finished)
        })
    }
}
