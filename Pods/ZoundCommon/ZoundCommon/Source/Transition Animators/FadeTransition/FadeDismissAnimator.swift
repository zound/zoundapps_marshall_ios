//
//  BlurDismissAnimator.swift
//  ZoundCommon
//
//  Created by Raul Andrisan on 11/10/2016.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit

public class FadeDismissAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    public func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.3
    }
    
    public func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let fromController = transitionContext.viewController(forKey: .from) else { return }
        guard let toView = transitionContext.view(forKey: .from) else { return }
        
        let container = transitionContext.containerView
        container.addSubview(toView)
        
        UIView.animate(withDuration: 0.3, animations: {
            fromController.view.alpha = 0.0
        }, completion: { finished in
            transitionContext.completeTransition(finished)
        })
    }
}
