//
//  Keyboard.swift
//  ZoundCommon
//
//  Created by Raul Andrisan on 02/03/2017.
//  Copyright © 2017 Zound Industries. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

public typealias KeyboardAnimationTuple = (frame: CGRect, animationDuration: TimeInterval, animationCurve: Int)

public class Keyboard {
    public static func keyboardFrameChangeAnimationInfoObservable() -> Observable<KeyboardAnimationTuple> {
        let keyboardWillShowUserInfo = NotificationCenter.default.rx.notification(UIResponder.keyboardWillShowNotification).map { $0.userInfo }
        let keyboardWillHideUserInfo = NotificationCenter.default.rx.notification(UIResponder.keyboardWillHideNotification).map { $0.userInfo }
        
        let keyboardFrameChangedAnimationInfo: Observable<KeyboardAnimationTuple> = Observable.of(keyboardWillShowUserInfo, keyboardWillHideUserInfo)
            .merge()
            .map { userInfo in
                let frame = (userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
                let animationDuration = (userInfo![UIResponder.keyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
                let animateCurve = (userInfo![UIResponder.keyboardAnimationCurveUserInfoKey]! as AnyObject).integerValue!
                
                return (frame: frame, animationDuration:animationDuration, animationCurve: animateCurve)
        }
        
        return keyboardFrameChangedAnimationInfo
    }
}
