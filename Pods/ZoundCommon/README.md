# ZoundCommon

[![CI Status](https://img.shields.io/travis/Bartosz Dolewski/ZoundCommon.svg?style=flat)](https://travis-ci.org/Bartosz Dolewski/ZoundCommon)
[![Version](https://img.shields.io/cocoapods/v/ZoundCommon.svg?style=flat)](https://cocoapods.org/pods/ZoundCommon)
[![License](https://img.shields.io/cocoapods/l/ZoundCommon.svg?style=flat)](https://cocoapods.org/pods/ZoundCommon)
[![Platform](https://img.shields.io/cocoapods/p/ZoundCommon.svg?style=flat)](https://cocoapods.org/pods/ZoundCommon)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

ZoundCommon is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'ZoundCommon'
```

## Author

Bartosz Dolewski, ext.bartosz.a.dolewski@tieto.com

## License

ZoundCommon is available under the MIT license. See the LICENSE file for more info.
