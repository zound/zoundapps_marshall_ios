# MinuetSDK

[![CI Status](https://img.shields.io/travis/Bartosz Dolewski/MinuetSDK.svg?style=flat)](https://travis-ci.org/Bartosz Dolewski/MinuetSDK)
[![Version](https://img.shields.io/cocoapods/v/MinuetSDK.svg?style=flat)](https://cocoapods.org/pods/MinuetSDK)
[![License](https://img.shields.io/cocoapods/l/MinuetSDK.svg?style=flat)](https://cocoapods.org/pods/MinuetSDK)
[![Platform](https://img.shields.io/cocoapods/p/MinuetSDK.svg?style=flat)](https://cocoapods.org/pods/MinuetSDK)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

MinuetSDK is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'MinuetSDK'
```

## Author

Bartosz Dolewski, ext.bartosz.a.dolewski@tieto.com

## License

MinuetSDK is available under the MIT license. See the LICENSE file for more info.
