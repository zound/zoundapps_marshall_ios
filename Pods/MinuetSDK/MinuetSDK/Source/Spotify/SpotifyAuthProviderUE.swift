//
//  SpotifyAuthProviderUE.swift
//  MinuetSDK
//
//  Created by Claudiu Alin Luminosu on 14/11/2017.
//  Copyright © 2017 Zound Industries. All rights reserved.
//

import Foundation
import Moya
import RxSwift
import SWXMLHash

class SpotifyAuthProviderUE: MoyaProvider<SpotifyAuthServiceUE>, SpotifyAuthProvider {
    override public init(endpointClosure: @escaping EndpointClosure = MoyaProvider<SpotifyAuthServiceUE>.defaultEndpointMapping,
                         requestClosure: @escaping RequestClosure = MoyaProvider<SpotifyAuthServiceUE>.defaultRequestMapping,
                         stubClosure: @escaping StubClosure = MoyaProvider.neverStub,
                         callbackQueue: DispatchQueue? = nil,
                         manager: Manager = MoyaProvider<Target>.defaultAlamofireManager(),
                         plugins: [PluginType] = [],
                         trackInflights: Bool = false) {
        super.init(endpointClosure: endpointClosure, requestClosure: requestClosure, stubClosure: stubClosure,callbackQueue: callbackQueue, manager: manager, plugins: plugins)
    }
    
    func swap(_ code: String, redirectUri: String) -> Observable<SpotifyToken> {
        return self.rx.request(SpotifyAuthServiceUE.swap(code: code, redirectUri: redirectUri)).mapObject(SpotifyToken.self).map{ token -> SpotifyToken in
            var newToken = token
            newToken.createdDate = Date()
            return newToken
            }.timeout(10.0, scheduler: MainScheduler.instance).asObservable()
    }
    
    func refresh(_ token: SpotifyToken) -> Observable<SpotifyToken> {
        if let refreshToken = token.refreshToken {
            return self.rx.request(SpotifyAuthServiceUE.refresh(refreshToken: refreshToken))
                .mapObject(SpotifyToken.self).asObservable().flatMapLatest{ refreshedToken -> Observable<SpotifyToken> in
                    if refreshedToken.accessToken != nil {
                        var newToken = refreshedToken
                        newToken.refreshToken = token.refreshToken
                        newToken.createdDate = Date()
                        return Observable.just(newToken)
                    } else {
                        return Observable.error(SpotifyLoginError.refreshFailed)
                    }
                }.timeout(10.0, scheduler: MainScheduler.instance)
        } else {
            return Observable.error(SpotifyAuthProviderError.noRefreshTokenProvided)
        }
    }
}
