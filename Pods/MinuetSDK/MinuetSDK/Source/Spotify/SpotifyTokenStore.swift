//
//  SpotifyTokenStore.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 20/05/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import ObjectMapper

public class SpotifyTokenStore {
    public static func getCurrentToken() -> SpotifyToken? {
        if let jsonString = minuetSDKUserDefaults?.object(forKey: "spotifyToken") as? String {
            if let token = Mapper<SpotifyToken>().map(JSONString: jsonString) {
                return token
            }
        }
        return nil
    }
    
    public static func storeToken(_ token: SpotifyToken) {
        let jsonString = token.toJSONString()
        minuetSDKUserDefaults?.set(jsonString, forKey: "spotifyToken")
    }
    
    public static func clearToken() {
        minuetSDKUserDefaults?.removeObject(forKey: "spotifyToken")
    }
}
