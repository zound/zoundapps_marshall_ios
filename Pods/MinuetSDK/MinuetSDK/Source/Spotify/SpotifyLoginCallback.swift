//
//  SpotifyLoginCallback.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 23/11/2016.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation

public struct SpotifyLoginCallback {
    public let url: URL
    public let sourceApplication: String?
}
