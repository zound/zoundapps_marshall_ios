//
//  SpotifyUser.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 30/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import ObjectMapper
import Foundation

public enum SpotifyProductType: String {
    case Premium = "premium"
    case Free = "free"
    case Open = "open"
}

public struct SpotifyUser: Mappable {
    public var country: String?
    public var displayName: String?
    public var externalSpotifyURL: URL?
    public var followersCount: Int?
    public var href: URL?
    public var id: String?
    public var images: [SpotifyImage]?
    public var productType: SpotifyProductType?
    public var uri: String?
    
    public init?(map: Map) {}
    
    public mutating func mapping(map: Map) {
        country            <-  map["country"]
        displayName        <-  map["display_name"]
        externalSpotifyURL <- (map["external_urls.spotify"], URLTransform())
        followersCount     <-  map["followers.total"]
        href               <- (map["href"], URLTransform())
        id                 <-  map["id"]
        images             <-  map["images"]
        productType        <- (map["product"], EnumTransform<SpotifyProductType>())
        uri                <-  map["uri"]
    }
}
