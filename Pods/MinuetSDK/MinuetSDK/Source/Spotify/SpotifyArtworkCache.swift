//
//  SpotifyArtworkCache.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 25/05/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation

public class SpotifyArtworkCache {
    public static let sharedInstace = SpotifyArtworkCache()
    var artworkForURI: [String: AnyObject]!
    
    public init() {
        if let data = minuetSDKUserDefaults?.object(forKey: "spotifyArtwork") as? Data {
            artworkForURI = NSKeyedUnarchiver.unarchiveObject(with: data) as? [String: AnyObject]
        }
        
        if artworkForURI == nil {
            artworkForURI = [:]
        }
    }
    
    func saveData() {
        let data = NSKeyedArchiver.archivedData(withRootObject: artworkForURI)
        minuetSDKUserDefaults?.set(data, forKey: "spotifyArtwork")
        minuetSDKUserDefaults?.synchronize()
    }
    
    public func clearData() {
        artworkForURI = [:]
        saveData()
    }
    
    public func storeArtworkURL(_ url: URL, forURI: String) {
        artworkForURI[forURI] = url as AnyObject?
        saveData()
    }
    
    public func artworkURLForURI(_ uri: String) -> URL? {
        return artworkForURI[uri] as? URL
    }
}
