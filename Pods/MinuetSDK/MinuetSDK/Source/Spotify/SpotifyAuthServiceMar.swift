//
//  SpotifyAuthServiceMar.swift
//  MinuetSDK
//
//  Created by Claudiu Alin Luminosu on 14/11/2017.
//  Copyright © 2017 Zound Industries. All rights reserved.
//

import Foundation
import Moya

enum SpotifyAuthServiceMar {
    case swap(code: String, redirectUri: String)
    case refresh(refreshToken: String)
}

extension SpotifyAuthServiceMar:TargetType {
    var headers: [String : String]? {
        return nil
    }
    
    var baseURL: URL {
        return URL(string: "https://marshallapp-dev-spotify.herokuapp.com")!
    }
    
    var path: String {
        switch self {
        case .swap(_): return "/swap"
        case .refresh(_): return "/refresh"
        }
    }
    
    var method: Moya.Method {
        return .post
    }
    
    var parameters: [String: Any]? {
        switch self {
        case .swap(let code, let redirectUri): return ["code":code as AnyObject, "redirect_uri": redirectUri as AnyObject]
        case .refresh(let refreshToken): return ["refresh_token":refreshToken as AnyObject]
        }
    }
    
    public var task: Task {
        if let parameters = self.parameters {
            return Task.requestParameters(parameters: parameters, encoding: URLEncoding.default)
        } else {
            return Task.requestPlain
        }
    }
    
    var sampleData: Data {
        return Data()
    }
}
