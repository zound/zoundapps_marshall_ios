//
//  SpotifyLoginRxDelegate.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 25/05/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation

import RxSwift
import RxCocoa

public class SpotifyLoginRxDelegate: SpotifyLoginDelegate {
    public let spotifyLoginDidFinish: PublishSubject<SpotifyToken> = PublishSubject()
    public let spotifyLoginDidError: PublishSubject<SpotifyLoginError> = PublishSubject()
    
    public func didFinishSpotifyLoginWithError(_ error: SpotifyLoginError) {
        spotifyLoginDidError.onNext(error)
    }
    
    public func didFinishSpotifyLoginWithAccessToken(_ accessToken: SpotifyToken) {
        spotifyLoginDidFinish.onNext(accessToken)
    }
}
