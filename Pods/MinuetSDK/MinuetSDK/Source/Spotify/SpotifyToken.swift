//
//  SpotifyToken.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 20/05/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import ObjectMapper

public struct SpotifyToken {
    public var accessToken: String?
    public var tokenType: String?
    public var expiresIn: Int?
    public var refreshToken: String?
    public var createdDate: Date?
    
    public var isExpired: Bool {
        if let date = createdDate {
            if let `expiresIn` = expiresIn {
                let secondsUntilExpiration = Date().timeIntervalSince(date)
                return Int(secondsUntilExpiration) > expiresIn
            }
        }
        return true
    }
}

extension SpotifyToken: Mappable {
    public init?(map: Map) {}
    
    public mutating func mapping(map: Map) {
        accessToken  <- map["access_token"]
        tokenType    <- map["token_type"]
        expiresIn    <- map["expires_in"]
        refreshToken <- map["refresh_token"]
        createdDate  <- (map["created_date"], DateTransform())
    }
}
