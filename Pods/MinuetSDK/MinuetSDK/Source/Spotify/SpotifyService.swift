//
//  SpotifyService.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 30/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import Moya

public enum SpotifyService {
    case getPresets
    case getPlaylists
    case getPlaylistInfo(spotifyURI: String)
    case getPlaylistArtwork(spotifyURI: String)
    case getAlbumArtwork(spotifyURI: String)
    case getArtistArtwork(spotifyURI: String)
    case getTrackArtwork(spotifyURI: String)
    case getUserArtwork(spotifyURI: String)
    case getUserInfo
}

struct SpotifyApiEndpoints {
    static let official = "https://api.spotify.com/v1"
    static let secret = "https://exp.wg.spotify.com/v1"
}

// MARK: - TargetType for Moya
extension SpotifyService: TargetType {
    public var headers: [String : String]? {
        return nil
    }
    
    public var task: Task {
        guard let parameters = self.parameters else { return Task.requestPlain }
        return Task.requestParameters(parameters: parameters, encoding: URLEncoding.default)
    }
    
    public var baseURL: URL {
        switch self {
        case .getPresets: return URL(string: SpotifyApiEndpoints.secret)!
        default: return URL(string: SpotifyApiEndpoints.official)!
        }
    }
    
    public var path: String {
        switch self {
        case .getPresets: return "/me/presets"
        case .getPlaylists: return "/me/playlists"
        case .getPlaylistInfo(let spotifyURI): return SpotifyService.playlistInfoURL(from: spotifyURI) ?? ""
        case .getPlaylistArtwork(let spotifyURI): return SpotifyService.playlistInfoURL(from: spotifyURI) ?? ""
        case .getAlbumArtwork(let spotifyURI): return SpotifyService.albumURL(from: spotifyURI) ?? ""
        case .getArtistArtwork(let spotifyURI): return SpotifyService.artistURL(from: spotifyURI) ??  ""
        case .getTrackArtwork(let spotifyURI): return SpotifyService.trackURL(from: spotifyURI) ??  ""
        case .getUserArtwork(let spotifyURI): return SpotifyService.userURL(from: spotifyURI) ??  ""
        case .getUserInfo: return "/me"
        }
    }
    
    public var method: Moya.Method {
        return .get
    }
    
    public var parameters: [String: Any]? {
        switch self {
        case .getPresets: return ["format":"blob" as AnyObject,
                                  "count":7 as AnyObject,
                                  "platform":"speaker" as AnyObject,
                                  "partner":"zound" as AnyObject]

        case .getPlaylists: return ["offset":0 as AnyObject,
                                    "limit":7 as AnyObject]

        case .getPlaylistArtwork: return ["fields":"images(url)" as AnyObject]
        case .getArtistArtwork: return nil
        case .getAlbumArtwork: return nil
        case .getTrackArtwork: return nil
        default: return nil
        }
    }
    
    public var sampleData: Data {
        return Data()
    }
}

// MARK: - Helpers
extension SpotifyService {
    static func playlistInfoURL(from spotifyURI: String) -> String? {
        let components = spotifyURI.components(separatedBy: ":").filter { $0.isEmpty == false }

        guard components.count >= 5 else { return nil }
        
        guard let userStringIndex = components.firstIndex(where: { $0 == "user" }),
            let userId = components[safe: userStringIndex + 1] else { return nil }

        guard let playlistStringIndex = components.firstIndex(where: { $0 == "playlist" }),
            let playlistId = components[safe: playlistStringIndex + 1] else { return nil }

        return "/users/\(userId)/playlists/\(playlistId)"
    }
    
    static func albumURL(from spotifyURI: String) -> String? {
        guard let albumId = spotifyURI.components(separatedBy: ":").last else { return nil }
        return "/albums/\(albumId)"
    }
    
    static func artistURL(from spotifyURI: String) -> String? {
        guard let artistId = spotifyURI.components(separatedBy: ":").last else { return nil }
        return "/artists/\(artistId)"
    }
    
    static func trackURL(from spotifyURI: String) -> String? {
        guard let trackId = spotifyURI.components(separatedBy: ":").last else { return nil }
        return "/tracks/\(trackId)"
    }
    
    static func userURL(from spotifyURI: String) -> String? {
        let components = spotifyURI.components(separatedBy: ":")
        guard spotifyURI.contains("station") && components.count > 3 else { return nil }
        
        let userId = components[3]
        return "/users/\(userId)"
    }
}
