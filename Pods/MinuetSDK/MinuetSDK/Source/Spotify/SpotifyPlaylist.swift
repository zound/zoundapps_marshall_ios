//
//  SpotifyPlaylist.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 30/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import ObjectMapper

public struct SpotifyPlaylist: Mappable {
    public var collaborative: Bool?
    public var externalSpotifyURL: URL?
    public var href: URL?
    public var id: String?
    public var images: [SpotifyImage]?
    public var name: String?
    public var owner: SpotifyUser?
    public var publicPlaylist: Bool?
    public var snapshotId: String?
    public var tracksHref: URL?
    public var tracksCount: Int?
    public var uri: String?
    
    public init?(map: Map) {}
    
    public mutating func mapping(map: Map) {
        
        collaborative       <-  map["collaborative"]
        externalSpotifyURL  <- (map["external_urls.spotify"], URLTransform())
        href                <- map["href"]
        id                  <- map["id"]
        images              <- map["images"]
        name                <- map["name"]
        owner               <- map["owner"]
        publicPlaylist      <- map["public"]
        snapshotId          <- map["snapshot_id"]
        tracksHref          <- map["tracks.href"]
        tracksCount         <- map["tracks.count"]
        uri                 <- map["uri"]
    }
}
