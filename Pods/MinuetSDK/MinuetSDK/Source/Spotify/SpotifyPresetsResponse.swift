//
//  SpotifyPresetsResponse.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 20/05/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import ObjectMapper

public struct SpotifyPresetsResponse: Mappable {
    public var presets: [SpotifyPreset]?
    
    public init?(map: Map) {}
    
    public mutating func mapping(map: Map) {
        presets <- map["presets"]
    }
}
