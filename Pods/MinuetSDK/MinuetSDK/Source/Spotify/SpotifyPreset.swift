//
//  SpotifyPreset.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 20/05/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import ObjectMapper

public struct SpotifyPreset: Mappable {
    public var blob: String?
    public var type: String?
    public var name: String?
    public var imageURL: String?
    public var spotifyURI: String? {
        return blob?.base64DecodedString()
    }
    
    public init?(map: Map) {}
    
    public mutating func mapping(map: Map) {
        blob     <-  map["blob"]
        type     <-  map["type"]
        name     <-  map["name"]
        imageURL <-  map["images.0.url"]
    }
}
