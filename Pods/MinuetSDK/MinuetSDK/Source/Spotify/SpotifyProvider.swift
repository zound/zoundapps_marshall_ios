 
 //
 //  SpotifyProvider.swift
 //  MinuetSDK
 //
 //  Created by Raul Andrisan on 31/03/16.
 //  Copyright © 2016 Zound Industries. All rights reserved.
 //
 
 import Foundation
 import Moya
 import RxSwift
 import RxCocoa
 
 public enum SpotifyProviderError: Swift.Error {
    case notPremiumUser
    case invalidSpotifyToken
    case loginNeeded
 }
 
 public class SpotifyProvider {
    let disposeBag = DisposeBag()
    public let tokenManager: SpotifyTokenManager
    
    public init(isUrbanearsApp : Bool) {
        tokenManager = SpotifyTokenManager(isUrbanearsApp: isUrbanearsApp)
    }
    
    public static func providerWithSpotifyToken(spotifyToken: SpotifyToken) -> MoyaProvider<SpotifyService> {
        let spotifyProvider = MoyaProvider<SpotifyService>(requestClosure: { (endpoint: Endpoint, done: MoyaProvider.RequestResultClosure) in
            let newEndpoint = endpoint.adding(newHTTPHeaderFields: ["Authorization":"Bearer \(spotifyToken.accessToken!)"])
            do {
                let urlRequest = try newEndpoint.urlRequest()
                done(.success(urlRequest))
            } catch {
                done(.failure(MoyaError.requestMapping(endpoint.url)))
            }
            return ()
        })
        return spotifyProvider
    }
    
    public func provider(loginIfNeeded: Bool = false, forceWebLogin: Bool = false) -> Observable<MoyaProvider<SpotifyService>> {
        let tokenManager = self.tokenManager
        return Observable.create {(observer) -> Disposable in
            var token = tokenManager.spotifyTokenVariable.asObservable().unwrapOptional().filter{ !$0.isExpired }
            if !tokenManager.hasToken {
                if loginIfNeeded {
                    token =  tokenManager.login(forceWebLogin: forceWebLogin)
                } else {
                    return Observable.error(SpotifyProviderError.loginNeeded).subscribe(observer)
                }
            } else if !tokenManager.tokenIsValid {
                token =  tokenManager.refreshToken()
            }
            
            return token.map{ token -> Observable<MoyaProvider<SpotifyService>> in
                if token.accessToken != nil {
                    return Observable.just(SpotifyProvider.providerWithSpotifyToken(spotifyToken: token))
                } else {
                    return Observable.error(SpotifyLoginError.authorizationFailed(spotifyError: "cannot_return_token"))
                }
                }.switchLatest().subscribe(observer)
        }
    }
    
    public func userInfo(forceWebLogin: Bool) -> Observable<SpotifyUser> {
        return self.provider(loginIfNeeded: true, forceWebLogin: forceWebLogin).flatMapLatest{ provider -> Observable<SpotifyUser> in
            return provider.rx.request(SpotifyService.getUserInfo).timeout(10.0, scheduler: MainScheduler.instance)
                .do(onSuccess:{
                    response in NSLog((try? response.mapString())!)
                })
                .mapObject(SpotifyUser.self).asObservable()
        }
    }
    
    public func isPremiumUser() -> Observable<Bool> {
        return self.provider().flatMapLatest{ provider in
            return provider.rx.request(SpotifyService.getUserInfo).timeout(10.0, scheduler: MainScheduler.instance)
                .mapObject(SpotifyUser.self)
                .map{spotifyUser in spotifyUser.productType == .Premium}
        }
    }
    
    public func suggestedPresets() -> Observable<[SpotifyPreset]> {
        return self.isPremiumUser().flatMap{[weak self] premiumUser -> Observable<[SpotifyPreset]> in
            guard let `self` = self else { return Observable.empty() }
            if premiumUser {
                return self.provider().flatMapLatest{ provider in
                    return provider.rx.request(SpotifyService.getPresets)
                        .mapObject(SpotifyPresetsResponse.self)
                        .map{ $0.presets ?? [] }
                        .map{ Array($0[0..<(min(7,$0.count))]) }
                }
            } else {
                return Observable.error(SpotifyProviderError.notPremiumUser)
            }
        }
    }
    
    public func artworkURLForPlaylistURI(_ playlistURI: String) -> Observable<URL?> {
        if let existingURL = SpotifyArtworkCache.sharedInstace.artworkURLForURI(playlistURI) {
            return Observable.just(existingURL)
        } else {
            return self.provider().flatMapLatest{ provider -> Observable<URL?> in
                var fetchURL: Observable<URL?> = Observable.just(nil)
                if playlistURI.contains("playlist") {
                    fetchURL = provider.rx.request(SpotifyService.getPlaylistArtwork(spotifyURI: playlistURI))
                        .mapObject(SpotifyPlaylist.self)
                        .map{ playlist in
                            playlist.images?.first?.url
                        }.asObservable()
                } else if playlistURI.contains("album") {
                    fetchURL = provider.rx.request(SpotifyService.getAlbumArtwork(spotifyURI: playlistURI))
                        .mapObject(SpotifyAlbum.self)
                        .map{ album in
                            album.images?.first?.url
                        }.asObservable()
                }
                else if playlistURI.contains("artist") {
                    fetchURL = provider.rx.request(SpotifyService.getArtistArtwork(spotifyURI: playlistURI))
                        .mapObject(SpotifyArtist.self)
                        .map{ artist in
                            artist.images?.first?.url
                        }.asObservable()
                }
                    
                else if playlistURI.contains("track") {
                    fetchURL = provider.rx.request(SpotifyService.getTrackArtwork(spotifyURI: playlistURI))
                        .mapObject(SpotifyTrack.self)
                        .map{ track in
                            track.images?.first?.url
                        }.asObservable()
                }
                    
                else if playlistURI.contains("user") {
                    
                    fetchURL = provider.rx.request(SpotifyService.getUserArtwork(spotifyURI: playlistURI))
                        .mapObject(SpotifyUser.self)
                        .map{ user in
                            user.images?.first?.url
                        }.asObservable()
                }
                
                return fetchURL.do(onNext:{ imageURL in
                    if let url = imageURL {
                        SpotifyArtworkCache.sharedInstace.storeArtworkURL(url, forURI: playlistURI)
                    }
                })
            }
        }
    }
 }
