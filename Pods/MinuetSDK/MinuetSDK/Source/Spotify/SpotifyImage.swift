//
//  SpotifyImage.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 30/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import ObjectMapper
import Foundation

public struct SpotifyImage: Mappable {
    public var width: Int?
    public var height: Int?
    public var url: URL?
    
    public init?(map: Map) {}
    
    public mutating func mapping(map: Map) {
        width  <- map["width"]
        height <- map["height"]
        url    <- (map["url"], URLTransform())
    }
}
