//
//  SpotifyAuthProviderError.swift
//  MinuetSDK
//
//  Created by Claudiu Alin Luminosu on 14/11/2017.
//  Copyright © 2017 Zound Industries. All rights reserved.
//

import Foundation

enum SpotifyAuthProviderError: Swift.Error {
    case noRefreshTokenProvided
}

