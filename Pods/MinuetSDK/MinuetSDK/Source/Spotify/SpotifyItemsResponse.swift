//
//  SpotifyItemsResponse.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 30/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import ObjectMapper

public struct SpotifyItemsResponse<T: Mappable>: Mappable {
    public var href: URL?
    public var items: [T]?
    public var limit: Int?
    public var next: URL?
    public var offset: Int?
    public var previous: URL?
    public var total: Int?
    
    public init?(map: Map) {}
    
    public mutating func mapping(map: Map) {
        href     <- (map["href"], URLTransform())
        items    <-  map["items"]
        limit    <-  map["limit"]
        offset   <-  map["offset"]
        next     <- (map["next"], URLTransform())
        previous <- (map["previous"], URLTransform())
        total    <-  map["total"]
    }
}
