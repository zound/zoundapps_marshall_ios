//
//  SpotifyLogin.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 30/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

let OpenSpotifyURLNotificationName = "OpenSpotifyURLNotification"
let nativeLoginAppURL = "spotify-action://authorize"
let webLoginURL = "https://accounts.spotify.com/authorize"
let responseType = "code"
let showDialog = false

enum SpotifyScope: String {
    case PlaylistReadPrivate = "playlist-read-private"
    case PlaylistReadCollaborative = "playlist-read-collaborative"
    case PlaylistModifyPublic = "playlist-modify-public"
    case PlaylistModifyPrivate = "playlist-modify-private"
    case Streaming = "streaming"
    case UserFollowModigy = "user-follow-modify"
    case UserFollowRead = "user-follow-read"
    case UserLibraryRead = "user-library-read"
    case UserLibraryModify = "user-library-modify"
    case UserReadPrivate = "user-read-private"
    case UserReadBrithdate = "user-read-birthdate"
    case UserReadEmail = "user-read-email"
    case UserTopRead = "user-top-read"
}

public enum SpotifyLoginError: Error {
    case authorizationFailed(spotifyError: String)
    case swapFailed
    case refreshFailed
    case cancelled
    case other(error: Error)
}

protocol SpotifyLoginDelegate: class {
    func didFinishSpotifyLoginWithAccessToken(_ accessToken: SpotifyToken)
    func didFinishSpotifyLoginWithError(_ error: SpotifyLoginError)
}

public class SpotifyLogin {
    weak var delegate: SpotifyLoginDelegate?
    var disposeBag = DisposeBag()
    var currentState: String?
    let isUrbanearsApp : Bool
    
    var clientId : String = ""
    var redirectURI : String = ""
    let spotifyAuthProvider : SpotifyAuthProvider
    
    public init(isUrbanearsApp:Bool) {
        self.isUrbanearsApp = isUrbanearsApp
        self.spotifyAuthProvider = self.isUrbanearsApp ?  SpotifyAuthProviderUE() : SpotifyAuthProviderMar()
        
        if let infoPlist = Bundle.main.infoDictionary {
            if let appSettings = infoPlist["AppSettings"] as? Dictionary<String, AnyObject> {
                if let clientID = appSettings["SpotifyClientId"] as? String {
                    self.clientId = clientID
                }
                if let redirectURI = appSettings["SpotifyCallbackURL"] as? String {
                    self.redirectURI = redirectURI
                } 
            }
        }
        
        NotificationCenter.default
            .rx.notification(Foundation.Notification.Name(rawValue: OpenSpotifyURLNotificationName))
            .subscribe(onNext:{ [weak self] notification in self?.handleCallback(notification) })
            .disposed(by: disposeBag)
        
        let didBecomeActive = NotificationCenter.default.rx.notification(UIApplication.didBecomeActiveNotification)
        didBecomeActive.subscribe(onNext: { [weak self] _ in
            DispatchQueue.main.async {
                self?.appDidBecomeActive()
            }
            
        }).disposed(by: disposeBag)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    public static func handleOpenURL(_ url: URL, from sourceApplication:String? = nil) {
        if let infoPlist = Bundle.main.infoDictionary,
            let appSettings = infoPlist["AppSettings"] as? Dictionary<String, AnyObject>,
            let redirectURI = appSettings["SpotifyCallbackURL"] as? String,
            url.absoluteString.hasPrefix(redirectURI){
            let loginCallback = SpotifyLoginCallback(url: url, sourceApplication: sourceApplication)
            NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: OpenSpotifyURLNotificationName),
                                            object: loginCallback)
        }
    }
    
    func appDidBecomeActive() {
        if currentState != nil {
            //user is expected to sign in to spotify, if they reach here first it means they came back to our app without doing anything (through the app switched for example or back arrow in the iOS status bar)
            currentState = nil
            self.delegate?.didFinishSpotifyLoginWithError(SpotifyLoginError.cancelled)
        }
    }
    
    func handleCallback(_ notification: Foundation.Notification) {
        func parseParametersString(_ parametersString: String) -> [String: String] {
            var dict = [String: String]()
            let parameters = parametersString.components(separatedBy: "&")
            for parameter in parameters {
                let parts = parameter.components(separatedBy: "=")
                if let key = parts[0].removingPercentEncoding {
                    if parts.count > 1 {
                        if let value = parts[1].removingPercentEncoding {
                            dict[key] = value
                        }
                    }
                }
            }
            return dict
        }
        
        if let loginCallback = notification.object as? SpotifyLoginCallback {
            let url = loginCallback.url
            var componentsDict: [String: String] = [:]
            if let components = URLComponents(url: url, resolvingAgainstBaseURL: false) {
                for item in components.queryItems ?? [] {
                    componentsDict[item.name] = item.value
                }
                
                if let fragment = url.fragment {
                    componentsDict += parseParametersString(fragment)
                }
            }
            
            currentState = nil
            
            if let _ = loginCallback.sourceApplication {
                /*if appName.contains("spotify") {
                 self.delegate?.didFinishSpotifyLoginWithError(SpotifyLoginError.authorizationFailed(spotifyError: "unknown_error"))
                 } else {*/
                
                if let code = componentsDict["code"] {
                    self.getTokensFromAuthCode(code)
                } else {
                    if let spotifyError = componentsDict["error"] {
                        self.delegate?.didFinishSpotifyLoginWithError(SpotifyLoginError.authorizationFailed(spotifyError: spotifyError))
                    }
                }
                //}
            }
        }
    }
    
    func getTokensFromAuthCode(_ code: String) {
        spotifyAuthProvider.swap(code, redirectUri: self.redirectURI).timeout(10.0, scheduler: MainScheduler.instance).subscribe(
            onNext: {[weak self] token in
                self?.delegate?.didFinishSpotifyLoginWithAccessToken(token)
                return
            },
            onError: {[weak self] error in
                NSLog("\(error.localizedDescription)")
                self?.delegate?.didFinishSpotifyLoginWithError(SpotifyLoginError.swapFailed)
                return
        })
            .disposed(by: disposeBag)
    }
    
    func getRefreshedTokenFromExpiredToken(_ token: SpotifyToken) {
        spotifyAuthProvider.refresh(token).subscribe(
            onNext: {[weak self] newToken in
                var newToken = newToken
                newToken.refreshToken = token.refreshToken
                self?.delegate?.didFinishSpotifyLoginWithAccessToken(newToken)
                return
            },
            onError: {[weak self] error in
                NSLog(error as! String)
                self?.delegate?.didFinishSpotifyLoginWithError(SpotifyLoginError.swapFailed)
                return
        })
            .disposed(by: disposeBag)
    }
    
    func supportsNativeAppAuthentication() -> Bool {
        return UIApplication.shared.canOpenURL(URL(string:nativeLoginAppURL)!)
    }
    
    func loginWith(clientId: String, redirectURI: String, state: String, scope: [SpotifyScope], foceWeb: Bool) {
        let encodedClientId = clientId.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlHostAllowed)!
        let encodedRedirectURI = redirectURI.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlHostAllowed)!
        let encodedState = state.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlHostAllowed)!
        let encodedScope = scope.map{$0.rawValue}.joined(separator: " ").addingPercentEncoding(withAllowedCharacters: CharacterSet.urlHostAllowed)!
        
        let params = "client_id=\(encodedClientId)&" +
            "redirect_uri=\(encodedRedirectURI)&" +
            "scope=\(encodedScope)&" +
            "response_type=\(responseType)&" +
            "state=\(encodedState)&" +
            "show_dialog=\(showDialog)&" +
        "utm_source=urbanears_app"
        
        var authorizeURLString = "\(webLoginURL)?\(params)"
        
        if supportsNativeAppAuthentication() && !foceWeb {
            authorizeURLString = "\(nativeLoginAppURL)?\(params)"
        }
        
        if let url = URL(string: authorizeURLString, relativeTo: nil) {
            if UIApplication.shared.canOpenURL(url) {
                currentState = state
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    func loginWithScope(_ scope: [SpotifyScope], state: String, forceWeb: Bool) {
        //configure the app at https://developer.spotify.com/my-applications/
        //Username:            ZoundIndustries
        //Password:            Zound2015
        
        loginWith(clientId: clientId, redirectURI: redirectURI, state: state, scope: scope, foceWeb: forceWeb)
    }
    
    func loginForUserInfoAndPresetsForceWebLogin(_ webLogin: Bool) {
        let randomNumber = Int(arc4random_uniform(999999999))
        loginWithScope([.UserReadPrivate, .Streaming], state: String(randomNumber), forceWeb: webLogin)
    }
}
