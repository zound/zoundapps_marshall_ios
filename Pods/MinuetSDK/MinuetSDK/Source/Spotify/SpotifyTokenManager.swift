//
//  SpotifyTokenManager.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 03/10/2016.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import RxSwift

public class SpotifyTokenManager {
    let disposeBag = DisposeBag()
    public let spotifyTokenVariable: Variable<SpotifyToken?> = Variable(nil)
    public let spotifyError: Variable<SpotifyLoginError?> = Variable(nil)
    var isLoggingIn = false
    var isRefreshing = false
    let isUrbanearsApp : Bool
    
    public var hasToken: Bool {
        return spotifyToken != nil
    }
    
    public var tokenIsValid: Bool {
        if let token = spotifyToken {
            return !token.isExpired
        }
        return false
    }
    
    public var spotifyToken: SpotifyToken? {
        get {
            return spotifyTokenVariable.value
        }
        set {
            spotifyTokenVariable.value = newValue
        }
    }
    
    let authProvider : SpotifyAuthProvider
    
    public init(isUrbanearsApp : Bool) {
        self.authProvider = isUrbanearsApp ? SpotifyAuthProviderUE() : SpotifyAuthProviderMar()
        self.isUrbanearsApp = isUrbanearsApp
        if let spotifyToken = SpotifyTokenStore.getCurrentToken() {
            self.spotifyToken = spotifyToken
        }
    }
    
    public func refreshToken() -> Observable<SpotifyToken> {
        if !isRefreshing && !isLoggingIn {
            if let token = self.spotifyToken {
                if hasToken {
                    isRefreshing = true
                    return authProvider.refresh(token).do(onNext:{ [weak self] token in
                        self?.updateToken(newToken: token)
                        }, onError:{ [weak self] error in
                            
                            self?.clearToken()
                            self?.isRefreshing = false
                    })
                } else {
                    return Observable.error(SpotifyLoginError.refreshFailed)
                }
            } else {
                return Observable.error(SpotifyLoginError.refreshFailed)
            }
        } else {
            return spotifyTokenVariable.asObservable().unwrapOptional().filter{ !$0.isExpired }
        }
    }
    
    public func clearToken() {
        self.spotifyToken = nil
        SpotifyTokenStore.clearToken()
    }
    
    private func updateToken(newToken: SpotifyToken) {
        isRefreshing = false
        isLoggingIn = false
        SpotifyTokenStore.storeToken(newToken)
        self.spotifyToken = newToken
    }
    
    public func login(forceWebLogin: Bool = false) -> Observable<SpotifyToken> {
        if !isRefreshing && !isLoggingIn {
            isLoggingIn = true
            self.spotifyError.value = nil
            return self.loginWithSpotify(forceWebLogin)
                .do(onNext: { [weak self] token in
                    self?.updateToken(newToken: token)
                    }, onError:{ [weak self] error in
                        self?.isLoggingIn = false
                })
            
        } else {
            return spotifyTokenVariable.asObservable().unwrapOptional().filter{ !$0.isExpired }
        }
    }
    
    public func loginWithSpotify(_ forceWebLogin: Bool) -> Observable<SpotifyToken> {
        return Observable.create { observer in
            var spotifyLogin: SpotifyLogin! = SpotifyLogin(isUrbanearsApp: self.isUrbanearsApp)
            var spotifyLoginDelegate: SpotifyLoginRxDelegate! = SpotifyLoginRxDelegate()
            spotifyLogin.delegate = spotifyLoginDelegate
            
            let cleanDisposable = Disposables.create {
                
                spotifyLogin = nil
                spotifyLoginDelegate = nil
            }
            
            spotifyLogin.loginForUserInfoAndPresetsForceWebLogin(forceWebLogin)
            
            let compositeDisposable = CompositeDisposable()
            let errorDisposable = spotifyLoginDelegate.spotifyLoginDidError.subscribe(onNext: { error in
                observer.onError(error)
            })
            
            let nextDisposable = spotifyLoginDelegate.spotifyLoginDidFinish.subscribe(onNext: { token in
                observer.onNext(token)
                observer.onCompleted()
            })
            
            _ = compositeDisposable.insert(errorDisposable)
            _ = compositeDisposable.insert(nextDisposable)
            _ = compositeDisposable.insert(cleanDisposable)
            
            return compositeDisposable
        }
    }
}
