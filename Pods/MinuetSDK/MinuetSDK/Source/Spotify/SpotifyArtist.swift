//
//  SpotifyArtist.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 04/10/2016.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import ObjectMapper

public struct SpotifyArtist: Mappable {
    public var externalSpotifyURL: URL?
    public var href: URL?
    public var id: String?
    public var images: [SpotifyImage]?
    public var name: String?
    public var uri: String?
    
    public init?(map: Map) {}
    public mutating func mapping(map: Map) {
        externalSpotifyURL  <- (map["external_urls.spotify"], URLTransform())
        href                <- map["href"]
        id                  <- map["id"]
        images              <- map["images"]
        name                <- map["name"]
        uri                 <- map["uri"]
    }
}
