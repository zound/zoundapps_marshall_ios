//
//  CachablePlugin.swift
//  MinuetSDK
//
//  Created by Dolewski Bartosz A (Ext) on 09/01/2019.
//

import Foundation
import Moya

protocol Cachable {
    var cachePolicy: URLRequest.CachePolicy { get }
}

final class CachablePlugin: PluginType {
    public func prepare(_ request: URLRequest, target: TargetType) -> URLRequest {
        guard let cachable = target as? Cachable else { return request }
        
        var mutableRequest = request
        mutableRequest.cachePolicy = cachable.cachePolicy
        return mutableRequest
    }
}
