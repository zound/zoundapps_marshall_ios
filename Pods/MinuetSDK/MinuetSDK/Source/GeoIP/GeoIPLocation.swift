//
//  GeoIPLocation.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 06/09/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import ObjectMapper

public struct GeoIPLocation {
    public var ip: String?
    public var countryCode: String?
    public var countryName: String?
    public var regionCode: String?
    public var regionName: String?
    public var city: String?
    public var latitude: Double?
    public var longitude: Double?
}

extension GeoIPLocation: Mappable {
    
    public init?(map: Map) {}
    public mutating func mapping(map: Map) {
        ip          <- map["ip"]
        countryCode <- map["country_code"]
        countryName <- map["country_name"]
        regionCode  <- map["region_code"]
        regionName  <- map["region_name"]
        city        <- map["city"]
        latitude    <- map["latitude"]
        longitude   <- map["longitude"]
    }
}
