//
//  GeoIPProvider.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 06/09/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import Moya
import RxSwift
import Moya_ObjectMapper

public class GeoIPProvider {
    var geoProvider: MoyaProvider<GeoIPService>
    var ipProvider: PublicIPProvider
    
    public init() {
        geoProvider = MoyaProvider<GeoIPService>(plugins: [CachablePlugin()])
        ipProvider = PublicIPProvider()
    }
    
    public func currentIPAddressLocation() -> Observable<GeoIPLocation> {
        return ipProvider
            .currentPublicIP()
            .flatMap { [geoProvider] publicIPResponse -> Observable<GeoIPLocation> in
                guard let ip = publicIPResponse.ip else {
                    return geoProvider.rx.request(GeoIPService.getLocation)
                        .mapObject(GeoIPLocation.self)
                        .asObservable()
                }
                
                return geoProvider.rx.request(GeoIPService.getLocationFor(ip: ip))
                    .mapObject(GeoIPLocation.self)
                    .asObservable()
        }
    }
}
