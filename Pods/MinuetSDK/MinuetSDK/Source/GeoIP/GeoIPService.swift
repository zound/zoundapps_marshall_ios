//
//  GeoIPService.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 06/09/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import Moya

enum GeoIPService {
    case getLocation
    case getLocationFor(ip: String)
}

extension GeoIPService: TargetType {
    var API_KEY: String {
        return "a40080b5a90fa8518a8faa4400f2fa8e"
    }
    
    var headers: [String : String]? {
        return nil
    }
    
    var baseURL: URL {
        switch self {
        case .getLocation:
            return URL(string: "http://api.ipstack.com/check?access_key=\(API_KEY)")!
        case .getLocationFor(let ip):
            return URL(string: "http://api.ipstack.com/\(ip)?access_key=\(API_KEY)")!
        }
    }
    
    var path: String {
        switch self {
        case .getLocation, .getLocationFor(_): return ""
        }
    }
    
    var method: Moya.Method {
        return .get
    }
    
    public var task: Task {
        return Task.requestPlain
    }
    
    var sampleData: Data {
        return Data()
    }
}

extension GeoIPService: Cachable {
    var cachePolicy: URLRequest.CachePolicy {
        return .returnCacheDataElseLoad
    }
}

