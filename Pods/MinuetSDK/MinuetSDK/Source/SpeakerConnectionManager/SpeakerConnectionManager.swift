//
//  SpeakerConnectionManager.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 27/04/2017.
//  Copyright © 2017 Zound Industries. All rights reserved.
//

import Foundation
import RxSwift

public enum SpeakerConnectionStatus: Equatable {
    case idle
    case disconnected(error: SpeakerNotifierError?)
    case paused
    case connecting
    case connected
}

public func ==(lhs: SpeakerConnectionStatus, rhs: SpeakerConnectionStatus) -> Bool {
    switch (lhs, rhs) {
    case (.idle, .idle): return true
    case (.paused, .paused): return true
    case (.connecting, .connecting): return true
    case (.connected, .connected): return true
    case (.disconnected(_), .disconnected(_)): return true
        
    default:
        return false
    }
}

public class SpeakerConnectionManager {
    let disposeBag = DisposeBag()
    var masterObserverDisposeBag = DisposeBag()
    
    public let clientSpeaker: Speaker
    var masterSpeakerVariable: Variable<Speaker>!
    public var masterSpeaker: Speaker {
        get {
            return masterSpeakerVariable.value
        }
        set {
            masterSpeakerVariable.value = newValue
        }
    }
    
    let clientStateManager: ClientStateManager
    
    public var clientState: Variable<GroupClientState?> {
        return clientStateManager.speakerState
    }
    
    let masterStateManager: MasterStateManager?
    
    public var masterState: Variable<GroupMasterState?> {
        return masterStateManager?.speakerState ?? Variable(nil)
    }
    
    public var clientNotifier: SpeakerNotifier {
        return clientStateManager.speakerNotifier
    }
    
    public var masterNotifier: SpeakerNotifier? {
        return masterStateManager?.speakerNotifier
    }
    
    public let provider: SpeakerProvider
    public var connectionStatus: Variable<SpeakerConnectionStatus> = Variable(SpeakerConnectionStatus.idle)
    
    public let audioSystem: AudioSystem
    
    public init(clientSpeaker: Speaker,
                clientNotifier: SpeakerNotifier,
                clientState: GroupClientState,
                masterSpeaker: Speaker,
                masterState: GroupMasterState,
                audioSystem: AudioSystem,
                speakerProvider: SpeakerProvider) {
        
        self.provider = speakerProvider
        self.audioSystem = audioSystem
        
        let clientSpeakerNotifier = SpeakerNotifier(speaker: clientSpeaker, provider: provider)
        self.clientSpeaker = clientSpeaker
        self.clientStateManager = ClientStateManager(speaker: clientSpeaker, speakerState: clientState, speakerNotifier: clientSpeakerNotifier,  provider: provider)
        
        self.masterSpeakerVariable = Variable(masterSpeaker)
        self.masterStateManager = MasterStateManager(speaker: masterSpeaker, speakerState: masterState, speakerNotifier: nil, provider: provider)
        
        NSLog("master is : \(masterSpeaker.friendlyName) client is \(clientSpeaker.friendlyName)")
        
        Observable.combineLatest(self.clientStateManager.connectionStatus.asObservable(),
                                 self.masterStateManager!.connectionStatus.asObservable()) { client, master in
                                    return (client: client, master: master) }
            .subscribe(onNext: { [weak self] client, master in
                self?.updateForClientStatus(client, masterStatus: master) })
            .disposed(by: disposeBag)
    }
    
    func updateForClientStatus(_ clientStatus: SpeakerConnectionStatus, masterStatus: SpeakerConnectionStatus) {
        NSLog("master status: \(masterStatus), clientStatus: \(clientStatus)")
        var newStatus = SpeakerConnectionStatus.idle
        switch clientStatus {
        case .connected:
            switch masterStatus {
            case .connected: newStatus = .connected
            case .connecting: newStatus = .connected
            case .idle: newStatus = .idle
            case .paused: newStatus = .paused
            case .disconnected(let masterError): newStatus = .disconnected(error: masterError)
            }
        case .connecting:
            switch masterStatus {
            case .connected: newStatus = .connecting
            case .connecting: newStatus = .connecting
            case .idle: newStatus = .connecting
            case .paused: newStatus = .connecting
            case .disconnected(let masterError): newStatus = .disconnected(error: masterError)
            }
        case .idle:
            switch masterStatus {
            case .connected: newStatus = .idle
            case .connecting: newStatus = .idle
            case .idle: newStatus = .idle
            case .paused: newStatus = .idle
            case .disconnected(let masterError): newStatus = .disconnected(error: masterError)
            }
        case .paused:
            switch masterStatus {
            case .connected: newStatus = .paused
            case .connecting: newStatus = .paused
            case .idle: newStatus = .paused
            case .paused: newStatus = .paused
            case .disconnected(let masterError): newStatus = .disconnected(error: masterError)
            }
        case .disconnected(let clientError):
            switch masterStatus {
            case .connected: newStatus = .disconnected(error: clientError)
            case .connecting: newStatus = .disconnected(error: clientError)
            case .idle: newStatus = .disconnected(error: clientError)
            case .paused: newStatus = .disconnected(error: clientError)
            case .disconnected(_): newStatus = .disconnected(error: clientError)
            }
        }
        self.connectionStatus.value = newStatus
    }
    
    func updateForMasterSpeaker(masterSpeaker: Speaker?) {
        updateForMasterSpeaker(masterSpeaker: masterSpeaker, initialStateFetch: true)
    }
    
    func updateForMasterSpeaker(masterSpeaker: Speaker?, initialStateFetch: Bool = false) {
        if let masterSpeaker = masterSpeaker {
            NSLog("changed master to \(masterSpeaker.friendlyName)")
            
            if masterSpeaker != clientSpeaker {
                let masterSpeakerNotifier = SpeakerNotifier(speaker: masterSpeaker, provider: provider)
                self.masterStateManager?.speaker = masterSpeaker
                self.masterSpeaker = masterSpeaker
                self.masterStateManager?.speakerNotifier = masterSpeakerNotifier
                masterSpeakerNotifier.connect()
                
                if initialStateFetch {
                    self.masterStateManager?.handleReconnect()
                }
            } else {
                NSLog("client is the same as master")
                self.masterStateManager?.speaker = clientSpeaker
                self.masterSpeaker = masterSpeaker
                self.masterStateManager?.speakerNotifier = clientStateManager.speakerNotifier
                clientStateManager.speakerNotifier.connect()
                
                if initialStateFetch {
                    self.masterStateManager?.handleReconnect()
                }
            }
        }
    }
    
    public func connect() {
        let speakerGroupComparer = {(rhs: SpeakerGroup?, lhs: SpeakerGroup?) in rhs == lhs }
        let speakerComparer = {(rhs: Speaker?, lhs: Speaker?) in rhs == lhs }
        
        self.masterObserverDisposeBag = DisposeBag()
        self.clientStateManager.speakerNotifier.connect()
        
        let clientSpeaker = self.clientSpeaker
        let masterSpeakerObservable = audioSystem.groups.asObservable()
            .map { $0.find({ $0.speakers.contains(clientSpeaker)}) }
            .distinctUntilChanged(speakerGroupComparer)
            .map { $0?.masterSpeaker }
            .distinctUntilChanged(speakerComparer)
        
        masterSpeakerObservable
            .skip(1)
            .subscribe(weak: self, onNext: SpeakerConnectionManager.updateForMasterSpeaker)
            .disposed(by: masterObserverDisposeBag)
        
        updateForMasterSpeaker(masterSpeaker: masterSpeaker, initialStateFetch: false)
    }
    
    public func reconnect() {
        clientNotifier.connect()
        if let masterNotifier = masterNotifier {
            if masterNotifier.speaker != clientNotifier.speaker {
                masterNotifier.connect()
            }
        }
    }
    
    public func cancelReconnect() {
        masterObserverDisposeBag = DisposeBag()
        connectionStatus.value = SpeakerConnectionStatus.disconnected(error: SpeakerNotifierError.canceledByUser)
        clientNotifier.disconnect()
        masterNotifier?.disconnect()
    }
}
