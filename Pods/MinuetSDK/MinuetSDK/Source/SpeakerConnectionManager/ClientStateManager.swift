//
//  ClientStateManager.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 31/10/2016.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import RxSwift

class ClientStateManager {
    let speaker: Speaker
    let speakerNotifier: SpeakerNotifier
    let provider: SpeakerProvider
    let speakerState: Variable<GroupClientState?>
    var connectionStatus: Variable<SpeakerConnectionStatus> = Variable(SpeakerConnectionStatus.idle)
    let disposeBag = DisposeBag()
    let nodeDependecies: [ScalarNode:[ScalarNode]]
    
    init(speaker: Speaker, speakerState: GroupClientState, speakerNotifier: SpeakerNotifier, provider: SpeakerProvider) {
        self.speaker = speaker
        self.provider = provider
        self.speakerNotifier = speakerNotifier
        self.speakerState = Variable(speakerState)
        
        nodeDependecies = [ ScalarNode.PlayStatus: [ScalarNode.PlayCaps],
                            ScalarNode.Mode: [ScalarNode.PlayCaps] ]
        
        setupBackgroundForegroundMonitoring()
        self.speakerNotifier.addDelegate(self)
    }
    
    func updateSpeakerStateWithNodes(_ nodes:[ScalarNode:String]) {
        var dependenciesToFetch: Set<ScalarNode> = Set<ScalarNode>()
        for (node,value) in nodes {
            speakerState.value?.updateWithNode(node, value: value)
            if let dependencies = nodeDependecies[node] {
                for node in dependencies {
                    dependenciesToFetch.insert(node)
                }
            }
            
            if node == ScalarNode.Mode {
                if let castMode = speakerState.value?.modes.find({$0.isCast}) {
                    if castMode.key == Int(value) {
                        //we've switched to cast
                        dependenciesToFetch.insert(ScalarNode.CastAppName)
                    }
                } else {
                    updateSpeakerStateWithNodes([ScalarNode.CastAppName: ""])
                }
            }
            if node == ScalarNode.NavPresetVersion {
                updatePresets()
            }
            if node == ScalarNode.BluetoothDevicesListVersion {
                updateBluetoothDevices()
            }
        }
        if dependenciesToFetch.count > 0 {
            fetchAndUpdateDependencies(Array(dependenciesToFetch))
        }
    }
    
    func updatePresets() {
        SpeakerPresets.getPresetsForSpeaker(speaker, provider: provider)
            .subscribe(onNext:{ [weak self] presets in
                self?.speakerState.value?.presets = presets })
            .disposed(by: disposeBag)
    }
    
    func updateBluetoothDevices() {
        let speaker = self.speaker
        provider.getListNode(ListNode.BluetoothDevices, forSpeaker: speaker)
            .catchError{ error -> Observable<[BluetoothDevice]> in return Observable.just([]) }
            .subscribe(onNext:{ [weak self] bluetoothDevices in
                self?.speakerState.value?.bluetoothDevices = bluetoothDevices })
            .disposed(by: disposeBag)
    }
    
    func fetchAndUpdateDependencies(_ nodes:[ScalarNode]) {
        NSLog("fetching dependent nodes \(nodes)")
        provider.getNodes(nodes, forSpeaker: speaker)
            .subscribe(weak: self, onNext: ClientStateManager.updateSpeakerStateWithNodes)
            .disposed(by: disposeBag)
    }
    
    deinit {
        speakerNotifier.removeDelegate(self)
    }
}

extension ClientStateManager: SpeakerNotifierDelegate {
    func delegateIdentifier() -> String {
        return "client_state_manager"+self.speaker.mac
    }
    
    func didConnectToSpeaker(_ speaker: Speaker) {
        switch connectionStatus.value {
        case .disconnected(_):
            handleReconnect()
        case .paused:
            handleUnpause()
        case .idle:
            self.connectionStatus.value = .connected
        default:
            break
        }
    }
    
    func handleUnpause() {
        self.connectionStatus.value = .connected
        provider.getClientStateForSpeaker(speaker)
            .subscribe(onNext: { [weak self] clientState in
                self?.speakerState.value = clientState
                }, onError: { [weak self] error in
                    NSLog("client state connection error \(error.localizedDescription)")
                    self?.connectionStatus.value = .disconnected(error: SpeakerNotifierError.networkConnectionLost)
                    self?.speakerNotifier.disconnect() })
            .disposed(by: disposeBag)
    }
    
    func handleReconnect() {
        self.connectionStatus.value = .connecting
        provider.getClientStateForSpeaker(speaker)
            .subscribe(onNext: { [weak self] clientState in
                self?.speakerState.value = clientState
                self?.connectionStatus.value = .connected
                }, onError: { [weak self] error in
                    NSLog("client state reconnect error \(error.localizedDescription)")
                    self?.speakerNotifier.disconnect() })
            .disposed(by: disposeBag)
    }
    
    func speaker(_ speaker: Speaker, didNotifyWithNodesValues nodes: [ScalarNode : String]) {
        NSLog("client notifications from \(speaker.friendlyName)")
        for key in nodes.keys.sorted(by: { v1,v2 in v1.rawValue < v2.rawValue }) {
            NSLog("\(key.rawValue) - \(nodes[key]!)")
        }
        updateSpeakerStateWithNodes(nodes)
    }
    
    func speaker(_ speaker: Speaker, disconnectedWithError error: SpeakerNotifierError?) {
        switch connectionStatus.value {
        case .connected, .connecting:
            self.connectionStatus.value = .disconnected(error: error)
            self.speakerState.value = nil
        default:
            break
        }
    }
}

extension ClientStateManager {
    func setupBackgroundForegroundMonitoring() {
        let didEnterBackground = NotificationCenter.default.rx.notification(UIApplication.didEnterBackgroundNotification)
        let willEnterForeground = NotificationCenter.default.rx.notification(UIApplication.willEnterForegroundNotification)
        
        didEnterBackground
            .subscribe(onNext: { [weak self] _ in
                self?.didEnterBackground() })
            .disposed(by: disposeBag)
        
        willEnterForeground
            .subscribe(onNext: { [weak self] _ in
                self?.willEnterForeground() })
            .disposed(by: disposeBag)
    }
    
    func didEnterBackground() {
        if case SpeakerConnectionStatus.connected = self.connectionStatus.value {
            self.connectionStatus.value = .paused
            self.speakerNotifier.disconnect(pause: true)
        }
    }
    
    func willEnterForeground() {
        if case SpeakerConnectionStatus.paused = self.connectionStatus.value {
            self.speakerNotifier.connect(forceNewSession: false)
        }
    }
}
