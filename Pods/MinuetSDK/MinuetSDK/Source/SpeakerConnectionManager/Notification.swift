//
//  Notification.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 25/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

public struct Notification {
    public let node: String
    public let value: String
    public let type: String
}
