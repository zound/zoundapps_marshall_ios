//
//  MasterStateManager.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 31/10/2016.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import RxSwift

class MasterStateManager {
    var speaker: Speaker?
    private var _speakerNotifier: SpeakerNotifier?
    var speakerNotifier: SpeakerNotifier? {
        get {
            return _speakerNotifier
        }
        set {
            _speakerNotifier?.removeDelegate(self)
            _speakerNotifier = newValue
            updateConnectionStatusFromNotifier()
            _speakerNotifier?.addDelegate(self)
        }
    }
    let provider: SpeakerProvider
    let speakerState: Variable<GroupMasterState?>
    var connectionStatus: Variable<SpeakerConnectionStatus> = Variable(SpeakerConnectionStatus.idle)
    
    let disposeBag = DisposeBag()
    
    let nodeDependecies: [ScalarNode:[ScalarNode]]
    
    init(speaker: Speaker, speakerState: GroupMasterState,speakerNotifier: SpeakerNotifier?, provider: SpeakerProvider) {
        
        self.speaker = speaker
        self.provider = provider
        self.speakerState = Variable(speakerState)
        
        nodeDependecies = [ ScalarNode.PlayName:
            [ScalarNode.PlayArtist,
             ScalarNode.PlayAlbum,
             ScalarNode.PlayGraphicUri,
             ScalarNode.PlayDuration],
                            ScalarNode.PlayStatus:
                                [ScalarNode.PlayCaps,
                                 ScalarNode.PlayGraphicUri,
                                 ScalarNode.PlayDuration],
                            ScalarNode.Mode:
                                [ScalarNode.PlayCaps,
                                 ScalarNode.NavCaps,
                                 ScalarNode.PlayGraphicUri,
                                 ScalarNode.PlayDuration]
        ]
        
        self.speakerNotifier = speakerNotifier
        setupBackgroundForegroundMonitoring()
    }
    
    func updateConnectionStatusFromNotifier() {
        if let connected = self.speakerNotifier?.connected {
            self.connectionStatus.value = connected ? SpeakerConnectionStatus.connected : SpeakerConnectionStatus.disconnected(error: SpeakerNotifierError.unknownError(underlyingError: nil))
        }
        else {
            self.connectionStatus.value = .idle
        }
    }
    
    func updateSpeakerStateWithNodes(_ nodes:[ScalarNode:String]) {
        var dependenciesToFetch: Set<ScalarNode> = Set<ScalarNode>()
        for (node,value) in nodes {
            var updateOnNotification = true
            if node == ScalarNode.PlayStatus && speakerState.value?.playStatus == PlayStatus(rawValue: Int(value)!) {
                updateOnNotification = false
            }
            if let userInteraction = speakerState.value?.userInteractionInProgress{
                
                if node == ScalarNode.MultiroomMasterVolume && userInteraction {
                    updateOnNotification = false
                }
            }
            
            if updateOnNotification {
                speakerState.value?.updateWithNode(node, value: value)
                if let dependencies = nodeDependecies[node] {
                    for node in dependencies {
                        dependenciesToFetch.insert(node)
                    }
                }
            }
            
            if node == ScalarNode.PlayStatus {
                if let currentMode = speakerState.value?.currentMode {
                    if currentMode.isCast {
                        dependenciesToFetch.insert(ScalarNode.CastAppName)
                    }
                }
            }
            
            if node == ScalarNode.Mode {
                if let castMode = speakerState.value?.modes.find({$0.isCast}) {
                    if castMode.key == Int(value) {
                        //we've switched to cast
                        dependenciesToFetch.insert(ScalarNode.CastAppName)
                    }
                } else {
                    updateSpeakerStateWithNodes([ScalarNode.CastAppName:""])
                }
            }
        }
        
        if dependenciesToFetch.count > 0 {
            fetchAndUpdateDependencies(Array(dependenciesToFetch))
        }
    }
    
    func fetchAndUpdateDependencies(_ nodes:[ScalarNode]) {
        NSLog("fetching dependent nodes \(nodes)")
        if let speaker = speaker {
            provider.getNodes(nodes, forSpeaker: speaker).subscribe(weak: self, onNext: MasterStateManager.updateSpeakerStateWithNodes).disposed(by: disposeBag)
        }
    }
    
    deinit {
        speakerNotifier?.removeDelegate(self)
    }
}

extension MasterStateManager: SpeakerNotifierDelegate {
    
    func delegateIdentifier() -> String {
        return "master_state_manager" + (self.speaker?.mac ?? "XX")
    }
    
    func didConnectToSpeaker(_ speaker: Speaker) {
        switch connectionStatus.value {
        case .disconnected(_):
            handleReconnect()
        case .paused:
            handleUnpause()
        case .idle:
            self.connectionStatus.value = .connected
        default:
            break
        }
    }
    
    func handleUnpause() {
        self.connectionStatus.value = .connected
        if let speaker = speaker {
            provider.getMasterStateForSpeaker(speaker)
                .subscribe(onNext: { [weak self] masterState in
                    self?.speakerState.value = masterState
                    }, onError: { [weak self] error in
                        self?.connectionStatus.value = .disconnected(error: SpeakerNotifierError.networkConnectionLost)
                        self?.speakerNotifier?.disconnect()
                }).disposed(by: disposeBag)
        }
    }
    
    func handleReconnect() {
        NSLog("reconnecting to \(self.speaker!.friendlyName)")
        self.connectionStatus.value = .connecting
        if let speaker = speaker {
            provider.getMasterStateForSpeaker(speaker)
                .subscribe(onNext: { [weak self] masterState in
                    self?.speakerState.value = masterState
                    self?.connectionStatus.value = .connected
                    }, onError: { [weak self] error in
                        NSLog("aaa \(error.localizedDescription)")
                        self?.speakerNotifier?.disconnect()
                }).disposed(by: disposeBag)
        }
    }
    
    func speaker(_ speaker: Speaker, didNotifyWithNodesValues nodes: [ScalarNode : String]) {
        NSLog("master notifications from \(speaker.friendlyName)")
        for key in nodes.keys.sorted(by: { v1,v2 in v1.rawValue < v2.rawValue }) {
            NSLog("\(key.rawValue) - \(nodes[key]!)")
        }
        updateSpeakerStateWithNodes(nodes)
    }
    
    func speaker(_ speaker: Speaker, disconnectedWithError error: SpeakerNotifierError?) {
        if error != nil {
            if case .paused = error! {
                self.connectionStatus.value = .paused
            }            
        }
        
        switch connectionStatus.value {
        case .connected, .connecting:
            self.connectionStatus.value = .disconnected(error: error)
            self.speakerState.value = nil
        default:
            break
        }
    }
}

extension MasterStateManager {
    func setupBackgroundForegroundMonitoring() {
        let didEnterBackground = NotificationCenter.default.rx.notification(UIApplication.didEnterBackgroundNotification)
        let willEnterForeground = NotificationCenter.default.rx.notification(UIApplication.willEnterForegroundNotification)
        
        didEnterBackground
            .subscribe(onNext: { [weak self] _ in
                self?.didEnterBackground()
            }).disposed(by: disposeBag)
        
        willEnterForeground
            .subscribe(onNext: { [weak self] _ in
                self?.willEnterForeground()
            }).disposed(by: disposeBag)
    }
    
    func didEnterBackground() {
        if case SpeakerConnectionStatus.connected = self.connectionStatus.value {
            self.connectionStatus.value = .paused
            self.speakerNotifier?.disconnect(pause:true)
        }
    }
    
    func willEnterForeground() {
        if case SpeakerConnectionStatus.paused = self.connectionStatus.value {
            self.speakerNotifier?.connect(forceNewSession: false)
        }
    }
}
