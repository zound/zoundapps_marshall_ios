//
//  SpeakerVolumeManager.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 27/04/2017.
//  Copyright © 2017 Zound Industries. All rights reserved.
//

import Foundation
import RxSwift
import AVFoundation
import JPSVolumeButtonHandler

public protocol SpeakerVolumeManagerDelegate: class {
    func speakerVolumeManagerDidChangeVolume(volumeManager: SpeakerVolumeManager);
}

public class SpeakerVolumeManager {
    let disposeBag = DisposeBag()
    let volumeCapture = JPSVolumeButtonHandler()
    
    var clientState: Variable<GroupClientState?> { return connectionManager.clientState }
    var masterState: Variable<GroupMasterState?> { return connectionManager.masterState }
    
    var clientSpeaker: Speaker { return connectionManager.clientSpeaker}
    var masterSpeaker: Speaker { return connectionManager.masterSpeaker}
    var provider: SpeakerProvider { return connectionManager.provider }
    
    let connectionManager: SpeakerConnectionManager
    public weak var delegate: SpeakerVolumeManagerDelegate?
    
    public init(connectionManager: SpeakerConnectionManager) {
        
        self.connectionManager = connectionManager
        setupVolumeCapture()
    }
    
    func setupVolumeCapture() {
        self.volumeCapture.downBlock = { [weak self] in
            self?.performVolumeDown()
        }
        
        self.volumeCapture.upBlock = { [weak self] in
            self?.performVolumeUp()
        }
        
        let otherAudioIsPlaying = Observable<Int>
            .interval(0.2, scheduler: MainScheduler.instance)
            .startWith(0).map { _ in
                return AVAudioSession.sharedInstance().secondaryAudioShouldBeSilencedHint || AVAudioSession.sharedInstance().isOtherAudioPlaying }
            .distinctUntilChanged()
        
        let isAirplayPlaying = Observable<Int>
            .interval(0.2, scheduler: MainScheduler.instance)
            .startWith(0).map { _ -> Bool in
                let currentRoute = AVAudioSession.sharedInstance().currentRoute
                for outputPort in currentRoute.outputs {
                    if outputPort.portType == AVAudioSession.Port.airPlay {
                        return true
                    }
                }
                return false }
            .distinctUntilChanged()
        
        let modeComparer = {(rhs: Mode, lhs: Mode) in rhs == lhs }
        let currentMode = self.masterState.asObservable()
            .unwrapOptional()
            .map{ $0.currentMode }
            .unwrapOptional()
            .distinctUntilChanged(modeComparer)
        
        let shouldDoVolumeCapture = Observable.combineLatest(currentMode, otherAudioIsPlaying, isAirplayPlaying) { currentMode, otherAudioIsPlaying, isAirplayPlaying -> Bool in
            //NSLog("current mode id = \(currentMode.id)), otherAudioIsPlaying =\(otherAudioIsPlaying)")
            return !(currentMode.id == "Bluetooth" || currentMode.id == "Airplay" || currentMode.id == "Spotify") && !isAirplayPlaying }
            .distinctUntilChanged()
        
        shouldDoVolumeCapture
            .subscribe( onNext:{ [weak self] doVolumeCapture in
                if doVolumeCapture {
                    NSLog("do volume capture")
                    self?.volumeCapture.start(true)
                } else {
                    NSLog("stop volume capture")
                    self?.volumeCapture.stop()
                }
            })
            .disposed(by: disposeBag)
        
        let clientVolumeObservable = self.clientState.asObservable().unwrapOptional().map{ $0.volume }.unwrapOptional().distinctUntilChanged()
        let masterVolumeObservable = self.masterState.asObservable().unwrapOptional().map{ $0.masterVolume }.unwrapOptional().distinctUntilChanged()
        let groupStateObservable = self.clientState.asObservable().unwrapOptional().map{ $0.groupState }.unwrapOptional().distinctUntilChanged()
        groupStateObservable.flatMapLatest{ groupState -> Observable<Int> in
            if [GroupState.solo].contains(groupState) {
                return clientVolumeObservable
            } else {
                return masterVolumeObservable
            }
            }.skip(1).subscribe(onNext: { [weak self] _ in
                
                self?.showMiniVolumeController()
            }).disposed(by: disposeBag)
    }
    
    func performVolumeUp() {
        changeVolume(with: 1, userVolumeSteps: 16)
        showMiniVolumeController()
    }
    
    func performVolumeDown() {
        changeVolume(with: -1, userVolumeSteps: 16)
        showMiniVolumeController()
        
    }
    
    func showMiniVolumeController() {
        self.delegate?.speakerVolumeManagerDidChangeVolume(volumeManager: self)
    }
    
    func newVolumeFrom(steps: Int, userVolumeSteps: Int, currentVolume: Int) -> Int {
        let speakerVolumeSteps = 32.0
        let relativeSpeakerVolume = Double(currentVolume)/speakerVolumeSteps
        let volumeInUserSteps = relativeSpeakerVolume*Double(userVolumeSteps)
        let speakerVolumeStepsPerUserStep = speakerVolumeSteps/Double(userVolumeSteps)
        let roundedVolumeInUserSteps = round(volumeInUserSteps)
        let newVolume = roundedVolumeInUserSteps + Double(steps)
        let newVolumeInSpeakerSteps = min(speakerVolumeSteps,max(0,newVolume * speakerVolumeStepsPerUserStep))
        
        return Int(newVolumeInSpeakerSteps)
    }
    
    func changeVolume(with steps: Int, userVolumeSteps: Int) {
        if let speakerState = self.clientState.value {
            if let groupState = speakerState.groupState {
                if [GroupState.solo].contains(groupState) {
                    if let clientVolume = clientState.value?.volume {
                        let newVolumeInSpeakerSteps = newVolumeFrom(steps: steps, userVolumeSteps: userVolumeSteps, currentVolume: clientVolume)
                        self.clientState.value?.volume = Int(newVolumeInSpeakerSteps)
                        self.provider.setNode(ScalarNode.Volume, value: String(newVolumeInSpeakerSteps), forSpeaker: clientSpeaker).subscribe().disposed(by: disposeBag)
                    }
                    
                } else if [GroupState.client, GroupState.server].contains(groupState) {
                    if let masterVolume = masterState.value?.masterVolume {
                        let newVolumeInSpeakerSteps = newVolumeFrom(steps: steps, userVolumeSteps: userVolumeSteps, currentVolume: masterVolume)
                        self.masterState.value?.masterVolume = Int(newVolumeInSpeakerSteps)
                        self.masterState.value?.userInteractionInProgress = true
                        self.provider.setNode(ScalarNode.MultiroomMasterVolume, value: String(newVolumeInSpeakerSteps), forSpeaker: masterSpeaker).subscribe(onNext:{ [weak self] done in
                            self?.masterState.value?.userInteractionInProgress = false
                        }).disposed(by: disposeBag)
                    }
                }
            }
        }
    }
}
