//
//  File.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 03/05/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import RxSwift
import Moya

public enum SpeakerNotifierError {
    case networkConnectionLost
    case sessionLost
    case unknownError(underlyingError: Swift.Error?)
    case canceledByUser
    case timeout
    case paused
}

public protocol SpeakerNotifierDelegate: class {
    func delegateIdentifier() -> String
    func didConnectToSpeaker(_ speaker:Speaker)
    func speaker(_ speaker: Speaker, didNotifyWithNodesValues:[ScalarNode: String])
    func speaker(_ speaker: Speaker, disconnectedWithError:SpeakerNotifierError?)
}

public class SpeakerNotifier {
    public let speaker: Speaker
    let provider: SpeakerProvider
    var delegates: Set<Weak>
    public var sessionIdsForSpeakers: [String: String] = [:]
    
    fileprivate let notificationsSubject = PublishSubject<[ScalarNode: String]>()
    var notifications: Observable<[ScalarNode: String]> {
        return notificationsSubject.asObservable()
    }
    
    fileprivate let connectedSubject = BehaviorSubject<Bool>(value: false)
    var connectedObservable: Observable<Bool> {
        return connectedSubject
    }
    
    let disposeBag = DisposeBag()
    var connectionDisposable: CompositeDisposable?
    
    public var connected: Bool {
        return (self.connectionDisposable != nil)
    }
    
    public func connect(forceNewSession: Bool = true){
        //NSLog("connect!")
        if !connected {
            connectionDisposable?.dispose()
            connectionDisposable = CompositeDisposable()
            _ = disposeBag.insert(connectionDisposable!)
            
            let storeSessionId: (String) -> Void = {[weak self] sessionId in
                if let speaker = self?.speaker {
                    self?.sessionIdsForSpeakers[speaker.mac] = sessionId
                }
            }
            
            let sessionId: Observable<String> = provider.sessionForSpeaker(speaker).do(onNext: storeSessionId)
            
            var notifications = sessionId.flatMapLatest{ [weak self] sessionId -> Observable<[ScalarNode:String]> in
                guard let `self` = self else { return Observable.error(NetRemoteResponseError.sessionLost) }
                return self.provider.notificationsForSpeaker(self.speaker, sessionId: sessionId)
            }
            
            if !forceNewSession {
                if let knownSessionId = sessionIdsForSpeakers[speaker.mac] {
                    notifications = self.provider.notificationsForSpeaker(self.speaker, sessionId: knownSessionId)
                } else {
                    notifications = Observable.error(NetRemoteResponseError.sessionLost)
                }
            }
            
            let notificationsDisposable = notifications.subscribe(
                onNext: {[weak self] nodes in self?.notifyWithNodes(nodes)},
                onError: { [weak self] error in
                    var notifierError = SpeakerNotifierError.unknownError(underlyingError: error)
                    if let moyaError = error as? MoyaError {
                        switch moyaError {
                        case .underlying(let underlyingError, _):
                            if let urlError = underlyingError as? URLError {
                                switch urlError.code {
                                case .networkConnectionLost:
                                    NSLog("networkConnectionLost ")
                                    notifierError = SpeakerNotifierError.networkConnectionLost
                                case .timedOut:
                                    NSLog("timeout!!")
                                    notifierError = SpeakerNotifierError.timeout
                                default: break
                                }
                            }
                        default: break
                        }
                    } else if let netRemoteResponseError = error as? NetRemoteResponseError {
                        switch netRemoteResponseError {
                        case .sessionLost:
                            notifierError = SpeakerNotifierError.sessionLost
                        default: break
                        }
                    }
                    self?.onNotificationsError(notifierError)
            })
            
            _ = connectionDisposable?.insert(notificationsDisposable)
            
            for weakDelegate in delegates {
                if let delegate = weakDelegate.recall() as? SpeakerNotifierDelegate {
                    delegate.didConnectToSpeaker(speaker)
                }
            }
            
            if connected == false {
                connectedSubject.onNext(true)
            }
        }
    }
    
    func notifyWithNodes(_ nodes: [ScalarNode: String]) {
        for weakDelegate in delegates {
            if let delegate = weakDelegate.recall() as? SpeakerNotifierDelegate {
                delegate.speaker(speaker, didNotifyWithNodesValues: nodes)
            }
        }
        
        NSLog("received notifications: \(nodes)")
        if !nodes.isEmpty {
            notificationsSubject.onNext(nodes)
        } else {
            NSLog("not sending empty notifications array")
        }
    }
    
    public func disconnect(pause: Bool = false) {
        connectionDisposable?.dispose()
        connectionDisposable = nil
        
        for weakDelegate in delegates {
            if let delegate = weakDelegate.recall() as? SpeakerNotifierDelegate {
                delegate.speaker(speaker, disconnectedWithError: (pause ? SpeakerNotifierError.paused : nil))
            }
        }
        
        connectedSubject.onNext(false)
    }
    
    func onNotificationsError(_ error: SpeakerNotifierError) {
        connectionDisposable = nil
        for weakDelegate in delegates {
            if let delegate = weakDelegate.recall() as? SpeakerNotifierDelegate {
                print(error)
                delegate.speaker(speaker, disconnectedWithError: error)
            }
        }
        connectedSubject.onNext(false)
    }
    
    public func addDelegate(_ delegate: SpeakerNotifierDelegate) {
        delegates.insert(Weak(value:delegate))
    }
    
    public func removeDelegate(_ delegate: SpeakerNotifierDelegate) {
        if let weakDelegate = delegates.filter({ weak in
            if let weakValue = weak.recall() as? SpeakerNotifierDelegate {
                return  weakValue.delegateIdentifier() == delegate.delegateIdentifier()
            }
            return false
        }).first {
            delegates.remove(weakDelegate)
        }
    }
    
    public init(speaker: Speaker, provider: SpeakerProvider) {
        self.speaker = speaker
        self.provider = provider
        self.delegates = Set<Weak>()
    }
    
    public func notificationsForNode(_ node: ScalarNode) -> Observable<String>{
        return self.notifications.filter({$0[node] != nil}).map { return $0[node]! }
    }
}
