//
//  BonjourServiceStore.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 12/07/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import ObjectMapper

class SpeakerInfoStore {
    var speakerInfoStorageKey = "speakerInfoCache"
    static let sharedInstance  = SpeakerInfoStore()
    var speakerInfo: [String: [ScalarNode: String]] = [:]
    
    init() {
        if let jsonData = minuetSDKUserDefaults?.data(forKey: speakerInfoStorageKey){
            if let jsonObject = (try? JSONSerialization.jsonObject(with: jsonData, options: [])) as? [String: [String: String]] {
                var speakerInfoByIP:[String: [ScalarNode: String]] = [:]
                for (ip, speakerInfo) in jsonObject {
                    var speakerInfoByEnum: [ScalarNode: String]  = [:]
                    for (nodeName, value) in speakerInfo {
                        if let node = ScalarNode(rawValue: nodeName) {
                            speakerInfoByEnum[node] = value
                        }
                    }
                    if speakerInfoByEnum.count > 0 {
                        speakerInfoByIP[ip] = speakerInfoByEnum
                    }
                }
                speakerInfo = speakerInfoByIP
            }
            else {
                speakerInfo = [:]
            }
        } else {
            speakerInfo = [:]
        }
    }
    
    func speakerInfoForMac(_ mac: String, wifi: String) -> [ScalarNode: String] {
        let key = keyFromMac(mac, andWifi: wifi)
        if let speakerInfo = speakerInfo[key] {
            return speakerInfo
        }
        return [:]
    }
    
    func storeSpeakerInfo(_ speakerInfo: [ScalarNode: String], forMac mac: String, wifi: String) {
        let key = keyFromMac(mac, andWifi: wifi)
        self.speakerInfo[key] = speakerInfo
        save()
    }
    
    func removeInfoForMac(_ mac: String, wifi: String) {
        let key = keyFromMac(mac, andWifi: wifi)
        self.speakerInfo.removeValue(forKey: key)
        save()
    }
    
    func clearStore() {
        speakerInfo.removeAll()
        save()
    }
    
    fileprivate func save() {
        var speakerInfoWithStringKeys: [String: [String: String]] = [:]
        for ip in speakerInfo.keys {
            if let speakerInfoForIP  = speakerInfo[ip] {
                var speakerInfoForIPWithStringKeys:[String: String] = [:]
                for key in speakerInfoForIP.keys {
                    speakerInfoForIPWithStringKeys[key.rawValue] = speakerInfoForIP[key]
                }
                speakerInfoWithStringKeys[ip] = speakerInfoForIPWithStringKeys
            }
        }
        
        if let jsonData = try? JSONSerialization.data(withJSONObject: speakerInfoWithStringKeys, options: []) {
            minuetSDKUserDefaults?.set(jsonData, forKey: speakerInfoStorageKey)
        }
    }
}

fileprivate func keyFromMac(_ mac: String, andWifi wifi: String) -> String {
    return "\(wifi)_\(mac)"
}
