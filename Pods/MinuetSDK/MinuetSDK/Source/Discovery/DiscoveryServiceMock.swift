//
//  DiscoveryServiceMock.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 29/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation

public class DiscoveryServiceMock: DiscoveryServiceType {
    public var delegates: Set<Weak> = Set<Weak> ()
    public var speakers: [Speaker]
    public var active: Bool = false {
        didSet {
            if(active) {
                for speaker in speakers {
                    notifyDelegatesAboutFoundSpeaker(speaker, info: [:])
                }
            }
        }
    }
    
    func updateWithSpeaker(_ speaker: Speaker) {}
    
    public func refresh(clear: Bool) {}
    
    public func confirmDeviceAtIP(_ ipAddress: String, removeImmediatelyIfFail: Bool = false) {}
    
    public func search() {
        isSearching = true
    }
    
    public func stop() {
        isSearching = false
    }
    
    public var isSearching: Bool  = false
    
    public init() {
        let sampleSpeaker = Speaker(friendlyName: "Klippan",
                                    ipAddress: "127.0.0.1",
                                    wifiName: "Demo",
                                    UDN: "003361C86906",
                                    color: nil)
        speakers = [sampleSpeaker]
    }
}
