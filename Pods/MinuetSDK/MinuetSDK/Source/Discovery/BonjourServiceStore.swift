//
//  BonjourServiceStore.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 12/07/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import ObjectMapper

class BonjourServiceStore {
    var bonjourServicesStorageKey = "bounjourServices"
    var bonjourServices: [BonjourService]
    static let sharedInstance  = BonjourServiceStore()
    
    init() {
        if let jsonString = minuetSDKUserDefaults?.object(forKey: bonjourServicesStorageKey) as? String {
            if let storedServices = Mapper<BonjourService>().mapArray(JSONString: jsonString) {
                bonjourServices = storedServices
            } else {
                bonjourServices = []
            }
        } else {
            bonjourServices = []
        }
    }
    
    func servicesForNetwork(_ network: String) -> [BonjourService] {
        return bonjourServices.filter({$0.network == network})
    }
    
    func storeService(_ service: BonjourService) {
        if bonjourServices.find({$0 == service}) == nil {
            bonjourServices.append(service)
            save()
        }
    }
    
    func removeService(_ service: BonjourService) {
        bonjourServices.removeObject(service)
        save()
    }
    
    func clearStore() {
        bonjourServices.removeAll()
        save()
    }
    
    func removeServiceWithName(_ name: String) {
        if let serviceToRemove = bonjourServices.find({$0.name == name}) {
            bonjourServices.removeObject(serviceToRemove)
            save()
        }
    }
    
    fileprivate func save() {
        let jsonString = bonjourServices.toJSONString()
        minuetSDKUserDefaults?.set(jsonString, forKey: bonjourServicesStorageKey)
    }
}
