//
//  NetworkInfo.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 09/08/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import SystemConfiguration.CaptiveNetwork
import RxSwift
import UIKit

public protocol NetworkInfoServiceDelegate: class {
    func didConnectToNetworkWithNetworkInfo(_ networkInfo: NetworkInfo)
    func didDisconnectFromNetwork()
    
    var delegateIdentifier: String { get }
}

public class NetworkInfoService {
    public static let sharedInstance = NetworkInfoService()
    public var networkInfo: NetworkInfo
    public var isConnected: Bool {
        return networkInfo.wifiName != nil
    }
    
    fileprivate var darwinNotificationsManager: DarwinNotificationsManager = DarwinNotificationsManager.sharedInstance()
    fileprivate var lastWifiName: String? = nil
    fileprivate var lastConnectedStatus: Bool = false
    
    let disposeBag = DisposeBag()
    fileprivate var delegates: Set<Weak> = Set<Weak>()
    
    public init() {
        self.networkInfo = NetworkInfo(wifiName: NetworkInfoService.wifiName, ipAddress: NetworkInfoService.wifiIPAddress)
        darwinNotificationsManager.register(forNetworkChangedNotification: { [weak self] in
            self?.handleNetworkChanged()
        })
        
        lastWifiName = NetworkInfoService.wifiName
        let ipAddress = NetworkInfoService.wifiIPAddress
        lastConnectedStatus = lastWifiName != nil && ipAddress != nil && ipAddress != ""
        
        let appDidBecomeActive = NotificationCenter.default.rx.notification(UIApplication.didBecomeActiveNotification)
        appDidBecomeActive
            .subscribe(onNext: { [weak self] _ in
                self?.handleNetworkChanged() })
            .disposed(by: disposeBag)
    }
    
    func handleNetworkChanged() {
        let newWifiName = NetworkInfoService.wifiName
        let ipAddress = NetworkInfoService.wifiIPAddress
        
        let networkInfo = NetworkInfo(wifiName: newWifiName, ipAddress: ipAddress)
        self.networkInfo = networkInfo
        
        let isConnected = newWifiName != nil && ipAddress != nil && ipAddress != ""
        
        let changedConnected = lastConnectedStatus != isConnected
        let wasConnectedButNotAnymore = lastWifiName != nil && newWifiName == nil
        let wasDisconnectedButNotAnymore = lastWifiName == nil && newWifiName != nil
        let wasConnectedAndWifiNameChanged = lastWifiName != nil && newWifiName != nil && lastWifiName != newWifiName
        
        let changedSSID =  wasConnectedButNotAnymore || wasDisconnectedButNotAnymore || wasConnectedAndWifiNameChanged
        
        if changedSSID {
            lastWifiName = newWifiName
        }
        if changedConnected {
            lastConnectedStatus = isConnected
        }
        
        if (changedSSID || changedConnected) && (newWifiName == nil || isConnected == false) {
            NSLog("did disconnect")
            for weakDelegate in delegates {
                if let delegate = weakDelegate.recall() as? NetworkInfoServiceDelegate {
                    delegate.didDisconnectFromNetwork()
                    NSLog("did tell delegate \(delegate.delegateIdentifier) disconnected")
                }
            }
        }
        
        if (changedSSID || changedConnected) && newWifiName != nil && isConnected {
            NSLog("did connect to another network")
            for weakDelegate in delegates {
                if let delegate = weakDelegate.recall() as? NetworkInfoServiceDelegate {
                    delegate.didConnectToNetworkWithNetworkInfo(networkInfo)
                    NSLog("did tell delegate \(delegate.delegateIdentifier) connected to another network")
                }
            }
        }
    }
    
    public let wifiReachable: Observable<Bool> = DefaultReachabilityService.sharedReachabilityService.reachability
        .map { reachability -> Bool in
            switch(reachability) {
            case .reachable(let viaWiFi): return viaWiFi
            case .unreachable: return false
            }
        }.distinctUntilChanged()
    
    
    public func addDelegate(_ delegate: NetworkInfoServiceDelegate) {
        let weakDelegate = delegates.find({ weak in
            if let weakValue = weak.recall() as? NetworkInfoServiceDelegate {
                return  weakValue.delegateIdentifier == delegate.delegateIdentifier
            }
            return false
        })
        if weakDelegate == nil {
            NSLog("network info add delegate \(delegate.delegateIdentifier)")
            delegates.insert(Weak(value:delegate))
        }
    }
    
    public func removeDelegate(_ delegate: NetworkInfoServiceDelegate) {
        if let weakDelegate = delegates.filter({ weak in
            if let weakValue = weak.recall() as? NetworkInfoServiceDelegate {
                return  weakValue.delegateIdentifier == delegate.delegateIdentifier
            }
            return false
        }).first {
            NSLog("network info removed delegate \(delegate.delegateIdentifier)")
            delegates.remove(weakDelegate)
        }
    }
    
    fileprivate static var wifiName: String? {
        guard Platform.isSimulator == false else { return "Simulator WiFi" }
        guard let interfaces = CNCopySupportedInterfaces() else { return nil }
        
        var wifiName: String? = nil
        for i in 0..<CFArrayGetCount(interfaces) {
            let interfaceName: UnsafeRawPointer = CFArrayGetValueAtIndex(interfaces, i)
            let rec = unsafeBitCast(interfaceName, to: AnyObject.self)
            let unsafeInterfaceData = CNCopyCurrentNetworkInfo("\(rec)" as CFString)
            
            if unsafeInterfaceData != nil {
                let interfaceData = unsafeInterfaceData! as NSDictionary
                wifiName = interfaceData["SSID"] as? String
            }
        }
        return wifiName
    }
    
    fileprivate static var wifiIPAddress: String? {
        let address : String? = WifiInfo.wifiAddress()
        return address
    }
}
