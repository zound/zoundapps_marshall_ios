//
//  BonjourServiceStore.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 12/07/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import ObjectMapper

class SpeakerNameMacStore {
    let speakerIndexStorageKey = "speakerIndexStorageKey"
    static let sharedInstance  = SpeakerNameMacStore()
    var macsForNames: [String: [String]]
    
    init() {
        if let jsonData = minuetSDKUserDefaults?.data(forKey: speakerIndexStorageKey) {
            if let macs = (try? JSONSerialization.jsonObject(with: jsonData, options: [.mutableContainers,.mutableLeaves])) {
                if let macs = macs as? [String:[String]] {
                    macsForNames = macs
                }
                else {
                    macsForNames = [:]
                }
            } else {
                macsForNames = [:]
            }
        } else {
            macsForNames = [:]
        }
    }
    
    func index(ofMac mac: String, forName name: String) -> Int? {
        if let macs = macsForNames[name] {
            if let index = macs.index(of: mac) {
                return index
            }
        }
        return nil
    }
    
    func store(mac: String, forName name:String) {
        if let macsForName = macsForNames[name] {
            if macsForName.contains(mac) {
                return
            } else {
                macsForNames[name]?.append(mac)
            }
        } else {
            macsForNames[name] = [mac]
        }
        save()
    }
    
    
    fileprivate func save() {
        if let data = try? JSONSerialization.data(withJSONObject: macsForNames, options: []) {
            minuetSDKUserDefaults?.set(data, forKey:speakerIndexStorageKey)
        }
    }
}
