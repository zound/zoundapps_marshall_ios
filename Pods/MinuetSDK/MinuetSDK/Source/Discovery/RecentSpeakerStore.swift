//
//  RecentSpeakerStore.swifgt
//  MinuetSDK
//
//  Created by Raul Andrisan on 12/07/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import ObjectMapper

public class RecentSpeakerStore {
    let speakerStorageKey: String
    let capacity: Int
    
    public var speakers: [Speaker]
    
    public init(storageKey: String, capacity: Int) {
        self.speakerStorageKey = storageKey
        self.capacity = capacity
        
        if let jsonString = minuetSDKUserDefaults?.object(forKey: speakerStorageKey) as? String {
            if let storedSpeakers = Mapper<Speaker>().mapArray(JSONString: jsonString) {
                speakers = storedSpeakers
            } else {
                speakers = []
            }
        } else {
            speakers = []
        }
    }
    
    public func storeSpeaker(_ speaker: Speaker) {
        speakers.removeObject(speaker)
        speakers.insert(speaker, at: 0)
        
        if speakers.count > capacity {
            speakers.removeLast(speakers.count - capacity)
        }
        
        save()
    }
    
    public func removeSpeaker(_ speaker: Speaker) {
        speakers.removeObject(speaker)
        save()
    }
    
    public func clearStore() {
        speakers.removeAll()
        save()
    }
    
    fileprivate func save() {
        let jsonString = speakers.toJSONString()
        minuetSDKUserDefaults?.set(jsonString, forKey: speakerStorageKey)
    }
}
