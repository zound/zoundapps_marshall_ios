//
//  BonjourService.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 12/07/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import ObjectMapper

struct BonjourService: Mappable {
    var name: String?
    var type: String?
    var domain: String?
    var network: String?
    
    init?(map: Map) {}
    
    init?(service: NetService, network: String) {
        name = service.name
        type = service.type
        domain = service.domain
        self.network = network
    }
    
    // Mappable
    mutating func mapping(map: Map) {
        name    <- map["name"]
        type    <- map["type"]
        domain  <- map["domain"]
        network <- map["network"]
    }
}

extension BonjourService: Equatable {}

func ==(lhs: BonjourService, rhs: BonjourService  ) -> Bool {
    return (lhs.name == rhs.name) && (lhs.type == rhs.type) && (lhs.network == rhs.network);
}
