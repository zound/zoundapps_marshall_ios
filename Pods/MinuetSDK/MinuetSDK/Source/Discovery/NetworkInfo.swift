//
//  NetworkInfo.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 09/08/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation

public struct NetworkInfo {
    public let wifiName: String?
    public let ipAddress: String?
}
