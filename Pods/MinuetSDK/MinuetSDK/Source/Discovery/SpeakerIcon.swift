//
//  File.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 22/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

struct SpeakerIcon {
    let mimeType: String
    let width: Int
    let height: Int
    let depth: Int
    let url: String
}
