//
//  BonjourServiceStore.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 12/07/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import ObjectMapper

class SpeakerStore {
    let speakerStorageKey: String
    var speakers: [Speaker]
    
    init(storageKey: String) {
        self.speakerStorageKey = storageKey
        if let jsonString = minuetSDKUserDefaults?.object(forKey: speakerStorageKey) as? String {
            if let storedSpeakers = Mapper<Speaker>().mapArray(JSONString:jsonString) {
                speakers = storedSpeakers
            } else {
                speakers = []
            }
        } else {
            speakers = []
        }
    }
    
    func speakersForWifi(_ wifiName: String) -> [Speaker] {
        return speakers.filter({$0.wifiName == wifiName})
    }
    
    func storeSpeaker(_ speaker: Speaker) {
        speakers.removeObject(speaker)
        speakers.append(speaker)
        save()
    }
    
    func removeSpeaker(_ speaker: Speaker) {
        speakers.removeObject(speaker)
        save()
    }
    
    func clearStore() {
        speakers.removeAll()
        save()
    }
    
    func removeSpeakerWithMac(_ mac: String) {
        if let serviceToRemove = speakers.find({$0.mac == mac}) {
            speakers.removeObject(serviceToRemove)
            save()
        }
    }
    
    fileprivate func save() {
        let jsonString = speakers.toJSONString()
        minuetSDKUserDefaults?.set(jsonString, forKey: speakerStorageKey)
    }
}
