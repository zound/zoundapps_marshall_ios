//
//  DiscoveryService.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 21/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import SystemConfiguration.CaptiveNetwork

enum RemovalReason {
    case refresh
    case lost
}

protocol BonjourDiscoveryServiceDelegate: class {
    func discoveryServiceDidFindDevice(_ device:BonjourDevice)
    func discoveryServiceDidLoseDevice(_ device:BonjourDevice, withRemovalReason: RemovalReason)
}

class BonjourDiscoveryService:NSObject, NetServiceDelegate, NetServiceBrowserDelegate {
    //let serviceType = "_spotify-connect._tcp."
    var supportedVendorIDs = ["zound_001","undok", "zound_002"]
    var serviceType = "_zound._tcp"
    let domain = "local."
    
    var browser: NetServiceBrowser!
    weak var delegate: BonjourDiscoveryServiceDelegate?
    
    var lastWifiName: String? = nil
    var lastConnectedStatus: Bool = false
    
    var currentlyResolvingServices: [NetService] = []
    var notifiedDevices: [BonjourDevice] = []
    var isSearching: Bool = false
    
    override init() {
        super.init()
        
        if let infoPlist = Bundle.main.infoDictionary {
            if let appSettings = infoPlist["AppSettings"] as? Dictionary<String, AnyObject> {
                if let serviceType = appSettings["BonjourServiceType"] as? String {
                    self.serviceType = "_\(serviceType)._tcp"
                }
                
                if let supportedVendorIDs = appSettings["SupportedVendorIds"] as? [String] {
                    self.supportedVendorIDs = supportedVendorIDs
                }
            }
        }
    }
    
    func search() {
        stop()
        browser = NetServiceBrowser()
        NetworkInfoService.sharedInstance.addDelegate(self)
        browser.delegate = self
        
        browser.searchForServices(ofType: serviceType, inDomain: domain)
        resolveKnownSpeakerHostnamesForCurrentWifi()
    }
    
    func stop() {
        currentlyResolvingServices.forEach{ $0.stop() }
        currentlyResolvingServices.removeAll()
        
        if let `browser` = browser {
            browser.stop()
        
        }
        browser = nil
        NetworkInfoService.sharedInstance.removeDelegate(self)
    }
    
    func refresh(clear: Bool) {
        if clear {
            self.clear()
        }
        
        stop()
        search()
    }
    
    func clear() {
        for bonjourDevice in notifiedDevices {
            self.delegate?.discoveryServiceDidLoseDevice(bonjourDevice, withRemovalReason:.refresh)
            notifiedDevices.removeObject(bonjourDevice)
        }
    }
    
    deinit{
        NSLog("deinit bonjour")
        NetworkInfoService.sharedInstance.removeDelegate(self)
    }
    
    func resolveService(_ service: NetService) {
        if currentlyResolvingServices.find({$0.name == service.name}) == nil {
            currentlyResolvingServices.append(service)
            service.delegate = self
            service.resolve(withTimeout: 5.0)
        }
    }
    
    func netServiceBrowser(_ browser: NetServiceBrowser, didFind service: NetService, moreComing: Bool) {
        if let foundService = BonjourService(service: service, network: wifiName ?? "") {
            BonjourServiceStore.sharedInstance.storeService(foundService)
        }
        resolveService(service)
    }
    
    func handleLostService(_ service: NetService) {
        if notifiedDevices.find({$0.bonjourName == service.name}) != nil {
            if service.stringIPAddress()?.first == nil {
                if let device = notifiedDevices.find({$0.bonjourName == service.name}) {
                    self.delegate?.discoveryServiceDidLoseDevice(device, withRemovalReason: .lost)
                    notifiedDevices.removeObject(device)
                }
            } else {
                let bonjourDevice = BonjourDevice(bonjourName: service.name,
                                                  ipAddress: service.stringIPAddress()!.first!,
                                                  wifiName: wifiName,
                                                  fsapiLocation: nil,
                                                  vendorId: nil,
                                                  deviceInfoLocation: nil)
                self.delegate?.discoveryServiceDidLoseDevice(bonjourDevice, withRemovalReason: .lost)
                notifiedDevices.removeObject(bonjourDevice)
            }
        } else {
            //NSLog("lost service \(service.name) was not reported as found or has already been annouced as lost")
        }
    }
    
    func netServiceBrowser(_ browser: NetServiceBrowser, didRemove service: NetService, moreComing: Bool) {
        service.stop()
        currentlyResolvingServices.removeObject(service)
        BonjourServiceStore.sharedInstance.removeServiceWithName(service.name)
        handleLostService(service)
        
        //NSLog("browser did lose service \(service)")
    }
    
    func netServiceBrowserWillSearch(_ browser: NetServiceBrowser) {
        //NSLog("netservice browser did start search")
    }
    
    func netServiceBrowser(_ browser: NetServiceBrowser, didNotSearch errorDict: [String : NSNumber]) {
        if let number = errorDict[NetService.errorCode] {
            if let error =  NetService.ErrorCode(rawValue: number.intValue) {
                NSLog("netservice error: \(error)")
            }
        }
        
        NSLog("netservice browser did not start search \(errorDict)")
    }
    
    func netServiceBrowserDidStopSearch(_ browser: NetServiceBrowser) {
        //NSLog("netservice browser did stop search")
    }
    
    //MARK : NetService Delegates
    func netServiceDidResolveAddress(_ sender: NetService) {
        let ipAddresses = sender.stringIPAddress()
        if let ipAddress = ipAddresses?.first {
            //NSLog("netservic  e did resolve \(sender.hostName!) \(sender.name)) \(ipAddress)")
            var vendorId: String?
            var deviceInfoLocation: String?
            var fsapiLocation: String?
            
            if let txtData = sender.txtRecordData() {
                let txtDictionary = NetService.dictionary(fromTXTRecord: txtData)
                if let vendorIdData = txtDictionary["VendorID"] {
                    vendorId = String(data:vendorIdData , encoding:String.Encoding.utf8)
                }
                if let deviceInfoLocationData = txtDictionary["DeviceInfoLocation"] {
                    deviceInfoLocation = String(data:deviceInfoLocationData , encoding:String.Encoding.utf8)
                }
                if let fsapiLocationData = txtDictionary["FsAPILocation"] {
                    fsapiLocation = String(data:fsapiLocationData , encoding:String.Encoding.utf8)
                }
                //list all keys in TXT dictionary
                //for key in txtDictionary.keys {
                //    let keyData = txtDictionary[key]!
                //    let dataString = String(data: keyData, encoding: NSUTF8StringEncoding)
                //      NSLog("\(ipAddress) - \(key) = \(dataString!)")
                //}
            }
            
            //filter out incompatible devices
            if let `vendorId` = vendorId {
                if supportedVendorIDs.contains(vendorId) {
                    if notifiedDevices.map({ $0.bonjourName }).contains(sender.name) == false {
                        let bonjourDevice = BonjourDevice(bonjourName: sender.name,
                                                          ipAddress: ipAddress,
                                                          wifiName: wifiName,
                                                          fsapiLocation: fsapiLocation,
                                                          vendorId: vendorId,
                                                          deviceInfoLocation: deviceInfoLocation)
                        notifiedDevices.append(bonjourDevice)
                        self.delegate?.discoveryServiceDidFindDevice(bonjourDevice)
                    } else {
                        //NSLog("not notifying because \(sender.name) was already announced")
                    }
                }
            }
        }
        sender.stop()
    }
    
    func netService(_ sender: NetService, didNotResolve errorDict: [String : NSNumber]) {
        currentlyResolvingServices.removeObject(sender)
        BonjourServiceStore.sharedInstance.removeServiceWithName(sender.name)
        handleLostService(sender)
        NSLog("netservice did not resolve \(errorDict)")
    }
    
    func netServiceDidStop(_ sender: NetService) {
        currentlyResolvingServices.removeObject(sender)
    }
    
    fileprivate var wifiName: String? {
        return NetworkInfoService.sharedInstance.networkInfo.wifiName
    }
    
    fileprivate var ipAddress: String? {
        return NetworkInfoService.sharedInstance.networkInfo.ipAddress
    }
    
    func resolveKnownSpeakerHostnamesForCurrentWifi() {
        if let wifiName = NetworkInfoService.sharedInstance.networkInfo.wifiName {
            for bonjourService in BonjourServiceStore.sharedInstance.servicesForNetwork(wifiName) {
                let netService = NetService(domain: bonjourService.domain!, type: bonjourService.type!, name: bonjourService.name!)
                resolveService(netService)
            }
        }
    }
}


extension BonjourDiscoveryService: NetworkInfoServiceDelegate {
    var delegateIdentifier: String {
        return "bonjour_discovery_service"
    }
    
    func didConnectToNetworkWithNetworkInfo(_ networkInfo: NetworkInfo) {
        clear()
        //search()
    }
    
    func didDisconnectFromNetwork() {
        clear()
        //stop()
    }
}

extension NetService {
    func stringIPAddress() -> [String]? {
        if let addresses = self.addresses
        {
            var ips: [String] = []
            for address in addresses
            {
                let ptr = (address as NSData).bytes.bindMemory(to: sockaddr_in.self, capacity: address.count)
                var addr = ptr.pointee.sin_addr
                let buf = UnsafeMutablePointer<Int8>.allocate(capacity: Int(INET6_ADDRSTRLEN))
                var family = ptr.pointee.sin_family
                var ipc:UnsafePointer<Int8>? = nil
                if family == __uint8_t(AF_INET)
                {
                    ipc = inet_ntop(Int32(family), &addr, buf, __uint32_t(INET6_ADDRSTRLEN))
                }
                else if family == __uint8_t(AF_INET6)
                {
                    let ptr6 = (address as NSData).bytes.bindMemory(to: sockaddr_in6.self, capacity: address.count)
                    var addr6 = ptr6.pointee.sin6_addr
                    family = ptr6.pointee.sin6_family
                    ipc = inet_ntop(Int32(family), &addr6, buf, __uint32_t(INET6_ADDRSTRLEN))
                }
                
                if let ip = String(validatingUTF8: ipc!)
                {
                    ips.append(ip)
                }
            }
            //let port = service.port
            return ips
        }
        return nil
    }
}
