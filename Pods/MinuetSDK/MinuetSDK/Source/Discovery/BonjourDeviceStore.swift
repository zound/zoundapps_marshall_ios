//
//  BonjourServiceStore.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 12/07/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import ObjectMapper

class BonjourDeviceStore {
    var deviceStoreKey = "deviceStoreKey"
    var devices: [BonjourDevice]
    static let sharedInstance  = BonjourDeviceStore()
    
    init() {
        if let jsonString = minuetSDKUserDefaults?.object(forKey: deviceStoreKey) as? String {
            if let storedDevices = Mapper<BonjourDevice>().mapArray(JSONString: jsonString) {
                devices = storedDevices
            } else {
                devices = []
            }
        } else {
            devices = []
        }
    }
    
    func devicesForWifi(_ wifiName: String) -> [BonjourDevice] {
        return devices.filter({$0.wifiName == wifiName})
    }
    
    func storeDevice(_ device: BonjourDevice) {
        if devices.find({$0 == device}) == nil {
            devices.append(device)
            save()
        }
    }
    
    func removeDevice(_ device: BonjourDevice) {
        devices.removeObject(device)
        save()
    }
    
    func clearStore() {
        devices.removeAll()
        save()
    }
    
    fileprivate func save() {
        let jsonString = devices.toJSONString()
        minuetSDKUserDefaults?.set(jsonString, forKey: deviceStoreKey)
    }
}
