//
//  DiscoveryService.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 21/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//
import RxSwift
import Foundation
import SWXMLHash

public protocol DiscoveryServiceType: class {
    var delegates: Set<Weak> { get set }
    func addDelegate(_ delegate: DiscoveryServiceDelegate)
    func removeDelegate(_ delegate: DiscoveryServiceDelegate)
    
    var speakers: [Speaker] { get }
    func refresh(clear: Bool)
    func search()
    func stop()
    var isSearching: Bool { get }
    
    func confirmDeviceAtIP(_ ipAddress: String)
    func confirmDeviceAtIP(_ ipAddress: String, removeImmediatelyIfFail: Bool)
}

public protocol DiscoveryServiceDelegate: class {
    func discoveryServiceDidFindSpeaker(_ speaker: Speaker, info: [ScalarNode: String])
    func discoveryServiceDidUpdateInfoForSpeaker(_ speaker: Speaker, info: [ScalarNode:String])
    func discoveryServiceDidLoseSpeaker(_ speaker: Speaker)
    func discoveryServiceDidStartSearching()
    func discoveryServiceDidFinishConfirmingKnownSpeakers()
    
    var delegateIdentifier: String { get }
}

extension DiscoveryServiceType {
    public func confirmDeviceAtIP(_ ipAddress: String) {
        confirmDeviceAtIP(ipAddress, removeImmediatelyIfFail: false)
    }
}

extension DiscoveryServiceDelegate {
    public func discoveryServiceDidStartSearching() {}
    
    public func discoveryServiceDidFindSpeaker(_ speaker: Speaker, info: [ScalarNode: String]) {}
    public func discoveryServiceDidUpdateInfoForSpeaker(_ speaker: Speaker, info: [ScalarNode:String]) {}
    public func discoveryServiceDidLoseSpeaker(_ speaker: Speaker) {}
    public func discoveryServiceDidFinishConfirmingKnownSpeakers() {}
}

public class DiscoveryService: DiscoveryServiceType, BonjourDiscoveryServiceDelegate {
    let numberOfFailuresBeforeRemoval = 2
    var vendorIdWhitelist = ["zound_001","zound_002", "undok"]
    var ipsWaitingForInfo = Set<String>()
    var ipsWaitingForInitialData = Set<String>()
    var ipsWaitingForConfirmation = Set<String>()
    var failedConfirmationsForIP: [String:Int] = [:]
    
    fileprivate var bonjourDiscoveryService: BonjourDiscoveryService!
    
    public var delegates: Set<Weak> = Set<Weak>()
    public var speakers: [Speaker] = []
    var speakersBeingConfirmed: Set<String> = Set<String>()
    var isConfirmingSpeakers: Bool = false
    
    var disposebag = DisposeBag()
    let provider = SpeakerProvider()
    var pollingTimerDisposable: Disposable? = nil
    
    let foreverSpeakerStore: SpeakerStore
    let lastSessionSpeakerStore: SpeakerStore
    
    public init() {
        lastSessionSpeakerStore = SpeakerStore(storageKey: "lastSessionSpeakers")
        foreverSpeakerStore = SpeakerStore(storageKey: "foreverSpeakers")
        bonjourDiscoveryService = BonjourDiscoveryService()
        bonjourDiscoveryService.delegate = self
        NetworkInfoService.sharedInstance.addDelegate(self)
        
        if let infoPlist = Bundle.main.infoDictionary {
            if let appSettings = infoPlist["AppSettings"] as? Dictionary<String, AnyObject> {
                if let supportedVendorIDs = appSettings["SupportedVendorIds"] as? [String] {
                    self.vendorIdWhitelist = supportedVendorIDs
                }
            }
        }
        
        let appDidBecomeActive = NotificationCenter.default.rx.notification(UIApplication.didBecomeActiveNotification)
        appDidBecomeActive.subscribe(onNext: { [weak self] _ in
            self?.onAppDidBecomeActive()
        }).disposed(by: disposebag)
    }
    
    func onAppDidBecomeActive() {
        startPollingTimer()
        pingAllKnownSpeakers()
    }
    
    public func search() {
        if let wifiName = NetworkInfoService.sharedInstance.networkInfo.wifiName {
            NSLog("adding speakers from last session: \(lastSessionSpeakerStore.speakersForWifi(wifiName).map{ $0.friendlyName })")
            for speaker in lastSessionSpeakerStore.speakersForWifi(wifiName) {
                if !self.speakers.contains(speaker) {
                    self.speakers.append(speaker)
                    failedConfirmationsForIP[speaker.ipAddress] = 1 //this way if the speaker fails it's first request it's removed (after 10s)
                    let info  = SpeakerInfoStore.sharedInstance.speakerInfoForMac(speaker.mac, wifi: wifiName)
                    notifyDelegatesAboutFoundSpeaker(speaker, info: info)
                    confirmSpeaker(speaker)
                }
            }
            for speaker in foreverSpeakerStore.speakersForWifi(wifiName) {
                confirmSpeaker(speaker)
            } 
        }
        
        NetworkInfoService.sharedInstance.addDelegate(self)
        bonjourDiscoveryService.search()
        startPollingTimer()
        notifyDelegatesAboutStartSearching()
    }
    
    public func clear() {
        let speakersToRemove = speakers
        self.speakers.removeAll()
        for speaker in speakersToRemove {
            notifyDelegatesAboutLostSpeaker(speaker)
        }
    }
    
    public func stop() {
        bonjourDiscoveryService.stop()
        stopPollingtimer()
        ipsWaitingForConfirmation.removeAll()
        ipsWaitingForInfo.removeAll()
        ipsWaitingForInitialData.removeAll()
        disposebag = DisposeBag()
        NetworkInfoService.sharedInstance.removeDelegate(self)
    }
    
    public var isSearching: Bool {
        return bonjourDiscoveryService.isSearching
    }
    
    public func refresh(clear: Bool) {
        if clear {
            if let wifiName = NetworkInfoService.sharedInstance.networkInfo.wifiName {
                for speaker in foreverSpeakerStore.speakersForWifi(wifiName) {
                    foreverSpeakerStore.removeSpeakerWithMac(speaker.mac)
                    SpeakerInfoStore.sharedInstance.removeInfoForMac(speaker.mac, wifi: wifiName)
                }
                for speaker in lastSessionSpeakerStore.speakersForWifi(wifiName) {
                    lastSessionSpeakerStore.removeSpeakerWithMac(speaker.mac)
                }
            }
            self.clear()
            self.speakersBeingConfirmed.removeAll()
        }
        
        NetworkInfoService.sharedInstance.addDelegate(self)
        bonjourDiscoveryService.refresh(clear: clear)
        
        ipsWaitingForConfirmation.removeAll()
        ipsWaitingForInfo.removeAll()
        ipsWaitingForInitialData.removeAll()
        disposebag = DisposeBag()
        startPollingTimer()
        notifyDelegatesAboutStartSearching()
        
        if !clear {
            for speaker in speakers {
                self.speakersBeingConfirmed.insert(speaker.ipAddress)
                confirmDeviceAtIP(speaker.ipAddress, removeImmediatelyIfFail: true)
            }
            
            isConfirmingSpeakers = true
            checkIfFinishedConfirmingKnownSpeakers()
        }
    }
    
    
    func startPollingTimer() {
        stopPollingtimer()
        pollingTimerDisposable = Observable<Int>.interval(20.0, scheduler: MainScheduler.instance).subscribe(weak: self, onNext: DiscoveryService.pingAllKnownSpeakers)
    }
    
    func stopPollingtimer() {
        pollingTimerDisposable?.dispose()
        pollingTimerDisposable = nil
    }
    
    func pingAllKnownSpeakers(_ count: Int = 0) {
        for speaker in speakers {
            confirmSpeaker(speaker)
        }
        if let wifiName = NetworkInfoService.sharedInstance.networkInfo.wifiName {
            let previouslyFoundSpeakers  = foreverSpeakerStore.speakersForWifi(wifiName)
            for speaker in previouslyFoundSpeakers {
                confirmSpeaker(speaker)
            }
        }
    }
    
    func discoveryServiceDidFindDevice(_ device: BonjourDevice) {
        NSLog("discovery did receive \(device.bonjourName)")
        if !isConfirmingSpeakerWithIPAdress(device.ipAddress) {
            let alreadyConfirmedSpeakerWithIPAddress = self.speakers.find({$0.ipAddress == device.ipAddress}) != nil
            if alreadyConfirmedSpeakerWithIPAddress {
                ipsWaitingForInitialData.insert(device.ipAddress)
            } else {
                ipsWaitingForConfirmation.insert(device.ipAddress)
            }
            
            getSpeakerAndInfo(forDeviceWithIP: device.ipAddress)
        } else {
            confirmDeviceAtIP(device.ipAddress)
        }
    }
    
    func discoveryServiceDidLoseDevice(_ device: BonjourDevice, withRemovalReason reason: RemovalReason) {
        NSLog("discovery did receive that bonjour device \(device.bonjourName) dissapeared")
        if reason == RemovalReason.lost {
            if let speaker = self.speakers.find({$0.ipAddress == device.ipAddress}) {
                confirmSpeaker(speaker)
            }
        }
        
        if reason == RemovalReason.refresh  {
            if let speaker = self.speakers.find({$0.ipAddress == device.ipAddress}) {
                speakers.removeObject(speaker)
                notifyDelegatesAboutLostSpeaker(speaker)
            }
        }
    }
    
    func confirmSpeaker(_ speaker: Speaker) {
        confirmDeviceAtIP(speaker.ipAddress)
    }
    
    
    public func confirmDeviceAtIP(_ ipAddress: String, removeImmediatelyIfFail: Bool = false) {
        if removeImmediatelyIfFail {
            failedConfirmationsForIP[ipAddress] = numberOfFailuresBeforeRemoval
        }
        
        if !ipsWaitingForConfirmation.contains(ipAddress) {
            ipsWaitingForConfirmation.insert(ipAddress)
            if !isConfirmingSpeakerWithIPAdress(ipAddress) {
                getSpeakerAndInfo(forDeviceWithIP: ipAddress)
            }
        }
    }
    
    typealias SpeakerAndInfo = (speaker: Speaker, speakerInfo: [ScalarNode: String])
    
    func getSpeakerAndInfo(forDeviceWithIP ipAdress: String) {
        if !ipsWaitingForInfo.contains(ipAdress) {
            ipsWaitingForInfo.insert(ipAdress)
            let wifiName = NetworkInfoService.sharedInstance.networkInfo.wifiName ?? ""
            getSpeakerInfo(forDeviceWithIP: ipAdress)
                .timeout(10.0, scheduler: MainScheduler.instance)
                .subscribe(
                    onNext: { [weak self] nodes in
                        guard let `self` = self else { return }
                        let wifiMac = nodes[ScalarNode.WlanMACAddress] ?? ""
                        let friendlyName = nodes[ScalarNode.FriendlyName] ?? "Placeholder"
                        let color = nodes[ScalarNode.Color]
                        
                        let speaker =  Speaker(friendlyName: friendlyName,
                                               ipAddress: ipAdress,
                                               wifiName: wifiName,
                                               UDN: wifiMac.deviceMac,
                                               color: color)
                        
                        self.gotUpdatedSpeaker(speaker, info: nodes)
                        self.ipsWaitingForInfo.remove(ipAdress)
                    },
                    onError:{ [weak self] error in
                        guard let `self` = self else { return }
                        self.ipsWaitingForInfo.remove(ipAdress)
                        self.failedToGetUpdateForIP(ipAdress)
                }).disposed(by: disposebag)
        }
    }
    
    func gotUpdatedSpeaker(_ speaker: Speaker, info: [ScalarNode: String]) {
        if let vendorId = info[.VendorId] {
            if vendorIdWhitelist.contains(vendorId) {
                lastSessionSpeakerStore.storeSpeaker(speaker)
                foreverSpeakerStore.storeSpeaker(speaker)
                
                SpeakerInfoStore.sharedInstance.storeSpeakerInfo(info, forMac: speaker.mac, wifi: speaker.wifiName)
                
                failedConfirmationsForIP.removeValue(forKey: speaker.ipAddress)
                
                speakersBeingConfirmed.remove(speaker.ipAddress)
                checkIfFinishedConfirmingKnownSpeakers()
                
                if ipsWaitingForConfirmation.contains(speaker.ipAddress) {
                    ipsWaitingForConfirmation.remove(speaker.ipAddress)
                    if !alreadyConfirmedSpeaker(speaker) {
                        speakers.append(speaker)
                        notifyDelegatesAboutFoundSpeaker(speaker, info: info)
                    } else {
                        if let existingSpeaker = speakers.find({ $0.mac == speaker.mac }) {
                            let changedFriendlyName = existingSpeaker.friendlyName != speaker.friendlyName
                            let changedIPAddress = existingSpeaker.ipAddress != speaker.ipAddress
                            if  changedFriendlyName || changedIPAddress {
                                self.speakers.removeObject(existingSpeaker)
                                lastSessionSpeakerStore.removeSpeakerWithMac(existingSpeaker.mac)
                                foreverSpeakerStore.removeSpeakerWithMac(existingSpeaker.mac)
                                notifyDelegatesAboutLostSpeaker(existingSpeaker)
                                SpeakerInfoStore.sharedInstance.removeInfoForMac(existingSpeaker.mac, wifi: existingSpeaker.wifiName)
                                
                                self.speakers.append(speaker)
                                notifyDelegatesAboutFoundSpeaker(speaker, info: info)
                            } else {
                                notifyDelegatesAboutUpdatedInfoForSpeaker(speaker, info: info)
                            }
                        } else {
                            notifyDelegatesAboutUpdatedInfoForSpeaker(speaker, info: info)
                        }
                    }
                }
                
                if ipsWaitingForInitialData.contains(speaker.ipAddress) {
                    ipsWaitingForInitialData.remove(speaker.ipAddress)
                    if alreadyConfirmedSpeaker(speaker) == false {
                        speakers.append(speaker)
                        notifyDelegatesAboutFoundSpeaker(speaker, info: info)
                    }
                }
            }
        }
    }
    
    func failedToGetUpdateForIP(_ ipAdress: String) {
        NSLog("failed to get info for speaker at ip \(ipAdress)")
        if ipsWaitingForInitialData.contains(ipAdress) {
            ipsWaitingForInitialData.remove(ipAdress)
            //the speaker is not responding even though it's been reported as found from bonjour, so we don't continue processing them
        }
        
        if ipsWaitingForConfirmation.contains(ipAdress) {
            ipsWaitingForConfirmation.remove(ipAdress)
            if let currentFailedCount = failedConfirmationsForIP[ipAdress] {
                failedConfirmationsForIP[ipAdress] = currentFailedCount + 1
            } else {
                failedConfirmationsForIP[ipAdress] = 1
            }
            removeFailedConfirmationSpeakers()
        }
    }
    
    func checkIfFinishedConfirmingKnownSpeakers() {
        if isConfirmingSpeakers {
            NSLog("speakers being cofirmed: \(speakersBeingConfirmed.count)")
            if speakersBeingConfirmed.count == 0 {
                isConfirmingSpeakers = false
                self.notifiyDelegatesAboutFinishedConfirmingKnownSpeakers()
            }
        }
    }
    
    func removeFailedConfirmationSpeakers() {
        for ipAdress in failedConfirmationsForIP.keys {
            if let currentFailedCount = failedConfirmationsForIP[ipAdress] {
                if currentFailedCount >= numberOfFailuresBeforeRemoval {
                    if let speaker = self.speakers.find({ $0.ipAddress == ipAdress }) {
                        NSLog("removing speaker at ip \(ipAdress)")
                        self.speakers.removeObject(speaker)
                        lastSessionSpeakerStore.removeSpeaker(speaker)
                        notifyDelegatesAboutLostSpeaker(speaker)
                        self.speakersBeingConfirmed.remove(speaker.ipAddress)
                        checkIfFinishedConfirmingKnownSpeakers()
                    }
                }
            }
        }
    }
    
    
    func getSpeakerInfo(forDeviceWithIP ipAdress: String) -> Observable<[ScalarNode: String]> {
        let presumedSpeaker =  Speaker(friendlyName: "(\(ipAdress))",
            ipAddress:ipAdress,
            wifiName: "WiFi",
            UDN: "123456789012",
            color: nil)
        //this list must be kept with a maximum of 10 nodes, since the multiple node requests are chunked in 10 node requests
        //more will work but will degrade the performance of discovery
        let nodesToFetch: [ScalarNode] = [.FriendlyName,        //1
            .WlanMACAddress,      //2
            .Color,               //3
            .MultiroomGroupId,    //4
            .MultiroomGroupName,  //5
            .MultiroomGroupState, //6
            .Mode,                //7
            .NavCurrentPreset,    //8
            .PlayStatus,          //9
            .VendorId             //10
        ]
        
        return provider.getNodes(nodesToFetch, forSpeaker: presumedSpeaker)
    }
    
    
    func alreadyConfirmedSpeaker(_ speaker: Speaker) -> Bool {
        let alreadyConfirmed = self.speakers.find({$0.mac == speaker.mac}) != nil
        return alreadyConfirmed
    }
    
    func isConfirmingSpeakerWithIPAdress(_ ipAddress: String) -> Bool {
        let alreadyConfirmed = self.ipsWaitingForInfo.contains(ipAddress)
        return alreadyConfirmed
    }
    
}

extension DiscoveryService: NetworkInfoServiceDelegate {
    public var delegateIdentifier: String {
        return "discovery_service"
    }
    
    public func didConnectToNetworkWithNetworkInfo(_ networkInfo: NetworkInfo) {
        clear()
        search()
    }
    
    public func didDisconnectFromNetwork() {
        clear()
        stop()
    }
    
}

extension String {
    var deviceMac: String {
        let components = self.split(separator: ":").map{ String($0) }
        guard components.count==6 else {
            return ""
        }
        
        var hexWithLeadingZeroComponents = [String]()
        for component in components {
            let intValue = Int(component, radix: 16)!
            let hexString = String(format: "%02x", intValue)
            hexWithLeadingZeroComponents.append(hexString)
        }
        return hexWithLeadingZeroComponents.joined(separator: "").uppercased()
    }
}

extension DiscoveryServiceType {
    public func addDelegate(_ delegate: DiscoveryServiceDelegate) {
        delegates.insert(Weak(value:delegate))
    }
    
    public func removeDelegate(_ delegate: DiscoveryServiceDelegate) {
        if let weakDelegate = delegates.filter({ weak in
            if let weakValue = weak.recall() as? DiscoveryServiceDelegate {
                return  weakValue.delegateIdentifier == delegate.delegateIdentifier
            }
            return false
        }).first {
            delegates.remove(weakDelegate)
        }
    }
    
    func notifyDelegatesAboutLostSpeaker(_ speaker: Speaker) {
        for weakDelegate in delegates {
            if let delegate = weakDelegate.recall() as? DiscoveryServiceDelegate {
                delegate.discoveryServiceDidLoseSpeaker(speaker)
            } else
            {
                NSLog("cannot convert")
            }
        }
    }
    
    func notifyDelegatesAboutFoundSpeaker(_ speaker: Speaker, info: [ScalarNode: String]) {
        for weakDelegate in delegates {
            if let delegate = weakDelegate.recall() as? DiscoveryServiceDelegate {
                delegate.discoveryServiceDidFindSpeaker(speaker, info: info)
            }
        }
    }
    
    func notifyDelegatesAboutUpdatedInfoForSpeaker(_ speaker: Speaker, info: [ScalarNode: String]) {
        for weakDelegate in delegates {
            if let delegate = weakDelegate.recall() as? DiscoveryServiceDelegate {
                delegate.discoveryServiceDidUpdateInfoForSpeaker(speaker, info: info)
            }
        }
    }
    
    func notifyDelegatesAboutStartSearching() {
        for weakDelegate in delegates {
            if let delegate = weakDelegate.recall() as? DiscoveryServiceDelegate {
                delegate.discoveryServiceDidStartSearching()
            }
        }
    }
    
    func notifiyDelegatesAboutFinishedConfirmingKnownSpeakers() {
        for weakDelegate in delegates {
            if let delegate = weakDelegate.recall() as? DiscoveryServiceDelegate {
                delegate.discoveryServiceDidFinishConfirmingKnownSpeakers()
            }
        }
    }
}

extension DiscoveryService {
    func getInfoForDevice(_ bonjourDevice: BonjourDevice) -> Observable<Speaker> {
        let deviceInfoLocation = bonjourDevice.deviceInfoLocation ?? "/dd.xml"
        let request = URLRequest(url: URL(string: "http://\(bonjourDevice.ipAddress):8080\(deviceInfoLocation)")!)
        return URLSession.shared.rx.response(request: request).timeout(10.0, scheduler: MainScheduler.instance).map{ (response,data) in
            let xml = SWXMLHash.config { config in
                config.shouldProcessNamespaces = false
                config.shouldProcessLazily = false
                }.parse(data)
            let device = xml["root"]["device"]
            let friendlyName = device["friendlyName"].element?.text
            let UDN = device["UDN"].element?.text ?? ""
            /*let manufacturer = device["manufacturer"].element?.text ?? ""
             let manufacturerURL = device["manufacturerURL"].element?.text ?? ""
             let modelDescription = device["modelDescription"].element?.text ?? ""
             let modelName = device["modelName"].element?.text ?? ""
             let modelNumber = device["modelNumber"].element?.text ?? ""
             let modelURL = device["modelURL"].element?.text ?? ""
             let serialNumber = device["serialNumber"].element?.text ?? ""
             let multiroomVersion = device["fsns:X_audSyncProtocolID"].element?.text ?? ""
             let features = device["fsns:X_Features"].element?.text
             let featuresArray = features?.componentsSeparatedByString(", ")
             
             let icons = device["iconList"]["icon"].all.map{ (let icon) -> SpeakerIcon in
             
             let mimeType = icon["mimetype"].element?.text!
             let width = icon["width"].element?.text!
             let height = icon["height"].element?.text!
             let depth = icon["depth"].element?.text!
             let path = icon["url"].element?.text!
             return SpeakerIcon(mimeType:mimeType!, width:Int(width!) ?? 0, height: Int(height!) ?? 0, depth: Int(depth!) ?? 0, url: path!)
             }*/
            
            return Speaker(friendlyName: friendlyName ?? "",
                           ipAddress: bonjourDevice.ipAddress,
                           wifiName: bonjourDevice.wifiName!,
                           UDN: UDN,
                           color: nil)
        }
    }
}
