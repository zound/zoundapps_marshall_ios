//
//  Speaker.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 11/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import ObjectMapper

struct BonjourDevice {
    var bonjourName: String
    var ipAddress: String
    var wifiName: String?
    var fsapiLocation: String?
    var vendorId: String?
    var deviceInfoLocation: String?
}

extension BonjourDevice: Equatable {}

func ==(lhs: BonjourDevice, rhs: BonjourDevice  ) -> Bool {
    return (lhs.bonjourName == rhs.bonjourName)
}

extension BonjourDevice: Mappable {
    init?(map: Map) {
        bonjourName = ""
        ipAddress = ""
    }
    
    // Mappable
    mutating func mapping(map: Map) {
        bonjourName        <- map["bonjourName"]
        ipAddress          <- map["ipAddress"]
        wifiName           <- map["wifiName"]
        fsapiLocation      <- map["fsapiLocation"]
        vendorId           <- map["vendorId"]
        deviceInfoLocation <- map["deviceInfoLocation"]
    }
}
