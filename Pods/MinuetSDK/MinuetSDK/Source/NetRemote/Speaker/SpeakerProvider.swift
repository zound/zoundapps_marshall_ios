////
//  Provider+Speaker.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 11/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import Moya
import RxSwift
import SWXMLHash

public class SpeakerProvider {
    static let networkOperationsLogEnabled = false
    static let notificationsLogEnabled = false
    
    let moyaProvider: MoyaProvider<NetRemoteService>
    
    var validModesForSpeaker: [Speaker: [Mode]] = [:]
    var firmwareVersionForSpeaker: [Speaker: String] = [:]
    
    public init() {
        moyaProvider = MoyaProvider(endpointClosure: MoyaProvider<NetRemoteService>.defaultEndpointMapping,
                                    requestClosure: MoyaProvider<NetRemoteService>.defaultRequestMapping,
                                    stubClosure: NetRemoteService.stubClosure,
                                    manager: MoyaProvider<NetRemoteService>.defaultAlamofireManager(),
                                    plugins: [],
                                    trackInflights: false)
    }
    
    func sessionForSpeaker(_ speaker: Speaker) -> Observable<String> {
        return moyaProvider.rx.request(.createSession(speaker: speaker))
            .asObservable()
            .mapFSAPIResponse()
            .map { (sessionResponse: FSAPICreateSessionResponse) in return sessionResponse.sessionId! }
    }
    
    func notificationsForSessionId(_ sessionId: String, speaker: Speaker) -> Observable<[ScalarNode:String]> {
        let publishSubject: BehaviorSubject<Bool> = BehaviorSubject(value: true)
        return moyaProvider.rx.request(NetRemoteService.getNotifications(sessionId: sessionId, speaker: speaker))
            .asObservable()
            .repeatWhen(notificationHandler: { return publishSubject })
            .mapFSAPIResponse()
            .map { (response: FSAPIGetNotificationsResponse) in
                var notifications: [ScalarNode:String] =  [:]
                for notification in response.notifications {
                    if SpeakerProvider.notificationsLogEnabled {
                        NSLog("\(notification)")
                    }
                    if let node = ScalarNode(rawValue: notification.node.lowercased()) {
                        notifications[node] = notification.value
                    } else {
                        NSLog("ScalarNode not found for \(notification.node.lowercased())")
                    }
                }
                return notifications }
            .do(onNext: { _ in
                publishSubject.onNext(true)
            })
    }
    
    public func notificationsForSpeaker(_ speaker: Speaker, sessionId: String? = nil) ->Observable<[ScalarNode: String]> {
        var sessionIdObservable: Observable<String> = Observable
            .error(NetRemoteResponseError.sessionLost)
        
        if sessionId != nil {
            sessionIdObservable = Observable.just(sessionId!)
        } else {
            sessionIdObservable = sessionForSpeaker(speaker)
        }
        
        return sessionIdObservable
            .flatMap { [weak self] sessionid in
                return self?.notificationsForSessionId(sessionid, speaker: speaker) ?? Observable.empty()
        }
    }
    
    public func getNode(_ node: ScalarNode, forSpeaker speaker: Speaker) -> Observable<String?> {
        let start = CACurrentMediaTime()
        return moyaProvider.rx.request(.getNode(node: node, speaker: speaker))
            .asObservable()
            .mapFSAPIResponse()
            .map { (response: FSAPIGetNodeResponse) in
                let end = CACurrentMediaTime()
                if SpeakerProvider.networkOperationsLogEnabled { NSLog("\(String(format:"%.02f",end-start)) s did get node \(node.rawValue)") }
                return response.value
        }
    }
    
    public func setNode(_ node: ScalarNode, value:String, forSpeaker speaker: Speaker) -> Observable<Bool> {
        let start = CACurrentMediaTime()
        NSLog("set node \(node.rawValue) to value \(value)")
        return moyaProvider.rx.request(.setNode(node: node, value: value, speaker: speaker))
            .asObservable()
            .mapFSAPIResponse()
            .map { (response: FSAPISetNodeResponse) in
                let end = CACurrentMediaTime()
                NSLog("\(String(format:"%.02f", end - start)) s did set node \(node.rawValue) to value \(value)")
                return response.status == "FS_OK"
        }
    }
    
    func getMockListNode<T:XMLIndexerDeserializable>(_ listNode: ListNode) -> Observable<[T]> {
        let bundle = Bundle.main
        if let urlForFile = bundle.url(forResource: listNode.rawValue, withExtension: "xml") {
            if let data = try? Data(contentsOf: urlForFile) {
                let xml = SWXMLHash.config { config in
                    config.shouldProcessNamespaces = false
                    config.shouldProcessLazily = false
                    }.parse(data)
                
                if let fsapiResponseNode = try? xml.byKey("fsapiResponse") {
                    if let fsapiResponse : FSAPIGetListNodeResponse<T> = try? fsapiResponseNode.value() {
                        let items = fsapiResponse.items ?? []
                        return Observable.just(items)
                    }
                }
            }
        }
        
        return Observable.just([])
    }
    
    public func getListNode<T:XMLIndexerDeserializable>(_ listNode: ListNode, startIndex: Int32 = 0, maxCount: Int32? = nil, forSpeaker speaker: Speaker) -> Observable<[T]> {
        let start = CACurrentMediaTime()
        return collectListComponentsFromObservable(getCompleteListNode(listNode,
                                                                       startIndex: startIndex,
                                                                       endIndex: (maxCount != nil ? Int32(maxCount! + startIndex) : Int32.max),
                                                                       forSpeaker: speaker))
            .do(onNext: { items in
                let end = CACurrentMediaTime()
                if SpeakerProvider.networkOperationsLogEnabled { NSLog("\(String(format:"%.02f",end-start)) s - got \(speaker.friendlyName) list node \(listNode.rawValue)")}
            })
    }
    
    public func getCompleteListNode<T:XMLIndexerDeserializable>(_ listNode: ListNode, startIndex: Int32 = 0, endIndex: Int32 = 0, forSpeaker speaker: Speaker) -> Observable<[T]> {
        let MAX_ITEMS_PER_REQUEST = 20
        let getListNode: Observable<[T]> = Observable.create { [weak self] subscriber in
            guard let `self` = self else { return Disposables.create() }
            
            let netRemoteRequest = NetRemoteService.getList(list: listNode,
                                                            startIndex: Int(Int32(startIndex) - Int32(1)),
                                                            maxCount: min(MAX_ITEMS_PER_REQUEST, Int(endIndex - startIndex)),
                                                            speaker: speaker)
            
            var disposable: Disposable? = self.moyaProvider.rx.request(netRemoteRequest)
                .asObservable()
                .mapFSAPIResponse()
                .subscribe( onNext: { (response: FSAPIGetListNodeResponse<T>) in
                    
                    var nextPage: Observable<[T]> = Observable.empty()
                    let lastIndex = Int32(response.maxKey)
                    if response.listEnd == false && (lastIndex < endIndex) && ((lastIndex + 1) != endIndex) && response.status == "FS_OK" {
                        nextPage = self.getCompleteListNode(listNode, startIndex: lastIndex + 1, endIndex: endIndex, forSpeaker: speaker)
                    } else {
                        //end of list
                    }
                    
                    _ = Observable.just(response.items ?? [])
                        .concat(nextPage)
                        .subscribe(subscriber)
                })
            return Disposables.create {
                disposable?.dispose()
                disposable = nil
            }
        }
        
        return getListNode
    }
    
    func collectListComponentsFromObservable<T:XMLIndexerDeserializable>(_ observable: Observable<[T]>) -> Observable<[T]> {
        return Observable.create { subscriber in
            var list = [T]()
            var disposable: Disposable? = observable
                .subscribe(onNext: { (partOfList:[T]) -> (Void) in
                    list.append(contentsOf: partOfList)
                }, onError: { error in
                    subscriber.onError(error)
                }, onCompleted: {
                    subscriber.onNext(list)
                    subscriber.onCompleted()
                })
            return Disposables.create {
                disposable?.dispose()
                disposable = nil
            }
        }
    }
    
    fileprivate func partitionNodes(_ nodes: [ScalarNode], inPagesWithSize pageSize: Int) -> [[ScalarNode]] {
        let pages = nodes.count / pageSize
        let remainder = nodes.count % pageSize
        var partitions =  [[ScalarNode]]()
        
        for page in 0..<pages {
            let subnodes = Array(nodes[page * pageSize..<(page + 1) * pageSize])
            partitions.append(subnodes)
        }
        if remainder > 0 {
            let subnodes = Array(nodes[pages * pageSize..<(pages*pageSize + remainder)])
            partitions.append(subnodes)
        }
        
        return partitions
    }
    
    public func getNodes(_ nodes: [ScalarNode], forSpeaker speaker: Speaker) -> Observable<[ScalarNode: String]> {
        //we partition the nodes into pages of max size 10, a hardcoded maximum in the firmwre
        let requests = partitionNodes(nodes, inPagesWithSize: 10).map { moyaProvider.rx.request(NetRemoteService.getNodes(nodes: $0, speaker: speaker)).asObservable() }
        
        //zip sends next whenever all of the observable sequences have produced an element at a corresponding index.
        let start = CACurrentMediaTime()
        return Observable.zip(requests) { (responses: [Response]) in
            var collectedResponses = [FSAPIGetNodeResponse]()
            for response in responses {
                
                if let multiResponse:FSAPIGetMultipleNodeResponse = try? response.mapFSAPIResponse() {
                    collectedResponses.append(contentsOf: multiResponse.responses)
                }
            }
            return FSAPIGetMultipleNodeResponse(responses: collectedResponses) }
            .map { (multipleResponse: FSAPIGetMultipleNodeResponse) in
                var responses = [ScalarNode: String]()
                for (index,response) in multipleResponse.responses.enumerated() {
                    let node = nodes[index]
                    responses[node] = response.value
                }
                return responses }
            .do(onNext: { nodesFetched in
                let end = CACurrentMediaTime()
                if SpeakerProvider.networkOperationsLogEnabled {
                    NSLog("\(String(format:"%.02f",end-start)) s - \(speaker.friendlyName) fetched nodes: \(nodesFetched.map{ key, value in key.rawValue})")
                }
            })
    }
    
    public func setNodes(_ nodes: [ScalarNode: String], forSpeaker speaker: Speaker) -> Observable<[ScalarNode: Bool]> {
        let requests = partitionNodes(Array(nodes.keys), inPagesWithSize: 10).map { page in
            var pageDict = [ScalarNode:String]()
            for node in page {
                pageDict[node] = nodes[node]
            }
            return pageDict
            }.map { moyaProvider.rx.request(NetRemoteService.setNodes(nodes: $0, speaker: speaker))
                .asObservable() }
        
        return Observable
            .zip(requests) { (responses: [Response]) in
                var collectedResponses = [FSAPISetNodeResponse]()
                
                for response in responses {
                    if let multiResponse: FSAPISetMultipleNodeResponse = try? response
                        .mapFSAPIResponse() { collectedResponses.append(contentsOf: multiResponse.responses) }
                }
                
                return FSAPISetMultipleNodeResponse(responses: collectedResponses) }
            .map { (multipleResponse: FSAPISetMultipleNodeResponse)  in
                var responses = [ScalarNode:Bool]()
                for (node, _) in nodes {
                    responses[node] = false
                }
                
                for response in multipleResponse.responses {
                    if let scalarNodeName = response.node {
                        if let scalarNode = ScalarNode(rawValue: scalarNodeName) {
                            responses[scalarNode] = (response.status == "FS_OK")
                        }
                    }
                }
                return responses
        }
    }
    
    public func sendCommand(_ command: PlayControl, toSpeaker speaker: Speaker) -> Observable<Bool> {
        return moyaProvider.rx.request(.setNode(node:ScalarNode.PlayControl, value:String(command.rawValue), speaker: speaker))
            .asObservable()
            .mapFSAPIResponse()
            .map { (response: FSAPISetNodeResponse) in return response.status == "FS_OK" }
    }
    
    public func pollNode(_ node: ScalarNode, atInterval interval: TimeInterval, onSpeaker speaker: Speaker) -> Observable<String?> {
        let poller = Observable<Int>
            .interval(interval, scheduler: MainScheduler.instance)
            .startWith(0)
            .flatMapLatest { [weak self] count -> Observable<String?> in
                guard let `self` = self else { return Observable.empty() }
                return self.getNode(node, forSpeaker: speaker)
        }
        //let optionalStringComparer = {(rhs: String?, lhs: String?) in rhs == lhs}
        return poller//.distinctUntilChanged(optionalStringComparer)
    }
    
    public func pollNodes(_ nodes: [ScalarNode], atInterval interval: TimeInterval, onSpeaker speaker: Speaker) -> Observable<[ScalarNode: String]> {
        let ping = Observable<Int>
            .interval(interval, scheduler: MainScheduler.instance)
            .startWith(0)
        
        let poller = ping
            .map { [weak self] count -> Observable<[ScalarNode: String]> in
                guard let `self` = self else { return Observable.empty() }
                return self.getNodes(nodes, forSpeaker: speaker)
                    .timeout(interval - 0.1, scheduler: MainScheduler.instance)
                    .do(onError: { error in
                        NSLog("error!!!! \(error.localizedDescription)")
                    }) }
            .switchLatest()
        
        return poller
    }
    
    public func getDeviceListAllUpdatesWithPollingInterval(_ interval: TimeInterval, speaker: Speaker) -> Observable<[MultiroomDevice]> {
        typealias MultiroomState = (groupId: String?, groupState: String?, groupName: String?, groupClientIndex: String?, multiroomVersion: String?)
        let multiroomState = pollNodes([.MultiroomGroupId,
                                        .MultiroomGroupState,
                                        .MultiroomGroupName,
                                        .MultiroomDeviceClientIndex,
                                        .MultiroomListVersion], atInterval: interval, onSpeaker: speaker)
            .map { response -> MultiroomState in
                let groupId = response[.MultiroomGroupId]
                let groupName = response[.MultiroomGroupName]
                let groupState = response[.MultiroomGroupState]
                let groupClientIndex = response[.MultiroomDeviceClientIndex]
                let groupListVersion = response[.MultiroomListVersion]
                return MultiroomState(groupId: groupId, groupState: groupState, groupName: groupName, groupClientIndex: groupClientIndex, multiroomVersion: groupListVersion) }
            .share()
        
        let multiroomVersion = multiroomState
            .map { $0.multiroomVersion != nil ? Int($0.multiroomVersion!)! : 0 }
            .distinctUntilChanged()
        
        let multiroomDevices = multiroomVersion
            .flatMapLatest { [weak self] multiroomVersion -> Observable<[MultiroomDevice]> in
                guard let `self` = self else { return Observable.just([]) }
                return self.getListNode(ListNode.MultiroomList, forSpeaker: speaker)
        }
        
        let optionalStringComparer = {(rhs: String?, lhs: String?) in rhs == lhs}
        let groupId = multiroomState
            .map { $0.groupId }
            .distinctUntilChanged(optionalStringComparer)
        
        let groupName = multiroomState
            .map { $0.groupName }
            .distinctUntilChanged(optionalStringComparer)
        
        let groupState = multiroomState
            .map { $0.groupState }
            .distinctUntilChanged(optionalStringComparer)
        
        let groupClientIndex = multiroomState
            .map { $0.groupClientIndex }
            .distinctUntilChanged(optionalStringComparer)
        
        let speakerAsMultiroomDevice = Observable.combineLatest(groupId,
                                                                groupName,
                                                                groupClientIndex,
                                                                groupState) { (groupId, groupName, groupClientIndex, groupState) -> MultiroomDevice in
                                                                    var groupRole = GroupRole.none
                                                                    if let groupState = groupState {
                                                                        switch groupState {
                                                                        case "0": groupRole = GroupRole.none
                                                                        case "1": groupRole = GroupRole.client
                                                                        case "2": groupRole = GroupRole.server
                                                                        default: groupRole = GroupRole.none
                                                                        }
                                                                    }
                                                                    
                                                                    let clientNumber = groupClientIndex != nil ? Int(groupClientIndex!) : nil
                                                                    let multiroomDevice = MultiroomDevice(udn: speaker.UDN,
                                                                                                          friendlyName: speaker.friendlyName,
                                                                                                          ipAddress: speaker.ipAddress,
                                                                                                          audioSyncVersion: "128",
                                                                                                          groupId: groupId == "" ? nil : groupId,
                                                                                                          groupName: groupName == "" ? nil : groupName,
                                                                                                          groupRole: groupRole,
                                                                                                          clientNumber: clientNumber,
                                                                                                          key: 0)
                                                                    return multiroomDevice
        }
        
        let multiroomPolling = Observable.combineLatest(multiroomDevices,
                                                        speakerAsMultiroomDevice) { multiroomDevices, speakerAsMultiroomDevice -> [MultiroomDevice] in
                                                            return multiroomDevices + [speakerAsMultiroomDevice] }
            .do(onNext: { devices in
                //NSLog(devices.map{device in "name: \(device.friendlyName), clientIndex: \(device.clientNumber)"})
            })
        
        return multiroomPolling
    }
    
    public func getAboutStateForSpeaker(_ speaker: Speaker) -> Observable<AboutState> {
        let aboutNodes: [ScalarNode] = [.Color,
                                        .WlanSSID,
                                        .WlanSignalStrength,
                                        .Version,
                                        .CastVersion,
                                        .BuildVersion]
        
        let getAboutNodes = getNodes(aboutNodes, forSpeaker: speaker)
        
        return getAboutNodes
            .map { requestedNodes  in
                let color = requestedNodes[.Color]
                let wifi = requestedNodes[.WlanSSID]?.base16DecodedString()
                let strength = Int(requestedNodes[.WlanSignalStrength] ?? "0")
                let fwVersion = requestedNodes[.Version]
                let castVersion = requestedNodes[.CastVersion]
                let buildVersion = requestedNodes[.BuildVersion]
                
                return AboutState(color: color,
                                  wifiName: wifi,
                                  wifiSignal: strength,
                                  softwareVersion: fwVersion,
                                  castVersion: castVersion,
                                  buildVersion: buildVersion)
        }
    }
    
    public func getUpdateStateForSpeaker(_ speaker: Speaker) -> Observable<UpdateState> {
        return getNode(ScalarNode.UpdateState, forSpeaker: speaker)
            .catchError { error in return Observable.just("0") }
            .map { nodeValue in
                guard let stringValue = nodeValue,
                    let rawValue = Int(stringValue),
                    let updateValue = UpdateState(rawValue: rawValue) else { return .idle }
                
                return updateValue
        }
    }
    
    public func getTimeZoneStateForSpeaker(_ speaker: Speaker) -> Observable<TimeZoneState> {
        let timezone: [ScalarNode] = [.TimeZone]
        let getCurrentTimeZone = getNodes(timezone, forSpeaker: speaker)
        
        let getTimeZones: Observable<[TimeZone]> = getMockListNode(ListNode.TimeZones)
            .catchError { error -> Observable<[TimeZone]> in return Observable.just([]) }
        
        return Observable.combineLatest(getCurrentTimeZone, getTimeZones) { requestedNodes, timeZones in
            let currentTimeZone = requestedNodes[.TimeZone] ?? ""
            return TimeZoneState(timeZones: timeZones, currentTimeZoneName: currentTimeZone)
        }
    }
    
    public func getLedIntensityStateForSpeaker(_ speaker: Speaker) -> Observable<Int> {
        return getNode(ScalarNode.LedIntensity, forSpeaker: speaker)
            .catchError { error in return Observable.just("0") }
            .map { nodeValue in
                guard let stringValue = nodeValue,
                    let intValue = Int(stringValue) else { return 0 }
                return intValue
        }
    }
    
    public func getUserDataStoreForSpeaker(_ speaker: Speaker) -> Observable<[String: Any]> {
        return getNode(ScalarNode.UserDataStore, forSpeaker: speaker)
            .catchError { error in return Observable.just("{\"s\":0}") }
            .map { $0 == "" ? "{\"s\":0}" : $0 }
            .map { nodeValue in
                guard let stringValue = nodeValue,
                    let data = stringValue.data(using: .utf8),
                    let jsonObject = (try? JSONSerialization.jsonObject(with: data, options: [])) as? [String: Any] else { return [:] }
                
                return jsonObject
        }
    }
    
    public func getDidSetupFromUserDataStoreInSpeaker(_ speaker: Speaker) -> Observable<Bool> {
        return getUserDataStoreForSpeaker(speaker)
            .map { userDataStore in
                guard let didSetup = userDataStore["s"] as? Int else { return false }
                return didSetup == 1
        }
    }
    
    public func setDidSetupInUserDataStoreForSpeaker(_ speaker: Speaker, withValue value: Bool) -> Observable<Bool> {
        return getUserDataStoreForSpeaker(speaker)
            .flatMap { [weak self] userDataStore -> Observable<Bool> in
                guard let `self` = self else { return Observable.just(false) }
                
                var jsonObject = userDataStore
                jsonObject["s"] = value ? 1 : 0
                
                if let jsonData = try? JSONSerialization.data(withJSONObject: jsonObject, options: []),
                    let jsonString = String(data: jsonData, encoding: .utf8) {
                    NSLog("will set did setup presets to device")
                    return self.setNode(ScalarNode.UserDataStore, value: jsonString, forSpeaker: speaker)
                } else {
                    return Observable.just(false)
                }
        }
    }
    
    
    public func getEqualizerStateForSpeaker(_ speaker: Speaker) -> Observable<EqState> {
        let bandsNodes: [ScalarNode] = [.EqCustomParam0,
                                        .EqCustomParam1,
                                        .EqPreset]
        
        let getBandsAndPreset = getNodes(bandsNodes, forSpeaker: speaker)
        
        let getEqBands: Observable<[EqBand]> = getListNode(ListNode.EqBands, forSpeaker: speaker)
            .catchError { error in return Observable.just([]) }
        
        let getEqPresets: Observable<[EqPreset]> = getListNode(ListNode.EqPresets, forSpeaker: speaker)
            .catchError { error in return Observable.just([]) }
        
        return Observable.combineLatest(getBandsAndPreset, getEqBands, getEqPresets) { requestedNodes, eqBands, eqPresets in
            var presetIndex = 0
            
            if  let presetIndexString = requestedNodes[.EqPreset] {
                if let presetIndexValue = Int(presetIndexString) {
                    presetIndex = presetIndexValue
                }
            }
            
            let nodeForBandKey = [0: ScalarNode.EqCustomParam0,
                                  1: ScalarNode.EqCustomParam1]
            var bandValues: [EqBand:Int] = [:]
            
            for band in eqBands {
                var bandValue: Int? = nil
                if let nodeForBand = nodeForBandKey[band.key] {
                    if let stringValue = requestedNodes[nodeForBand] {
                        if let value = Int(stringValue) {
                            bandValue = value
                        }
                    }
                }
                
                if bandValue != nil {
                    bandValues[band] = bandValue!
                }
            }
            
            return EqState(eqPresets: eqPresets,
                           eqBands: eqBands,
                           currentEqPresetIndex: UInt32(presetIndex),
                           bandValues: bandValues)
        }
    }
    
    public static func authorizedPresetsWithArtwork(_ speaker: Speaker, provider: SpeakerProvider) -> Observable<[Preset]> {
        return SpeakerPresets.getPresetsForSpeaker(speaker, provider: provider)
    }
    
    public func groupVolumesForGroups(_ groups: [SpeakerGroup], knownVolumes: [Speaker: SpeakerVolume] = [:]) -> Observable<[SpeakerGroup: GroupVolume]> {
        var observables = [Observable<GroupVolume?>]()
        for group in groups {
            observables.append(
                groupVolumeForGroup(group, knownVolumes: knownVolumes)
                    .timeout(1.5, scheduler: MainScheduler.instance)
                    .catchErrorJustReturn(GroupVolume(masterVolume: nil, clientsVolumes: nil)))
        }
        
        return Observable.combineLatest(observables) { dictArray in
            var volumesForGroups = [SpeakerGroup:GroupVolume]()
            for (index, group) in groups.enumerated() {
                
                let volume = dictArray[index]
                volumesForGroups[group] = volume
            }
            
            return volumesForGroups
        }
    }
    
    public func setTOSAcceptedForGroup(_ group: SpeakerGroup) -> Observable<Bool> {
        var observables = [Observable<Bool>]()
        
        for speaker in group.speakers {
            observables.append(
                setNode(ScalarNode.CastTOS, value: "1", forSpeaker: speaker)
                    .timeout(1.5, scheduler: MainScheduler.instance)
                    .catchErrorJustReturn(false))
        }
        
        return Observable.combineLatest(observables) { operationSuccesses in return true }
    }
    
    public func setNetworkOptimizationForSpeakers(_ speakers: [Speaker], value: String) -> Observable<Bool> {
        var observables = [Observable<Bool>]()
        
        for speaker in speakers {
            observables.append(
                setNode(ScalarNode.MultiroomNetworkOptimization, value: value, forSpeaker: speaker)
                    .timeout(1.5, scheduler: MainScheduler.instance)
                    .catchErrorJustReturn(false))
        }
        
        return  Observable.combineLatest(observables) { operationSuccesses in return true }
    }
    
    public func groupVolumeForGroup(_ group: SpeakerGroup, knownVolumes: [Speaker: SpeakerVolume] = [:]) -> Observable<GroupVolume?> {
        guard let masterSpeaker = group.masterSpeaker else { return Observable.just(GroupVolume(masterVolume: nil, clientsVolumes: [:])) }
        
        var multiroomMasterVolume: Observable<String?> = Observable.just(nil)
        
        if group.isMulti {
            multiroomMasterVolume = self.getNode(ScalarNode.MultiroomMasterVolume, forSpeaker: masterSpeaker)
        }
        
        let speakersVolume = getVolumesForSpeakers(group.speakers, knownVolumes:  knownVolumes)
        return Observable.combineLatest(multiroomMasterVolume, speakersVolume){ masterVolumeString, speakersVolume -> GroupVolume? in
            var masterVolume: Int? = nil
            if let masterVolumeValue = masterVolumeString {
                masterVolume = Int(masterVolumeValue)
            }
            
            return GroupVolume(masterVolume: masterVolume, clientsVolumes: speakersVolume)
        }
    }
    
    public func getClientIndexesForGroup(_ group: SpeakerGroup) -> Observable<[UInt: Speaker]> {
        guard let masterSpeaker = group.masterSpeaker else { return Observable.just([:]) }
        
        return self.getListNode(ListNode.MultiroomList, forSpeaker: masterSpeaker)
            .map { (multiroomDevices: [MultiroomDevice]) -> [UInt: Speaker] in
                var clientIndexes: [UInt: Speaker] = [:]
                
                for device in multiroomDevices {
                    if let clientNumber = device.clientNumber,
                        let speaker = group.speakers.find({ $0.mac == device.mac }) {
                        clientIndexes[UInt(clientNumber)] = speaker
                    }
                }
                return clientIndexes
        }
    }
    
    public func getGroupVolumesIncludingClientIndexes(_ indexes: [UInt: Speaker], fromMasterSpeaker masterSpeaker: Speaker) -> Observable<[Speaker: SpeakerVolume]> {
        var nodesWanted: [ScalarNode] = [ScalarNode.Volume, ScalarNode.Mute]
        
        for index in indexes.keys {
            if index < 4 {
                nodesWanted.append(ScalarNode.multiClientMuteNodeForIndex(index)!)
                nodesWanted.append(ScalarNode.multiClientVolumeNodeForIndex(index)!)
            }
        }
        
        return getNodes(nodesWanted, forSpeaker: masterSpeaker)
            .map { nodes in
                var clientsVolumes = [Speaker : SpeakerVolume]()
                
                let masterSpeakerVolumeValue: Int? = intValueForNode(ScalarNode.Volume, fromNodesResponse: nodes)
                let masterSpeakerMuteValue: Bool = boolValueForNode(ScalarNode.Mute, fromNodesResponse: nodes)
                let masterSpeakerVolume = SpeakerVolume(volume: masterSpeakerVolumeValue,
                                                        mute: masterSpeakerMuteValue,
                                                        bass: nil,
                                                        treble: nil)
                
                clientsVolumes[masterSpeaker] = masterSpeakerVolume
                
                for index in indexes.keys {
                    if let muteNode = ScalarNode.multiClientMuteNodeForIndex(index),
                        let volumeNode = ScalarNode.multiClientVolumeNodeForIndex(index) {
                        
                        let volumeValue: Int? = intValueForNode(volumeNode, fromNodesResponse: nodes)
                        let muteValue: Bool = boolValueForNode(muteNode, fromNodesResponse: nodes)
                        let clientVolume = SpeakerVolume(volume: volumeValue,
                                                         mute: muteValue,
                                                         bass: nil,
                                                         treble: nil)
                        let speaker = indexes[index]!
                        clientsVolumes[speaker] = clientVolume
                    }
                }
                
                return clientsVolumes
        }
    }
    
    public func getNetworkOptimizationInfo(master speaker: Speaker) -> Observable<NetworkOptimizationInfo?> {
        return self.getNodes([ScalarNode.MultiroomNetworkOptimization,
                              ScalarNode.Version], forSpeaker: speaker)
            .timeout(5.0, scheduler: MainScheduler.instance)
            .map { results -> NetworkOptimizationInfo? in
                guard let optimizationString = results[.MultiroomNetworkOptimization] else { return nil }
                let optimization = optimizationString == "1"
                return NetworkOptimizationInfo(speaker: speaker, optimization: optimization)
        }
    }
    
    public func getVolumesForSpeakers(_ speakers: [Speaker], knownVolumes:[Speaker: SpeakerVolume] = [:]) -> Observable<[Speaker: SpeakerVolume]> {
        var observables = [Observable<SpeakerVolume>]()
        
        for speaker in speakers {
            var speakerVolume: Observable<SpeakerVolume> = Observable.empty()
            if let knownVolume = knownVolumes[speaker] {
                speakerVolume = Observable.just(knownVolume)
            } else {
                speakerVolume = getNodes([ScalarNode.Volume, ScalarNode.VolumeSteps, ScalarNode.Mute, ScalarNode.EqCustomParam0, ScalarNode.EqCustomParam1], forSpeaker: speaker)
                    .timeout(1.0, scheduler: MainScheduler.instance)
                    .catchErrorJustReturn([:])
                    .map { dict in
                        let volume = dict[.Volume]
                        let volumeInt: Int? = volume != nil ? Int(volume!) : nil
                        let mute = dict[.Mute]
                        let muteBool: Bool? = mute != nil ? (mute == "1") : nil
                        let bass = intValueForNode(ScalarNode.EqCustomParam0, fromNodesResponse: dict)
                        let treble = intValueForNode(ScalarNode.EqCustomParam1, fromNodesResponse: dict)
                        
                        return SpeakerVolume(volume: volumeInt,
                                             mute: muteBool,
                                             bass: bass,
                                             treble: treble)
                }
            }
            
            observables.append(speakerVolume)
        }
        return Observable.combineLatest(observables) { dictArray in
            var volumesForSpeakers = [Speaker: SpeakerVolume]()
            
            for (index, speakerVolume) in dictArray.enumerated() {
                let speaker = speakers[index]
                volumesForSpeakers[speaker] = speakerVolume
            }
            return volumesForSpeakers
        }
    }
    
    public func getClientStateForSpeaker(_ speaker: Speaker) -> Observable<GroupClientState> {
        let nodesToGet: [ScalarNode] = [.Mode,
                                        .Volume,
                                        .Mute,
                                        .PlayCaps,
                                        .NavCurrentPreset,
                                        .BluetoothDiscoverableState,
                                        .FriendlyName,
                                        .MultiroomGroupState,
                                        .SpotifyPlaylistName,
                                        .SpotifyPlaylistURI]
        
        let getScalarNodes = getNodes(nodesToGet, forSpeaker: speaker)
            .flatMapLatest { nodes -> Observable<[ScalarNode: String]> in
                
                //use this to delay loading to test long lonading scenarios
                //return Observable<Int>.timer(3, scheduler: MainScheduler.instance).map{ _ in return nodes }
                return Observable.just(nodes) }
        
        let getValidModes  = getValidModesForSpeaker(speaker)
        let getBluetoothDevices: Observable<[BluetoothDevice]> = getListNode(ListNode.BluetoothDevices, forSpeaker: speaker)
            .catchError { error in return Observable.just([]) }
        
        let getPresets = SpeakerPresets.getPresetsForSpeaker(speaker, provider: self)
        return Observable.combineLatest(getScalarNodes, getValidModes, getPresets, getBluetoothDevices) { requestedNodes, validModes, presets, bluetoothDevices in
            var modeIndex: Int? = nil
            if let modeIndexString = requestedNodes[.Mode],
                let modeIndexInt = Int(modeIndexString) {
                modeIndex = modeIndexInt
            }
            
            var presetIndex: Int? = nil
            if let presetIndexString = requestedNodes[.NavCurrentPreset],
                let presetIndexInt = UInt32(presetIndexString),
                presetIndexInt != UInt32.max {
                
                presetIndex = Int(presetIndexInt)
            }
            
            var volume: Int? = nil
            if let volumeString = requestedNodes[.Volume],
                let volumeInt = Int(volumeString) {
                volume = volumeInt
            }
            
            let mute = requestedNodes[.Mute] == "1"
            let playCaps = PlayCaps(rawValue: Int(requestedNodes[.PlayCaps] ?? "0" ) ?? 0)
            let groupState = GroupState(rawValue: Int(requestedNodes[.MultiroomGroupState] ?? "0") ?? 0)
            let bluetoothDiscoverable = requestedNodes[.BluetoothDiscoverableState] == "1"
            
            return GroupClientState(presetIndex: presetIndex,
                                    modeIndex: modeIndex,
                                    volume: volume,
                                    mute: mute,
                                    playCaps: playCaps,
                                    groupState: groupState,
                                    bluetoothDiscoverable: bluetoothDiscoverable,
                                    friendlyName: speaker.friendlyName,
                                    presets: presets,
                                    modes: validModes,
                                    bluetoothDevices: bluetoothDevices)
        }
    }
    
    public func getMasterStateForSpeaker(_ speaker: Speaker) -> Observable<GroupMasterState> {
        let nodesToGet: [ScalarNode] = [.Power,
                                        .Mode,
                                        .MultiroomMasterVolume,
                                        .PlayCaps,
                                        .NavCaps,
                                        .PlayStatus,
                                        .PlayName,
                                        .PlayText,
                                        .PlayArtist,
                                        .PlayAlbum,
                                        .PlayGraphicUri,
                                        .PlayDuration,
                                        .PlayPosition,
                                        .PlayShuffle,
                                        .PlayRepeat,
                                        .CastAppName,
                                        .SpotifyPlaylistName,
                                        .SpotifyPlaylistURI]
        
        let getScalarNodes = getNodes(nodesToGet, forSpeaker: speaker)
            .flatMapLatest { nodes -> Observable<[ScalarNode: String]> in
                //use this to delay loading to test long lonading scenarios
                //return Observable<Int>.timer(3, scheduler: MainScheduler.instance).map{ _ in return nodes }
                return Observable.just(nodes) }
        
        let getValidModes  = getValidModesForSpeaker(speaker)
        
        return Observable.combineLatest(getScalarNodes, getValidModes) { requestedNodes, validModes in
            let power = requestedNodes[.Power] == "1"
            
            var modeIndex: Int? = nil
            if let modeIndexString = requestedNodes[.Mode],
                let modeIndexInt = Int(modeIndexString) {
                modeIndex = modeIndexInt
            }
            
            var masterVolume: Int?  = nil
            if let masterVolumeString = requestedNodes[.MultiroomMasterVolume],
                let masterVolumeInt = Int(masterVolumeString) {
                masterVolume = masterVolumeInt
            }
            
            let playCaps = PlayCaps(rawValue: Int(requestedNodes[.PlayCaps] ?? "0" ) ?? 0)
            let navCaps = NavCaps(rawValue: Int(requestedNodes[.NavCaps] ?? "0" ) ?? 0)
            let playStatus = PlayStatus(rawValue: Int(requestedNodes[.PlayStatus] ?? "0" ) ?? 0)
            
            let playName = requestedNodes[.PlayName]
            let playText = requestedNodes[.PlayText]
            let playArtist = requestedNodes[.PlayArtist]
            let playAlbum = requestedNodes[.PlayAlbum]
            let playGraphicUri = requestedNodes[.PlayGraphicUri]
            let castAppName = requestedNodes[.CastAppName]
            
            var playDuration: Int = 0
            if let playDurationString = requestedNodes[.PlayDuration],
                let playDurationInt = Int(playDurationString) {
                playDuration = playDurationInt
            }
            
            var playPosition: Int = 0
            if let playPositionString = requestedNodes[.PlayPosition],
                let playPositionInt = Int(playPositionString) {
                playPosition = playPositionInt
            }
            
            let playShuffle = requestedNodes[.PlayShuffle] == "1"
            let playRepeat = requestedNodes[.PlayRepeat] == "1"
            
            let spotifyPlaylistName = requestedNodes[.SpotifyPlaylistName]
            let spotifyPlaylistURI = requestedNodes[.SpotifyPlaylistURI]
            
            return GroupMasterState(power: power,
                                    modeIndex: modeIndex,
                                    masterVolume: masterVolume,
                                    navCaps: navCaps,
                                    playCaps: playCaps,
                                    playStatus: playStatus,
                                    playName: playName,
                                    playText: playText,
                                    playArtist: playArtist,
                                    playAlbum: playAlbum,
                                    playGraphicURI: playGraphicUri,
                                    castAppName: castAppName,
                                    duration: playDuration,
                                    position: playPosition,
                                    shuffle: playShuffle,
                                    repeats: playRepeat,
                                    spotifyPlaylistName: spotifyPlaylistName,
                                    spotifyPlaylistURI: spotifyPlaylistURI,
                                    modes: validModes,
                                    userInteractionInProgress: false)
        }
    }
    
    public func getFirmwareVersionForSpeaker(_ speaker: Speaker) -> Observable<String?> {
        var getFirmwareVersion = Observable<String?>.just(nil)
        
        if let firmwareVersion = self.firmwareVersionForSpeaker[speaker] {
            getFirmwareVersion = Observable.just(firmwareVersion)
        } else {
            getFirmwareVersion = getNode(ScalarNode.Version, forSpeaker: speaker)
                .do(onNext: { [weak self] version in
                    self?.firmwareVersionForSpeaker[speaker] = version
                })
                .catchError { error in return Observable.just(nil) }
        }
        
        return getFirmwareVersion
    }
    
    public func getValidModesForSpeaker(_ speaker: Speaker) -> Observable<[Mode]> {
        var getValidModes = Observable<[Mode]>.just([])
        
        if let modes = self.validModesForSpeaker[speaker] {
            getValidModes = Observable.just(modes)
        } else {
            getValidModes = getListNode(ListNode.ValidModes, forSpeaker: speaker)
                .do(onNext: { [weak self] modes in
                    self?.validModesForSpeaker[speaker] = modes
                })
                .catchError { error in return Observable.just([]) }
        }
        
        return getValidModes
    }
    
    public func getNowPlayingStateForSpeaker(_ speaker: Speaker) -> Observable<NowPlayingState> {
        let nodesToGet: [ScalarNode] = [.Mode,
                                        .NavCurrentPreset,
                                        .PlayStatus]
        
        let getScalarNodes = getNodes(nodesToGet, forSpeaker: speaker)
        let getValidModes = getValidModesForSpeaker(speaker)
        
        return Observable.combineLatest(getScalarNodes, getValidModes) { requestedNodes, validModes in
            return NowPlayingState.from(nodeStringValues: requestedNodes, modes: validModes)
        }
    }
}

func intValueForNode(_ node: ScalarNode, fromNodesResponse nodes: [ScalarNode: String]) -> Int? {
    guard let stringValue = nodes[node] else { return nil }
    return Int(stringValue)
}

func boolValueForNode(_ node: ScalarNode, fromNodesResponse nodes: [ScalarNode: String]) -> Bool {
    guard let stringValue = nodes[node] else { return false }
    return stringValue == "1"
}
