//
//  UpdateState.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 02/09/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation

public enum UpdateState: Int {
    case idle = 0
    case checkInProgress
    case updateAvailable
    case updateNotAvailable
    case checkFailed
}
