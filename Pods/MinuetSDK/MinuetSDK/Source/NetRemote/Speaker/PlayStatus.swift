//
//  PlayStatus.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 11/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation

public enum PlayStatus: Int {
    case idle = 0
    case buffering
    case playing
    case paused
    case rebuffering
    case error
    case stopped
}
