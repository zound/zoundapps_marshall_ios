//
//  EqPreset.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 18/05/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation

public struct EqPreset {
    public let key: Int
    public let name: String
}

extension EqPreset: Hashable {
    public var hashValue: Int {
        return key
    }
}

extension EqPreset: Equatable {}

public func ==(lhs: EqPreset, rhs: EqPreset) -> Bool {
    return lhs.key == rhs.key
}
