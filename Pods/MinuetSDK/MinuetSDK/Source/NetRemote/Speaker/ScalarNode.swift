//
//  ScalarNode.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 14/07/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation

public enum ScalarNode: String {
    case FriendlyName  = "netremote.sys.info.friendlyname"
    case Color         = "netRemote.platform.OEM.colorProduct"
    case Version       = "netremote.sys.info.version"
    case BuildVersion  = "netremote.sys.info.buildversion"
    case ModelName     = "netRemote.sys.info.modelName"
    case PIN           = "netremote.sys.info.radiopin"
    case VendorId      = "netremote.sys.info.netremotevendorid"
    case ActiveSession = "netremote.sys.info.activesession"
    case VolumeSteps   = "netremote.sys.caps.volumesteps"
    case Power         = "netremote.sys.power"
    case Mode          = "netremote.sys.mode"
    case Volume        = "netremote.sys.audio.volume"
    case Mute          = "netremote.sys.audio.mute"
    case FactoryReset  = "netremote.sys.factoryreset"
    case State         = "netremote.sys.state"
    case UtcOffset     = "netRemote.sys.clock.utcOffset"
    case TimeZone      = "netRemote.sys.clock.timeZone"
    case UserDataStore = "netremote.misc.nvs.data"
    case AutoPlayIR    = "netRemote.sys.cfg.irautoplayflag"
    
    case WlanMACAddress     = "netremote.sys.net.wlan.macaddress"
    case WlanSSID           = "netremote.sys.net.wlan.connectedssid"
    case WlanSignalStrength = "netremote.sys.net.wlan.rssi"
    case WlanIPAddress      = "netremote.sys.net.ipconfig.address"
    case WlanDHCP           = "netremote.sys.net.ipconfig.dhcp"
    case WlanSubnetMask     = "netremote.sys.net.ipconfig.subnetmask"
    case WlanDNSPrimary     = "netremote.sys.net.ipconfig.dnsprimary"
    case WlanDNSSecondary   = "netremote.sys.net.ipconfig.dnssecondary"
    
    case LedIntensity      = "netRemote.platform.oem.ledIntensity"
    case LedIntensitySteps = "netRemote.platform.oem.ledintensitysteps"
    
    case PlayCaps       = "netremote.play.caps"
    case PlayStatus     = "netremote.play.status"
    case PlayName       = "netremote.play.info.name"
    case PlayText       = "netremote.play.info.text"
    case PlayAlbum      = "netremote.play.info.album"
    case PlayArtist     = "netremote.play.info.artist"
    case PlayGraphicUri = "netremote.play.info.graphicuri"
    case PlayDuration   = "netremote.play.info.duration"
    case PlayPosition   = "netremote.play.position"
    case PlayError      = "netremote.play.errorstr"
    
    case PlayShuffle       = "netremote.play.shuffle"
    case PlayShuffleStatus = "netremote.play.shufflestatus"
    case PlayRepeat        = "netremote.play.repeat"
    
    case PlayAddPreset       = "netremote.play.addpreset"
    case PlayAddPresetStatus = "netremote.play.addpresetstatus"
    case PlayControl         = "netremote.play.control"
    
    case EqPreset       = "netremote.sys.audio.eqpreset"
    case EqCustomParam0 = "netremote.sys.audio.eqcustom.param0"
    case EqCustomParam1 = "netremote.sys.audio.eqcustom.param1"
    case EqLoudness     = "netremote.sys.audio.eqloudness"
    
    case NavError      = "netremote.nav.errorstr"
    case NavNumItems   = "netremote.nav.numitems"
    case NavState      = "netremote.nav.state"
    case NavStatus     = "netremote.nav.status"
    case NavDepth      = "netremote.nav.depth"
    case NavCaps       = "netremote.nav.caps"
    case NavSearchTerm = "netremote.nav.searchterm"
    
    case NavActionNavigate     = "netremote.nav.action.navigate"
    case NavActionSelectPreset = "netremote.nav.action.selectpreset"
    case NavActionSelectItem   = "netremote.nav.action.selectitem"
    
    case NavCurrentPreset = "netremote.nav.preset.currentpreset"
    case NavPresetVersion = "netremote.nav.preset.listversion"
    
    case NavPresetUploadType       = "netremote.nav.preset.upload.type"
    case NavPresetUploadName       = "netremote.nav.preset.upload.name"
    case NavPresetUploadArtworkURL = "netRemote.nav.preset.upload.artworkUrl"
    case NavPresetUploadBlob       = "netremote.nav.preset.upload.blob"
    case NavPresetUploadUpload     = "netremote.nav.preset.upload.upload"
    
    case NavPresetDownloadType       = "netremote.nav.preset.download.type"
    case NavPresetDownloadName       = "netremote.nav.preset.download.name"
    case NavPresetDownloadArtworkURL = "netRemote.nav.preset.download.artworkUrl"
    case NavPresetDownloadBlob       = "netremote.nav.preset.download.blob"
    case NavPresetDownloadDownload   = "netremote.nav.preset.download.download"
    case NavPresetDelete             = "netremote.nav.preset.delete"
    
    case NavPresetSwapIndex1 = "netremote.nav.preset.swap.index1"
    case NavPresetSwapIndex2 = "netremote.nav.preset.swap.index2"
    case NavPresetSwapSwap   = "netremote.nav.preset.swap.swap"
    
    case MultiroomListVersion          = "netremote.multiroom.device.listallversion"
    case MultiroomMasterVolume         = "netremote.multiroom.group.mastervolume"
    case MultiroomNetworkOptimization  = "netRemote.multiroom.device.transportOptimisation"
    case MultiroomGroupCreate          = "netremote.multiroom.group.create"
    case MultiroomGroupDestroy         = "netremote.multiroom.group.destroy"
    case MultiroomGroupAddClient       = "netremote.multiroom.group.addclient"
    case MultiroomGroupRemoveClient    = "netremote.multiroom.group.removeclient"
    case MultiroomGroupBecomeServer    = "netremote.multiroom.group.becomeserver"
    case MultiroomGroupState           = "netremote.multiroom.group.state"
    case MultiroomGroupName            = "netremote.multiroom.group.name"
    case MultiroomGroupId              = "netremote.multiroom.group.id"
    case MultiroomGroupStreamable      = "netremote.multiroom.group.streamable"
    case MultiroomGroupAttachedClients = "netremote.multiroom.group.attachedclients"
    
    case MultiroomSingleGroupState = "netremote.multiroom.singlegroup.state"
    
    case MultiroomMaxClients      = "netremote.multiroom.caps.maxclients"
    case MultiroomProtocolVersion = "netremote.multiroom.caps.protocolversion"
    
    case MultiroomDeviceServerStatus = "netremote.multiroom.device.serverstatus"
    case MultiroomDeviceClientStatus = "netremote.multiroom.device.clientstatus"
    case MultiroomDeviceClientIndex  = "netremote.multiroom.device.clientindex"
    
    case MultiroomClientStatus0 = "netremote.multiroom.client.status0"
    case MultiroomClientStatus1 = "netremote.multiroom.client.status1"
    case MultiroomClientStatus2 = "netremote.multiroom.client.status2"
    case MultiroomClientStatus3 = "netremote.multiroom.client.status3"
    
    case MultiroomClientVolume0 = "netremote.multiroom.client.volume0"
    case MultiroomClientVolume1 = "netremote.multiroom.client.volume1"
    case MultiroomClientVolume2 = "netremote.multiroom.client.volume2"
    case MultiroomClientVolume3 = "netremote.multiroom.client.volume3"
    
    case MultiroomClientMute0   = "netremote.multiroom.client.mute0"
    case MultiroomClientMute1   = "netremote.multiroom.client.mute1"
    case MultiroomClientMute2   = "netremote.multiroom.client.mute2"
    case MultiroomClientMute3   = "netremote.multiroom.client.mute3"
    
    case CastTOS         = "netremote.cast.tos"
    case CastUsageReport = "netremote.cast.usagereport"
    case CastAppName     = "netremote.cast.appname"
    case CastVersion     = "netremote.cast.version"
    
    case BluetoothDiscoverableState  = "netremote.bluetooth.discoverablestate"
    case BluetoothDevicesListVersion = "netremote.bluetooth.connecteddeviceslistversion"
    
    case SpotifyLoginWithToken = "netremote.spotify.loginusingoauthtoken"
    case SpotifyLoggedInState  = "netremote.spotify.loggedinstate"
    case SpotifyUsername       = "netremote.spotify.username"
    case SpotifyPlaylistName   = "netremote.spotify.playlist.name"
    case SpotifyPlaylistURI    = "netremote.spotify.playlist.uri"
    
    case UpdateState    = "netremote.sys.isu.state"
    case UpdateControl  = "netremote.sys.isu.control"
    case UpdateProgress = "netremote.sys.isu.softwareupdateprogress"
    
    case UpdateVersion   = "netremote.sys.isu.version"
    case UpdateSummary   = "netremote.sys.isu.summary"
    case UpdateMandatory = "netremote.sys.isu.mandatory"
    
    private static let muteNodeForClientIndex: [UInt: ScalarNode] = [0: ScalarNode.MultiroomClientMute0,
                                                                     1: ScalarNode.MultiroomClientMute1,
                                                                     2: ScalarNode.MultiroomClientMute2,
                                                                     3: ScalarNode.MultiroomClientMute3]
    
    private static let volumeNodeForIndex: [UInt: ScalarNode] = [0: ScalarNode.MultiroomClientVolume0,
                                                                 1: ScalarNode.MultiroomClientVolume1,
                                                                 2: ScalarNode.MultiroomClientVolume2,
                                                                 3: ScalarNode.MultiroomClientVolume3]
}

extension ScalarNode {
    public static func multiClientMuteNodeForIndex(_ index: UInt) -> ScalarNode? {
        guard index < 4 else { return nil }
        return muteNodeForClientIndex[index]
    }
    
    public static func multiClientVolumeNodeForIndex(_ index: UInt) -> ScalarNode? {
        guard index < 4 else { return nil }
        return volumeNodeForIndex[index]
    }
}
