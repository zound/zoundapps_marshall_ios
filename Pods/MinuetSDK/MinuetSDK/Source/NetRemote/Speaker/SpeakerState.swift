//
//  SpeakerState.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 11/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation

public struct GroupMasterState {
    public var power: Bool?
    public var modeIndex: Int?
    public var masterVolume: Int?
    public var navCaps: NavCaps?
    public var playCaps: PlayCaps?
    public var playStatus: PlayStatus?
    public var playName: String?
    public var playText: String?
    public var playArtist: String?
    public var playAlbum: String?
    public var playGraphicURI: String?
    public var castAppName: String?
    public var duration: Int?
    public var position: Int?
    public var shuffle: Bool
    public var repeats: Bool
    public var spotifyPlaylistName: String?
    public var spotifyPlaylistURI: String?
    public var modes: [Mode]
    public var userInteractionInProgress: Bool = false
    public var isPlayingOrBuffering: Bool {
        if let playStatus = self.playStatus {
            return [.buffering, .rebuffering, .playing].contains(playStatus)
        }
        return false
    }
    
    public var isPlaying: Bool {
        if let playStatus = self.playStatus {
            return playStatus == .playing
        }
        return false
    }
    
    public var currentMode: Mode? {
        if let `modeIndex` = modeIndex {
            return self.modes.filter({$0.key == modeIndex}).first
        }
        return nil
    }
    
    public var currentPlayableItem: PlayableItem? {
        return currentMode
    }
    
    public mutating func updateWithNode(_ node: ScalarNode, value: String) {
        switch node {
        case .Power: self.power = (value == "1")
        case .Mode: self.modeIndex = Int(value)
        case .MultiroomMasterVolume: self.masterVolume = Int(value)
        case .PlayCaps: self.playCaps = PlayCaps(rawValue: Int(value)!)
        case .NavCaps: self.navCaps = NavCaps(rawValue: Int(value)!)
        case .PlayStatus: self.playStatus = PlayStatus(rawValue: Int(value)!)
        case .PlayName: self.playName = value
        case .PlayText: self.playText = value
        case .PlayArtist: self.playArtist = value
        case .PlayAlbum: self.playAlbum = value
        case .PlayGraphicUri: self.playGraphicURI = value
        case .PlayDuration: self.duration = Int(value)
        case .PlayPosition: self.position = Int(value)
        case .PlayShuffle: self.shuffle = value == "1"
        case .PlayRepeat: self.repeats = value == "1"
        case .CastAppName: self.castAppName = value
        case .SpotifyPlaylistURI: self.spotifyPlaylistURI = value
        case .SpotifyPlaylistName: self.spotifyPlaylistName = value
        default: break;
        }
    }
}

public struct GroupClientState {
    public var presetIndex: Int?
    public var modeIndex: Int?
    public var volume: Int?
    public var mute: Bool?
    public var playCaps: PlayCaps?
    public var groupState: GroupState?
    public var bluetoothDiscoverable: Bool
    public var friendlyName: String
    public var presets: [Preset]
    public var modes: [Mode]
    public var bluetoothDevices: [BluetoothDevice]
    
    public var currentPlayableItem: PlayableItem? {
        var currentPlayableItem: PlayableItem?
        if self.currentPreset == nil && self.currentMode == nil {
            currentPlayableItem = nil
        }
        if self.currentPreset == nil && self.currentMode != nil {
            currentPlayableItem = currentMode
        }
        if self.currentPreset != nil {
            
            currentPlayableItem = currentPreset
        }
        
        return currentPlayableItem
    }
    
    public var currentMode: Mode? {
        if let `modeIndex` = modeIndex {
            return self.modes.filter({$0.key == modeIndex}).first
        }
        return nil
    }
    
    public var currentPreset: Preset? {
        if let `presetIndex` = presetIndex {
            return self.presets.filter({ ($0.number - 1) == presetIndex}).first
        }
        return nil
    }
    
    public mutating func updateWithNode(_ node: ScalarNode, value: String) {
        switch node {
        case .Mode: self.modeIndex = Int(value)
        case .Volume: self.volume = Int(value)
        case .Mute: self.mute = value == "1"
        case .PlayCaps: self.playCaps = PlayCaps(rawValue: Int(value)!)
        case .BluetoothDiscoverableState: self.bluetoothDiscoverable = value == "1"
        case .NavCurrentPreset:
            if let intValue = UInt32(value) {
                if intValue == UInt32.max {
                    self.presetIndex = nil
                } else {
                    self.presetIndex = Int(intValue)
                }
            }
        case .MultiroomGroupState: self.groupState = GroupState(rawValue: Int(value)!)
        case .FriendlyName: self.friendlyName = value
        default: break;
        }
    }
}
