//
//  PlayableItem.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 05/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation

public enum SelectableType {
    case preset(number: Int)
    case aux
    case rca
    case bluetooth
    case cloud
    case internetRadio
    
    public var number: Int? {
        switch self {
        case .preset(let number): return number
        default: return 0
        }
    }
    
    var typeId: String {
        switch self {
        case .preset(_): return "preset"
        case .aux: return "aux"
        case .rca: return "rca"
        case .bluetooth: return "bluetooth"
        case .cloud: return "cloud"
        case .internetRadio: return "internetRadio"
        }
    }
    
    public var iconName: String? {
        switch self {
        case .aux: return "source_aux_icon"
        case .rca: return "source_rca_icon"
        case .bluetooth: return "source_bluetooth_icon"
        case .cloud: return "source_cloud_icon_small"
        case .internetRadio: return "source_cloud_icon"
            
        default: return nil
        }
    }
    
    public var smallIconName: String? {
        switch self {
        case .aux: return "mini_player_aux_source"
        case .rca: return "mini_player_rca_source"
        case .bluetooth: return "mini_player_bluetooth_source"
        case .cloud: return "mini_player_cloud_source"
        case .internetRadio: return "mini_player_cloud_source"
            
        default: return nil
        }
    }
}

extension SelectableType: Equatable {}

public func ==(lhs: SelectableType, rhs: SelectableType) -> Bool {
    return lhs.typeId == rhs.typeId && lhs.number == rhs.number
}

public protocol PlayableItem {
    var selectableType:SelectableType { get }
    var playableTitle: String { get }
}

extension PlayableItem {
    public var playableTitle: String {
        return "not set"
    }
}
