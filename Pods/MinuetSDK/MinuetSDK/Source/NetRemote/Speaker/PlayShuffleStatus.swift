//
//  PlayShuffleStatus.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 11/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation

public enum PlayShuffleStatus: Int {
    case ok = 0
    case shuffling
    case tooManyItems
    case unsupported
}
