//
//  Speaker.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 21/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//
import Foundation
import ObjectMapper

public struct Speaker {
    public var friendlyName: String
    public var ipAddress: String
    public var wifiName: String
    public var UDN: String
    
    public var features:[String] { return ["Audsync"] }
    public var pinCode: String { return "1234" }
    public var mac: String {
        guard UDN.count >= 12 else { return "000000000000" }
        let part = UDN.index(UDN.endIndex, offsetBy: -12)
        return String(UDN[part...])
    }
    
    public var hasMultiroom: Bool {
        return features.contains("AudioSync") || features.contains("Audsync")
    }
    
    public var color: String?
    public var nameIndexKey: String {
        let colorString = color != nil ? "\(color!) " : ""
        return "\(colorString)\(friendlyName)"
    }
    
    public var nameIndex:Int {
        return SpeakerNameMacStore.sharedInstance.index(ofMac: self.mac, forName: nameIndexKey) ?? 0
    }
    
    public var friendlyNameWithIndex: String {
        if nameIndex > 0 {
            return "\(friendlyName) (\(nameIndex + 1))"
        } else {
            return friendlyName
        }
    }
    
    public var sortName: String {
        if nameIndex > 0 {
            return "\(friendlyName) (\(nameIndex + 1))"
        } else {
            return friendlyName
        }
    }
    
    public init(friendlyName: String, ipAddress: String, wifiName: String, UDN: String, color: String?) {
        self.friendlyName = friendlyName
        self.ipAddress = ipAddress
        self.wifiName = wifiName
        self.UDN = UDN
        self.color = color
        
        SpeakerNameMacStore.sharedInstance.store(mac: self.mac, forName: nameIndexKey)
    }
}

extension Speaker: Hashable {
    public var hashValue: Int {
        return mac.hashValue
    }
}

extension Speaker: Equatable {}

public func ==(lhs: Speaker, rhs: Speaker  ) -> Bool {
    return (lhs.ipAddress == rhs.ipAddress) && (lhs.mac == rhs.mac)
}

extension Speaker: Mappable {
    public init?(map: Map) {
        friendlyName = "init mappable friendly name"
        UDN = "000000000000"
        ipAddress = "0.0.0.0"
        wifiName = "init wifi name"
    }
    // Mappable
    public mutating func mapping(map: Map) {
        friendlyName <- map["friendlyName"]
        ipAddress    <- map["ipAddress"]
        wifiName     <- map["wifiName"]
        UDN          <- map["UDN"]
        color        <- map["color"]
    }
}
