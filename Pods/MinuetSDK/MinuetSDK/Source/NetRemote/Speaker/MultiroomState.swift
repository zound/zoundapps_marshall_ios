//
//  MultiroomState.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 29/10/2016.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
public struct MultiroomState {
    public var groupName: String?
    public var groupId: String?
    public var groupState: GroupState?
    public var clientIndex: Int?
    public var networkOptimization: Bool?
}

extension MultiroomState {
    static func from(nodeStringValues info: [ScalarNode: String]) -> MultiroomState {
        let groupName = info[ScalarNode.MultiroomGroupName]
        let groupId = info[ScalarNode.MultiroomGroupId]
        
        var groupClientIndex: Int?
        if let groupClientIndexString = info[ScalarNode.MultiroomDeviceClientIndex] {
            groupClientIndex = Int(groupClientIndexString)
        }
        
        var groupState: GroupState?
        if let groupStateString = info[ScalarNode.MultiroomGroupState] {
            groupState = GroupState(rawValue: Int(groupStateString)!)
        }
        
        var networkOptimization: Bool?
        if let networkOptimizationString = info[ScalarNode.MultiroomNetworkOptimization] {
            networkOptimization = networkOptimizationString == "1"
        }
        
        let multiroomState = MultiroomState(groupName:groupName == "" ? nil : groupName,
                                            groupId: groupId == "" ? nil : groupId,
                                            groupState: groupState,
                                            clientIndex: groupClientIndex,
                                            networkOptimization: networkOptimization)
        
        return multiroomState
    }
}
