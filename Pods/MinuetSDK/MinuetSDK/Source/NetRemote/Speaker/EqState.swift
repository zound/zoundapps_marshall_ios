//
//  EqualizerState.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 18/05/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation

public struct EqState {
    public let eqPresets: [EqPreset]
    public let eqBands: [EqBand]
    public var currentEqPresetIndex: UInt32
    public var bandValues: [EqBand:Int]
    
    public var bassBand: EqBand? {
        return self.eqBands.filter({$0.name == "Bass"}).first
    }
    
    public var trebleBand: EqBand? {
        return self.eqBands.filter({$0.name == "Treble"}).first
    }
    
    public var customPreset: EqPreset? {
        return self.eqPresets.filter({$0.name == "My EQ"}).first
    }
}
