//
//  Mode.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 25/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

public struct Mode: PlayableItem {
    public let key: Int
    public let id: String
    public let selectable: Bool
    public let label: String
    public let streamable: Bool?
    public let modeType: String?
    
    public var selectableType: SelectableType {
        switch id {
        case "AUXIN": return .aux
        case "Bluetooth": return .bluetooth
        case "Spotify": return .cloud
        case "IR": return .internetRadio
        case "RCA": return .rca
        default: return .cloud
        }
    }
    
    public var isStandby: Bool { return id == "Standby" }
    public var isAudsync: Bool { return id == "Audsync" }
    public var isCast: Bool { return id == "Google Cast" }
    public var isIR: Bool { return id == "IR" }
    public var isSpotify: Bool { return id == "Spotify" }
}

extension Mode: Equatable {}

public func == (lhs: Mode, rhs: Mode) -> Bool {
    return lhs.key == rhs.key
}
