//
//  PlayControl.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 11/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation

public enum UpdateControl: Int {
    case idle = 0
    case performUpdate
    case checkForUpdate    
}
