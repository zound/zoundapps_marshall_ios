//
//  AboutState.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 19/05/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation

public struct AboutState {
    public let color: String?
    public let wifiName: String?
    public let wifiSignal: Int?
    public let softwareVersion: String?
    public let castVersion: String?
    public let buildVersion: String?
}
