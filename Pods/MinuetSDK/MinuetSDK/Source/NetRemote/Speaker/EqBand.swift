//
//  EqBand.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 18/05/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation

public struct EqBand {
    public let key: Int
    public let name: String
    public let min: Int
    public let max: Int
}

extension EqBand: Hashable {
    public var hashValue: Int {
        return key
    }
}

extension EqBand: Equatable {}

public func ==(lhs: EqBand, rhs: EqBand) -> Bool {
    return lhs.key == rhs.key
}
