//
//  Preset.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 16/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//
import Foundation
import RxSwift

public enum PresetType {
    case spotify
    case internetRadio
    case unknown
    
    public func modeIdentifier() -> String {
        switch  self {
        case .spotify: return "Spotify"
        case .internetRadio: return "IR"
        case .unknown: return "Preset"
        }
    }
}

public struct Preset: PlayableItem {
    public let name: String
    public let number: Int
    public let imageURL: URL?
    public let typeID: String?
    public let blob: String?
    public let playlistUrl: String?
    
    public init(name: String,
                number: Int,
                imageURL: URL?,
                typeID: String?,
                blob: String?,
                playlistUrl: String?) {
        self.name = name
        self.number = number
        self.imageURL = imageURL
        self.typeID = typeID
        self.blob = blob
        self.playlistUrl = playlistUrl
    }
    
    public var selectableType: SelectableType {
        return SelectableType.preset(number: number)
    }
    
    public var playableTitle: String { return name }
    
    public var isEmpty: Bool {
        return name == ""
    }
    
    public var didSetPresetType: Variable<Bool> {
        return Variable(false)
    }
    
    public var type: PresetType? {
        if let typeID = self.typeID {
            didSetPresetType.value = false
            if typeID == "Spotify" {
                didSetPresetType.value = true
                return PresetType.spotify
            } else if typeID == "IR" {
                didSetPresetType.value = true
                return PresetType.internetRadio
            } else {
                return nil
            }
        }
        return nil
    }
}

extension Preset: Equatable {}

public func ==(lhs: Preset, rhs: Preset) -> Bool {
    return lhs.name == rhs.name && lhs.number == rhs.number && lhs.imageURL == rhs.imageURL
}

public extension Preset {
    public var spotifyURI: String? {
        guard self.type == PresetType.spotify else {
            return nil
        }
        
        guard self.blob?.isEmpty == false else {
            NSLog("ZOUND_1259 Received Spotify blob is empty, using playlistUrl: %@", self.playlistUrl ?? "<playlistUrl empty>" )
            return self.playlistUrl
        }
        
        guard let decodedBlob = self.blob?.base64DecodedData(), decodedBlob.count > 0 else {
            NSLog("ZOUND_1259 Received Spotify blob is invalid and cannot be decoded" )
            return nil
        }
        
        var spotifyURILength: UInt8 = 0
        let uriLengthRange: Range<Int> = 1..<3
        (decodedBlob.subdata(in: uriLengthRange) as NSData).getBytes(&spotifyURILength, length: MemoryLayout<UInt8>.size)
        
        // Temporary solution:
        // Check if sub-data lenght is not bigger than entire blob. This is because blob send by firmware contains information
        // about lenght of it's data. This information can be corrupted by bug in firmware and that will lead to crash if we don't validate it
        
        // Blob has size of (Spotify's data size info) + Spotify's data so Spotify URI lenght should be smaller than entire blob
        guard spotifyURILength < decodedBlob.count else {
            return nil
        }
        
        let uriDataRange: Range<Int> = 2..<(2+Int(spotifyURILength))
        let spotifyURIData = decodedBlob.subdata(in: uriDataRange)
        
        if let spotifyURI = String(data: spotifyURIData, encoding: String.Encoding.utf8) {
            if let spotifyLocation = String(data: spotifyURIData, encoding: String.Encoding.utf8)?.range(of: "spotify:") {
                
                let spotifyURI = spotifyURI.replacingCharacters(in: spotifyURI.startIndex..<spotifyLocation.lowerBound, with: "")
                NSLog("ZOUND_1259 Received Spotify blob is decoded, spotifyURI: %@", spotifyURI)
                return spotifyURI
            }
        }
        
        return nil
    }
    
    public var vTunerID: String? {
        if self.type == PresetType.internetRadio {
            if let blob = self.blob {
                if let blobBase64DecodedData = blob.base64DecodedData() {
                    if let radioID = String(data: blobBase64DecodedData as Data, encoding:  String.Encoding.utf8) {
                        let trimmedID = radioID.replacingOccurrences(of: "\0", with: "")
                        return trimmedID
                    }
                }
            }
        }
        return nil
    }
}
