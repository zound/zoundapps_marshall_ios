//
//  PlayControl.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 11/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation

public enum PlayControl: Int {
    case stop = 0
    case play
    case pause
    case next
    case previous
}
