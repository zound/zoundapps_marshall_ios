//
//  SpeakerVolume.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 15/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation

public struct SpeakerVolume {
    public var volume: Int?
    public var mute: Bool?
    public var bass: Int?
    public var treble: Int?
    public var volumeSteps: Int? {
        return 33
    }

    public init(volume: Int?,
                mute: Bool?,
                bass: Int?,
                treble: Int?) {
        self.volume = volume
        self.mute = mute
        self.bass = bass
        self.treble = treble
    }
}
