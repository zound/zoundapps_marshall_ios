//
//  TimeZone.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 19/06/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation


public struct TimeZone {
    public let key: Int
    public let name: String
    public let offset: Int
    
    public var displayName: String {
        return name.replacingOccurrences(of: "/", with: " - ").replacingOccurrences(of: "_", with: " ")
    }
}

extension TimeZone: Equatable {}

public func == (lhs: TimeZone, rhs: TimeZone) -> Bool {
    return lhs.key == rhs.key && lhs.name == rhs.name && lhs.offset == rhs.offset
}
