//
//  PlayCaps.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 11/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation

public struct PlayCaps: OptionSet {
    public let rawValue: Int
    
    public init(rawValue: Int) {
        self.rawValue = rawValue
    }
    
    public static let None          = PlayCaps(rawValue: 0)
    public static let Pause         = PlayCaps(rawValue: 1 << 0)
    public static let Stop          = PlayCaps(rawValue: 1 << 1)
    public static let SkipNext      = PlayCaps(rawValue: 1 << 2)
    public static let SkipPrevious  = PlayCaps(rawValue: 1 << 3)
    public static let FastForward   = PlayCaps(rawValue: 1 << 4)
    public static let Rewind        = PlayCaps(rawValue: 1 << 5)
    public static let Shuffle       = PlayCaps(rawValue: 1 << 6)
    public static let Repeat        = PlayCaps(rawValue: 1 << 7)
    public static let Seek          = PlayCaps(rawValue: 1 << 8)
    public static let ApplyFeedback = PlayCaps(rawValue: 1 << 9)
    public static let Scrobbling    = PlayCaps(rawValue: 1 << 10)
    public static let AddPreset     = PlayCaps(rawValue: 1 << 11)
    
    public var canPause: Bool { return self.contains(.Pause) }
    public var canStop: Bool { return self.contains(.Stop) }
    public var canSkipNext: Bool { return self.contains(.SkipNext) }
    public var canSkipPrevious: Bool { return self.contains(.SkipPrevious) }
    public var canFastForward: Bool { return self.contains(.FastForward) }
    public var canRewind: Bool { return self.contains(.Rewind) }
    public var canShuffle: Bool { return self.contains(.Shuffle) }
    public var canRepeat: Bool { return self.contains(.Repeat) }
    public var canSeek: Bool { return self.contains(.Seek) }
    public var canApplyFeedback: Bool { return self.contains(.ApplyFeedback) }
    public var canScrobble: Bool { return self.contains(.Scrobbling) }
    public var canAddPreset: Bool { return self.contains(.AddPreset) }
}
