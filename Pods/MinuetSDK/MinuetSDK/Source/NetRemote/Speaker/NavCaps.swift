//
//  PlayCaps.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 11/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation

public struct NavCaps: OptionSet {
    public let rawValue: Int
    
    public init(rawValue: Int) {
        self.rawValue = rawValue
    }
    
    public static let None       = NavCaps(rawValue: 0)
    public static let Navigation = NavCaps(rawValue: 1 << 0)
    public static let Presets    = NavCaps(rawValue: 1 << 1)
    
    public var canNavigate: Bool { return self.contains(.Navigation) }
    public var canListPresets: Bool { return self.contains(.Presets) }
}
