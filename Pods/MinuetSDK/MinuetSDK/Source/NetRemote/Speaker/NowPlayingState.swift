//
//  NowPlayingState.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 09/06/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation

public struct NowPlayingState {
    public var modeIndex: Int?
    public var presetIndex: Int?
    public var playStatus: PlayStatus?
    public var modes: [Mode]
    
    public init(modeIndex: Int?,
                presetIndex: Int?,
                playStatus: PlayStatus?,
                modes: [Mode]) {
        
        self.modeIndex = modeIndex
        self.presetIndex = presetIndex
        self.playStatus = playStatus
        self.modes = modes
    }
    
    public var isIdle: Bool? {
        return playStatus == PlayStatus.idle || playStatus == PlayStatus.stopped || playStatus == PlayStatus.paused || playStatus == PlayStatus.error || currentMode?.isAudsync == true || currentMode?.isStandby == true
    }

    public var currentMode: Mode? {
        if let `modeIndex` = modeIndex {
            return self.modes.filter({$0.key == modeIndex}).first
        }
        return nil
    }
}

extension NowPlayingState {
    static func from(nodeStringValues values: [ScalarNode: String],modes:[Mode]) -> NowPlayingState {
        let modeIndex = values[.Mode] != nil ? Int(values[.Mode]!) : nil
        let presetIndex = values[.NavCurrentPreset] != nil ? Int(values[.NavCurrentPreset]!) : nil
        var playStatus = PlayStatus.idle
        if let playStatusValueString = values[.PlayStatus] {
            if let playStatusValue = Int(playStatusValueString) {
                if let playStatusEnum = PlayStatus(rawValue:playStatusValue) {
                    playStatus = playStatusEnum
                }
            }
        }
        
        return NowPlayingState(
            modeIndex: modeIndex,
            presetIndex: presetIndex != nil && UInt32(presetIndex!) != UInt32.max ? presetIndex : nil,
            playStatus: playStatus,
            modes: modes)
    }
}
