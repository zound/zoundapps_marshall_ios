//
//  ListNode.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 14/07/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
public enum ListNode: String {
    case ValidModes       = "netremote.sys.caps.validmodes"
    case EqPresets        = "netremote.sys.caps.eqpresets"
    case EqBands          = "netremote.sys.caps.eqbands"
    case TimeZones        = "netRemote.sys.caps.utcsettingslist"
    case NavList          = "netremote.nav.list"
    case Presets          = "netremote.nav.presets"
    case MultiroomList    = "netremote.multiroom.device.listall"
    case BluetoothDevices = "netremote.bluetooth.connecteddevices"
    
    var keyAttributeName: String {
        return "key"
    }
}

enum ValidModesField: String {
    case Key        = "key"
    case Id         = "id"
    case Selectable = "selectable"
    case Label      = "label"
    case Streamable = "streamable"
    case ModeType   = "modetype"
}

enum EqPresetsField: String {
    case Key   = "key"
    case Label = "label"
}

enum EqBandsField: String {
    case Key   = "key"
    case Label = "label"
    case Min   = "min"
    case Max   = "max"
}

enum NavListField: String {
    case Key       = "key"
    case Name      = "name"
    case TypeField = "type"
    case SubType   = "subtype"
}

enum PresetsField: String {
    case Key             = "key"
    case Name            = "name"
    case TypeField       = "type"
    case UniqIDField     = "uniqid"
    case BlobField       = "blob"
    case PlaylistUrl     = "playlisturl"
    case ArtworkURLField = "artworkurl"
}

enum BluetoothDeviceField: String {
    case Key   =  "key"
    case State = "devicestate"
    case Name  = "devicename"
}

enum TimeZoneField: String {
    case Key    = "key"
    case Time   = "time"
    case Region = "region"
}

enum MultiroomListField: String {
    case Key              = "key"
    case UDN              = "udn"
    case FriendlyName     = "friendlyname"
    case IPAddress        = "ipaddress"
    case AudioSyncVersion = "audiosyncversion"
    case GroupId          = "groupid"
    case GroupName        = "groupname"
    case GroupRole        = "grouprole"
    case ClientNumber     = "clientnumber"
}
