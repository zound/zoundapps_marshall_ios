//
//  BluetoothDevice.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 19/06/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation

public enum BlueoothDeviceState: Int {
    case disconnected
    case connected
    case paired
}

public struct BluetoothDevice {
    public let key: Int
    public let name: String
    public let state: BlueoothDeviceState
}

extension BluetoothDevice: Equatable {}

public func == (lhs: BluetoothDevice, rhs: BluetoothDevice) -> Bool {
    return lhs.key == rhs.key && lhs.name == rhs.name && lhs.state == rhs.state
}
