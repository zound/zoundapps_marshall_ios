//
//  Preset+XML.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 12/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import SWXMLHash

extension Preset: XMLIndexerDeserializable {
    public static func deserialize(_ node: XMLIndexer) throws -> Preset {

        let fields = node["field"]
        let nameString: String? = try? fields.withAttribute("name", PresetsField.Name.rawValue).children[0].value()
        let typeString: String? = try? fields.withAttribute("name", PresetsField.TypeField.rawValue).children[0].value()
        let imageURLNode = try? fields.withAttribute("name", PresetsField.ArtworkURLField.rawValue).children[0]
        let imageURLString: String? = imageURLNode != nil ? try? imageURLNode!.value() : nil
        let imageURL = imageURLString != nil ? URL(string: imageURLString!) : nil
        let blobString: String? = try? fields.withAttribute("name", PresetsField.BlobField.rawValue).children[0].value()
        let playlistUrlString: String? = try? fields.withAttribute("name", PresetsField.PlaylistUrl.rawValue).children[0].value()
        let keyAttribute = node.element?.allAttributes[ValidModesField.Key.rawValue]
        
        return Preset(
            name: nameString ?? "",
            number: ((Int(keyAttribute!.text))! + 1),
            imageURL: imageURL,
            typeID: typeString,
            blob: blobString,
            playlistUrl: playlistUrlString
        )
    }
}

