//
//  Preset+XML.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 12/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import SWXMLHash

extension MultiroomDevice: XMLIndexerDeserializable {
    public static func deserialize(_ node: XMLIndexer) throws -> MultiroomDevice {
        let fields = node["field"]
        let udnElement =                 try fields.withAttribute("name", MultiroomListField.UDN.rawValue).children[0]
        let friendlyNameElement =        try fields.withAttribute("name", MultiroomListField.FriendlyName.rawValue).children[0]
        let ipAddressElement =           try fields.withAttribute("name", MultiroomListField.IPAddress.rawValue).children[0]
        let audioSyncVersionElement =    try fields.withAttribute("name", MultiroomListField.AudioSyncVersion.rawValue).children[0]
        let groupIdElement =             try fields.withAttribute("name", MultiroomListField.GroupId.rawValue).children[0]
        let groupNameElement =           try fields.withAttribute("name", MultiroomListField.GroupName.rawValue).children[0]
        let groupRoleElement =           try fields.withAttribute("name", MultiroomListField.GroupRole.rawValue).children[0]
        let clientNumberString: String = try fields.withAttribute("name", MultiroomListField.ClientNumber.rawValue).children[0].value()
        let keyAttributeElement = node.element?.allAttributes[MultiroomListField.Key.rawValue]
        
        let groupRole = GroupRole.from(groupRoleString: try? groupRoleElement.value())
        
        return try MultiroomDevice(udn: udnElement.value(),
                                   friendlyName: friendlyNameElement.value(),
                                   ipAddress: ipAddressElement.value(),
                                   audioSyncVersion: audioSyncVersionElement.value(),
                                   groupId: (groupIdElement.value() == "" ? nil : groupIdElement.value()),
                                   groupName: (groupNameElement.value() == "" ? nil : groupNameElement.value()),
                                   groupRole: groupRole,
                                   clientNumber: clientNumberString != "" ? Int(clientNumberString) : nil,
                                   key: Int(keyAttributeElement!.text)!
        )
    }
}

