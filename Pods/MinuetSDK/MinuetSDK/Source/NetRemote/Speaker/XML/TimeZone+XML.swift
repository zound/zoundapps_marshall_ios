//
//  BluetoothDevice+XML.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 19/06/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import SWXMLHash

extension TimeZone: XMLIndexerDeserializable {
    public static func deserialize(_ node: XMLIndexer) throws -> TimeZone {
        let fields = node["field"]
        let offset: String =  try fields.withAttribute("name", TimeZoneField.Time.rawValue).children[0].value()
        let region: String =  try fields.withAttribute("name", TimeZoneField.Region.rawValue).children[0].value()
        let keyAttribute = node.element?.allAttributes[TimeZoneField.Key.rawValue]
        
        return TimeZone(key: Int(keyAttribute!.text) ?? 0,
                        name: region,
                        offset: Int(offset) ?? 0)
    }
}
