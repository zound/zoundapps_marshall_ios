//
//  Notification+XML.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 25/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import SWXMLHash

extension Notification: XMLIndexerDeserializable {
    public static func deserialize(_ node: XMLIndexer) throws -> Notification {
        let nodeNameArribute = node.element!.allAttributes["node"]!
        return try Notification(
            node: nodeNameArribute.text,
            value: node["value"].children[0].value(),
            type: node["value"].children[0].element!.name
        )
    }
}
