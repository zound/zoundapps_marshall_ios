//
//  EqPreset+XML.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 18/05/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import SWXMLHash

extension EqPreset: XMLIndexerDeserializable {
    public static func deserialize(_ node: XMLIndexer) throws -> EqPreset {
        let fields = node["field"]
        let name: String = try fields.withAttribute("name", EqPresetsField.Label.rawValue).children[0].value()
        let keyAttribute = node.element?.allAttributes[ValidModesField.Key.rawValue]
        
        return EqPreset(
            key: Int(keyAttribute!.text) ?? 0,
            name: name
        )
    }
}
