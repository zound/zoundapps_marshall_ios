//
//  BluetoothDevice+XML.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 19/06/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import SWXMLHash

extension BluetoothDevice: XMLIndexerDeserializable {
    public static func deserialize(_ node: XMLIndexer) throws -> BluetoothDevice {
        let fields = node["field"]
        let name: String =        try fields.withAttribute("name", BluetoothDeviceField.Name.rawValue).children[0].value()
        let stateString: String = try fields.withAttribute("name", BluetoothDeviceField.State.rawValue).children[0].value()
        let keyAttribute = node.element?.allAttributes[ValidModesField.Key.rawValue]
        
        var stateValue = BlueoothDeviceState.disconnected
        if let stateRawValue = Int(stateString) {
            if let deviceState = BlueoothDeviceState(rawValue: stateRawValue) {
                stateValue = deviceState
            }
        }
        
        return BluetoothDevice(
            key: Int(keyAttribute!.text) ?? 0,
            name: name,
            state: stateValue
        )
    }
}
