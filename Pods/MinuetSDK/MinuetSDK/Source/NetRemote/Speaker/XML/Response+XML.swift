//
//  Response+XML.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 25/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//
import RxSwift
import Moya
import SWXMLHash

extension ObservableType where E == Response {
    func mapFSAPIResponse<T: XMLIndexerDeserializable>() -> Observable<T> {
        return flatMap { response -> Observable<T> in
            return Observable.just(try response.mapFSAPIResponse())
        }
    }
}

extension Response {
    /// Maps data received from the signal into a JSON object.
    func mapFSAPIResponse<T: XMLIndexerDeserializable>() throws -> T {
        if(statusCode == 200) {
            //let string = String(data: data, encoding: String.Encoding.utf8)
            //NSLog(string!)
            let xml = SWXMLHash.config { config in
                config.shouldProcessNamespaces = false
                config.shouldProcessLazily = false
                }.parse(data)
            
            if let fsapiResponseNode = try? xml.byKey("fsapiResponse") {
                let fsapiResponse : T = try fsapiResponseNode.value()
                return fsapiResponse
                
            } else if let fsapiReponseNode = try? xml.byKey("fsapiGetMultipleResponse") {
                
                let fsapiResponse : T = try fsapiReponseNode.value()
                return fsapiResponse
            }  else if let fsapiReponseNode = try? xml.byKey("fsapiSetMultipleResponse") {
                
                let fsapiResponse : T = try fsapiReponseNode.value()
                return fsapiResponse
            }
            
            throw NetRemoteResponseError.rootNodeNodeFound
        } else if (statusCode == 403) {
            throw NetRemoteResponseError.invalidPIN
        }  else if (statusCode == 404) {
            throw NetRemoteResponseError.sessionLost
        }
        
        throw NetRemoteResponseError.unidentified
    }
}

enum NetRemoteResponseError: Swift.Error {
    case rootNodeNodeFound
    case sessionLost
    case invalidPIN
    case unidentified
}
