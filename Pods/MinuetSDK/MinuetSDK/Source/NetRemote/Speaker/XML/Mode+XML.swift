//
//  Mode+XML.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 25/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import SWXMLHash
extension Mode: XMLIndexerDeserializable {
    public static func deserialize(_ node: XMLIndexer) throws -> Mode {
        let fields = node["field"]
        let id =         try fields.withAttribute("name", ValidModesField.Id.rawValue).children[0]
        let label =      try fields.withAttribute("name", ValidModesField.Label.rawValue).children[0]
        let modeType =   try fields.withAttribute("name", ValidModesField.ModeType.rawValue).children[0]
        let streamable = try fields.withAttribute("name", ValidModesField.Streamable.rawValue).children[0]
        let selectable = try fields.withAttribute("name", ValidModesField.Selectable.rawValue).children[0]
        let keyAttribute = node.element?.allAttributes[ValidModesField.Key.rawValue]
        
        return try Mode(
            key: (Int(keyAttribute!.text))!,
            id: id.value(),
            selectable: selectable.element?.text == "1",
            label: label.value(),
            streamable:  streamable.element?.text == "1",
            modeType: modeType.value()
        )
    }
}
