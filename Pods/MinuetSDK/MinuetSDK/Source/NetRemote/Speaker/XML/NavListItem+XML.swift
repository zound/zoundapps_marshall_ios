//
//  Preset+XML.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 12/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import SWXMLHash

extension NavListItem: XMLIndexerDeserializable {
    public static func deserialize(_ node: XMLIndexer) throws -> NavListItem {
        let fields = node["field"]
        let name:String =     try fields.withAttribute("name", NavListField.Name.rawValue).children[0].value()
        let type:String =     try fields.withAttribute("name", NavListField.TypeField.rawValue).children[0].value()
        let subtype:String =  try fields.withAttribute("name", NavListField.SubType.rawValue).children[0].value()
        let keyAttribute = node.element?.allAttributes[ValidModesField.Key.rawValue]
        
        let typeInt = Int(type) ?? NavListType.unknown.rawValue
        let subTypeInt = Int(subtype) ?? NavListSubType.none.rawValue
        let key = UInt32(keyAttribute!.text)
        
        return NavListItem(
            name: name,
            type: NavListType(rawValue: typeInt) ?? NavListType.unknown,
            subtype: NavListSubType(rawValue: subTypeInt) ?? NavListSubType.none,
            key: key ?? 0
        )
    }
}

