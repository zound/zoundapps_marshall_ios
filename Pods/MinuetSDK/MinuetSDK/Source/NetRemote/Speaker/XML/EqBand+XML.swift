//
//  EqBand+XML.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 18/05/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import SWXMLHash

extension EqBand: XMLIndexerDeserializable {
    public static func deserialize(_ node: XMLIndexer) throws -> EqBand {
        let fields = node["field"]
        let name: String = try fields.withAttribute("name", EqBandsField.Label.rawValue).children[0].value()
        let min: Int =     try fields.withAttribute("name", EqBandsField.Min.rawValue).children[0].value()
        let max: Int =     try fields.withAttribute("name", EqBandsField.Max.rawValue).children[0].value()
        let keyAttribute = node.element?.allAttributes[ValidModesField.Key.rawValue]
        
        return EqBand(
            key: Int(keyAttribute!.text) ?? 0,
            name: name,
            min: min,
            max: max
        )
    }
}
