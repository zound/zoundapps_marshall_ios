//
//  AboutState.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 19/05/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation

public struct TimeZoneState {
    public let timeZones: [TimeZone]
    public let currentTimeZoneName: String
}
