//
//  NetRemoteService.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 24/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import Moya
import Alamofire

enum NetRemoteService {
    case getNode(node:ScalarNode, speaker: Speaker)
    case getNodes(nodes: [ScalarNode], speaker: Speaker)
    case setNode(node: ScalarNode, value: String, speaker: Speaker)
    case setNodes(nodes: [ScalarNode:String], speaker: Speaker)
    case getList(list:ListNode, startIndex:Int, maxCount:Int, speaker: Speaker)
    case getNotifications(sessionId: String, speaker: Speaker)
    case createSession(speaker: Speaker)
    case deleteSession(speaker: Speaker)
}

extension NetRemoteService:TargetType {
    var headers: [String : String]? {
        return nil
    }
    
    public var task: Task {
        if let parameters = self.parameters {
            return Task.requestParameters(parameters: parameters, encoding: NetRemoteParameterEncoding())
        } else {
            return Task.requestPlain
        }
    }
    
    fileprivate func urlForSpeaker(_ speaker: Speaker) -> URL {
        return URL(string: "http://"+(speaker.ipAddress)+"/"+"fsapi")!
    }
    
    var baseURL: URL {
        switch self {
        case .getNode(_, let speaker): return urlForSpeaker(speaker)
        case .getNodes(_, let speaker): return urlForSpeaker(speaker)
        case .setNode(_,_, let speaker): return urlForSpeaker(speaker)
        case .setNodes(_, let speaker): return urlForSpeaker(speaker)
        case .getList(_,_,_, let speaker): return urlForSpeaker(speaker)
        case .getNotifications(_, let speaker): return urlForSpeaker(speaker)
        case .createSession(let speaker): return urlForSpeaker(speaker)
        case .deleteSession(let speaker): return urlForSpeaker(speaker)
        }
    }
    var path: String {
        switch self {
        case .getNode(let node, _): return "/GET/"+node.rawValue
        case .getNodes(_, _): return "/GET_MULTIPLE"
        case .setNode(let node, _, _ ): return "/SET/" + node.rawValue
        case .setNodes(_, _ ): return "/SET_MULTIPLE"
        case .getList(let listNode,let startIndex,_, _): return "/LIST_GET_NEXT/\(listNode.rawValue)/\(startIndex)" 
        case .getNotifications(_,_): return "/GET_NOTIFIES"
        case .createSession(_): return "/CREATE_SESSION"
        case .deleteSession(_): return "/DELETE_SESSION"
        }
    }
    
    var method: Moya.Method {
        return .get
    }
    
    var parameters: [String: Any]? {
        switch self {
        case .getNode(_, let speaker):
            return ["pin":(speaker.pinCode as AnyObject)]
            
        case .setNode(_,let value,  let speaker):
            return ["pin":(speaker.pinCode as AnyObject),
                    "value":value as AnyObject]
            
        case .getNodes(let nodes, let speaker):
            var parameters: [String: AnyObject] = ["pin":(speaker.pinCode as AnyObject)]
            var i = 0
            parameters = nodes.reduce(parameters, {(dict, e) in
                var dictCopy = dict
                dictCopy["node\(i)"] = e.rawValue as AnyObject?
                i = i + 1
                return dictCopy
            })
            return parameters
            
        case .setNodes(let nodes, let speaker):
            var parameters: [String: AnyObject] = ["pin":(speaker.pinCode as AnyObject)]
            var i = 0
            parameters = nodes.reduce(parameters, {(dict, node) in
                var dictCopy = dict
                dictCopy["node\(i)"] = node.0.rawValue as AnyObject?
                dictCopy["node\(i)value"] = node.1 as AnyObject?
                i = i + 1
                return dictCopy
            })
            return parameters
            
        case .getList(_, _, let maxItems, let speaker):
            return ["pin":speaker.pinCode as AnyObject,
                    "maxItems":String(maxItems) as AnyObject]
            
        case .getNotifications(let sessionId, let speaker):
            return ["pin":speaker.pinCode as AnyObject,
                    "sid":sessionId as AnyObject]
            
        case .createSession(let speaker):
            return ["pin":speaker.pinCode as AnyObject]
            
        case .deleteSession(let speaker):
            return ["pin":speaker.pinCode as AnyObject]
        }
    }
    
    var sampleData: Data {
        let playCaps:PlayCaps = [PlayCaps.Pause, PlayCaps.Shuffle, PlayCaps.Repeat]
        let sampleNodes: [ScalarNode:String] = [
            .Power: "1",
            .Mode:"0",
            .Volume:"5",
            .VolumeSteps:"33",
            .Mute:"0",
            .PlayCaps: String(playCaps.rawValue),
            .PlayStatus: String(PlayStatus.paused.rawValue),
            .PlayName: "Bohemian Rhapsody",
            .PlayText: "Bayern 1",
            .PlayArtist: "Queen",
            .PlayAlbum: "Best of Queen",
            .PlayGraphicUri: "",
            .PlayDuration: "0",
            .PlayPosition: "0",
            .PlayShuffle: "1",
            .PlayRepeat: "1"
        ]
        
        switch self {
        case .setNode(_, _, _):
            return "<fsapiResponse><status>FS_OK</status></fsapiResponse>".data(using: String.Encoding.utf8)!
            
        case .getNode(let node, _):
            let value = sampleNodes[node]
            return "<fsapiResponse><status>FS_OK</status><value><c8_array>\(value ?? "")</c8_array></value></fsapiResponse>".data(using: String.Encoding.utf8)!
            
        case .getNodes(let nodes, _):
            let nodesStrings = NSMutableString()
            for node in nodes {
                let value = sampleNodes[node]
                let nodeResponse = "<fsapiResponse>" +
                    "<node>\(node.rawValue)</node>" +
                    "<status>FS_OK</status>" +
                    "<value><c8_array>\(value!)</c8_array></value>" +
                "</fsapiResponse>"
                nodesStrings.append(nodeResponse)
            }
            return "<fsapiGetMultipleResponse>\(nodesStrings)</fsapiGetMultipleResponse>".data(using: String.Encoding.utf8)!
            
        case .getList(let listNode, _, _, _):
            let bundle = Bundle.main
            let pathForFile = bundle.path(forResource: listNode.rawValue, ofType: "xml")
            let xmlString = try? NSString(contentsOfFile: pathForFile!, encoding: String.Encoding.utf8.rawValue)
            return xmlString!.data(using: String.Encoding.utf8.rawValue)!
            
        case .getNotifications(_,_):
            return "<fsapiResponse><status>FS_TIMEOUT</status></fsapiResponse>".data(using: String.Encoding.utf8)!
            
        case .createSession(_):
            return "<fsapiResponse><status>FS_OK</status><sessionId>868212735d</sessionId></fsapiResponse>".data(using: String.Encoding.utf8)!
            
        default: return Data()
        }
    }
}

struct NetRemoteParameterEncoding: ParameterEncoding {
    func encode(_ urlRequest: URLRequestConvertible, with parameters: Parameters?) throws -> URLRequest {
        var mutableURLRequest = urlRequest.urlRequest
        guard let parameters = parameters else { return urlRequest.urlRequest! }
        var encodingError: NSError? = nil
        
        func query(parameters: Parameters?) -> String {
            if let parameters = parameters {
                var components: [(String, String)] = []
                
                if let pin = parameters["pin"] {
                    components += [("pin", "\(pin)")]
                }
                
                for key in parameters.keys.sorted(by:<) {
                    let valueString = parameters[key]! as! String
                    let value = valueString.stringByAddingPercentEncodingForURLQueryParameter()!
                    
                    if key.hasPrefix("node") {
                        if key.hasSuffix("value") {
                            components += [("value", "\(value)")]
                        } else  {
                            components += [("node", "\(value)")]
                        }
                    } else if key != "pin" {
                        components += [(key, "\(value)")]
                    }
                }
                
                return (components.map { "\($0)=\($1)" } as [String]).joined(separator: "&")
            }
            return ""
        }
        
        if parameters.isEmpty == false {
            if var URLComponents = URLComponents(url: mutableURLRequest!.url!, resolvingAgainstBaseURL: false)
            {
                let percentEncodedQuery = (URLComponents.percentEncodedQuery.map { $0 + "&" } ?? "") + query(parameters:parameters)
                URLComponents.percentEncodedQuery = percentEncodedQuery
                mutableURLRequest?.url = URLComponents.url
            }
        }
        return mutableURLRequest!
    }
}

extension NetRemoteService {
    static var stubClosure = { (target: NetRemoteService) -> Moya.StubBehavior in
        return Moya.StubBehavior.never
    }
    
    static var mockStubClosure = { (target: NetRemoteService) -> Moya.StubBehavior in
        switch target {
        case .setNode(_, _, _): return StubBehavior.delayed(seconds: 0)
        case .getNode(_, _): return StubBehavior.delayed(seconds: 0)
        case .getNodes(_, _): return StubBehavior.delayed(seconds: 0)
        case .getList(_,_,_,_): return StubBehavior.delayed(seconds: 0)
        case .getNotifications(_,_): return StubBehavior.delayed(seconds: 30)
        case .createSession(_): return StubBehavior.delayed(seconds: 0)
        default: return Moya.StubBehavior.never
        }
    }
}
