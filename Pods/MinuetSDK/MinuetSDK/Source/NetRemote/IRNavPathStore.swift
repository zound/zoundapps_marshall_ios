//
//  SpotifyTokenStore.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 20/05/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import ObjectMapper

public class IRNavPathStore {
    public static func getNavPath() -> [UInt32] {
        if let navPath = minuetSDKUserDefaults?.object(forKey: "irNavPath") as? [UInt32] {
            return navPath
        }
        return []
    }
    
    public static func storeNavPath(_ navPath: [UInt32]) {
        minuetSDKUserDefaults?.set(navPath, forKey: "irNavPath")
    }
    
    public static func clearNavPath() {
        minuetSDKUserDefaults?.removeObject(forKey: "irNavPath")
    }
}
