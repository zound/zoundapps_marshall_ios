//
//  NetRemoteServiceResponses.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 25/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import SWXMLHash

enum FSAPIStatus {
    case ok
    case listEnd
    case error
    case timeout
}

struct FSAPIGetNodeResponse: XMLIndexerDeserializable {
    let status: String
    let value: String?
    let type: String?
    
    static func deserialize(_ node: XMLIndexer) throws -> FSAPIGetNodeResponse {
        var value: String?
        var type: String?
        
        if let valueNode = try? node.byKey("value") {
            value = try? valueNode.children[0].value()
            type = valueNode.children[0].element!.name
        }
        
        return try FSAPIGetNodeResponse(
            status: node["status"].value(),
            value: value,
            type: type
        )
    }
}

struct FSAPIGetMultipleNodeResponse: XMLIndexerDeserializable {
    let responses: [FSAPIGetNodeResponse]
    
    static func deserialize(_ node: XMLIndexer) throws -> FSAPIGetMultipleNodeResponse {
        return try FSAPIGetMultipleNodeResponse(
            responses: node["fsapiResponse"].value()
        )
    }
}

struct FSAPISetNodeResponse: XMLIndexerDeserializable {
    let status: String
    let node: String?
    
    static func deserialize(_ nodeIndexer: XMLIndexer) throws -> FSAPISetNodeResponse {
        return try FSAPISetNodeResponse(
            status: nodeIndexer["status"].value(),
            node: try? nodeIndexer["node"].value()
        )
    }
}

struct FSAPISetMultipleNodeResponse: XMLIndexerDeserializable {
    let responses: [FSAPISetNodeResponse]
    
    static func deserialize(_ node: XMLIndexer) throws -> FSAPISetMultipleNodeResponse {
        return try FSAPISetMultipleNodeResponse(
            responses: node["fsapiResponse"].value()
        )
    }
}

struct FSAPIGetListNodeResponse<T:XMLIndexerDeserializable>: XMLIndexerDeserializable {
    let status: String
    let items: [T]?
    let listEnd: Bool
    let maxKey: Int
    
    static func deserialize(_ node: XMLIndexer) throws -> FSAPIGetListNodeResponse {
        let listEnd: String? = try? node["listend"].value()
        var maxKeyInt: Int = 0
        if let maxKeyString = node["item"].all.last?.element?.attribute(by: "key")?.text {
            if let maxKey = Int(maxKeyString) {
                maxKeyInt = maxKey
            }
        }
        
        return try FSAPIGetListNodeResponse(
            status: node["status"].value(),
            items: try? node["item"].value(),
            listEnd: listEnd != nil,
            maxKey: maxKeyInt
        )
    }
}

struct FSAPIGetNotificationsResponse: XMLIndexerDeserializable {
    let status: String
    let notifications: [Notification]
    
    static func deserialize(_ node: XMLIndexer) throws -> FSAPIGetNotificationsResponse {
        
        var notifications: [Notification]?
        if let notificationsNode = try? node.byKey("notify") {
            notifications = try? notificationsNode.value()
        }
        
        return try FSAPIGetNotificationsResponse(
            status: node["status"].value(),
            notifications: notifications ?? []
        )
    }
}

struct FSAPICreateSessionResponse: XMLIndexerDeserializable {
    let status: String
    let sessionId: String?
    
    static func deserialize(_ node: XMLIndexer) throws -> FSAPICreateSessionResponse {
        return try FSAPICreateSessionResponse(
            status: node["status"].value(),
            sessionId: try node["sessionId"].value()
        )
    }
}

struct FSAPIDeleteSessionResponse: XMLIndexerDeserializable {
    let status: String
    
    static func deserialize(_ node: XMLIndexer) throws -> FSAPIDeleteSessionResponse {
        return try FSAPIDeleteSessionResponse(
            status: node["status"].value()
        )
    }
}
