//
//  GroupRole.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 04/05/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation

public enum GroupRole: Int {
    case none = 0
    case client
    case server
}

public extension GroupRole {
    public static func from(groupState: GroupState?) -> GroupRole {
        if let groupState = groupState {
            switch groupState {
                case .client: return GroupRole.client
                case .server: return GroupRole.server
                case .solo: return GroupRole.none
            }
        }
        else {
            return GroupRole.none
        }
    }
    
    public static func from(groupRoleString: String?) -> GroupRole {
        if let groupRoleString = groupRoleString {
            let groupRoleRawValue = Int(groupRoleString)!
            var groupRole = GroupRole.none
            
            if groupRoleRawValue == 0 {
                groupRole = GroupRole.none
            } else if groupRoleRawValue == 1 {
                groupRole = GroupRole.client
            } else if groupRoleRawValue  == 2 {
                groupRole = GroupRole.server
            }
            return groupRole
        }
        else {
            return GroupRole.none
        }
    }
}
