//
//  AudioSystem.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 04/05/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import RxSwift
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}

struct StreamingQualityConstants {
    static let key = "networkOptimizationSetting"
}

public enum GroupRetrievalError: Error {
    case cannotRetrieveGroupInfo(speaker: Speaker)
    case allSpeakersFailed
}

public protocol AudioSystemDelegate: class {
    func audioSystemWillMakeChanges()
    func audioSystemDidAddGroup(_ group: SpeakerGroup, fromMove: Bool)
    func audioSystemDidAddSpeaker(_ speaker: Speaker, toGroup group:SpeakerGroup, fromMove: Bool)
    func audioSystemDidRemoveGroup(_ group: SpeakerGroup, fromMove: Bool)
    func audioSystemDidRemoveSpeaker(_ speaker: Speaker, fromGroup group: SpeakerGroup, fromMove: Bool)
    func audioSystemDidMakeChanges()
    func audioSystemDidUpdateMasterTo(newMaster: Speaker?, inGroup group: SpeakerGroup)
    
    var delegateIdentifier: String { get }
}

public extension AudioSystemDelegate {
    func audioSystemWillMakeChanges() {}
    func audioSystemDidAddGroup(_ group: SpeakerGroup, fromMove: Bool) {}
    func audioSystemDidAddSpeaker(_ speaker: Speaker, toGroup group:SpeakerGroup, fromMove: Bool) {}
    func audioSystemDidRemoveGroup(_ group: SpeakerGroup, fromMove: Bool) {}
    func audioSystemDidRemoveSpeaker(_ speaker: Speaker, fromGroup group: SpeakerGroup, fromMove: Bool) {}
    func audioSystemDidMakeChanges() {}
    func audioSystemDidUpdateMasterTo(newMaster: Speaker?, inGroup group: SpeakerGroup) {}
}

public class AudioSystem: RunAfterDelay {
    typealias SpeakersAndMultiroomDevices = (speakers: [Speaker], multiroomDevices: [MultiroomDevice])
    
    var disposeBag = DisposeBag()
    public let groups: Variable<[SpeakerGroup]> = Variable([])
    public let speakers: Variable<[Speaker]> = Variable([])
    public var volumeInfo: Variable<[Speaker: SpeakerVolume]> = Variable([:])
    public var multiroomInfo: Variable<[Speaker: MultiroomState]> = Variable([:])
    public var nowPlayingInfo: Variable<[Speaker: NowPlayingState]> = Variable([:])
    public var unlockedInfo: Variable<[Speaker: Bool]> = Variable([:])
    public var updateAvailableInfo : Variable<[Speaker: Bool]> = Variable([:])
    public let discoveryService: DiscoveryServiceType
    
    let provider: SpeakerProvider
    var lastSpeakersAndMultiroomDevices: SpeakersAndMultiroomDevices?
    var groupUpdatesDisposable: Disposable?
    let refreshTrigger: PublishSubject<Bool> = PublishSubject()
    
    fileprivate var delegates: Set<Weak> = Set<Weak>()
    
    public var groupUpdatesActive: Bool {
        get {
            return groupUpdatesDisposable != nil
        }
        set {
            if newValue && groupUpdatesDisposable == nil {
                let groups = refreshTrigger.startWith(true)
                    .flatMapLatest { [weak self] _ -> Observable<SpeakersAndMultiroomDevices> in
                        guard let `self` = self else { return Observable.just(SpeakersAndMultiroomDevices(speakers: [], multiroomDevices:[])) }
                        return self.groupsObservable()
                }
                
                let multiroomInfo = self.multiroomInfo
                groupUpdatesDisposable = groups
                    .do(onNext: { [weak self] speakersAndMultiroomDevices in
                        guard let `self` = self else { return }
                        self.lastSpeakersAndMultiroomDevices = speakersAndMultiroomDevices
                    })
                    .map { groupsFromSpeakers($0.speakers, multiroomDevices: $0.multiroomDevices,multiroomInfo: multiroomInfo) }
                    .subscribe(weak: self, onNext: AudioSystem.updateWith)
                
                groupUpdatesDisposable?
                    .disposed(by: disposeBag)
            } else {
                groupUpdatesDisposable?
                    .dispose()
                
                groupUpdatesDisposable = nil
            }
        }
    }
    
    public var masterSpeaker: Speaker? {
        get {
            return speakers.value.filter { speaker in
                guard let multiroomInfo = self.multiroomInfo.value[speaker] else { return false }
                return multiroomInfo.groupState == .server
                }.first
        }
    }
    
    public init(discoveryService: DiscoveryServiceType, provider: SpeakerProvider) {
        self.discoveryService = discoveryService
        self.provider = provider
        
        discoveryService.addDelegate(self)
    }
    
    public func setMultiroomOn(_ multiroomOn: Bool, forSpeaker speaker: Speaker) {
        // Create new dispose bag so the old one may be deallocated
        disposeBag = DisposeBag()
        
        provider.setNode(ScalarNode.MultiroomSingleGroupState, value: multiroomOn ? "1" : "0", forSpeaker: speaker)
            .subscribe()
            .disposed(by: disposeBag)
        
        runAfterDelay(0.5) { [weak self] in
            guard let `self` = self else {return}
            self.groupUpdatesActive = false
            self.groupUpdatesActive = true
        }
        
        runAfterDelay(1.5) { [weak self] in
            guard let `self` = self else {return}
            self.groupUpdatesActive = false
            self.groupUpdatesActive = true
            
            // We don't want setting optimization flag when speaker is removed from the multi group
            guard multiroomOn else { return }
            guard let masterSpeaker = self.masterSpeaker else { return }
            
            guard speaker != masterSpeaker else {
                // Master speaker added. In this case we need:
                // 1. Get this speaker optimization level
                // 2. Store optimization level in multiroom info
                
                self.provider.getNode(ScalarNode.MultiroomNetworkOptimization, forSpeaker: masterSpeaker)
                    .timeout(5.0, scheduler: MainScheduler.instance)
                    .map { $0 == "1" }
                    .subscribe(onNext: { optimizationSetting in
                        guard var multiroomInfo = self.multiroomInfo.value[masterSpeaker] else { return }
                        multiroomInfo.networkOptimization = optimizationSetting
                        self.multiroomInfo.value[masterSpeaker] = multiroomInfo })
                    .disposed(by: self.disposeBag)
                return
            }
            // Check if speaker is contained in multiroom group, should always return true
            guard let group = self.speakersInMultiroom(master: masterSpeaker) else { return }
            guard group.contains(speaker) else { return }
            
            // We should have master's optimization setting available at this point
            guard let optimization = self.multiroomInfo.value[masterSpeaker]?.networkOptimization else { return }
            self.networkOptimization(enabled: optimization, for: speaker)
        }
    }
    
    public func speakersInMultiroom(master speaker: Speaker) -> [Speaker]? {
        return groups.value.find { $0.speakers.contains(speaker) }.map { $0.speakers }
    }
    
    // Set optimization for all speakers on a list
    public func networkOptimization(enabled flag: Bool) {
        let allSpeakers = self.speakers.value
        allSpeakers.forEach {
            networkOptimization(enabled: flag, for: $0)
        }
    }
    
    // Set optimization for all speakers in group
    public func networkOptimizationInMultiRoom(enabled flag: Bool) {
        guard let master = masterSpeaker else { return }
        guard let multiroomSpeakers = speakersInMultiroom(master: master) else { return }
        
        provider.setNetworkOptimizationForSpeakers(multiroomSpeakers, value: flag ? "1" : "0")
            .subscribe(onNext: { success in
                guard success else { return }
                multiroomSpeakers.forEach { [unowned self] speaker in
                    guard var multiroomInfo = self.multiroomInfo.value[speaker] else { return }
                    multiroomInfo.networkOptimization = flag
                    self.multiroomInfo.value[speaker] = multiroomInfo
                }
            })
            .disposed(by: disposeBag)
    }
    
    // Set optimization level for selected speaker
    public func networkOptimization(enabled flag: Bool, for speaker: Speaker) {
        provider.setNetworkOptimizationForSpeakers([speaker], value: flag ? "1" : "0").subscribe(onNext: { success in
            guard success else { return }
            guard var multiroomInfo = self.multiroomInfo.value[speaker] else { return }
            multiroomInfo.networkOptimization = flag
            self.multiroomInfo.value[speaker] = multiroomInfo
        }).disposed(by: disposeBag)
    }
    
    
    func groupsObservable() -> Observable<SpeakersAndMultiroomDevices> {
        let groups =  speakers.asObservable().flatMapLatest{ speakers -> Observable<SpeakersAndMultiroomDevices> in
            var failedSpeakers: [Speaker] = []
            let speakerToQuery:Observable<Speaker?> = Observable<Speaker?>.create { observer in
                let compositeDisposable = CompositeDisposable()
                if speakers.count > 0 && failedSpeakers.count == speakers.count {
                    observer.onError(GroupRetrievalError.allSpeakersFailed)
                }
                if let speaker = speakers.find( {$0.hasMultiroom && !failedSpeakers.contains($0)} ) {
                    observer.onNext(speaker)
                    observer.onCompleted()
                } else {
                    observer.onNext(nil)
                    observer.onCompleted()
                }
                return compositeDisposable
            }
            
            return speakerToQuery
                .flatMapLatest { [weak self] speaker -> Observable<SpeakersAndMultiroomDevices> in
                    guard let `self` = self else { return Observable.empty() }
                    if let `speaker` = speaker {
                        return self.provider.getDeviceListAllUpdatesWithPollingInterval(5.0, speaker: speaker)
                            .map { SpeakersAndMultiroomDevices(speakers: speakers, multiroomDevices: $0)}
                            .catchError { error in
                                NSLog("fetch multiroom errror: \(error)")
                                return Observable.error(GroupRetrievalError.cannotRetrieveGroupInfo(speaker:speaker))
                        }
                    }
                    return Observable.just(SpeakersAndMultiroomDevices(speakers: speakers, multiroomDevices:[]))
                }
                .do(onNext: { [weak self] speakersAndMultiroomDevices in
                    guard let `self` = self else { return }
                    let multiroomDevices = speakersAndMultiroomDevices.multiroomDevices
                    for device in multiroomDevices {
                        if self.discoveryService.speakers.find({ $0.ipAddress == device.ipAddress}) == nil {
                            //NSLog("confirming device from MR hint")
                            self.discoveryService.confirmDeviceAtIP(device.ipAddress)
                        }
                    }
                })
                .do(onError: { error in
                    let groupError = error as! GroupRetrievalError
                    switch groupError {
                    case .cannotRetrieveGroupInfo(let speaker):
                        NSLog("error while retrieving MR info from \(speaker.friendlyName) - error: \(error)")
                        failedSpeakers.append(speaker)
                    case .allSpeakersFailed:
                        NSLog("all speakers discovered failed their requests")
                        failedSpeakers.removeAll()
                    }
                }).retry()
            
        }
        
        let initialGroups = speakers.asObservable().map{ SpeakersAndMultiroomDevices(speakers: $0, multiroomDevices: []) }
        
        return Observable.combineLatest(initialGroups,groups) { initialGroups, groups in
            if groups.multiroomDevices.count == 0 {
                return initialGroups
            }
            return groups
        }
    }
    
    func updateWith(speakerGroups: [SpeakerGroup]) {
        //NSLog("speaker groups: \(speakerGroups.map { $0.groupName })")
        let newSpeakerGroups = speakerGroups
        let oldSpeakerGroups = self.groups.value
        
        let newGroups = Set(newSpeakerGroups)
        let oldGroups = Set(oldSpeakerGroups)
        let commonGroups = newGroups.intersection(oldGroups)
        let addedGroups = newGroups.subtracting(commonGroups)
        let removedGroups = oldGroups.subtracting(commonGroups)
        
        self.notifyDelegatesAboutWillMakeChanges()
        
        for removedGroup in removedGroups {
            self.groups.value.removeObject(removedGroup)
            //check if new groups countain the speakers from the removed group
            let fromMove = newGroups.filter({$0.speakers.contains(where: {removedGroup.speakers.contains($0)})}).count > 0
            self.notifyDelegatesAboutDidRemoveGroup(removedGroup, fromMove:  fromMove)
        }
        
        for addedGroup in addedGroups {
            self.groups.value.append(addedGroup)
            //check if old groups contain the speakers in the new group
            let fromMove = oldGroups.filter({$0.speakers.contains(where: {addedGroup.speakers.contains($0)})}).count > 0
            self.notifyDelegatesAboutDidAddGroup(addedGroup, fromMove:  fromMove)
        }
        
        for group in commonGroups {
            if let oldMaintaintedGroup = oldSpeakerGroups.find({$0.groupId == group.groupId}) {
                if let newMaintainedGroup = newSpeakerGroups.find({$0.groupId == group.groupId}) {
                    self.groups.value.removeObject(oldMaintaintedGroup)
                    self.groups.value.append(newMaintainedGroup)
                    
                    if oldMaintaintedGroup.groupName != newMaintainedGroup.groupName {
                        self.notifyDelegatesAboutDidRemoveGroup(oldMaintaintedGroup, fromMove: false)
                        self.notifyDelegatesAboutDidAddGroup(newMaintainedGroup, fromMove: false)
                    } else {
                        let oldSpeakers = Set(oldMaintaintedGroup.speakers)
                        let newSpeakers = Set(newMaintainedGroup.speakers)
                        let commonSpeakers = oldSpeakers.intersection(newSpeakers)
                        let addedSpeakers = newSpeakers.subtracting(commonSpeakers)
                        let removedSpeakers = oldSpeakers.subtracting(commonSpeakers)
                        
                        //we ar removing in reverse alphabetical order to avoid missing indexes
                        for speaker in removedSpeakers.sorted(by: { s1, s2 in s1.sortName.lowercased() > s2.friendlyName.lowercased()}) {
                            //check if any new groups contain the speaker - then it's being moved
                            let fromMove = newGroups.filter({$0.speakers.contains(speaker)}).count > 0
                            self.notifyDelegatesAboutDidRemoveSpeaker(speaker, fromGroup: newMaintainedGroup, fromMove:  fromMove)
                        }
                        
                        //we ar removing in alphabetical order to avoid missing indexes from random order
                        for speaker in addedSpeakers.sorted(by: { s1, s2 in s1.sortName.lowercased() < s2.friendlyName.lowercased()}) {
                            //check if any old groups contain the speaker - then it's being moved
                            let fromMove = oldGroups.filter({$0.speakers.contains(speaker)}).count > 0
                            self.notifyDelegatesAboutDidAddSpeaker(speaker, toGroup: newMaintainedGroup, fromMove:  fromMove)
                        }
                        
                        for speaker in commonSpeakers {
                            if let oldSpeaker = oldMaintaintedGroup.speakers.find({ $0 == speaker}),
                                let newSpeaker = newMaintainedGroup.speakers.find({ $0 == speaker}) {
                                if newSpeaker.friendlyName != oldSpeaker.friendlyName {
                                    self.notifyDelegatesAboutDidRemoveSpeaker(oldSpeaker, fromGroup: newMaintainedGroup, fromMove:  false)
                                    self.notifyDelegatesAboutDidAddSpeaker(newSpeaker, toGroup: newMaintainedGroup, fromMove:  false)
                                }
                            }
                        }
                        
                        if oldMaintaintedGroup.masterSpeaker != newMaintainedGroup.masterSpeaker {
                            self.notifyDelegatesAboutUpdatedMasterTo(newMaster: newMaintainedGroup.masterSpeaker, inGroup: newMaintainedGroup)
                        }
                    }
                }
            }
        }
        
        self.notifyDelegatesAboutDidMakeChanges()
    }
    
    public func refresh(clear: Bool) {
        refreshTrigger.onNext(true)
        discoveryService.refresh(clear: clear)
    }
}

public func alphabeticalIndexForSpeaker(_ speaker: Speaker, toInsertInto speakers: [Speaker]) -> Int {
    var index = 0
    
    for currentSpeaker in speakers {
        if currentSpeaker.sortName.lowercased() < speaker.sortName.lowercased() {
            index = index + 1
        } else {
            break
        }
    }
    return index
}

public extension AudioSystem {
    public func addDelegate(_ delegate: AudioSystemDelegate) {
        delegates.insert(Weak(value:delegate))
    }
    
    public func removeDelegate(_ delegate: AudioSystemDelegate) {
        if let weakDelegate = delegates.filter({ weak in
            if let weakValue = weak.recall() as? AudioSystemDelegate {
                return  weakValue.delegateIdentifier == delegate.delegateIdentifier
            }
            return false
        }).first {
            delegates.remove(weakDelegate)
        }
    }
    
    func notifyDelegatesAboutWillMakeChanges() {
        delegates.forEach{ ($0.recall() as? AudioSystemDelegate)?.audioSystemWillMakeChanges() }
    }
    
    func notifyDelegatesAboutDidMakeChanges() {
        delegates.forEach{ ($0.recall() as? AudioSystemDelegate)?.audioSystemDidMakeChanges() }
    }
    
    func notifyDelegatesAboutDidAddGroup(_ group: SpeakerGroup, fromMove: Bool) {
        delegates.forEach{ ($0.recall() as? AudioSystemDelegate)?.audioSystemDidAddGroup(group, fromMove: fromMove) }
    }
    
    func notifyDelegatesAboutDidRemoveGroup(_ group: SpeakerGroup, fromMove: Bool) {
        delegates.forEach{ ($0.recall() as? AudioSystemDelegate)?.audioSystemDidRemoveGroup(group, fromMove: fromMove) }
    }
    
    func notifyDelegatesAboutUpdatedMasterTo(newMaster: Speaker?, inGroup group: SpeakerGroup) {
        delegates.forEach{ ($0.recall() as? AudioSystemDelegate)?.audioSystemDidUpdateMasterTo(newMaster: newMaster, inGroup: group)}
    }
    
    func notifyDelegatesAboutDidAddSpeaker(_ speaker: Speaker,toGroup group: SpeakerGroup, fromMove: Bool) {
        delegates.forEach{ ($0.recall() as? AudioSystemDelegate)?.audioSystemDidAddSpeaker(speaker, toGroup: group, fromMove: fromMove) }
    }
    
    func notifyDelegatesAboutDidRemoveSpeaker(_ speaker: Speaker,fromGroup group: SpeakerGroup, fromMove: Bool) {
        delegates.forEach{ ($0.recall() as? AudioSystemDelegate)?.audioSystemDidRemoveSpeaker(speaker, fromGroup: group, fromMove: fromMove) }
    }
}


extension AudioSystem: DiscoveryServiceDelegate {
    public func discoveryServiceDidFindSpeaker(_ speaker: Speaker, info: [ScalarNode: String]) {
        let index = alphabeticalIndexForSpeaker(speaker, toInsertInto: self.speakers.value)
        updateMultiroomState(withInfo: info, forSpeaker: speaker)
        updateNowplayingState(withInfo: info, forSpeaker: speaker)
        updateVolume(withInfo: info, forSpeaker: speaker)
        updateUnlocked(forSpeaker: speaker)
        updateSpeakerUpdateStatus(forSpeaker: speaker)
        
        if let _ = speakers.value.index (where: { $0.mac == speaker.mac}) {
            NSLog("duplicate added")
        }
        self.speakers.value.insert(speaker, at: index)
        
        networkOptimization(enabled: UserDefaults.standard.bool(forKey: StreamingQualityConstants.key))
    }
    
    public func discoveryServiceDidUpdateInfoForSpeaker(_ speaker: Speaker, info: [ScalarNode:String]) {
        //NSLog("= did update info for speaker: \(speaker.friendlyName)")
        updateMultiroomState(withInfo: info, forSpeaker: speaker)
        updateNowplayingState(withInfo: info, forSpeaker: speaker)
        updateVolume(withInfo: info, forSpeaker: speaker)
    }
    
    public func discoveryServiceDidLoseSpeaker(_ speaker: Speaker) {
        //NSLog("- did lost speaker: \(speaker.friendlyName)")
        if let index = speakers.value.index (where: { $0.mac == speaker.mac}) {
            speakers.value.remove(at: index)
        }
    }
    
    func updateNowplayingState(withInfo info: [ScalarNode: String], forSpeaker speaker: Speaker) {
        provider.getValidModesForSpeaker(speaker)
            .map { modes -> NowPlayingState in
                return NowPlayingState.from(nodeStringValues: info, modes: modes) }
            .subscribe(onNext: { [weak self] nowPlayingState in
                guard let `self` = self else { return }
                self.nowPlayingInfo.value[speaker] = nowPlayingState })
            .disposed(by: disposeBag)
    }
    
    func updateUnlocked(forSpeaker speaker: Speaker) {
        provider.getFirmwareVersionForSpeaker(speaker)
            .map { version -> Bool in
                return (version != nil) ? version!.contains("unlocked") : false }
            .subscribe(onNext: { [weak self] isUnlocked in
                guard let `self` = self else { return }
                self.unlockedInfo.value[speaker] = isUnlocked
                if isUnlocked == true {
                    NotificationCenter.default.post(name: NSNotification.Name("UnlockedFound"), object: nil, userInfo: ["speakerMac" : speaker.mac])
                } })
            .disposed(by: disposeBag)
    }
    
    public func isSpeakerUnlocked(_ speaker : Speaker) -> Bool {
        if let unlocked = self.unlockedInfo.value[speaker] {
            return unlocked
        }
        return false
    }
    
    //MARK:- Firmware Update
    func updateSpeakerUpdateStatus(forSpeaker speaker: Speaker) {
        self.provider.getUpdateStateForSpeaker(speaker)
            .subscribe(onNext: { [weak self] updateStatus in
                guard let `self` = self else { return }
                
                switch updateStatus {
                case .updateAvailable:
                    NSLog("[UPDATE] speaker %@ has update available !!!", speaker.friendlyName)
                    self.updateAvailableInfo.value[speaker] = true
                default:
                    self.updateAvailableInfo.value[speaker] = false
                } })
            .disposed(by: disposeBag)
    }
    
    public func isUpdateAvailable(for speaker : Speaker) -> Bool {
        guard let updateInfo = self.updateAvailableInfo.value[speaker] else { return false }
        return updateInfo
    }
    
    func updateMultiroomState(withInfo info: [ScalarNode: String], forSpeaker speaker: Speaker) {
        // Keep current optimization info
        let optimizationEnabled = self.multiroomInfo.value[speaker]?.networkOptimization
        self.multiroomInfo.value[speaker] = MultiroomState.from(nodeStringValues: info)
        self.multiroomInfo.value[speaker]?.networkOptimization = optimizationEnabled
    }
    
    func updateVolume(withInfo info: [ScalarNode: String], forSpeaker speaker: Speaker) {
        if let volumeValueString = info[ScalarNode.Volume],
            let muteValueString = info[ScalarNode.Mute] {
            
            let volume = Int(volumeValueString)
            let mute = muteValueString == "1"
            
            self.volumeInfo.value[speaker] = SpeakerVolume(volume: volume,
                                                           mute: mute,
                                                           bass: nil,
                                                           treble: nil)
        }
    }
    
    public var delegateIdentifier: String {
        return "audio_system"
    }
}

func groupsFromSpeakers(_ speakers:[Speaker], multiroomDevices: [MultiroomDevice], multiroomInfo: Variable<[Speaker: MultiroomState]>) -> [SpeakerGroup] {
    let speakersByMac: [String: Speaker] = speakers.reduce([String: Speaker](), { (dict, speaker) in
        var dict = dict
        dict[speaker.mac] = speaker
        return dict
    })
    
    let multiroomDevicesByMac: [String: MultiroomDevice] = multiroomDevices.reduce([String: MultiroomDevice](), { (dict, multiroomDevice) in
        var dict = dict
        dict[multiroomDevice.mac] = multiroomDevice
        return dict
    })
    
    let clientIndexes = clientIndexesForSpeakers(speakersByMac, fromMultiroomDevices: multiroomDevicesByMac)
    
    var groupsByGroupId: [String: SpeakerGroup] = [:]
    
    let speakerMacs = Set(speakers.map{ $0.mac })
    let devicesMacs = Set(multiroomDevices.map{ $0.mac })
    let speakersNotInMultiroomList = speakerMacs.subtracting(devicesMacs)
    
    var enhancedMultiroomDevices = multiroomDevices
    for mac in speakersNotInMultiroomList {
        if let speaker = speakersByMac[mac] {
            if let info = multiroomInfo.value[speaker] {
                //NSLog("adding missing multiroom device to list")
                let multiroomDevice = MultiroomDevice(udn: speaker.UDN,
                                                      friendlyName: speaker.friendlyName,
                                                      ipAddress: speaker.ipAddress,
                                                      audioSyncVersion: "128",
                                                      groupId: info.groupId,
                                                      groupName: info.groupName,
                                                      groupRole: GroupRole.from(groupState: info.groupState),
                                                      clientNumber: info.clientIndex,
                                                      key: enhancedMultiroomDevices.count)
                enhancedMultiroomDevices.append(multiroomDevice)
            }
        }
    }
    
    for multiroomDevice in enhancedMultiroomDevices {
        if let groupId = multiroomDevice.groupId { //the speaker is present in a group
            if let group = groupsByGroupId[groupId] {
                if let speaker = speakersByMac[multiroomDevice.mac] {
                    var appendedGroup = group
                    appendedGroup.speakers.append(speaker)
                    groupsByGroupId[groupId] = appendedGroup
                } else {
                    //the speaker was not found though discovery yet
                }
            } else {
                if let speaker = speakersByMac[multiroomDevice.mac] {
                    if let masterSpeakerMac = enhancedMultiroomDevices.filter({ $0.groupId == groupId && $0.groupRole == GroupRole.server }).first?.mac {
                        let masterSpeaker = speakersByMac[masterSpeakerMac]
                        //if the master has not been discovered it will be nil
                        let newGroup = SpeakerGroup(isMulti: true,
                                                    groupName: multiroomDevice.groupName,
                                                    groupId: groupId,
                                                    speakers: [speaker],
                                                    multiroomDevicesForSpeakers: clientIndexes,
                                                    masterSpeaker:masterSpeaker)
                        groupsByGroupId[groupId] = newGroup
                    } else
                    {
                        //the group has no master - it will show up as faded on the UI
                        let newGroup = SpeakerGroup(isMulti: true,
                                                    groupName: multiroomDevice.groupName,
                                                    groupId: groupId, speakers: [speaker],
                                                    multiroomDevicesForSpeakers: clientIndexes,
                                                    masterSpeaker:nil)
                        groupsByGroupId[groupId] = newGroup
                    }
                } else {
                    //NSLog("the speaker was not found though discovery yet - \(multiroomDevice.friendlyName)")
                    //the speaker was not found though discovery yet
                }
            }
        } else { //the speaker is not in a group
            if let speaker = speakersByMac[multiroomDevice.mac] {
                let groupId = "single_"+speaker.mac
                let newGroup = SpeakerGroup(isMulti: false,
                                            groupName: speaker.friendlyNameWithIndex,
                                            groupId:groupId,
                                            speakers: [speaker],
                                            multiroomDevicesForSpeakers: nil,
                                            masterSpeaker:  speaker)
                groupsByGroupId[groupId] = newGroup
            } else {
                //NSLog("the speaker was not found though discovery yet - \(multiroomDevice.friendlyName)")
                //the speaker was not found though discovery yet
            }
        }
    }
    
    let groupsArray = Array(groupsByGroupId.values)
    
    var sortedGroupsArray:[SpeakerGroup] = []
    for group in groupsArray {
        var groupToSort = group
        groupToSort.speakers.sort(by: { (speaker1, speaker2) -> Bool in
            speaker1.sortName.lowercased() < speaker2.sortName.lowercased()
        })
        sortedGroupsArray.append(groupToSort)
    }
    sortedGroupsArray.sort(by: { (group1, group2) -> Bool in
        return group1.groupName?.lowercased() < group2.groupName?.lowercased()
    })
    
    return sortedGroupsArray
}

func clientIndexesForSpeakers(_ speakersByMac: [String:Speaker], fromMultiroomDevices multiroomDevicesByMac: [String:MultiroomDevice]) -> [Speaker: MultiroomDevice] {
    var indexes = [Speaker: MultiroomDevice]()
    let macs = Array(speakersByMac.keys)
    for mac in macs {
        
        let speaker = speakersByMac[mac]!
        if let mrDevice = multiroomDevicesByMac[mac] {
            indexes[speaker] = mrDevice
        }
    }
    
    return indexes
}
