//
//  MultiroomGroup.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 04/05/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation

public struct SpeakerGroup {
    public let isMulti: Bool
    public let groupName: String?
    public let groupId: String
    public var speakers: [Speaker]
    public var multiroomDevicesForSpeakers: [Speaker: MultiroomDevice]?
    public var masterSpeaker: Speaker?
}

extension SpeakerGroup: Hashable {
    public var hashValue: Int {
        return groupId.hashValue
    }
}

extension SpeakerGroup: Equatable {}

public func ==(lhs: SpeakerGroup, rhs: SpeakerGroup  ) -> Bool {
    return (lhs.groupId == rhs.groupId)
}
