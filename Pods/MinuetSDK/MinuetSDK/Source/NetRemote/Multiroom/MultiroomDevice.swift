//
//  MultiroomDevice.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 03/05/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation

public struct MultiroomDevice {
    public let udn: String
    public let friendlyName: String
    public let ipAddress: String
    public let audioSyncVersion: String
    public let groupId: String?
    public let groupName: String?
    public let groupRole: GroupRole
    public let clientNumber: Int?
    public let key: Int
    public var mac: String {
        guard udn.count >= 12 else { return "" }
        let part = udn.index(udn.endIndex, offsetBy: -12)
        return String(udn[part...])
    }
}
