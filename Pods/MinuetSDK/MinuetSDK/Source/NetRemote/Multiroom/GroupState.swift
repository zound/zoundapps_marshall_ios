//
//  GroupState.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 11/08/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation

public enum GroupState: Int {
    case solo = 0
    case client = 1
    case server = 2
}
