//
//  GroupVolume.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 07/06/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation

public struct GroupVolume {
    public var masterVolume: Int?
    public var clientsVolumes: [Speaker: SpeakerVolume]?
    
    public init(masterVolume: Int?,
                clientsVolumes: [Speaker: SpeakerVolume]?) {
        self.masterVolume = masterVolume
        self.clientsVolumes = clientsVolumes
    }
    
    public var isMute: Bool {
        var mute = false
        if let clients = clientsVolumes {
            for (_, volume) in clients {
                
                if volume.mute == true {
                    mute = true
                    break
                }
            }
        }
        return mute
    }
}
