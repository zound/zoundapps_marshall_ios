//
//  SpeakerNavPage.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 04/05/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation


public struct SpeakerNavPage {
    public var items: [NavListItem]
    public let depth: Int?
    public let numItems: Int
    public var canLoadMore: Bool
}
