//
//  SpeakerNavBrowser.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 04/05/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import RxSwift

public enum NavError: Error {
    case fail
    case fatalError
}

public enum NavStatus: Int {
    case waiting = 0
    case ready = 1
    case fail = 2
    case fatalError = 3
    case readyRoot = 4
}

public protocol SpeakerNavBrowserDelegate: class {
    func didLoadMoreForPage(_ page: SpeakerNavPage, navBrowser: SpeakerNavBrowser)
    func didNavigateToPage(_ page: SpeakerNavPage, navBrowser: SpeakerNavBrowser)
    func didNavigateToParentPage(_ page: SpeakerNavPage, navBrowser: SpeakerNavBrowser)
    func didResetNavigationInNavBrowser(_ navBrowser: SpeakerNavBrowser)
    func navBrowser(_ navBrowser: SpeakerNavBrowser, didReceiveFatalError error: Error)
}

public class SpeakerNavBrowser {
    let speaker: Speaker
    let speakerNotifier: SpeakerNotifier
    let provider: SpeakerProvider
    static let geoIPProvider = GeoIPProvider()
    public let loadingPage: Variable<Bool> = Variable(false)
    public let canNavigateToParent: Variable<Bool> = Variable(false)
    var disposeBag = DisposeBag()
    public weak var delegate: SpeakerNavBrowserDelegate?
    
    public init(speaker: Speaker, speakerNotifier: SpeakerNotifier, provider: SpeakerProvider) {
        self.speaker = speaker
        self.speakerNotifier = speakerNotifier
        self.provider = provider
    }
    
    public func initNavigationIfNeeded(_ ifNeeded: Bool, steps:[UInt32]?) {
        let resetIfNeeded = SpeakerNavBrowser.resetNavigationIfNeeded(ifNeeded, onSpeaker: speaker, speakerNotifier: speakerNotifier, provider: provider).flatMapLatest{[weak self] _ -> Observable<SpeakerNavPage> in
            
            guard let `self` = self else { return Observable.empty() }
            if let `steps` = steps {
                return SpeakerNavBrowser.navigateWithSteps(steps, reset: true, speaker: self.speaker, speakerNotifier: self.speakerNotifier, provider: self.provider)
                
            } else {
                return SpeakerNavBrowser.currentPageForSpeaker(self.speaker, speakerNotifier: self.speakerNotifier, provider: self.provider)
            }
        }
        
        resetIfNeeded
            .subscribe(onNext: {[weak self] speakerNavPage in
                self?.didResetNavigation()
                self?.didNavigateToPage(speakerNavPage)
                }, onError: { [weak self] error in
                    if let err = error as? NavError {
                        self?.didReceiveError(err)
                    } else {
                        self?.didReceiveError(NavError.fatalError)
                    }
            }).disposed(by: disposeBag)
    }
    
    public func cancelOperations() {
        self.disposeBag = DisposeBag()
    }
    
    public func navigateToParent() {
        SpeakerNavBrowser.navigateToParentOnSpeaker(speaker, speakerNotifier: speakerNotifier, provider: provider)
            .subscribe(onNext: {[weak self] speakerNavPage in
                self?.didNavigateToParentPage(speakerNavPage)
                }, onError: { [weak self] error in
                    self?.didReceiveError(error as! NavError)
            }).disposed(by: disposeBag)
    }
    
    public func navigateToPageAtIndex(_ index: UInt32) {
        SpeakerNavBrowser.navigateToPageAtIndex(index, speaker: speaker, speakerNotifier: speakerNotifier, provider: provider)
            .subscribe(onNext: {[weak self] speakerNavPage in
                self?.didNavigateToPage(speakerNavPage)
                }, onError: { [weak self] error in
                    self?.didReceiveError(error as! NavError)
            }).disposed(by: disposeBag)
    }
    
    public func navigateToNavigationItem(_ navItem: NavListItem) {
        switch navItem.type {
        case .directory:    navigateToPageAtIndex(navItem.key)
        case .playableItem: selectItemAtIndex(navItem.key)
        default: break;
        }
    }
    
    public func navigateWithSteps(_ steps: [UInt32]) {
        SpeakerNavBrowser.navigateWithSteps(steps, reset: true, speaker: speaker, speakerNotifier: speakerNotifier, provider: provider)
            .subscribe(onNext: {[weak self] speakerNavPage in
                self?.didResetNavigation()
                self?.didNavigateToPage(speakerNavPage)
                }, onError: { [weak self] error in
                    self?.didReceiveError(error as! NavError)
            }).disposed(by: disposeBag)
    }
    
    public func search(_ query: String) {
        SpeakerNavBrowser.searchWithTerm(query, speaker: speaker, speakerNotifier: speakerNotifier, provider: provider)
            .subscribe(onNext: {[weak self] speakerNavPage in
                self?.didResetNavigation()
                self?.didNavigateToPage(speakerNavPage)
                }, onError: { [weak self] error in
                    if let error = error as? NavError {
                        self?.didReceiveError(error)
                    } else {
                        self?.didReceiveError(NavError.fatalError)
                    }
            }).disposed(by: disposeBag)
    }
    
    public func selectItemAtIndex(_ index: UInt32) {
        SpeakerNavBrowser.selectPlayableItemAtIndex(index, speaker: speaker, speakerNotifier: speakerNotifier, provider: provider)
            .subscribe()
            .disposed(by: disposeBag)
    }
    
    public func loadMoreForPage(_ page: SpeakerNavPage) {
        SpeakerNavBrowser.loadMoreForPage(page, speaker: speaker, speakerNotifier: speakerNotifier, provider: provider)
            .subscribe(onNext: {[weak self] speakerNavPage in
                self?.didLoadMoreForPage(speakerNavPage)
                }, onError: { [weak self] error in
                    self?.didReceiveError(error)
            }).disposed(by: disposeBag)
    }
    
    fileprivate func didResetNavigation() {
        self.delegate?.didResetNavigationInNavBrowser(self)
    }
    
    fileprivate func didNavigateToParentPage(_ page: SpeakerNavPage) {
        self.delegate?.didNavigateToParentPage(page, navBrowser: self)
    }
    
    fileprivate func didNavigateToPage(_ page: SpeakerNavPage) {
        self.delegate?.didNavigateToPage(page, navBrowser: self)
    }
    
    fileprivate func didLoadMoreForPage(_ page: SpeakerNavPage) {
        self.delegate?.didLoadMoreForPage(page, navBrowser: self)
    }
    
    fileprivate func didReceiveError(_ error: Error) {
        self.delegate?.navBrowser(self, didReceiveFatalError: error)
    }
    
    fileprivate static func currentPageForSpeaker(_ speaker:Speaker, speakerNotifier: SpeakerNotifier, provider: SpeakerProvider) -> Observable<SpeakerNavPage> {
        let initialPage = SpeakerNavPage(items: [], depth: nil, numItems: 0, canLoadMore: true)
        return loadMoreForPage(initialPage, speaker: speaker, speakerNotifier: speakerNotifier, provider: provider)
    }
    
    fileprivate static func loadMoreForPage(_ page: SpeakerNavPage, speaker:Speaker, speakerNotifier: SpeakerNotifier, provider: SpeakerProvider) -> Observable<SpeakerNavPage> {
        let defaultLoadLength = 20
        let maxCurrentKey = page.items.map{ Int($0.key) }.max() ?? -1
        return itemsFromKey(Int32(maxCurrentKey+1), length: defaultLoadLength, onSpeaker: speaker, speakerNotifier: speakerNotifier, provider: provider).map{ loadResult in
            let maxKey = Int(loadResult.navItems.map{ $0.key }.max() ?? 0)
            let canLoadMore = (loadResult.numItems > (maxKey + 1)) && (defaultLoadLength >= loadResult.navItems.count)
            return SpeakerNavPage(items: (page.items + loadResult.navItems), depth: loadResult.navDepth, numItems: loadResult.numItems, canLoadMore: canLoadMore)
        }
    }
    
    fileprivate static func selectPlayableItemAtIndex(_ index: UInt32, speaker:Speaker, speakerNotifier: SpeakerNotifier, provider: SpeakerProvider) -> Observable<Bool> {
        return provider.setNode(ScalarNode.NavActionSelectItem, value: String(index), forSpeaker: speaker)
    }
    
    fileprivate static func navigateToParentOnSpeaker(_ speaker:Speaker, speakerNotifier: SpeakerNotifier, provider: SpeakerProvider) -> Observable<SpeakerNavPage> {
        return navigateToPageAtIndex(UInt32.max, speaker: speaker, speakerNotifier: speakerNotifier, provider: provider)
    }
    
    fileprivate static func searchWithTerm(_ term: String, speaker: Speaker, speakerNotifier: SpeakerNotifier, provider: SpeakerProvider) -> Observable<SpeakerNavPage> {
        let defaultSearchSteps: Observable<[UInt32]> = Observable.just([2,2])
        
        return SpeakerNavBrowser.setSearchTerm(term, onSpeaker: speaker, provider: provider)
            .flatMapLatest { _ in defaultSearchSteps }
            .flatMapLatest { searchSteps in
                SpeakerNavBrowser.navigateWithSteps(searchSteps, reset: true, speaker: speaker, speakerNotifier: speakerNotifier, provider: provider)
        }
    }
    
    fileprivate static func navigateWithSteps(_ indexes: [UInt32], reset: Bool,  speaker:Speaker, speakerNotifier: SpeakerNotifier, provider: SpeakerProvider) -> Observable<SpeakerNavPage> {
        var navigationSteps = [Observable.just(true)]
        for index in indexes {
            let navigationStep = provider.setNode(ScalarNode.NavActionNavigate, value: String(index), forSpeaker: speaker)
                .flatMapLatest{_ in waitForReadyOnSpeaker(onSpeaker: speaker, speakerNotifier: speakerNotifier, provider: provider) }
            navigationSteps.append(navigationStep)
        }
        
        let resetStep = reset ? resetNavigationIfNeeded(false, onSpeaker: speaker, speakerNotifier: speakerNotifier, provider: provider) : Observable.just(true)
        
        return resetStep
            .flatMapLatest{_ in Observable.concat(navigationSteps).skip(max(0,indexes.count)) }
            .flatMapLatest{_ in currentPageForSpeaker(speaker, speakerNotifier: speakerNotifier, provider: provider) }
            .catchError{ error in
                return Observable.empty()
        }
    }
    
    fileprivate static func navigateToPageAtIndex(_ index: UInt32, speaker:Speaker, speakerNotifier: SpeakerNotifier, provider: SpeakerProvider) -> Observable<SpeakerNavPage> {
        return provider.setNode(ScalarNode.NavActionNavigate, value: String(index), forSpeaker: speaker)
            .flatMapLatest{_ in waitForReadyOnSpeaker(onSpeaker: speaker, speakerNotifier: speakerNotifier, provider: provider) }
            .flatMapLatest{_ in currentPageForSpeaker(speaker, speakerNotifier: speakerNotifier, provider: provider) }
            .catchError{ error in
                return Observable.empty()
        }
    }
    
    fileprivate typealias NavigationLoadResult = (navItems:[NavListItem], navDepth: Int?, numItems: Int)
    
    fileprivate static func itemsFromKey(_ key: Int32, length: Int, onSpeaker speaker:Speaker, speakerNotifier: SpeakerNotifier, provider: SpeakerProvider) -> Observable<NavigationLoadResult> {
        typealias NavigationParams = (navDepth: Int?, numItems: Int)
        
        let navParams = provider.getNodes([ScalarNode.NavDepth, ScalarNode.NavNumItems], forSpeaker: speaker).map{ nodes -> NavigationParams in
            var navDepth: Int = 0
            var numItems: Int = 0
            
            if let navDepthString = nodes[ScalarNode.NavDepth],
                let navDepthInt = Int(navDepthString) {
                navDepth = navDepthInt
            }
            
            if let navItemsString = nodes[ScalarNode.NavNumItems],
                let navItemsInt = Int(navItemsString) {
                numItems = navItemsInt
            }
            
            return NavigationParams(navDepth: navDepth, numItems: numItems)
        }
        
        let navResult = navParams.flatMapLatest{ (navParams: NavigationParams) -> Observable<NavigationLoadResult> in
            if navParams.numItems > 0 {
                return provider.getListNode(ListNode.NavList, startIndex: key, maxCount: Int32(length), forSpeaker: speaker).map{
                    NavigationLoadResult(navItems: $0, navDepth: navParams.navDepth, numItems: navParams.numItems)
                }
            } else {
                let emptyNavListItem = NavListItem(name: "Empty", type: .fetchErrorItem, subtype: .none, key: 0)
                return Observable.just(NavigationLoadResult(navItems: [emptyNavListItem], navDepth: navParams.navDepth, numItems: 1))
            }
        }
        
        return navResult
    }
    
    fileprivate static func getState(onSpeaker speaker:Speaker, provider: SpeakerProvider) -> Observable<Bool> {
        return provider.getNode(ScalarNode.NavState, forSpeaker: speaker).map{ $0 == "1" }
    }
    
    public static func setStateOn(_ on: Bool, onSpeaker speaker:Speaker, provider: SpeakerProvider) -> Observable<Bool> {
        return provider.setNode(ScalarNode.NavState, value: (on ? "1" : "0"), forSpeaker: speaker)
    }
    
    fileprivate static func setSearchTerm(_ term: String, onSpeaker speaker:Speaker, provider: SpeakerProvider) -> Observable<Bool> {
        return provider.setNode(ScalarNode.NavSearchTerm, value: term, forSpeaker: speaker)
    }
    
    fileprivate static func waitForReadyOnSpeaker(onSpeaker speaker:Speaker, speakerNotifier: SpeakerNotifier, provider: SpeakerProvider) -> Observable<Bool> {
        let navStatusNotifications = speakerNotifier.notificationsForNode(ScalarNode.NavStatus).map { nodeValue -> NavStatus in
            if let intValue = Int(nodeValue) {
                if let navStatus = NavStatus(rawValue: intValue) {
                    return navStatus
                } else {
                    return NavStatus.fail
                }
            } else {
                return NavStatus.fail
            }
        }
        let navStatus = provider.getNode(ScalarNode.NavStatus, forSpeaker: speaker).map { navStatusValue -> NavStatus in
            if let `navStatusValue` = navStatusValue {
                if let navStatusIntValue = Int(navStatusValue) {
                    if let status = NavStatus(rawValue: navStatusIntValue) {
                        return status
                    }
                }
            }
            return NavStatus.fatalError
        }
        
        let wait = Observable.from([navStatus, navStatusNotifications]).merge().filter({ $0 != .waiting }).take(1).flatMapLatest{ (navStatus: NavStatus) -> Observable<Bool> in
            if navStatus == .fail {
                return Observable.error(NavError.fail)
            }
            if navStatus == .fatalError {
                return Observable.error(NavError.fatalError)
            }
            return Observable.just(true)
        }
        
        return wait.do(onNext: {
            NSLog("nav status: \($0)")
        }).do(onError:{
            NSLog("nav error \($0)")
        })
    }
    
    fileprivate static func resetNeededOnSpeaker(onSpeaker speaker:Speaker,speakerNotifier: SpeakerNotifier, provider: SpeakerProvider) -> Observable<Bool> {
        return getState(onSpeaker: speaker, provider: provider).map { navigationOn -> Bool in
            if navigationOn {
                return false
            } else {
                return true
            }
        }
    }
    
    static func resetNavigationIfNeeded(_ checkResetNeeded:Bool, onSpeaker speaker:Speaker, speakerNotifier: SpeakerNotifier, provider: SpeakerProvider) -> Observable<Bool> {
        let resetNavigation = setStateOn(false, onSpeaker: speaker, provider: provider).flatMapLatest{ _ in
            setStateOn(true, onSpeaker: speaker, provider: provider)
            }.flatMapLatest{ done in
                return waitForReadyOnSpeaker(onSpeaker: speaker, speakerNotifier: speakerNotifier, provider: provider)
        }
        
        if checkResetNeeded {
            return resetNeededOnSpeaker(onSpeaker:speaker, speakerNotifier: speakerNotifier, provider: provider).flatMapLatest{ needed -> Observable<Bool> in
                if needed {
                    return resetNavigation
                } else {
                    return Observable.just(true)
                }
            }
        } else {
            return resetNavigation
        }
    }
    
}
