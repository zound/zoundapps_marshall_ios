//
//  NavListItem.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 04/05/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation

public enum NavListType: Int {
    case directory = 0
    case playableItem = 1
    case searchDirectory = 2
    case unknown = 3
    case fetchErrorItem = 4
}

public enum NavListSubType: Int {
    case none = 0
    case station = 1
    case podcast = 2
    case track = 3
}

public struct NavListItem {
    public let name: String
    public let type: NavListType
    public let subtype: NavListSubType
    public let key: UInt32
    
    public init(name: String, type: NavListType, subtype: NavListSubType, key: UInt32) {
        self.name = name
        self.type = type
        self.subtype = subtype
        self.key = key
    }
}
