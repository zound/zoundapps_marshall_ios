//
//  MinuetSDK.h
//  MinuetSDK
//
//  Created by Raul Andrisan on 25/04/2017.
//  Copyright © 2017 Zound Industries. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for MinuetSDK.
FOUNDATION_EXPORT double MinuetSDKVersionNumber;

//! Project version string for MinuetSDK.
FOUNDATION_EXPORT const unsigned char MinuetSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <MinuetSDK/PublicHeader.h>

#import <MinuetSDK/MinuetSDK-Bridging-Header.h>
