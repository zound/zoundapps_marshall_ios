//
//  Platform.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 23/09/2016.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation

struct Platform {
    static var isSimulator: Bool {
        return TARGET_OS_SIMULATOR != 0 // Use this line in Xcode 7 or newer
    }
}
