//
//  RxSwift+Extensions.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 23/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import RxSwift
import Moya
import SWXMLHash

extension ObservableType {
    func replaceWith<R>(_ value: R) -> Observable<R> {
        return map { _ in value }
    }
}

extension Observable {
    func repeatForever() -> Observable<Element> {
        return Observable.create({ observer -> Disposable in
            return self.subscribeForever(self,
                                         nextClosure: { object in
                                            observer.onNext(object)
                                            return
            },
                                         errorClosure: { (error, disposable) in
                                            disposable.dispose()
                                            observer.onError(error)
            },
                                         completedClosure: { disposable in
                                            //resubscribe
                                            return })
        })
    }
    
    func subscribeForever(_ observableAny: AnyObject, nextClosure: @escaping (Element)->(), errorClosure: @escaping (Swift.Error, Disposable)->(), completedClosure: @escaping (Disposable)->()) -> Disposable {
        
        let compositeDisposable = CompositeDisposable()
        if let observable = observableAny as? Observable<Element> {
            let recursiveScheduler = CurrentThreadScheduler.instance
            let schedulingDisposable = recursiveScheduler.scheduleRecursive((), action: { state, recurse in
                let selfDisposable = CompositeDisposable()
                if let  selfDisposableKey = compositeDisposable.insert(selfDisposable) {
                    let subscriptionDisposable = observable.subscribe(
                        onNext: { element in
                            nextClosure(element)
                            return
                    },
                        onError: { error in
                            errorClosure(error, compositeDisposable)
                            compositeDisposable.remove(for:selfDisposableKey)
                            //recurse()
                            return
                    },
                        onCompleted: {
                            completedClosure(compositeDisposable)
                            compositeDisposable.remove(for:selfDisposableKey)
                            //recurse()
                            return
                    },
                        onDisposed: nil)
                    recurse(())
                    
                    _ = selfDisposable.insert(subscriptionDisposable)
                }
            })
            _ = compositeDisposable.insert(schedulingDisposable)
            
        }
        return compositeDisposable
    }
}

extension ObservableType {
    
    func repeatWhen<O: ObservableConvertibleType>(notificationHandler: (() -> O)) -> Observable<E> {
        return notificationHandler()
            .asObservable()
            .flatMapLatest { _ in
                return self
        }
    }
}

protocol Optionable {
    associatedtype WrappedType
    func unwrapOptional() -> WrappedType
    func isEmpty() -> Bool
}

extension Optional : Optionable {
    typealias WrappedType = Wrapped
    func unwrapOptional() -> WrappedType {
        return self!
    }
    
    func isEmpty() -> Bool {
        return !(flatMap({_ in true}) == true)
    }
}

extension Observable where Element : Optionable {
    func unwrapOptional() -> Observable<Element.WrappedType> {
        return self
            .filter {value in
                return !value.isEmpty()
            }
            .map {value -> Element.WrappedType in
                value.unwrapOptional()
        }
    }
}

extension ObservableType where E == Bool {
    /// Boolean not operator
    func not() -> Observable<Bool> {
        return self.map(!)
    }
}
