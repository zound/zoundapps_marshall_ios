//
//  Dictionary+Operations.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 30/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation

func += <KeyType, ValueType> (left: inout Dictionary<KeyType, ValueType>, right: Dictionary<KeyType, ValueType>) {
    for (k, v) in right {
        left.updateValue(v, forKey: k)
    }
}
