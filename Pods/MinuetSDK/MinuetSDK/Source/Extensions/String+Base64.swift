//
//  String+Base64.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 20/05/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation

extension String {
    func base64EncodedString() -> String {
        guard let plainData = (self as NSString).data(using: String.Encoding.utf8.rawValue) else {
            fatalError()
        }
        
        let base64String = plainData.base64EncodedString(options: Data.Base64EncodingOptions(rawValue: 0))
        return base64String as String
    }
    
    func base64DecodedString() -> String {
        if let decodedData = Data(base64Encoded: self, options:Data.Base64DecodingOptions(rawValue: 0)),
            let decodedString = NSString(data: decodedData, encoding: String.Encoding.utf8.rawValue) {
            return decodedString as String
        } else {
            return self
        }
    }
    
    /// Create NSData from hexadecimal string representation
    ///
    /// This takes a hexadecimal representation and creates a NSData object. Note, if the string has any spaces, those are removed. Also if the string started with a '<' or ended with a '>', those are removed, too. This does no validation of the string to ensure it's a valid hexadecimal string
    ///
    /// The use of `strtoul` inspired by Martin R at http://stackoverflow.com/a/26284562/1271826
    ///
    /// - returns: NSData represented by this hexadecimal string. Returns nil if string contains characters outside the 0-9 and a-f range.
    
    func base64DecodedData() -> Data? {
        return Data(base64Encoded: self, options: Data.Base64DecodingOptions.ignoreUnknownCharacters)
    }
}
