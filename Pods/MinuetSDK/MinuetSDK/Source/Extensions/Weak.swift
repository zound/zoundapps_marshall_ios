//
//  Weak.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 27/04/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation

public class Weak : Hashable {
    var valueAny : Any?
    public weak var value : AnyObject?
    
    public init(value: Any) {
        
        self.value = value as AnyObject?
    }
    
    public func recall() -> Any? {
        if let value = value {
            return value
        } else if let value = valueAny {
            return value
        }
        return nil
    }
    
    public var hashValue: Int {
        return ObjectIdentifier(self).hashValue
    }
}


public func ==(lhs: Weak, rhs: Weak) -> Bool {
    return ObjectIdentifier(lhs) == ObjectIdentifier(rhs)
}
