//
//  FSWACService.m
//  UNDOK
//
//  Created by Raul Andrisan on 01/02/16.
//  Copyright © 2016 Frontier Silicon. All rights reserved.
//

#import "FSWACService.h"

@interface FSWACService()<EAWiFiUnconfiguredAccessoryBrowserDelegate> {
    BOOL _active;
    
}
@property (nonatomic, strong) EAWiFiUnconfiguredAccessoryBrowser* browser;
@property NSMutableArray* mutableAccessories;
@property (nonatomic, strong) NSArray* ssidWhitelist;

@end


@implementation FSWACService

+(EAWiFiUnconfiguredAccessoryBrowser *)sharedBrowser {
    
    static dispatch_once_t pred;
    static EAWiFiUnconfiguredAccessoryBrowser *shared = nil;
    dispatch_once(&pred, ^{
        shared = [[EAWiFiUnconfiguredAccessoryBrowser alloc] initWithDelegate:nil queue:nil];
    });
    return shared;
}


- (instancetype)initWithSSIDWhitelist:(NSArray*)ssidWhitelist
{
    self = [super init];
    if (self) {
        
        _ssidWhitelist = ssidWhitelist;
        _mutableAccessories = [[NSMutableArray alloc] init];
        self.browser = [FSWACService sharedBrowser];
        self.browser.delegate = self;
        if(self.browser.unconfiguredAccessories.count>0) {
            NSInteger index = 0;
            for(EAWiFiUnconfiguredAccessory* acc in self.browser.unconfiguredAccessories) {
                
                if([_delegate respondsToSelector:@selector(didFinishConfigurationForAccessory:)]) {
                    [self.delegate didFindAccessory:acc];
                }
                [self insertObject:acc inAccessoriesAtIndex:index];
                index++;
            }

        }

    }
    return self;
}

-(void)dealloc {
    
    [self.browser stopSearchingForUnconfiguredAccessories];
}

-(NSPredicate*)predicateFromSSIDWhitelist:(NSArray*)whitelist {
    
    if (whitelist!=nil && whitelist.count > 0) {
        NSMutableString* predicateText = [[NSMutableString alloc] init];
        
        NSUInteger i = 0;
        NSUInteger count = whitelist.count;
        for (NSString* ssid in whitelist) {
            NSString* condition = [NSString stringWithFormat:@"ssid BEGINSWITH '%@'",ssid];
            [predicateText appendString:condition];
            if (i < count-1) {
                [predicateText appendString:@" OR "];
            }
            i = i + 1;
        }
        return [NSPredicate predicateWithFormat:predicateText];
    }
    return nil;
}
-(void)setActive:(BOOL)active {
    
    _active = active;
    if(active) {
        NSPredicate* predicate = [self predicateFromSSIDWhitelist:self.ssidWhitelist];
        [self.browser startSearchingForUnconfiguredAccessoriesMatchingPredicate:predicate];
        
    } else {

        [self.browser stopSearchingForUnconfiguredAccessories];
    }
}

-(BOOL)active {
    
    return _active;
}


- (void)accessoryBrowser:(EAWiFiUnconfiguredAccessoryBrowser *)browser didUpdateState:(EAWiFiUnconfiguredAccessoryBrowserState)state {
    
    self.serviceState = (NSInteger)state;
    if([_delegate respondsToSelector:@selector(didUpdateState:)]) {
        [self.delegate didUpdateState:(FSWACServiceState)state];
    }
    NSLog(@"STATE CHANGED TO %@", [self descriptionForUnconfiguredAccessoryBrowserState:state]);
}

- (void)accessoryBrowser:(EAWiFiUnconfiguredAccessoryBrowser *)browser didFindUnconfiguredAccessories:(NSSet *)accessories {
    
    [accessories.allObjects enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        if([self->_delegate respondsToSelector:@selector(didFinishConfigurationForAccessory:)]) {
            [self.delegate didFindAccessory:obj];
        }
        NSLog(@"WAC found accessory named:%@ with ssid:%@", ((EAWiFiUnconfiguredAccessory*)obj).name, ((EAWiFiUnconfiguredAccessory*)obj).ssid );
        [self insertObject:obj inAccessoriesAtIndex:idx];
    }];

}

- (void)accessoryBrowser:(EAWiFiUnconfiguredAccessoryBrowser *)browser didRemoveUnconfiguredAccessories:(NSSet *)accessories {

    [accessories.allObjects enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        NSInteger index = [self.accessories indexOfObject:obj];
        if(index!=NSNotFound) {
            if([self->_delegate respondsToSelector:@selector(didFinishConfigurationForAccessory:)]) {
                [self.delegate didLoseAccessory:obj];
            }
            NSLog(@"WAC lost accessory %@", ((EAWiFiUnconfiguredAccessory*)obj).name);
            [self removeObjectFromAccessoriesAtIndex:index];
        }
    }];
}

- (void)accessoryBrowser:(EAWiFiUnconfiguredAccessoryBrowser *)browser didFinishConfiguringAccessory:(EAWiFiUnconfiguredAccessory *)accessory withStatus:(EAWiFiUnconfiguredAccessoryConfigurationStatus)status {
    
    if(self.delegate) {
        
        if(status == EAWiFiUnconfiguredAccessoryConfigurationStatusSuccess) {
            
            if([_delegate respondsToSelector:@selector(didFinishConfigurationForAccessory:)]) {
                [self.delegate didFinishConfigurationForAccessory:accessory];
            }
        } else if(status == EAWiFiUnconfiguredAccessoryConfigurationStatusFailed) {
            
            if([_delegate respondsToSelector:@selector(didFailConfigurationForAccessory:)]) {
                [self.delegate didFailConfigurationForAccessory:accessory];
            }
        } else if(status == EAWiFiUnconfiguredAccessoryConfigurationStatusUserCancelledConfiguration) {
            
            if([_delegate respondsToSelector:@selector(didCancelConfigurationForAccessory:)]) {
                [self.delegate didCancelConfigurationForAccessory:accessory];
            }
        }
    }
    NSLog(@"STATUS FOR %@ CHANGED TO %@",accessory.name, [self descriptionForUnconfiguredAccessoryConfigurationStatus:status]);
}

-(void)configureAccessory:(id)accessory withConfigurationUIOnViewController:(UIViewController*)viewContorller {
    
    [self.browser configureAccessory:accessory withConfigurationUIOnViewController:viewContorller];
}


-(NSArray *)accessories {
    
    return [[self mutableAccessories] copy];
}

-(NSUInteger)countOfAccessories {
    
    return _mutableAccessories.count;
}

-(id)objectInAccessoriesAtIndex:(NSUInteger)index {
    
    return [_mutableAccessories objectAtIndex:index];
}

-(void)insertObject:(EAWiFiUnconfiguredAccessory *)accessory inAccessoriesAtIndex:(NSUInteger)index {
    
    [_mutableAccessories insertObject:accessory atIndex:index];
}

-(void)removeObjectFromAccessoriesAtIndex:(NSUInteger)index {
    
    [_mutableAccessories removeObjectAtIndex:index];
    
}
-(void)replaceObjectInAccessoriesAtIndex:(NSUInteger)index withObject:(id)object {
    
    [_mutableAccessories replaceObjectAtIndex:index withObject:object];
}


-(NSString*)descriptionForUnconfiguredAccessoryBrowserState:(EAWiFiUnconfiguredAccessoryBrowserState)state {
    
    NSString* stateString = nil;
    
    switch (state) {
        case EAWiFiUnconfiguredAccessoryBrowserStateConfiguring: stateString =  @"BrowserStateConfiguring"; break;
        case EAWiFiUnconfiguredAccessoryBrowserStateSearching:  stateString =  @"BrowserStateSearching"; break;
        case EAWiFiUnconfiguredAccessoryBrowserStateStopped:  stateString =  @"BrowserStateSearchStopped"; break;
        case EAWiFiUnconfiguredAccessoryBrowserStateWiFiUnavailable:  stateString =  @"BrowserStateWiFiUnavailable"; break;
    }
    
    return stateString;
}

-(NSString*)descriptionForUnconfiguredAccessoryConfigurationStatus:(EAWiFiUnconfiguredAccessoryConfigurationStatus)status {
    
    NSString* statusString = nil;
    
    switch (status) {
        case EAWiFiUnconfiguredAccessoryConfigurationStatusFailed: statusString =  @"ConfigurationStatusFailed"; break;
        case EAWiFiUnconfiguredAccessoryConfigurationStatusSuccess:  statusString =  @"ConfigurationStatusSuccess"; break;
        case EAWiFiUnconfiguredAccessoryConfigurationStatusUserCancelledConfiguration:  statusString =  @"ConfigurationStatusUserCancelledConfiguration"; break;
    }
    
    return statusString;
}


@end
