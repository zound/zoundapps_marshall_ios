//
//  WACService.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 18/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import ExternalAccessory
import Foundation
import UIKit
import RxSwift

@objc public enum WACServiceState: Int {
    case wiFiUnavailable
    case stopped
    case searching
    case configuring
    
    public var displayValue: String {
        switch self {
        case .configuring: return "Configuring"
        case .stopped: return "Stopped"
        case .searching: return "Searching"
        case .wiFiUnavailable: return "WiFiUnavailable"
        }
    }
}

public protocol WACServiceType: class {
    var unconfiguredSpeakers: [UnconfiguredSpeaker] { get }
    var serviceState: WACServiceState { get }
    var delegate: WACServiceDelegate? { get set }
    func configureSpeaker(_ speaker: UnconfiguredSpeaker, onViewController:UIViewController)
    var active: Bool { get set }
}


public protocol WACServiceDelegate: class {
    func didFailConfigurationForAccessory(_ speaker:UnconfiguredSpeaker)
    func didCancelConfigurationForAccessory(_ speaker:UnconfiguredSpeaker)
    func didFinishConfigurationForAccessory(_ speaker:UnconfiguredSpeaker)
    func didAdd(unconfiguredSpeaker: UnconfiguredSpeaker)
    func didRemove(unconfiguredSpeaker: UnconfiguredSpeaker)
}

public class WACService: NSObject, WACServiceType, FSWACServiceDelegate {
    public var unconfiguredSpeakers: [UnconfiguredSpeaker] = []
    public var serviceState: WACServiceState
    public weak var delegate: WACServiceDelegate?
    
    var isScanning = false
    var isConfiguring = false
    let wacService: FSWACService
    
    public override init() {
        serviceState = .stopped
        active = false
        
        var ssidWhitelist: [String]? = nil
        if let infoPlist = Bundle.main.infoDictionary {
            if let appSettings = infoPlist["AppSettings"] as? Dictionary<String, AnyObject> {
                if let suportedSSIDs = appSettings["SupportedSSIDs"] as? [String] {
                    ssidWhitelist = suportedSSIDs
                }
            }
        }
        
        if let availableSSIDs = ssidWhitelist, availableSSIDs.count>0 {
            wacService = FSWACService(ssidWhitelist: availableSSIDs)
        } else {
            wacService = FSWACService(ssidWhitelist: nil)
        }
        
        super.init()
        
        wacService.delegate = self
        if wacService.accessories.count > 0 {
            var index: Int = 0
            for acc in wacService.accessories {
                if let castAccessory = acc as? NSObject {
                    let unconfiguredSpeaker = unconfiguredSpeakerFromAccessory(castAccessory)
                    unconfiguredSpeakers.insert(unconfiguredSpeaker, at: index)
                    self.delegate?.didAdd(unconfiguredSpeaker: unconfiguredSpeaker)
                    index = index + 1
                }
            }
        }
    }
    
    public var active: Bool {
        didSet {
            wacService.active = active
        }
    }
    
    deinit {
        NSLog("updated service state Stopped deinit")
        wacService.active = false
    }
    
    public func didFailConfiguration(for accessory: EAWiFiUnconfiguredAccessory!) {
        let unconfiguredSpeaker = unconfiguredSpeakerFromAccessory(accessory)
        self.delegate?.didFailConfigurationForAccessory(unconfiguredSpeaker)
        
    }
    
    public func didCancelConfiguration(for accessory: EAWiFiUnconfiguredAccessory!) {
        let unconfiguredSpeaker = unconfiguredSpeakerFromAccessory(accessory)
        self.delegate?.didCancelConfigurationForAccessory(unconfiguredSpeaker)
    }
    
    public func didFinishConfiguration(for accessory: EAWiFiUnconfiguredAccessory!) {
        let unconfiguredSpeaker = unconfiguredSpeakerFromAccessory(accessory)
        self.delegate?.didFinishConfigurationForAccessory(unconfiguredSpeaker)
    }
    
    public func didUpdate(_ state: FSWACServiceState) {
        serviceState = WACServiceState(rawValue:Int(state.rawValue))!
        isScanning = (serviceState == .searching)
        isConfiguring = (serviceState == .configuring)
        print ("updated service state \(serviceState.displayValue)")
        
        runAfterDelay(0.7, block: { [weak self] in
            if (self?.serviceState == .stopped) && self?.active == true {
                self?.wacService.active = true
            }
        })
    }
    
    public func didFind(_ accessory: EAWiFiUnconfiguredAccessory!) {
        let unconfiguredSpeaker = unconfiguredSpeakerFromAccessory(accessory)
        if unconfiguredSpeakers.find({$0.mac == unconfiguredSpeaker.mac}) == nil {
            unconfiguredSpeakers.append(unconfiguredSpeaker)
            delegate?.didAdd(unconfiguredSpeaker: unconfiguredSpeaker)
        }
    }
    
    public func didLose(_ accessory: EAWiFiUnconfiguredAccessory!) {
        if let speakerToRemove = self.unconfiguredSpeakers.filter({ $0.wacMac == accessory.macAddress }).first {
            if let indexToRemove = self.unconfiguredSpeakers.index(of: speakerToRemove) {
                if(indexToRemove != NSNotFound) {
                    unconfiguredSpeakers.remove(at: indexToRemove)
                    self.delegate?.didRemove(unconfiguredSpeaker: speakerToRemove)
                }
            }
        }
    }
    
    func unconfiguredSpeakerFromAccessory(_ accessory: AnyObject) -> UnconfiguredSpeaker {
        let unconfiguredSpeaker = UnconfiguredSpeaker(model: accessory.name, ssid: accessory.ssid, wacMac: accessory.macAddress)
        return unconfiguredSpeaker
    }
    
    public func configureSpeaker(_ speaker: UnconfiguredSpeaker, onViewController:UIViewController) {
        var unconfigAccessory: AnyObject?
        for acc in wacService.accessories {
            if (acc as AnyObject).macAddress == speaker.wacMac {
                unconfigAccessory = acc as AnyObject?
                break;
            }
        }
        
        if let acc = unconfigAccessory {
            wacService.configureAccessory(acc, withConfigurationUIOn: onViewController)
        }
    }
}
