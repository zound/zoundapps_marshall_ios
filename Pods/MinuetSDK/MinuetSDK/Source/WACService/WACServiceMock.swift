//
//  WACServiceMock.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 29/03/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import UIKit

public class WACServiceMock: NSObject, WACServiceType {

    public var unconfiguredSpeakers: [UnconfiguredSpeaker]
    public var serviceState: WACServiceState
    public weak var delegate: WACServiceDelegate?
    
    fileprivate var _sampleUnconfiguredSpeakers:[UnconfiguredSpeaker]
    
    public override init() {
        
        unconfiguredSpeakers = [UnconfiguredSpeaker]()
        serviceState = .stopped
        active = false
        _sampleUnconfiguredSpeakers = [
            UnconfiguredSpeaker(model:"Klippan", ssid: "Stammen Indigo [69:08]", wacMac: "0:33:61:C8:69:80"),
            UnconfiguredSpeaker(model:"Klippan", ssid: "Stammen Indigo [69:08]", wacMac: "0:33:61:C8:69:80"),
            UnconfiguredSpeaker(model:"Klippan", ssid: "Stammen Indigo [69:08]", wacMac: "0:33:61:C8:69:80"),
            UnconfiguredSpeaker(model:"Klippan", ssid: "Stammen Indigo [69:08]", wacMac: "0:33:61:C8:69:80"),
            UnconfiguredSpeaker(model:"Klippan", ssid: "Stammen Indigo [69:08]", wacMac: "0:33:61:C8:69:80"),
        ]
        
        super.init()
    }
    
    public var active: Bool {
        
        didSet {
            if active {
                
            }
        }
    }
    
    public func configureSpeaker(_ speaker: UnconfiguredSpeaker, onViewController:UIViewController) {
        
        self.delegate?.didFinishConfigurationForAccessory(speaker)
    }
}
