//
//  SingleSpeakerStore.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 13/06/2017.
//  Copyright © 2017 Zound Industries. All rights reserved.
//

import Foundation
import ObjectMapper

public class SingleSpeakerStore {
    public let key: String
    public var speaker: Speaker? {
        didSet {
            save()
        }
    }
    
    public init(key: String) {
        self.key = key
        
        if let jsonString = minuetSDKUserDefaults?.object(forKey: key) as? String {
            if let storedSpeaker = Mapper<Speaker>().map(JSONString:jsonString) {
                speaker = storedSpeaker
            } else {
                speaker = nil
            }
        } else {
            speaker = nil
        }
    }
    
    fileprivate func save() {
        if let speaker = speaker {
            let jsonString = speaker.toJSONString()
            minuetSDKUserDefaults?.set(jsonString, forKey: key)
        } else {
            minuetSDKUserDefaults?.removeObject(forKey: key)
        }
    }
}
