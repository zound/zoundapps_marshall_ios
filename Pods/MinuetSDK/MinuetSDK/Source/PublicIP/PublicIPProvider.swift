//
//  PublicIPProvider.swift
//  MinuetSDK
//
//  Created by Dolewski Bartosz A (Ext) on 19/12/2018.
//

import Foundation
import Moya
import RxSwift
import Moya_ObjectMapper

public class PublicIPProvider {
    var provider: MoyaProvider<PublicIPService>
    
    public init() {
        let publicIPProvider = MoyaProvider<PublicIPService>()
        provider = publicIPProvider
    }
    
    public func currentPublicIP() -> Observable<PublicIP> {
        return provider.rx.request(PublicIPService.getIP)
            .mapObject(PublicIP.self)
            .asObservable()
    }
}
