//
//  PublicIPService.swift
//  MinuetSDK
//
//  Created by Dolewski Bartosz A (Ext) on 19/12/2018.
//

import Foundation
import Moya

enum PublicIPService {
    case getIP
}

extension PublicIPService: TargetType {
    var baseURL: URL {
        switch self {
            case .getIP: return URL(string: "https://api.ipify.org?format=json")!
        }
    }
    
    var path: String {
        return ""
    }
    
    var method: Moya.Method {
        return .get
    }
    
    var sampleData: Data {
        return Data(base64Encoded: "127.127.127.127")!
    }
    
    public var task: Task {
        return Task.requestPlain
    }
    
    var headers: [String : String]? {
        return nil
    }
}
