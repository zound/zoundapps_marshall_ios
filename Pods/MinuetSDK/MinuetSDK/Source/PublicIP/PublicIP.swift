
//
//  PublicIP
//  MinuetSDK
//
//  Created by Dolewski Bartosz A (Ext) on 19/12/2018.
//

import Foundation
import ObjectMapper

public struct PublicIP {
    public var ip: String?
}

extension PublicIP: Mappable {
    public init?(map: Map) {}
    
    public mutating func mapping(map: Map) {
        ip <- map["ip"]
    }
}
