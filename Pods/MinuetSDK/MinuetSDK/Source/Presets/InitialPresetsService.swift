//
//  InitialPresetsService.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 17/10/2016.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import Moya

enum InitialPresetsService {
    case getPresets(baseURL: URL, path: String)
}

extension InitialPresetsService:TargetType {
    var headers: [String : String]? {
        return nil
    }
    
    var baseURL: URL {
        switch self {
        case .getPresets(let baseURL, _): return baseURL
        }
        
    }
    
    var path: String {
        switch self {
        case .getPresets(_, let path): return path
        }
    }
    
    var method: Moya.Method {
        return .get
    }
    
    var parameters: [String: Any]? {
        switch self {
        case .getPresets: return nil
        }
    }
    
    public var task: Task {
        if let parameters = self.parameters {
            return Task.requestParameters(parameters: parameters, encoding: URLEncoding.default)
        } else {
            return Task.requestPlain
        }
    }
    
    var sampleData: Data {
        return Data()
    }
}
