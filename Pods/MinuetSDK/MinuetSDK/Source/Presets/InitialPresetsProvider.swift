//
//  InitialPresetsProvider.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 17/10/2016.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import Moya
import RxSwift

public class InitialPresetsProvider {
    var provider: MoyaProvider<InitialPresetsService>
    let baseURL: URL
    let path: String
    
    public init(baseURL: URL, path: String) {
        let initialPresetsProvider = MoyaProvider<InitialPresetsService>()
        provider = initialPresetsProvider
        self.baseURL = baseURL
        self.path = path
    }
    
    public func initialPresets() -> Observable<SpeakerInitialPresets> {
        return provider.rx.request(InitialPresetsService.getPresets(baseURL: self.baseURL, path: self.path))
            .mapObject(SpeakerInitialPresets.self).asObservable()
    }
    
    public static func presetsForCountry(_ countryCode: String?, inInitialPresets initialPresets: SpeakerInitialPresets) ->  [SpeakerInitialPreset] {
        if let countryPresets = initialPresets.countryPresets {
            if let countryCode = countryCode {
                if let countryPresets = countryPresets[countryCode] {
                    return countryPresets
                }
            }
        }
        return initialPresets.defaultPresets ?? []
    }
    
    public static func fallbackPresetsForCountry(_ countryCode: String?, inInitialPresets initialPresets: SpeakerInitialPresets) -> [SpeakerInitialPreset] {
        if let countryPresets = initialPresets.countryPresets {
            if let countryCode = countryCode {
                if let countryPresets = countryPresets[countryCode] {
                    return countryPresets
                }
            }
        }
        return initialPresets.defaultPresets ?? []
    }
}
