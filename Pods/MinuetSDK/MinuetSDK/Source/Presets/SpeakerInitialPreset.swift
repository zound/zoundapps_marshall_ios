//
//  SpeakerInitialPreset.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 02/08/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import ObjectMapper

public struct SpeakerInitialPreset: Mappable {
    public var stationId: String?
    public var stationName: String?
    public var artworkUrl: String?
    
    public init?(map: Map) {}
    public mutating func mapping(map: Map) {
        stationId   <- map["stationId"]
        stationName <- map["stationName"]
        artworkUrl  <- map["artworkUrl"]
    }
}
