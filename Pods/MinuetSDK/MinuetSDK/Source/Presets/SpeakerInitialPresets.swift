//
//  SpeakerInitila.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 02/08/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import ObjectMapper

public struct SpeakerInitialPresets: Mappable {
    public var defaultPresets: [SpeakerInitialPreset]?
    public var countryPresets: [String:[SpeakerInitialPreset]]?
    
    public init?(map: Map) {}
    
    public mutating func mapping(map: Map) {
        defaultPresets <- map["default"]
        countryPresets <- map["countries"]
    }
}
