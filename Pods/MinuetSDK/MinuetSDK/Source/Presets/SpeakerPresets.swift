//
//  SpeakerPresets.swift
//  MinuetSDK
//
//  Created by Raul Andrisan on 24/05/16.
//  Copyright © 2016 Zound Industries. All rights reserved.
//

import Foundation
import RxSwift

public enum SpeakerPresetsError: Error {
    case spotifyNotAuthenticated
    case presetHasNoType
}

public class SpeakerPresets {
    public static func getPresetsForSpeaker(_ speaker: Speaker, provider: SpeakerProvider) -> Observable<[Preset]> {
        let fetchedPresets: Observable<[Preset]> = provider
            .setNode(ScalarNode.NavState, value: "1", forSpeaker: speaker)
            .flatMap { didSetNode -> Observable<[Preset]> in
                return provider.getListNode(ListNode.Presets, maxCount: 7, forSpeaker: speaker)
        }
        return fetchedPresets
    }
    
    public static func uploadPresets(_ presets: [Preset], toSpeaker speaker: Speaker, provider: SpeakerProvider) -> Observable<Bool> {
        var uploadPresets = provider
            .setNode(ScalarNode.NavState, value: "0", forSpeaker: speaker)
            .flatMapLatest { done in
                provider.setNode(ScalarNode.NavState, value: "1", forSpeaker: speaker)
        }
        
        for preset in presets {
            uploadPresets = uploadPresets
                .flatMapLatest { done -> Observable<Bool> in
                    return SpeakerPresets.uploadPreset(preset, toSpeaker: speaker, provider: provider).flatMapLatest { done in return Observable.just(done) }
                        .catchErrorJustReturn(false)
            }
        }
        return uploadPresets
    }
    
    public static func uploadPreset(_ preset: Preset, toSpeaker speaker: Speaker, provider: SpeakerProvider) -> Observable<Bool> {
        guard let blob = preset.blob else { return Observable.just(false) }
        let presetUploadNodes: [ScalarNode: String]  =
            [ScalarNode.NavPresetDelete: String(preset.number - 1),
             ScalarNode.NavPresetUploadType: preset.type!.modeIdentifier(),
             ScalarNode.NavPresetUploadName: preset.name,
             ScalarNode.NavPresetUploadArtworkURL: preset.imageURL?.absoluteString ?? "",
             ScalarNode.NavPresetUploadBlob: blob]
        
        NSLog("ZOUND_1259 Uploading preset \(presetUploadNodes) with blob field: \(blob)")
        
        return provider.setNodes(presetUploadNodes, forSpeaker: speaker)
            .flatMapLatest { nodeSuccesses -> Observable<[ScalarNode: Bool]>  in
                return provider.setNode(ScalarNode.NavPresetUploadUpload, value: String(preset.number - 1), forSpeaker: speaker)
                    .map { success in
                        var nodeSuccessesCopy = nodeSuccesses
                        nodeSuccessesCopy[ScalarNode.NavPresetUploadUpload] = success
                        return nodeSuccessesCopy }
            }.map { nodesSuccesses in
                var allSuccessful = true
                for (_, value) in nodesSuccesses {
                    if value == false {
                        allSuccessful = false
                        break;
                    }
                }
                return allSuccessful
        }
    }
}
