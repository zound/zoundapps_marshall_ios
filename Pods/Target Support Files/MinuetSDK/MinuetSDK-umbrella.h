#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "DarwinNotificationsManager.h"
#import "WifiInfo.h"
#import "MinuetSDK-Bridging-Header.h"
#import "MinuetSDK.h"
#import "FSWACService.h"

FOUNDATION_EXPORT double MinuetSDKVersionNumber;
FOUNDATION_EXPORT const unsigned char MinuetSDKVersionString[];

