#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "UIImage+Trim.h"

FOUNDATION_EXPORT double ZoundCommonVersionNumber;
FOUNDATION_EXPORT const unsigned char ZoundCommonVersionString[];

