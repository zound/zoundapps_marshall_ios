//: A UIKit based Playground for presenting user interface
import MinuetSDK
import PlaygroundSupport

let provider = SpeakerProvider()
let discoveryService = DiscoveryService()

let audioSystem = AudioSystem(discoveryService: discoveryService, provider: provider)

audioSystem.groupUpdatesActive = true

PlaygroundPage.current.needsIndefiniteExecution = true

let speakerCount = audioSystem.groups
    .asObservable()
    .map { $0.count }
    .subscribe(onNext: { count in
        print(count)
    })

let speakerName = audioSystem.speakers
    .asObservable()
    .subscribe(onNext: { speakers in
        speakers.forEach {
            print("Name: " + $0.friendlyName)
        }
    })

discoveryService.search()

